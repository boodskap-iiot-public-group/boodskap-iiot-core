/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class ThreadContext {

	public static final ObjectMapper mapper = new ObjectMapper();
	public static final ObjectMapper ciMapper = new ObjectMapper();
	
	static {
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		//ciMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		ciMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		ciMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"));
		//ciMapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
		//ciMapper.configure(Feature.ALLOW_MISSING_VALUES, true);
		
	}
	
	public static final TypeReference<Map<String, Object>> TR = new TypeReference<Map<String, Object>>() {
	};
	
	public static final ThreadLocal<Calendar> CALENDAR = new ThreadLocal<Calendar>() {
		@Override
		protected Calendar initialValue() {
			return Calendar.getInstance();
		}
	};
	
	public static final Gson GSON = new GsonBuilder().enableComplexMapKeySerialization().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	public static final Gson GSONP = new GsonBuilder().enableComplexMapKeySerialization().setDateFormat("yyyy-MM-dd HH:mm:ss").setPrettyPrinting().create();
	
	public static String toJSON(Object object){
		try {
			return mapper.writeValueAsString(object);
		} catch (Exception e) {
			throw new StorageException(e);
		}
		//return new JSONObject(object).toString();
	}
	
	public static String toJSONPretty(Object object){
		return new JSONObject(toJSON(object)).toString(4);
		//return new JSONObject(object).toString(4);
	}
	
	public static <T> T jsonToObject(Object ciObject, Class<T> c){
		return ThreadContext.jsonToObject(new JSONObject(ciObject).toString(), c);
	}

	public static <T> T jsonToObject(JSONObject json, Class<T> c){
		return ThreadContext.jsonToObject(json.toString(), c);
	}

	public static <T> T jsonToObject(String dataJson, Class<T> c){

		try {
			return mapper.readValue(dataJson, c);
		} catch (Exception e) {
			throw new StorageException(e);
		}
		//return GSON.fromJson(dataJson, c);
	}

	public static String mapToJson( Map<String, Object> props){
		return new JSONObject(props).toString();
	}

	public static Map<String, Object> jsonToMap(String dataJson){
		return GSON.fromJson(dataJson, new TypeToken<HashMap<String, Object>>() {}.getType());
	}
	
	 
	@SuppressWarnings("unchecked")
	public static Map<String, Object> toMap(JsonNode node){
		return mapper.convertValue(node, Map.class);
	}
	 
	public static String toGSONPretty(Object object){
		return GSONP.toJson(object);
	}
	
	public static String toGSON(Object object){
		return GSON.toJson(object);
	}
	
	public static <T> T gsonToObject(Object ciObject, Class<T> c){
		String json = toGSON(ciObject);
		return GSON.fromJson(json, c);
	}

	public static <T> T gsonToObject(JSONObject json, Class<T> c){
		return GSON.fromJson(json.toString(), c);
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> gsonToMap(String dataJson){
		return GSON.fromJson(dataJson, Map.class);
	}
	
	public static Map<String, Object> gsonToMap(JSONObject object){
		return gsonToMap(object.toString());
	}

}
