package io.boodskap.iot;

public class RuntimeContext {
	
	private static final RuntimeContext instance = new RuntimeContext();
	
	private boolean masterNode;

	private RuntimeContext() {
	}

	public static final RuntimeContext get() {
		return instance;
	}

	public boolean isMasterNode() {
		return masterNode;
	}

	public void setMasterNode(boolean masterNode) {
		this.masterNode = masterNode;
	}

}
