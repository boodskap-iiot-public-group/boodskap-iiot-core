/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.AssetDAO;
import io.boodskap.iot.dao.EntityIterator;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
@JsonSerialize(as=IAsset.class)
public interface IAsset extends IDomainObject {
	
	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public static AssetDAO<IAsset> dao(){
		return AssetDAO.get();
	}

	static public void createOrUpdate(IAsset e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IAsset> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IAsset> load() throws StorageException{
		return dao().load();

	}
	
	static public long count() throws StorageException{
		return dao().count();

	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IAsset> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);

	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IAsset> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IAsset> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey,query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey,what,how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey,query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IAsset create(String domainKey, String assetId) {
		return dao().create(domainKey,assetId);
	}
	
	static public IAsset get(String domainKey, String id) throws StorageException{
		return dao().get(domainKey,id);
	}
	
	static public void delete(String domainKey, String assetId) throws StorageException{
		dao().delete(domainKey,assetId);
	}
	
	static public Collection<IAsset> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey,page,pageSize);
	}
	
	static public Collection<IAsset> listNext(String domainKey, String assetId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey,assetId,page,pageSize);

	}

	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void save() {
		IAsset.dao().createOrUpdate(this);
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void copy(Object other) {
		
		IAsset o = (IAsset) other;
		
		setAssetId(o.getAssetId());
		
		IDomainObject.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getAssetId();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setAssetId(String assetId);

}
