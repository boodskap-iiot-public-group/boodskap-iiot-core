/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.TemplateDAO;

@JsonSerialize(as=ITemplate.class)
public interface ITemplate extends ISystemTemplate, IDomainObject {

	//======================================
	// DAO Methods
	//======================================
	
	public static TemplateDAO<ITemplate> dao() {
		return TemplateDAO.get();
	}
	
	public static Class<? extends ITemplate> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<ITemplate> load() throws StorageException{
		return dao().load();
	}
	
	public static EntityIterator<ITemplate> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}

	public static ITemplate create(String domainKey, String name) throws StorageException{
		return dao().create(domainKey, name);
	}

	public static ITemplate get(String domainKey, String name) throws StorageException{
		return dao().get(domainKey, name);
	}

	public static void delete(String domainKey, String name) throws StorageException{
		dao().delete(domainKey, name);
	}

	public static Collection<ITemplate> list(boolean load, String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, page, pageSize);
	}

	public static Collection<ITemplate> listNext(boolean load, String domainKey, String name, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, name, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ITemplate.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
}
