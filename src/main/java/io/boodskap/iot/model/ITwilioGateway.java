/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.TwilioGatewayDAO;

@JsonSerialize(as=ITwilioGateway.class)
public interface ITwilioGateway extends IDomainObject {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static TwilioGatewayDAO<ITwilioGateway> dao() {
		return TwilioGatewayDAO.get();
	}
	
	public static Class<? extends ITwilioGateway> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<ITwilioGateway> load() throws StorageException{
		return dao().load();
	}
	
	public static EntityIterator<ITwilioGateway> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}

	public static ITwilioGateway create(String domainKey) throws StorageException{
		return dao().create(domainKey);
	}

	public static ITwilioGateway get(String domainKey) throws StorageException{
		return dao().get(domainKey);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ITwilioGateway.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getSid();

	public void setSid(String sid);

	public String getToken();

	public void setToken(String token);

	public String getAuthtoken();

	public void setAuthtoken(String token);

	public String getPrimaryPhone();

	public void setPrimaryPhone(String primaryPhone);

	public boolean isDebug();

	public void setDebug(boolean debug);

}
