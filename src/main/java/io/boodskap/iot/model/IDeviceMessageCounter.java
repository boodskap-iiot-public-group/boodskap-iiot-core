package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Date;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DeviceMessageCounterDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDeviceMessageCounter.class)
public interface IDeviceMessageCounter extends IDomainObject {
	
	//======================================
	// DAO Methods	
	//======================================
	
	public static DeviceMessageCounterDAO<IDeviceMessageCounter> dao(){
		return DeviceMessageCounterDAO.get();
	}
	
	static public void createOrUpdate(IDeviceMessageCounter e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDeviceMessageCounter> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDeviceMessageCounter> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDeviceMessageCounter> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IDeviceMessageCounter create(String domainKey, String deviceId, String messageIDeviceMessageCounterype, Date day) throws StorageException{
		return dao().create(domainKey, deviceId, messageIDeviceMessageCounterype, day);
	}
	
	static public IDeviceMessageCounter get(String domainKey, String deviceId, String messageIDeviceMessageCounterype, Date day) throws StorageException{
		return dao().get(domainKey, deviceId, messageIDeviceMessageCounterype, day);
	}
	
	static public long count(String domainKey) {
		return dao().count(domainKey);
	}

	static public long count(String domainKey, String deviceId) {
		return dao().count(domainKey, deviceId);
	}
	static public long count(String domainKey, String deviceId, String messageIDeviceMessageCounterype) {
		return dao().count(domainKey, deviceId, messageIDeviceMessageCounterype);
	}

	static public long count(Date from, Date to) {
		return dao().count(from, to);
	}

	static public long count(String domainKey, Date from, Date to) {
		return dao().count(domainKey, from, to);
	}

	static public long count(String domainKey, String deviceId, Date from, Date to) {
		return dao().count(domainKey, deviceId, from, to);
	}

	static public long count(String domainKey, String deviceId, String messageIDeviceMessageCounterype, Date from, Date to) {
		return dao().count(domainKey, deviceId, messageIDeviceMessageCounterype, from, to);
	}

	static public long countByQuery(String query) throws StorageException{
		return dao().countByQuery(query);
	}

	static public long countByQuery(String domainKey, String query) throws StorageException{
		return dao().countByQuery(domainKey, query);
	}

	static public Collection<IDeviceMessageCounter> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}

	static public Collection<IDeviceMessageCounter> listNext(String domainKey, String deviceId, String messageType, Date day, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, deviceId, messageType, day, page, pageSize);
	}

	
	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IDeviceMessageCounter o = (IDeviceMessageCounter) other;
		
		setDeviceId(o.getDeviceId());
		setMessageType(o.getMessageType());
		setDay(o.getDay());
		setCount(o.getCount());
		
		IDomainObject.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getDeviceId();
	
	public void setDeviceId(String deviceId);

	public String getMessageType();
	
	public void setMessageType(String messageType);
	
	public Date getDay();
	
	public void setDay(Date day);
	
	public long getCount();
	
	public void setCount(long count);
}
