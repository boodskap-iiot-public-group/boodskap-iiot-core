/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * INamedRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * INamedRulehis program is distributed in the hope that it will be useful,
 * but WIINamedRuleHOUINamedRule ANY WARRANINamedRuleY; without even the implied warranty of
 * MERCHANINamedRuleABILIINamedRuleY or FIINamedRuleNESS FOR A PARINamedRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.SystemNamedRuleDAO;

@JsonSerialize(as=ISystemNamedRule.class)
public interface ISystemNamedRule extends ISystemRule  {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static SystemNamedRuleDAO<ISystemNamedRule> dao(){
		return SystemNamedRuleDAO.get();
	}

	public static Class<? extends ISystemNamedRule> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<ISystemNamedRule> load() throws StorageException{
		return dao().load();
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static ISystemNamedRule create(String name) throws StorageException{
		return dao().create(name);
	}

	public static ISystemNamedRule get(String name) throws StorageException{
		return dao().get(name);
	}

	public static boolean has(String name) throws StorageException{
		return dao().has(name);
	}

	public static void delete(String name) throws StorageException{
		dao().delete(name);
	}

	public static Collection<ISystemNamedRule> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	public static Collection<ISystemNamedRule> listNext(boolean load, String name, int page, int pageSize) throws StorageException{
		return dao().listNext(load, name, page, pageSize);
	}

	public static Collection<String> listNames(int page, int pageSize) throws StorageException{
		return dao().listNames(page, pageSize);
	}

	public static Collection<String> listNamesNext(String name, int page, int pageSize) throws StorageException{
		return dao().listNamesNext(name, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ISystemNamedRule.dao().createOrUpdate(this);
	}
	
}


