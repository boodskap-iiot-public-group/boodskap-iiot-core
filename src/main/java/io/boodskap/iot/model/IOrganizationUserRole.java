/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OrganizationUserRoleDAO;

@JsonSerialize(as=IOrganizationUserRole.class)
public interface IOrganizationUserRole extends IOrganizationRole{

	//======================================
	// DAO Methods
	//======================================
	
	public static OrganizationUserRoleDAO<IOrganizationUserRole> dao(){
		return OrganizationUserRoleDAO.get();
	}
	
	static public void createOrUpdate(IOrganizationUserRole e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IOrganizationUserRole> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IOrganizationUserRole> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IOrganizationUserRole> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IOrganizationUserRole> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IOrganizationUserRole> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public EntityIterator<IOrganizationUserRole> load(String domainKey, String orgId) throws StorageException{
		return dao().load(domainKey, orgId);
	}
	
	static public SearchResult<IOrganizationUserRole> searchByQuery(String domainKey, String orgId, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, orgId, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String orgId, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, orgId, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String orgId, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, orgId, query);
	}

	static public JSONObject updateByQuery(String domainKey, String orgId, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, orgId, what, how);
	}

	static public IOrganizationUserRole create(String domainKey, String orgId, String userId, String name) throws StorageException{
		return dao().create(domainKey, orgId, userId, name);
	}

	static public IOrganizationUserRole get(String domainKey, String orgId, String userId, String name) throws StorageException{
		return dao().get(domainKey, orgId, userId, name);
	}

	static public long count(String domainKey, String orgId, String userId) throws StorageException{
		return dao().count(domainKey, orgId, userId);
	}
	
	static public void delete(String domainKey, String orgId, String userId) throws StorageException{
		dao().delete(domainKey, orgId, userId);
	}

	static public void delete(String domainKey, String orgId, String userId, String name) throws StorageException{
		dao().delete(domainKey, orgId, userId, name);
	}

	static public Collection<IOrganizationUserRole> list(String domainKey, String orgId, String userId) throws StorageException{
		return dao().list(domainKey, orgId, userId);
	}


	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		IOrganizationUserRole.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IOrganizationUserRole o = (IOrganizationUserRole) other;
		
		setUserId(o.getUserId());
		
		IOrganizationRole.super.copy(other);
	}
		
	//======================================
	// 
	//======================================
	
	public String getUserId();
	
	public void setUserId(String userId);
}
