/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IMessageRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IMessageRulehis program is distributed in the hope that it will be useful,
 * but WIIMessageRuleHOUIMessageRule ANY WARRANIMessageRuleY; without even the implied warranty of
 * MERCHANIMessageRuleABILIIMessageRuleY or FIIMessageRuleNESS FOR A PARIMessageRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.SystemMessageRuleDAO;

@JsonSerialize(as=ISystemMessageRule.class)
public interface ISystemMessageRule extends IRule {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static SystemMessageRuleDAO<ISystemMessageRule> dao() {
		return SystemMessageRuleDAO.get();
	}
	
	public static Class<? extends ISystemMessageRule> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<ISystemMessageRule> load() throws StorageException{
		return dao().load();
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static ISystemMessageRule create(String specId) throws StorageException{
		return dao().create(specId);
	}

	public static ISystemMessageRule get(String specId) throws StorageException{
		return dao().get(specId);
	}

	public static boolean has(String specId) throws StorageException{
		return dao().has(specId);
	}

	public static void delete(String specId) throws StorageException{
		dao().delete(specId);
	}

	public static Collection<String> listSpecs(int page, int pageSize) throws StorageException{
		return dao().listSpecs(page, pageSize);
	}

	public static Collection<String> listSpecsNext(String specId, int page, int pageSize) throws StorageException{
		return dao().listSpecsNext(specId, page, pageSize);
	}

	public static Collection<ISystemMessageRule> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	public static Collection<ISystemMessageRule> listNext(boolean load, String specId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, specId, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ISystemMessageRule.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		ISystemMessageRule o = (ISystemMessageRule) other;
		
		setSpecId(o.getSpecId());
		
		IRule.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getSpecId();

	public void setSpecId(String specId);

}
