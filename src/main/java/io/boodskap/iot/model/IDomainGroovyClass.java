package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainGroovyClassDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainGroovyClass.class)
public interface IDomainGroovyClass extends IGroovyClass, IDomainContent {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static DomainGroovyClassDAO<IDomainGroovyClass> dao(){
		return DomainGroovyClassDAO.get();
	}
	
	static public void createOrUpdate(IDomainGroovyClass e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDomainGroovyClass> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainGroovyClass> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
		
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainGroovyClass> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IDomainGroovyClass> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainGroovyClass> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}
	
	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IDomainGroovyClass create(String domainKey, String loader, String pkg, String name) throws StorageException{
		return dao().create(domainKey, loader, pkg, name);
	}
	
	static public IDomainGroovyClass get(String domainKey, String loader, String pkg, String name) throws StorageException{
		return dao().get(domainKey, loader, pkg, name);
	}
	
	static public long count(String domainKey, String loader) throws StorageException{
		return dao().count(domainKey, loader);
	}
	
	static public long count(String domainKey, String loader, String pkg) throws StorageException{
		return dao().count(domainKey, loader, pkg);
	}
	
	static public long countArchive(String domainKey, String loader, String fileName) throws StorageException{
		return dao().countArchive(domainKey, loader, fileName);
	}
	
	static public long countArchive(String domainKey, String loader, String fileName, String pkg) throws StorageException{
		return dao().countArchive(domainKey, loader, fileName, pkg);
	}
	
	static public void delete(String domainKey, String loader) throws StorageException{
		dao().delete(domainKey, loader);
	}
	
	static public void delete(String domainKey, String loader, String pkg) throws StorageException{
		dao().delete(domainKey, loader, pkg);
	}
	
	static public void delete(String domainKey, String loader, String pkg, String name) throws StorageException{
		dao().delete(domainKey, loader, pkg, name);
	}
	
	static public void deleteArchive(String domainKey, String loader, String fileName) throws StorageException{
		dao().deleteArchive(domainKey, loader, fileName);
	}
	
	static public Collection<IDomainGroovyClass> listClasses(String domainKey, String loader, int page, int pageSize) throws StorageException{
		return dao().listClasses(domainKey, loader, page, pageSize);
	}
	
	static public Collection<IDomainGroovyClass> listNextClasses(String domainKey, String loader, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextClasses(domainKey, loader, pkg, name, page, pageSize);
	};
	
	static public Collection<IDomainGroovyClass> listPackageClasses(String domainKey, String loader, String pkg, int page, int pageSize) throws StorageException{
		return dao().listPackageClasses(domainKey, loader, pkg, page, pageSize);
	}
	
	static public Collection<IDomainGroovyClass> listNextPackageClasses(String domainKey, String loader, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextPackageClasses(domainKey, loader, pkg, name, page, pageSize);
	}
	
	static public Collection<IDomainGroovyClass> listArchiveClasses(String domainKey, String loader, String fileName, int page, int pageSize) throws StorageException{
		return dao().listArchiveClasses(domainKey, loader, fileName, page, pageSize);
	}
	
	static public Collection<IDomainGroovyClass> listNextArchiveClasses(String domainKey, String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextArchiveClasses(domainKey, loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<IDomainGroovyClass> listArchivePackageClasses(String domainKey, String loader, String fileName, String pkg, int page, int pageSize) throws StorageException{
		return dao().listArchivePackageClasses(domainKey, loader, fileName, pkg, page, pageSize);
	}
	
	static public Collection<IDomainGroovyClass> listNextArchivePackageClasses(String domainKey, String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextArchivePackageClasses(domainKey, loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<String> listPackages(String domainKey, String loader, int page, int pageSize) throws StorageException{
		return dao().listPackages(domainKey, loader, page, pageSize);
	}
	
	static public Collection<String> listNextPackages(String domainKey, String loader, String pkg, int page, int pageSize) throws StorageException{
		return dao().listNextPackages(domainKey, loader, pkg, page, pageSize);
	}
	
	static public Collection<String> listArchivePackages(String domainKey, String loader, String fileName, int page, int pageSize) throws StorageException{
		return dao().listArchivePackages(domainKey, loader, fileName, page, pageSize);
	}
	

	

	
	
	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		IDomainGroovyClass.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IDomainGroovyClass o = (IDomainGroovyClass) other;
		
		setDomainKey(o.getDomainKey());
		
		IGroovyClass.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getDomainKey();
	
	public void setDomainKey(String domainKey);
}
