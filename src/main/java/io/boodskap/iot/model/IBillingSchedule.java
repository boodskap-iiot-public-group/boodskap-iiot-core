package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Date;

import org.json.JSONObject;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.BillingScheduleDAO;
import io.boodskap.iot.dao.EntityIterator;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
public interface IBillingSchedule extends IDomainObject {

	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public static BillingScheduleDAO<IBillingSchedule> dao(){
		return BillingScheduleDAO.get();
	}
	
	static public void createOrUpdate(IBillingSchedule e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IBillingSchedule> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IBillingSchedule> load() throws StorageException{
		return dao().load(); 
	}	
	
	static public long count() throws StorageException{
		return dao().count(); 
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IBillingSchedule> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize); 
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how); 
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query); 
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how); 
	}
	
	static public EntityIterator<IBillingSchedule> load(String domainKey) throws StorageException{
		return dao().load(domainKey); 
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey); 
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey); 
	}
	
	static public SearchResult<IBillingSchedule> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey,query,page,pageSize); 
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey,what,how); 
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey,query); 
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey,what,how); 
	}

	static public IBillingSchedule create(String domainKey, String targetDomain, String scheduleId) {
		return dao().create(domainKey,targetDomain,scheduleId); 
	}

	static public IBillingSchedule get(String domainKey, String targetDomain, String scheduleId) throws StorageException{
		return dao().get(domainKey,targetDomain,scheduleId); 
	}
	
	static public void delete(String domainKey, String targetDomain) throws StorageException{
		dao().delete(domainKey,targetDomain); 
	}
	
	static public void delete(String domainKey, String targetDomain, String scheduleId) throws StorageException{
		dao().delete(domainKey,targetDomain,scheduleId); 
	}
	
	static public Collection<IBillingSchedule> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey,page,pageSize); 
	}
	
	static public Collection<IBillingSchedule> listNext(String domainKey, String targetDomain, String scheduleId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey,targetDomain,scheduleId,page,pageSize); 
	}


	
	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void save() {
		IBillingSchedule.dao().createOrUpdate(this);
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void copy(Object other) {
		
		IBillingSchedule o = (IBillingSchedule) other;
		
		setTargetDomain(o.getTargetDomain());
		setInvoicename(o.getInvoicename());
		setInvoicecode(o.getInvoicecode());
		setFrequency(o.getFrequency());
		setCurrency(o.getCurrency());
		setPayername(o.getPayername());
		setDiscounteditems(o.isDiscounteditems());
		setEnabled(o.isEnabled());
		setObj(o.getObj());
		setStartdate(o.getStartdate());
		setStartevery(o.getStartevery());
		setWeekday(o.getWeekday());
		setBillingtype(o.getBillingtype());
		setExecuted(o.isExecuted());
		setScheduleId(o.getScheduleId());
		
		IDomainObject.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getTargetDomain();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setTargetDomain(String targetDomain);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getScheduleId();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setScheduleId(String scheduleId);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getInvoicename();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setInvoicename(String invoicename);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getInvoicecode();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setInvoicecode(String invoicecode);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getFrequency();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setFrequency(String frequency);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getCurrency();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setCurrency(String currency);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getCompanyname();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setCompanyname(String companyname);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getPayername();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setPayername(String payername);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public boolean isDiscounteditems();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setDiscounteditems(boolean discounteditems);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public boolean isEnabled();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setEnabled(boolean enabled);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getObj();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setObj(String obj);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public Date getStartdate();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setStartdate(Date startdate);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public Date getEnddate();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setEnddate(Date enddate);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getStartevery();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setStartevery(String startevery);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public int getWeekday();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setWeekday(int weekday);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getBillingtype();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setBillingtype(String billingtype);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public boolean isExecuted();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setExecuted(boolean executed);

}
