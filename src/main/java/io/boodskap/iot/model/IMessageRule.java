/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IMessageRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IMessageRulehis program is distributed in the hope that it will be useful,
 * but WIIMessageRuleHOUIMessageRule ANY WARRANIMessageRuleY; without even the implied warranty of
 * MERCHANIMessageRuleABILIIMessageRuleY or FIIMessageRuleNESS FOR A PARIMessageRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.MessageRuleDAO;

@JsonSerialize(as=IMessageRule.class)
public interface IMessageRule extends IRule {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static MessageRuleDAO<IMessageRule> dao() {
		return MessageRuleDAO.get();
	}
	
	static public void createOrUpdate(IMessageRule e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IMessageRule> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IMessageRule> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IMessageRule> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IMessageRule> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IMessageRule> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IMessageRule create(String domainKey, String specId) throws StorageException{
		return dao().create(domainKey, specId);
	}

	static public IMessageRule get(String domainKey, String specId) throws StorageException{
		return dao().get(domainKey, specId);
	}

	static public boolean has(String domainKey, String specId) throws StorageException{
		return dao().has(domainKey, specId);
	}

	static public void delete(String domainKey, String specId) throws StorageException{
		dao().delete(domainKey, specId);
	}

	static public Collection<String> listSpecs(String domainKey, int page, int pageSize) throws StorageException{
		return dao().listSpecs(domainKey, page, pageSize);
	}

	static public Collection<String> listSpecsNext(String domainKey, String specId, int page, int pageSize) throws StorageException{
		return dao().listSpecsNext(domainKey, specId, page, pageSize);
	}

	static public Collection<IMessageRule> list(boolean load, String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, page, pageSize);
	}

	static public Collection<IMessageRule> listNext(boolean load, String domainKey, String specId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, specId, page, pageSize);	
	}


	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IMessageRule.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IMessageRule o = (IMessageRule) other;
		
		setSpecId(o.getSpecId());
		
		IRule.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getSpecId();

	public void setSpecId(String specId);

}
