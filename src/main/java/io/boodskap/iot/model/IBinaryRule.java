/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.BinaryRuleDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IBinaryRule.class)
public interface IBinaryRule extends IRule {

	//======================================
	// DAO Methods
	//======================================
	
	public static BinaryRuleDAO<IBinaryRule> dao(){
		return BinaryRuleDAO.get();
	}
	
	static public void createOrUpdate(IBinaryRule e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IBinaryRule> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IBinaryRule> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IBinaryRule> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}
	
	static public EntityIterator<IBinaryRule> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IBinaryRule> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey,query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey,what,how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey,query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey,what,how);
	}
	
	static public IBinaryRule create(String domainKey, String type) {
		return dao().create(domainKey,type);
	}

	static public IBinaryRule get(String domainKey, String type) throws StorageException{
		return dao().get(domainKey,type);
	}

	static public void delete(String domainKey, String type) throws StorageException{
		dao().delete(domainKey,type);
	}

	static public Collection<IBinaryRule> list(boolean load, String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(load,domainKey,page,pageSize);
	}

	static public Collection<IBinaryRule> listNext(boolean load, String domainKey, String type, int page, int pageSize) throws StorageException{
		return dao().listNext(load,domainKey,type,page,pageSize);
	}

	static public Collection<String> listTypes(String domainKey, int page, int pageSize) throws StorageException{
		return dao().listTypes(domainKey,page,pageSize);
	}

	static public Collection<String> listTypesNext(String domainKey, String type, int page, int pageSize) throws StorageException{
		return dao().listTypesNext(domainKey,type,page,pageSize);
	}
	

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IBinaryRule.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IBinaryRule o = (IBinaryRule) other;
		
		setType(o.getType());
		
		IRule.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getType() ;

	public void setType(String type) ;
}
