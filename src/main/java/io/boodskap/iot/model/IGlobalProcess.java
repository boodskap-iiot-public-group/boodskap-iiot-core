/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * INamedRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * INamedRulehis program is distributed in the hope that it will be useful,
 * but WIINamedRuleHOUINamedRule ANY WARRANINamedRuleY; without even the implied warranty of
 * MERCHANINamedRuleABILIINamedRuleY or FIINamedRuleNESS FOR A PARINamedRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Map;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.GlobalProcessDAO;

@JsonSerialize(as=IGlobalProcess.class)
public interface IGlobalProcess extends ISystemRule  {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static GlobalProcessDAO<IGlobalProcess> dao(){
		return GlobalProcessDAO.get();
	}

	static public void createOrUpdate(IGlobalProcess e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IGlobalProcess> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IGlobalProcess> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IGlobalProcess> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IGlobalProcess create(String id) throws StorageException{
		return dao().create(id);
	}

	static public IGlobalProcess get(String id) throws StorageException{
		return dao().get(id);
	}

	static public boolean has(String id) throws StorageException{
		return dao().has(id);
	}

	static public void delete(String id) throws StorageException{
		dao().delete(id);
	}

	static public Collection<IGlobalProcess> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	static public Collection<IGlobalProcess> listNext(boolean load, String id, int page, int pageSize) throws StorageException{
		return dao().listNext(load, id, page, pageSize);
	}

	static public Collection<String> listIds(int page, int pageSize) throws StorageException{
		return dao().listIds(page, pageSize);
	}

	static public Collection<String> listIdsNext(String id, int page, int pageSize) throws StorageException{
		return dao().listIdsNext(id, page, pageSize);
	}


	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IGlobalProcess.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getId();
	
	public void setId(String id);
	
	public Map<String, Object> getInput();
	
	public void setInput(Map<String, Object> input);
	
	public Map<String, Object> getOutput();
	
	public void setOutput(Map<String, Object> output);
}


