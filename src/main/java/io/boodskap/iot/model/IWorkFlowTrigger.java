package io.boodskap.iot.model;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=IWorkFlowTrigger.class)
public interface IWorkFlowTrigger extends Serializable {

	//======================================
	// Attributes
	//======================================
	
}
