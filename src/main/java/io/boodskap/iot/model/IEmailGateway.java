/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EmailGatewayDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IEmailGateway.class)
public interface IEmailGateway extends IDomainObject {

	//======================================
	// DAO Methods
	//======================================
	
	public static EmailGatewayDAO<IEmailGateway> dao(){
		return EmailGatewayDAO.get();
	}

	static public void createOrUpdate(IEmailGateway e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IEmailGateway> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IEmailGateway> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IEmailGateway> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IEmailGateway> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IEmailGateway> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IEmailGateway create(String domainKey) throws StorageException{
		return dao().create(domainKey);
	}
	
	static public IEmailGateway get(String domainKey) throws StorageException{
		return dao().get(domainKey);
	}

	

	
	

	//======================================
	// DAO Methods
	//======================================
	
	@Override
	public default void save() {
		IEmailGateway.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getHost();

	public void setHost(String host);

	public int getPort();

	public void setPort(int port);

	public String getUser();

	public void setUser(String user);

	public String getPassword();

	public void setPassword(String password);

	public boolean isSsl();

	public void setSsl(boolean ssl);

	public String getPrimaryEmail();

	public void setPrimaryEmail(String primaryEmail);

	public String getBounceEmail();

	public void setBounceEmail(String bounceEmail);

	public boolean isTls();

	public void setTls(boolean tls);

	public boolean isDebug();

	public void setDebug(boolean debug);

}
