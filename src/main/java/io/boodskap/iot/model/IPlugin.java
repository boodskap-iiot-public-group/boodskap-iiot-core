/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.PluginDAO;
import io.boodskap.iot.plugin.PluginType;

@JsonSerialize(as=IPlugin.class)
public interface IPlugin extends IModel {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static PluginDAO<IPlugin> dao() {
		return PluginDAO.get();
	}
	
	static public void createOrUpdate(IPlugin e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IPlugin> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IPlugin> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IPlugin> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IPlugin create(String pluginId, String version){
		return dao().create(pluginId, version);
	}
	
	static public long count(String pluginId) throws StorageException{
		return dao().count(pluginId);
	}

	static public IPlugin get(String pluginId) throws StorageException{
		return dao().get(pluginId);
	}

	static public IPlugin get(String pluginId, String version) throws StorageException{
		return dao().get(pluginId, version);
	}

	static public IPlugin getByContextId(String contextId) throws StorageException{
		return dao().getByContextId(contextId);
	}

	static public void delete(String pluginId) throws StorageException{
		dao().delete(pluginId);
	}

	static public void delete(String pluginId, String version) throws StorageException{
		dao().delete(pluginId, version);
	}

	static public Collection<IPlugin> list(int page, int pageSize) throws StorageException{
		return dao().list(page, pageSize);
	}

	static public Collection<IPlugin> listNext(String pluginId, int page, int pageSize) throws StorageException{
		return dao().listNext(pluginId, page, pageSize);
	}


	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IPlugin.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getPluginId();
	
	public void setPluginId(String pluginId);
	
	public String getVersion();
	
	public void setVersion(String version);
	
	public String getContextId();
	
	public void setContextId(String contextId);
	
	public String getSystemId();
	
	public void setSystemId(String systemId);
	
	public PluginType getType();
	
	public void setType(PluginType type);
	
	public String getAuthor();
	
	public void setAuthor(String author);
	
	public String getClazz();
	
	public void setClazz(String clazz);
	
	public String getCrc();
	
	public void setCrc(String crc);
	
	public String getReadme();
	
	public void setReadme(String readme);
	
	public String getJsonContent();
	
	public void setJsonContent(String jsonContent);
	
}
