/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.ThreadContext;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
@JsonSerialize(as=IStorageObject.class)
public interface IStorageObject extends Serializable{

	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void copy(Object other) {
		
		IStorageObject o = (IStorageObject) other;
		
		setRegisteredStamp(o.getRegisteredStamp());
		setUpdatedStamp(o.getUpdatedStamp());
		setExtraProperties(o.getExtraProperties());
		
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@JsonIgnore
	public default JSONObject dynamicProperties() {
		return new JSONObject( null != getExtraProperties() ? getExtraProperties() : "{}");
	}
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void dynamicProperties(JSONObject dynamicProperties) {
		if(null == dynamicProperties) return;
		setExtraProperties(dynamicProperties.toString());
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void dynamicProperties(Object dynamicProperties) {
		if(null == dynamicProperties) return;
		if(dynamicProperties instanceof JSONObject) {
			dynamicProperties((JSONObject) dynamicProperties);
		}else {
			dynamicProperties(new JSONObject(dynamicProperties));
		}
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default String toJSONString() {
		try {
			return ThreadContext.toJSON(this);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default String toJSONPrettyString() {
		try {
			return ThreadContext.toGSONPretty(this);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default JSONObject toJSON() {
		return new JSONObject(this);
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default Map<String, Object> toMap() {
		return ThreadContext.jsonToMap(toJSONString());
	}

	//======================================
	// Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void save() throws StorageException;

	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public Date getRegisteredStamp();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setRegisteredStamp(Date registeredStamp);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public Date getUpdatedStamp();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setUpdatedStamp(Date updatedStamp);
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getExtraProperties();
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setExtraProperties(String extraProperties);

}
