/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DeviceCounterDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDeviceCounter.class)
public interface IDeviceCounter extends IDomainObject {

	//======================================
	// DAO Methods
	//======================================
	
	public static DeviceCounterDAO<IDeviceCounter> dao(){
		return DeviceCounterDAO.get();
	}
	
	static public void createOrUpdate(IDeviceCounter e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDeviceCounter> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDeviceCounter> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDeviceCounter> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}
	
	static public EntityIterator<IDeviceCounter> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDeviceCounter> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey,query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey,what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what,how);
	}
	
	static public IDeviceCounter create(String domainKey, String deviceId) throws StorageException{
		return dao().create(domainKey,deviceId);
	}
	
	static public IDeviceCounter getCounter() throws StorageException{
		return dao().getCounter();
	}

	static public IDeviceCounter getDomainCounter(String domainKey) throws StorageException{
		return dao().getDomainCounter(domainKey);
	}
	static public IDeviceCounter getDeviceCounter(String domainKey, String deviceId) throws StorageException{
		return dao().getDeviceCounter(domainKey,deviceId);
	}

	static public long getNextCorrelationId(String domainKey, String deviceId) throws StorageException{
		return dao().getNextCorrelationId(domainKey,deviceId);
	}

	static public void incrementUdp(String domainKey, String deviceId) throws StorageException{
		dao().incrementUdp(domainKey,deviceId);
	}

	static public void incrementLoRa(String domainKey, String deviceId) throws StorageException{
		dao().incrementLoRa(domainKey,deviceId);
	}

	static public void incrementMqtt(String domainKey, String deviceId) throws StorageException{
		dao().incrementMqtt(domainKey,deviceId);
	}

	static public void incrementHttp(String domainKey, String deviceId) throws StorageException{
		dao().incrementHttp(domainKey,deviceId);
	}

	static public void incrementFcm(String domainKey, String deviceId) throws StorageException{
		dao().incrementFcm(domainKey,deviceId);
	}

	static public void incrementCoAP(String domainKey, String deviceId) throws StorageException{
		dao().incrementCoAP(domainKey,deviceId);
	}

	static public void incrementTcp(String domainKey, String deviceId) throws StorageException{
		dao().incrementTcp(domainKey,deviceId);
	}

	static public void incrementCommands(String domainKey, String deviceId) throws StorageException{
		dao().incrementCommands(domainKey,deviceId);
	}

	static public long countUdp() throws StorageException{
		return dao().countUdp();
	}

	static public long countLoRa() throws StorageException{
		return dao().countLoRa();
	}

	static public long countMqtt() throws StorageException{
		return dao().countMqtt();
	}

	static public long countHttp() throws StorageException{
		return dao().countHttp();
	}

	static public long countFcm() throws StorageException{
		return dao().countFcm();
	}

	static public long countCoAP() throws StorageException{
		return dao().countCoAP();
	}

	static public long countTcp() throws StorageException{
		return dao().countTcp();
	}

	static public long countCommands() throws StorageException{
		return dao().countCommands();
	}
	
	static public long countUdp(String domainKey) throws StorageException{
		return dao().countUdp(domainKey);
	}

	static public long countLoRa(String domainKey) throws StorageException{
		return dao().countLoRa(domainKey);
	}

	static public long countMqtt(String domainKey) throws StorageException{
		return dao().countMqtt(domainKey);
	}

	static public long countHttp(String domainKey) throws StorageException{
		return dao().countHttp(domainKey);
	}

	static public long countFcm(String domainKey) throws StorageException{
		return dao().countFcm(domainKey);
	}

	static public long countCoAP(String domainKey) throws StorageException{
		return dao().countCoAP(domainKey);
	}

	static public long countTcp(String domainKey) throws StorageException{
		return dao().countTcp(domainKey);
	}

	static public long countCommands(String domainKey) throws StorageException{
		return dao().countCommands(domainKey);
	}

	static public long countUdp(String domainKey, String deviceId) throws StorageException{
		return dao().countUdp(domainKey,deviceId);
	}

	static public long countLoRa(String domainKey, String deviceId) throws StorageException{
		return dao().countLoRa(domainKey,deviceId);
	}

	static public long countMqtt(String domainKey, String deviceId) throws StorageException{
		return dao().countMqtt(domainKey,deviceId);
	}

	static public long countHttp(String domainKey, String deviceId) throws StorageException{
		return dao().countHttp(domainKey,deviceId);
	}

	static public long countFcm(String domainKey, String deviceId) throws StorageException{
		return dao().countFcm(domainKey,deviceId);
	}

	static public long countCoAP(String domainKey, String deviceId) throws StorageException{
		return dao().countCoAP(domainKey,deviceId);
	}

	static public long countTcp(String domainKey, String deviceId) throws StorageException{
		return dao().countTcp(domainKey,deviceId);
	}

	static public long countCommands(String domainKey, String deviceId) throws StorageException{
		return dao().countUdp(domainKey,deviceId);
	}
	

	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		IDeviceCounter.dao().createOrUpdate(this);
	}
	
	@Override
	public default void copy(Object other) {
		
		IDeviceCounter o = (IDeviceCounter) other;
		
		setDeviceId(o.getDeviceId());
		setUdpMessages(o.getUdpMessages());
		setMqttMessages(o.getMqttMessages());
		setHttpMessages(o.getHttpMessages());
		setFcmMessages(o.getFcmMessages());
		setCommands(o.getCommands());
		setCoapMessages(o.getCoapMessages());
		setTcpMessages(o.getTcpMessages());
		setLoraMessages(o.getLoraMessages());
		
		IDomainObject.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getDeviceId() ;

	public void setDeviceId(String deviceId);

	public long getUdpMessages();

	public void setUdpMessages(long udpMessages);

	public long getMqttMessages();

	public void setMqttMessages(long mqttMessages);

	public long getHttpMessages();

	public void setHttpMessages(long httpMessages);

	public long getFcmMessages();

	public void setFcmMessages(long fcmMessages);

	public long getCommands();

	public void setCommands(long commands);

	public long getCoapMessages();

	public void setCoapMessages(long coapMessages);

	public long getTcpMessages();

	public void setTcpMessages(long tcpMessages);

	public long getLoraMessages();

	public void setLoraMessages(long loraMessages);
}
