package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.SystemFileDAO;

@JsonSerialize(as=ISystemFile.class)
public interface ISystemFile extends IRawContent, IModel{

	//======================================
	// DAO Methods
	//======================================
		
	public static SystemFileDAO<ISystemFile> dao() {
		return SystemFileDAO.get();
	}
	
	static public void createOrUpdate(ISystemFile e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends ISystemFile> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<ISystemFile> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<ISystemFile> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public ISystemFile create(String fileId) throws StorageException{
		return dao().create(fileId);
	}
	
	static public ISystemFile get(String fileId) throws StorageException{
		return dao().get(fileId);
	}

	static public void deleteById(String fileId) throws StorageException{
		dao().deleteById(fileId);
	}

	static public Collection<ISystemFile> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	static public Collection<ISystemFile> listNext(boolean load, String fileId, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}


	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ISystemFile.dao().createOrUpdate(this);
	}
	
	@Override
	public default void copy(Object other) {
		
		ISystemFile o = (ISystemFile) other;
		
		setFileId(o.getFileId());
		setMediaType(o.getMediaType());
		setData(o.getData());
		setTags(o.getTags());
		
		IModel.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getFileId();
	
	public void setFileId(String fileId);

	public String getMediaType();

	public void setMediaType(String mediaType);

	public String getTags();

	public void setTags(String tags);

	//======================================
	// Helpers
	//======================================
	
	public IFileContent createContent();
	
}
