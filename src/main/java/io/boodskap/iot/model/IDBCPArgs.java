package io.boodskap.iot.model;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Jegan Vincent
 * @see 5.0.0-00
 */
@JsonSerialize(as=IDBCPArgs.class)
public interface IDBCPArgs extends Serializable {

	public String getUrl();

	public void setUrl(String url);

	public String getUsername();

	public void setUsername(String username);

	public String getPassword();

	public void setPassword(String password);

	public String getDriverClassName();

	public void setDriverClassName(String driverClassName);

	public Boolean getAbandonedUsageTracking();

	public void setAbandonedUsageTracking(Boolean abandonedUsageTracking);

	public Boolean getAccessToUnderlyingConnectionAllowed();

	public void setAccessToUnderlyingConnectionAllowed(Boolean accessToUnderlyingConnectionAllowed);

	public Boolean getCacheState();

	public void setCacheState(Boolean cacheState);

	public Boolean getDefaultAutoCommit();

	public void setDefaultAutoCommit(Boolean defaultAutoCommit);

	public String getDefaultCatalog();

	public void setDefaultCatalog(String defaultCatalog);

	public Integer getDefaultQueryTimeout();

	public void setDefaultQueryTimeout(Integer defaultQueryTimeout);

	public Boolean getDefaultReadOnly();

	public void setDefaultReadOnly(Boolean defaultReadOnly);

	public String getDefaultSchema();

	public void setDefaultSchema(String defaultSchema);

	public Integer getDefaultTransactionIsolation();

	public void setDefaultTransactionIsolation(Integer defaultTransactionIsolation);

	public Boolean getEnableAutoCommitOnReturn();

	public void setEnableAutoCommitOnReturn(Boolean enableAutoCommitOnReturn);

	public Boolean getFastFailValidation();

	public void setFastFailValidation(Boolean fastFailValidation);

	public Integer getInitialSize();

	public void setInitialSize(Integer initialSize);

	public Boolean getLifo();

	public void setLifo(Boolean lifo);

	public Boolean getLogAbandoned();

	public void setLogAbandoned(Boolean logAbandoned);

	public Boolean getLogExpiredConnections();

	public void setLogExpiredConnections(Boolean logExpiredConnections);

	public Integer getLoginTimeout();

	public void setLoginTimeout(Integer loginTimeout);

	public Long getMaxConnLifetimeMillis();

	public void setMaxConnLifetimeMillis(Long maxConnLifetimeMillis);

	public Integer getMaxIdle();

	public void setMaxIdle(Integer maxIdle);

	public Integer getMaxOpenPreparedStatements();

	public void setMaxOpenPreparedStatements(Integer maxOpenPreparedStatements);

	public Integer getMaxTotal();

	public void setMaxTotal(Integer maxTotal);

	public Long getMaxWaitMillis();

	public void setMaxWaitMillis(Long maxWaitMillis);

	public Long getMinEvictableIdleTimeMillis();

	public void setMinEvictableIdleTimeMillis(Long minEvictableIdleTimeMillis);

	public Integer getMinIdle();

	public void setMinIdle(Integer minIdle);

	public Integer getNumTestsPerEvictionRun();

	public void setNumTestsPerEvictionRun(Integer numTestsPerEvictionRun);

	public Boolean getPoolPreparedStatements();

	public void setPoolPreparedStatements(Boolean poolPreparedStatements);

	public Boolean getRemoveAbandonedOnBorrow();

	public void setRemoveAbandonedOnBorrow(Boolean removeAbandonedOnBorrow);

	public Boolean getRemoveAbandonedOnMaintenance();

	public void setRemoveAbandonedOnMaintenance(Boolean removeAbandonedOnMaintenance);

	public Boolean getRollbackOnReturn();

	public void setRollbackOnReturn(Boolean rollbackOnReturn);

	public Long getSoftMinEvictableIdleTimeMillis();

	public void setSoftMinEvictableIdleTimeMillis(Long softMinEvictableIdleTimeMillis);

	public Boolean getTestOnBorrow();

	public void setTestOnBorrow(Boolean testOnBorrow);

	public Boolean getTestOnCreate();

	public void setTestOnCreate(Boolean testOnCreate);

	public Boolean getTestOnReturn();

	public void setTestOnReturn(Boolean testOnReturn);

	public Boolean getTestWhileIdle();

	public void setTestWhileIdle(Boolean testWhileIdle);

	public Long getTimeBetweenEvictionRunsMillis();

	public void setTimeBetweenEvictionRunsMillis(Long timeBetweenEvictionRunsMillis);

	public String getValidationQuery();

	public void setValidationQuery(String validationQuery);

	public Integer getValidationQueryTimeout();

	public void setValidationQueryTimeout(Integer validationQueryTimeout);

}
