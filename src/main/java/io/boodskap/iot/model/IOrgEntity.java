package io.boodskap.iot.model;

public interface IOrgEntity extends IEntity {

	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void copy(Object other) {
		
		IOrgEntity o = (IOrgEntity) other;
		
		setOrgId(o.getOrgId());

		IEntity.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getOrgId();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setOrgId(String orgId);
}
