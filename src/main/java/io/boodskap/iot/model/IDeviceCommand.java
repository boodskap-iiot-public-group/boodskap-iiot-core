/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.DeviceCommandTarget;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DeviceCommandDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDeviceCommand.class)
public interface IDeviceCommand extends IBaseCommand{

	//======================================
	// DAO Methods
	//======================================
	
	public static DeviceCommandDAO<IDeviceCommand> dao(){
		return DeviceCommandDAO.get();
	}
	
	static public void createOrUpdate(IDeviceCommand e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDeviceCommand> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDeviceCommand> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDeviceCommand> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IDeviceCommand> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDeviceCommand> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IDeviceCommand create(String domainKey, String deviceId, long uid) {
		return dao().create(domainKey, deviceId, uid);
	}

	static public IDeviceCommand get(String domainKey, String deviceId, long uid) throws StorageException{
		return dao().get(domainKey, deviceId, uid);
	}
	
	static public long count(String domainKey, String deviceId) throws StorageException{
		return dao().count(domainKey, deviceId);
	}

	static public Collection<IDeviceCommand> list(String domainKey, String deviceId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, deviceId, page, pageSize);
	}

	static public Collection<IDeviceCommand> listNext(String domainKey, String deviceId, long uid, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, deviceId,uid,page, pageSize);
	}



	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDeviceCommand.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IDeviceCommand o = (IDeviceCommand) other;
		
		setTarget(o.getTarget());
		setIncludeMe(o.isIncludeMe());
		setDeviceId(o.getDeviceId());
		setGroups(o.getGroups());
		
		IBaseCommand.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public DeviceCommandTarget getTarget();

	public void setTarget(DeviceCommandTarget target);

	public boolean isIncludeMe();

	public void setIncludeMe(boolean includeMe);

	public String getDeviceId();

	public void setDeviceId(String deviceId);

	public Collection<String> getGroups();

	public void setGroups(Collection<String> groups);

}
