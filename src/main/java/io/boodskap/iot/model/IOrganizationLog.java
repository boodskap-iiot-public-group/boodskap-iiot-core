package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Date;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OrganizationLogDAO;

@JsonSerialize(as=IDomainLog.class)
public interface IOrganizationLog extends IDomainLog {

	//======================================
	// DAO Methods
	//======================================
	
	public static OrganizationLogDAO<IOrganizationLog> dao() {
		return OrganizationLogDAO.get();
	}

	public static Class<? extends IOrganizationLog> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<IOrganizationLog> load() throws StorageException{
		return dao().load();
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static SearchResult<IOrganizationLog> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	public static SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	public static JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	public static JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	public static EntityIterator<IOrganizationLog> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	public static long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	public static void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	public static SearchResult<IOrganizationLog> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	public static SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	public static JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	public static JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	public static EntityIterator<IOrganizationLog> load(String domainKey, String orgId) throws StorageException{
		return dao().load(domainKey, orgId);
	}
	
	public static long count(String domainKey, String orgId) throws StorageException{
		return dao().count(domainKey, orgId);
	}
	
	public static void delete(String domainKey, String orgId) throws StorageException{
		dao().delete(domainKey, orgId);
	}
	
	public static SearchResult<IOrganizationLog> searchByQuery(String domainKey, String orgId, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, orgId, query, page, pageSize);
	}

	public static SearchResult<JSONObject> selectByQuery(String domainKey, String orgId, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, orgId, what, how);
	}
	
	public static JSONObject deleteByQuery(String domainKey, String orgId, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, orgId, query);
	}

	public static JSONObject updateByQuery(String domainKey, String orgId, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, orgId, what, how);
	}
	
	public static IOrganizationLog create(String domainKey, String orgId, String id) throws StorageException{
		return dao().create(domainKey, orgId, id);
	}
	
	public static IOrganizationLog get(String domainKey, String orgId, String id) throws StorageException{
		return dao().get(domainKey, orgId, id);
	}
	
	public static Collection<IOrganizationLog> list(String domainKey, String orgId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, orgId, page, pageSize);
	}
	
	public static Collection<IOrganizationLog> listNext(String domainKey, String orgId, String id, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, orgId, id, page, pageSize);
	}
	
	public static void delete(String domainKey, String orgId, String id) throws StorageException{
		dao().delete(domainKey, orgId, id);
	}

	public static void deleteBefore(String domainKey, String orgId, Date date) throws StorageException{
		dao().deleteBefore(domainKey, orgId, date);
	}

	public static void deleteAfter(String domainKey, String orgId, Date date) throws StorageException{
		dao().deleteAfter(domainKey, orgId, date);
	}

	public static void deleteBetween(String domainKey, String orgId, Date from, Date to) throws StorageException{
		dao().deleteBetween(domainKey, orgId, from, to);
	}

	public static void deleteBefore(String orgId, Date date) throws StorageException{
		dao().deleteBefore(orgId, date);
	}

	public static void deleteAfter(String orgId, Date date) throws StorageException{
		dao().deleteAfter(orgId, date);
	}

	public static void deleteBetween(String orgId, Date from, Date to) throws StorageException{
		dao().deleteBetween(orgId, from, to);
	}

	public static void deleteBefore(Date date) throws StorageException{
		dao().deleteBefore(date);
	}

	public static void deleteAfter(Date date) throws StorageException{
		dao().deleteAfter(date);
	}

	public static void deleteBetween(Date from, Date to) throws StorageException{
		dao().deleteBetween(from, to);
	}

	//======================================
	// Default Methods
	//======================================
	
	@Override
	default void save() {
		dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================

	public void setOrgId(String orgId);
	
	public String getOrgId();
}
