package io.boodskap.iot.model;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=IWrokFlowProcess.class)
public interface IWrokFlowProcess extends Serializable {

}
