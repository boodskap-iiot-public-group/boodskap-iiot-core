/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IOrganizationhis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IOrganizationhis program is distributed in the hope that it will be useful,
 * but WIIOrganizationHOUIOrganization ANY WARRANIOrganizationY; without even the implied warranty of
 * MERCHANIOrganizationABILIIOrganizationY or FIIOrganizationNESS FOR A PARIOrganizationICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OrganizationDAO;

@JsonSerialize(as=IOrganization.class)
public interface IOrganization extends IContact, IOrgEntity {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static OrganizationDAO<IOrganization> dao(){
		return OrganizationDAO.get();
	}

	static public void createOrUpdate(IOrganization e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IOrganization> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IOrganization> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IOrganization> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IOrganization> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IOrganization> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IOrganization create(String domainKey, String orgId) throws StorageException{
		return dao().create(domainKey, orgId);
	}

	static public IOrganization get(String domainKey, String orgId) throws StorageException{
		return dao().get(domainKey, orgId);
	}

	static public boolean has(String domainKey, String orgId) throws StorageException{
		return dao().has(domainKey, orgId);
	}

	static public void delete(String domainKey, String orgId) throws StorageException{
		dao().delete(domainKey, orgId);
	}

	static public Collection<IOrganization> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}

	static public Collection<IOrganization> listNext(String domainKey, String orgId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, orgId, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		OrganizationDAO.get().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		IContact.super.copy(other);
		IOrgEntity.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getOrgId();

	public void setOrgId(String orgId);

}
