package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IC3P0Args;

public class C3P0Args implements IC3P0Args{

	private static final long serialVersionUID = -6282704203132021004L;
	
	@PojoField private String jdbcUrl = null;
	@PojoField private String user = null;
	@PojoField private String password = null;
	@PojoField private String driverClass = null;
	@PojoField private Integer acquireIncrement = null;
	@PojoField private Integer acquireRetryAttempts = null;
	@PojoField private Integer acquireRetryDelay = null;
	@PojoField private Boolean autoCommitOnClose = null;
	@PojoField private String automaticTestTable = null;
	@PojoField private Boolean breakAfterAcquireFailure = null;
	@PojoField private Integer checkoutTimeout = null;
	@PojoField private Boolean debugUnreturnedConnectionStackTraces = null;
	@PojoField private Boolean forceIgnoreUnresolvedTransactions = null;
	@PojoField private Boolean forceSynchronousCheckins = null;
	@PojoField private Boolean forceUseNamedDriverClass = null;
	@PojoField private String identityToken = null;
	@PojoField private Integer idleConnectionTestPeriod = null;
	@PojoField private Integer initialPoolSize = null;
	@PojoField private Integer loginTimeout = null;
	@PojoField private Integer maxAdministrativeTaskTime = null;
	@PojoField private Integer maxConnectionAge = null;
	@PojoField private Integer maxIdleTime = null;
	@PojoField private Integer maxIdleTimeExcessConnections = null;
	@PojoField private Integer maxPoolSize = null;
	@PojoField private Integer maxStatements = null;
	@PojoField private Integer maxStatementsPerConnection = null;
	@PojoField private Integer minPoolSize = null;
	@PojoField private Integer numHelperThreads = null;
	@PojoField private String overrideDefaultPassword = null;
	@PojoField private String overrideDefaultUser = null;
	@PojoField private String preferredTestQuery = null;
	@PojoField private Boolean privilegeSpawnedThreads = null;
	@PojoField private Integer propertyCycle = null;
	@PojoField private Integer statementCacheNumDeferredCloseThreads = null;
	@PojoField private Boolean testConnectionOnCheckin = null;
	@PojoField private Boolean testConnectionOnCheckout = null;
	@PojoField private Integer unreturnedConnectionTimeout = null;
	@PojoField private String userOverridesAsString = null;
	@PojoField private Boolean usesTraditionalReflectiveProxies = null;

	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDriverClass() {
		return driverClass;
	}

	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}

	public Integer getAcquireIncrement() {
		return acquireIncrement;
	}

	public void setAcquireIncrement(Integer acquireIncrement) {
		this.acquireIncrement = acquireIncrement;
	}

	public Integer getAcquireRetryAttempts() {
		return acquireRetryAttempts;
	}

	public void setAcquireRetryAttempts(Integer acquireRetryAttempts) {
		this.acquireRetryAttempts = acquireRetryAttempts;
	}

	public Integer getAcquireRetryDelay() {
		return acquireRetryDelay;
	}

	public void setAcquireRetryDelay(Integer acquireRetryDelay) {
		this.acquireRetryDelay = acquireRetryDelay;
	}

	public Boolean getAutoCommitOnClose() {
		return autoCommitOnClose;
	}

	public void setAutoCommitOnClose(Boolean autoCommitOnClose) {
		this.autoCommitOnClose = autoCommitOnClose;
	}

	public String getAutomaticTestTable() {
		return automaticTestTable;
	}

	public void setAutomaticTestTable(String automaticTestTable) {
		this.automaticTestTable = automaticTestTable;
	}

	public Boolean getBreakAfterAcquireFailure() {
		return breakAfterAcquireFailure;
	}

	public void setBreakAfterAcquireFailure(Boolean breakAfterAcquireFailure) {
		this.breakAfterAcquireFailure = breakAfterAcquireFailure;
	}

	public Integer getCheckoutTimeout() {
		return checkoutTimeout;
	}

	public void setCheckoutTimeout(Integer checkoutTimeout) {
		this.checkoutTimeout = checkoutTimeout;
	}

	public Boolean getDebugUnreturnedConnectionStackTraces() {
		return debugUnreturnedConnectionStackTraces;
	}

	public void setDebugUnreturnedConnectionStackTraces(Boolean debugUnreturnedConnectionStackTraces) {
		this.debugUnreturnedConnectionStackTraces = debugUnreturnedConnectionStackTraces;
	}

	public Boolean getForceIgnoreUnresolvedTransactions() {
		return forceIgnoreUnresolvedTransactions;
	}

	public void setForceIgnoreUnresolvedTransactions(Boolean forceIgnoreUnresolvedTransactions) {
		this.forceIgnoreUnresolvedTransactions = forceIgnoreUnresolvedTransactions;
	}

	public Boolean getForceSynchronousCheckins() {
		return forceSynchronousCheckins;
	}

	public void setForceSynchronousCheckins(Boolean forceSynchronousCheckins) {
		this.forceSynchronousCheckins = forceSynchronousCheckins;
	}

	public Boolean getForceUseNamedDriverClass() {
		return forceUseNamedDriverClass;
	}

	public void setForceUseNamedDriverClass(Boolean forceUseNamedDriverClass) {
		this.forceUseNamedDriverClass = forceUseNamedDriverClass;
	}

	public String getIdentityToken() {
		return identityToken;
	}

	public void setIdentityToken(String identityToken) {
		this.identityToken = identityToken;
	}

	public Integer getIdleConnectionTestPeriod() {
		return idleConnectionTestPeriod;
	}

	public void setIdleConnectionTestPeriod(Integer idleConnectionTestPeriod) {
		this.idleConnectionTestPeriod = idleConnectionTestPeriod;
	}

	public Integer getInitialPoolSize() {
		return initialPoolSize;
	}

	public void setInitialPoolSize(Integer initialPoolSize) {
		this.initialPoolSize = initialPoolSize;
	}

	public Integer getLoginTimeout() {
		return loginTimeout;
	}

	public void setLoginTimeout(Integer loginTimeout) {
		this.loginTimeout = loginTimeout;
	}

	public Integer getMaxAdministrativeTaskTime() {
		return maxAdministrativeTaskTime;
	}

	public void setMaxAdministrativeTaskTime(Integer maxAdministrativeTaskTime) {
		this.maxAdministrativeTaskTime = maxAdministrativeTaskTime;
	}

	public Integer getMaxConnectionAge() {
		return maxConnectionAge;
	}

	public void setMaxConnectionAge(Integer maxConnectionAge) {
		this.maxConnectionAge = maxConnectionAge;
	}

	public Integer getMaxIdleTime() {
		return maxIdleTime;
	}

	public void setMaxIdleTime(Integer maxIdleTime) {
		this.maxIdleTime = maxIdleTime;
	}

	public Integer getMaxIdleTimeExcessConnections() {
		return maxIdleTimeExcessConnections;
	}

	public void setMaxIdleTimeExcessConnections(Integer maxIdleTimeExcessConnections) {
		this.maxIdleTimeExcessConnections = maxIdleTimeExcessConnections;
	}

	public Integer getMaxPoolSize() {
		return maxPoolSize;
	}

	public void setMaxPoolSize(Integer maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}

	public Integer getMaxStatements() {
		return maxStatements;
	}

	public void setMaxStatements(Integer maxStatements) {
		this.maxStatements = maxStatements;
	}

	public Integer getMaxStatementsPerConnection() {
		return maxStatementsPerConnection;
	}

	public void setMaxStatementsPerConnection(Integer maxStatementsPerConnection) {
		this.maxStatementsPerConnection = maxStatementsPerConnection;
	}

	public Integer getMinPoolSize() {
		return minPoolSize;
	}

	public void setMinPoolSize(Integer minPoolSize) {
		this.minPoolSize = minPoolSize;
	}

	public Integer getNumHelperThreads() {
		return numHelperThreads;
	}

	public void setNumHelperThreads(Integer numHelperThreads) {
		this.numHelperThreads = numHelperThreads;
	}

	public String getOverrideDefaultPassword() {
		return overrideDefaultPassword;
	}

	public void setOverrideDefaultPassword(String overrideDefaultPassword) {
		this.overrideDefaultPassword = overrideDefaultPassword;
	}

	public String getOverrideDefaultUser() {
		return overrideDefaultUser;
	}

	public void setOverrideDefaultUser(String overrideDefaultUser) {
		this.overrideDefaultUser = overrideDefaultUser;
	}

	public String getPreferredTestQuery() {
		return preferredTestQuery;
	}

	public void setPreferredTestQuery(String preferredTestQuery) {
		this.preferredTestQuery = preferredTestQuery;
	}

	public Boolean getPrivilegeSpawnedThreads() {
		return privilegeSpawnedThreads;
	}

	public void setPrivilegeSpawnedThreads(Boolean privilegeSpawnedThreads) {
		this.privilegeSpawnedThreads = privilegeSpawnedThreads;
	}

	public Integer getPropertyCycle() {
		return propertyCycle;
	}

	public void setPropertyCycle(Integer propertyCycle) {
		this.propertyCycle = propertyCycle;
	}

	public Integer getStatementCacheNumDeferredCloseThreads() {
		return statementCacheNumDeferredCloseThreads;
	}

	public void setStatementCacheNumDeferredCloseThreads(Integer statementCacheNumDeferredCloseThreads) {
		this.statementCacheNumDeferredCloseThreads = statementCacheNumDeferredCloseThreads;
	}

	public Boolean getTestConnectionOnCheckin() {
		return testConnectionOnCheckin;
	}

	public void setTestConnectionOnCheckin(Boolean testConnectionOnCheckin) {
		this.testConnectionOnCheckin = testConnectionOnCheckin;
	}

	public Boolean getTestConnectionOnCheckout() {
		return testConnectionOnCheckout;
	}

	public void setTestConnectionOnCheckout(Boolean testConnectionOnCheckout) {
		this.testConnectionOnCheckout = testConnectionOnCheckout;
	}

	public Integer getUnreturnedConnectionTimeout() {
		return unreturnedConnectionTimeout;
	}

	public void setUnreturnedConnectionTimeout(Integer unreturnedConnectionTimeout) {
		this.unreturnedConnectionTimeout = unreturnedConnectionTimeout;
	}

	public String getUserOverridesAsString() {
		return userOverridesAsString;
	}

	public void setUserOverridesAsString(String userOverridesAsString) {
		this.userOverridesAsString = userOverridesAsString;
	}

	public Boolean getUsesTraditionalReflectiveProxies() {
		return usesTraditionalReflectiveProxies;
	}

	public void setUsesTraditionalReflectiveProxies(Boolean usesTraditionalReflectiveProxies) {
		this.usesTraditionalReflectiveProxies = usesTraditionalReflectiveProxies;
	}

}
