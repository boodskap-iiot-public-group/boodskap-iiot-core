package io.boodskap.iot.model.pojo;

import io.boodskap.iot.DataFormat;
import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.ISystemProperty;

public class SystemProperty extends AbstractModel implements ISystemProperty {

	private static final long serialVersionUID = -9065643553441625993L;
	
	@PojoField private DataFormat format = null;
	@PojoField private String value = null;
	
	public SystemProperty() {
	}
	
	public SystemProperty(String name) {
		super(name);
	}

	@Override
	public DataFormat getFormat() {
		return format;
	}

	@Override
	public void setFormat(DataFormat format) {
		this.format = format;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((format == null) ? 0 : format.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemProperty other = (SystemProperty) obj;
		if (format != other.format)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
