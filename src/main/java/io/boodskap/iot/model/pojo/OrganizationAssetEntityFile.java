package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IOrganizationAssetEntityFile;

public class OrganizationAssetEntityFile extends OrganizationEntityFile implements IOrganizationAssetEntityFile {

	private static final long serialVersionUID = -6219447926450733949L;
	
	@PojoField private String assetId;
	
	public OrganizationAssetEntityFile() {
	}
	
	public OrganizationAssetEntityFile(String domainKey, String orgId, String assetId, String entityType, String entityId, String fileId) {
		super(domainKey, orgId, entityType, entityId, fileId);
		this.assetId = assetId;
	}

	@Override
	public String getAssetId() {
		return assetId;
	}

	@Override
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((assetId == null) ? 0 : assetId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganizationAssetEntityFile other = (OrganizationAssetEntityFile) obj;
		if (assetId == null) {
			if (other.assetId != null)
				return false;
		} else if (!assetId.equals(other.assetId))
			return false;
		return true;
	}

}
