package io.boodskap.iot.model.pojo;

import java.util.Arrays;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IJarFile;

public class JarFile extends AbstractModel implements IJarFile {
	
	private static final long serialVersionUID = -2413865885729775868L;
	
	@PojoField private String loader;
	@PojoField private String fileName;
	@PojoField private byte[] objectCode;
	
	public JarFile() {
	}
	
	public JarFile(String loader, String fileName) {
		this.loader=loader;
		this.fileName=fileName;
	}
	

	@Override
	public final String getLoader() {
		return loader;
	}

	@Override
	public final void setLoader(String loader) {
		this.loader = loader;
	}

	@Override
	public final String getFileName() {
		return fileName;
	}

	@Override
	public final void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public final byte[] getData() {
		return objectCode;
	}

	@Override
	public final void setData(byte[] objectCode) {
		this.objectCode = objectCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + ((loader == null) ? 0 : loader.hashCode());
		result = prime * result + Arrays.hashCode(objectCode);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JarFile other = (JarFile) obj;
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		} else if (!fileName.equals(other.fileName))
			return false;
		if (loader == null) {
			if (other.loader != null)
				return false;
		} else if (!loader.equals(other.loader))
			return false;
		if (!Arrays.equals(objectCode, other.objectCode))
			return false;
		return true;
	}
	
}
