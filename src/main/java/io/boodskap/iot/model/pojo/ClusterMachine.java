package io.boodskap.iot.model.pojo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IClusterMachine;
import io.boodskap.iot.model.INameValuePair;

public class ClusterMachine extends AbstractDomainObject implements IClusterMachine {

	private static final long serialVersionUID = 44307397709074189L;
	
	@PojoField private String clusterId;
	@PojoField private String targetDomainKey;
	@PojoField private String machineId;
	@PojoField private MachineStatus status;
	@PojoField private int cpuSlots;
	@PojoField private int cpuCores;
	@PojoField private List<NameValuePair> properties = new ArrayList<>();
	
	public ClusterMachine() {
	}
	
	public ClusterMachine(String domainKey, String targetDomainKey, String clusterId, String machineId) {
		super(domainKey);
		this.clusterId = clusterId;
		this.targetDomainKey = targetDomainKey;
		this.machineId = machineId;
	}

	@Override
	public INameValuePair createNameValuePair(String name, String value) {
		return new NameValuePair(name, value);
	}

	@Override
	public String getClusterId() {
		return clusterId;
	}

	@Override
	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	@Override
	public String getTargetDomainKey() {
		return targetDomainKey;
	}

	@Override
	public void setTargetDomainKey(String targetDomainKey) {
		this.targetDomainKey = targetDomainKey;
	}

	@Override
	public String getMachineId() {
		return machineId;
	}

	@Override
	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	@Override
	public MachineStatus getStatus() {
		return status;
	}

	@Override
	public void setStatus(MachineStatus status) {
		this.status = status;
	}

	@Override
	public int getCpuSlots() {
		return cpuSlots;
	}

	@Override
	public void setCpuSlots(int cpuSlots) {
		this.cpuSlots = cpuSlots;
	}

	@Override
	public int getCpuCores() {
		return cpuCores;
	}

	@Override
	public void setCpuCores(int cpuCores) {
		this.cpuCores = cpuCores;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NameValuePair> getProperties() {
		return properties;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setProperties(Collection<? extends INameValuePair> properties) {
		this.properties.clear();
		this.properties.addAll((Collection<? extends NameValuePair>) properties);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((clusterId == null) ? 0 : clusterId.hashCode());
		result = prime * result + cpuCores;
		result = prime * result + cpuSlots;
		result = prime * result + ((machineId == null) ? 0 : machineId.hashCode());
		result = prime * result + ((properties == null) ? 0 : properties.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((targetDomainKey == null) ? 0 : targetDomainKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClusterMachine other = (ClusterMachine) obj;
		if (clusterId == null) {
			if (other.clusterId != null)
				return false;
		} else if (!clusterId.equals(other.clusterId))
			return false;
		if (cpuCores != other.cpuCores)
			return false;
		if (cpuSlots != other.cpuSlots)
			return false;
		if (machineId == null) {
			if (other.machineId != null)
				return false;
		} else if (!machineId.equals(other.machineId))
			return false;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (!properties.equals(other.properties))
			return false;
		if (status != other.status)
			return false;
		if (targetDomainKey == null) {
			if (other.targetDomainKey != null)
				return false;
		} else if (!targetDomainKey.equals(other.targetDomainKey))
			return false;
		return true;
	}

}
