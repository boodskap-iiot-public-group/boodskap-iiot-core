package io.boodskap.iot.model.pojo;

import io.boodskap.iot.LogSeverity;
import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.ISystemLog;

public class SystemLog extends AbstractStorageObject implements ISystemLog{

	private static final long serialVersionUID = -6890613869147495703L;

	@PojoField private String id;
	@PojoField private String path;
	@PojoField private LogSeverity severity;
	@PojoField private String message;
	@PojoField private String trace;
	@PojoField private String className;
	@PojoField private String fileName;
	@PojoField private int lineNumber;

	public SystemLog() {
	}

	public SystemLog(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public LogSeverity getSeverity() {
		return severity;
	}

	public void setSeverity(LogSeverity severity) {
		this.severity = severity;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

}
