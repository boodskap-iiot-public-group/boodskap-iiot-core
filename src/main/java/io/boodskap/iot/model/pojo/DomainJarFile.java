package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDomainJarFile;

public class DomainJarFile extends JarFile implements IDomainJarFile {

	private static final long serialVersionUID = -4761925915183687004L;
	
	@PojoField private String domainKey;

	
	public DomainJarFile(String domainKey, String loader, String fileName) {
		super(loader, fileName);
		this.domainKey = domainKey;
	}

	@Override
	public final String getDomainKey() {
		return domainKey;
	}

	@Override
	public final void setDomainKey(String domainKey) {
		this.domainKey = domainKey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((domainKey == null) ? 0 : domainKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainJarFile other = (DomainJarFile) obj;
		if (domainKey == null) {
			if (other.domainKey != null)
				return false;
		} else if (!domainKey.equals(other.domainKey))
			return false;
		return true;
	}

}
