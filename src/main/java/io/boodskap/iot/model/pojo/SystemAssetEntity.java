package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.ISystemAssetEntity;

public class SystemAssetEntity extends SystemEntity implements ISystemAssetEntity {
	
	private static final long serialVersionUID = 2882976847537967569L;

	@PojoField private String assetId;

	public SystemAssetEntity() {
	}
	
	public SystemAssetEntity(String assetId, String entityType, String entityId) {
		super(entityType, entityId);
		this.assetId = assetId;
	}
	
	public SystemAssetEntity(String domainKey, String assetId, String entityType, String entityId) {
		super(domainKey, entityType, entityId);
		this.assetId = assetId;
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

}
