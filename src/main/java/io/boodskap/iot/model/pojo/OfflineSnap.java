package io.boodskap.iot.model.pojo;

import java.util.Arrays;
import java.util.Objects;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IOfflineSnap;

public class OfflineSnap extends AbstractDomainObject implements IOfflineSnap {

	private static final long serialVersionUID = 1074590347354679901L;
	
	@PojoField private String deviceId;
	@PojoField private String camera;
	@PojoField private String mime;
	@PojoField private byte[] data;
	@PojoField private String id;
	
	public OfflineSnap() {
	}
	
	public OfflineSnap(String domainKey, String deviceId, String camera, String id) {
		super(domainKey);
		this.deviceId = deviceId;
		this.camera = camera;
		this.id = id;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getCamera() {
		return camera;
	}

	public void setCamera(String camera) {
		this.camera = camera;
	}

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(data);
		result = prime * result + Objects.hash(camera, deviceId, id, mime);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OfflineSnap other = (OfflineSnap) obj;
		return Objects.equals(camera, other.camera) && Arrays.equals(data, other.data)
				&& Objects.equals(deviceId, other.deviceId) && Objects.equals(id, other.id)
				&& Objects.equals(mime, other.mime);
	}

}
