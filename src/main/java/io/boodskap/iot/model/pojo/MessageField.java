package io.boodskap.iot.model.pojo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IMessageField;

@JsonSerialize(as=MessageField.class)
public class MessageField extends AbstractField implements IMessageField {

	private static final long serialVersionUID = -6373844951592507837L;

	@PojoField private boolean indexed;
	@PojoField private boolean fulltextIndexed;
	
	public MessageField(){
	}

	public MessageField(String domainKey, String specId, String name) {
		super(domainKey, specId, name);
	}

	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}

	public boolean isFulltextIndexed() {
		return fulltextIndexed;
	}

	public void setFulltextIndexed(boolean fulltextIndexed) {
		this.fulltextIndexed = fulltextIndexed;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (fulltextIndexed ? 1231 : 1237);
		result = prime * result + (indexed ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageField other = (MessageField) obj;
		if (fulltextIndexed != other.fulltextIndexed)
			return false;
		if (indexed != other.indexed)
			return false;
		return true;
	}

}
