package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDomainAssetEntityFile;

public class DomainAssetEntityFile extends DomainEntityFile implements IDomainAssetEntityFile {

	private static final long serialVersionUID = -6219447926450733949L;
	
	@PojoField private String assetId;
	
	public DomainAssetEntityFile() {
	}
	
	public DomainAssetEntityFile(String domainKey, String assetId, String entityType, String entityId, String fileId) {
		super(domainKey, entityType, entityId, fileId);
		this.assetId = assetId;
	}

	@Override
	public String getAssetId() {
		return assetId;
	}

	@Override
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((assetId == null) ? 0 : assetId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainAssetEntityFile other = (DomainAssetEntityFile) obj;
		if (assetId == null) {
			if (other.assetId != null)
				return false;
		} else if (!assetId.equals(other.assetId))
			return false;
		return true;
	}

}
