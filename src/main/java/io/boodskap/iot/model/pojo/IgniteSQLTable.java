package io.boodskap.iot.model.pojo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.boodskap.iot.CacheAtomicityMode;
import io.boodskap.iot.CacheMode;
import io.boodskap.iot.CacheWriteSynchronizationMode;
import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IIgniteSQLField;
import io.boodskap.iot.model.IIgniteSQLTable;

public class IgniteSQLTable extends AbstractDomainObject implements IIgniteSQLTable{

	private static final long serialVersionUID = 4087544526150308155L;
	
	@PojoField private List<IgniteSQLField> fields = new ArrayList<>();
	@PojoField private List<String> primaryKeyFields = new ArrayList<String>();
	@PojoField private List<String> indexFields = null;
	@PojoField private String affinityField = null;
	@PojoField private Integer backups = null;
	@PojoField private CacheMode cacheMode;
	@PojoField private CacheAtomicityMode cacheAtomicityMode;
	@PojoField private CacheWriteSynchronizationMode cacheWriteSynchronizationMode;
	
	public IgniteSQLTable() {
	}

	public IgniteSQLTable(String domainKey, String name) {
		super(domainKey, name);
	}

	public List<String> getPrimaryKeyFields() {
		return primaryKeyFields;
	}

	public void setPrimaryKeyFields(List<String> primaryKeyFields) {
		this.primaryKeyFields = primaryKeyFields;
	}

	public List<String> getIndexFields() {
		return indexFields;
	}

	public void setIndexFields(List<String> indexFields) {
		this.indexFields = indexFields;
	}

	public String getAffinityField() {
		return affinityField;
	}

	public void setAffinityField(String affinityField) {
		this.affinityField = affinityField;
	}

	public Integer getBackups() {
		return backups;
	}

	public void setBackups(Integer backups) {
		this.backups = backups;
	}

	public CacheMode getCacheMode() {
		return cacheMode;
	}

	public void setCacheMode(CacheMode cacheMode) {
		this.cacheMode = cacheMode;
	}

	public CacheAtomicityMode getCacheAtomicityMode() {
		return cacheAtomicityMode;
	}

	public void setCacheAtomicityMode(CacheAtomicityMode cacheAtomicityMode) {
		this.cacheAtomicityMode = cacheAtomicityMode;
	}

	public CacheWriteSynchronizationMode getCacheWriteSynchronizationMode() {
		return cacheWriteSynchronizationMode;
	}

	public void setCacheWriteSynchronizationMode(CacheWriteSynchronizationMode cacheWriteSynchronizationMode) {
		this.cacheWriteSynchronizationMode = cacheWriteSynchronizationMode;
	}

	@Override
	public List<IgniteSQLField> getFields() {
		return fields;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setFields(Collection<? extends IIgniteSQLField> fields) {
		this.fields.clear();
		this.fields.addAll((Collection<? extends IgniteSQLField>) fields);
	}

}
