package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IJavaClass;

public class JavaClass extends AbstractEntity implements IJavaClass {
	
	private static final long serialVersionUID = 4142202831819785684L;
	
	@PojoField private String loader;
	@PojoField private String pkg;
	@PojoField private String name;

	public JavaClass() {
	}
	public JavaClass(String loader, String pkg, String name) {
		this.loader = loader;
		this.pkg = pkg;
		this.name = name;
	}

	@Override
	public final String getLoader() {
		return loader;
	}

	@Override
	public final void setLoader(String loader) {
		this.loader = loader;
	}

	@Override
	public final String getPkg() {
		return pkg;
	}

	@Override
	public final void setPkg(String pkg) {
		this.pkg = pkg;
	}

	@Override
	public final String getName() {
		return name;
	}

	@Override
	public final void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((loader == null) ? 0 : loader.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pkg == null) ? 0 : pkg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JavaClass other = (JavaClass) obj;
		if (loader == null) {
			if (other.loader != null)
				return false;
		} else if (!loader.equals(other.loader))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pkg == null) {
			if (other.pkg != null)
				return false;
		} else if (!pkg.equals(other.pkg))
			return false;
		return true;
	}

}
