package io.boodskap.iot.model.pojo;

import java.util.Objects;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IClassLoader;

public class ClassLoader extends AbstractModel implements IClassLoader {
	
	private static final long serialVersionUID = -2371836345034347875L;
	
	@PojoField private String loader;
	@PojoField private String parentLoader;
	
	public ClassLoader() {
	}

	public ClassLoader(String loader) {
		this.loader = loader;
	}

	@Override
	public final String getLoader() {
		return loader;
	}

	@Override
	public final void setLoader(String loader) {
		this.loader = loader;
	}

	@Override
	public String getParentLoader() {
		return parentLoader;
	}

	@Override
	public void setParentLoader(String parentLoader) {
		this.parentLoader = parentLoader;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(loader, parentLoader);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassLoader other = (ClassLoader) obj;
		return Objects.equals(loader, other.loader) && Objects.equals(parentLoader, other.parentLoader);
	}

}
