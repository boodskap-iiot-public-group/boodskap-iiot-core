package io.boodskap.iot.model.pojo;

import java.util.Objects;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDomainClassLoader;

public class DomainClassLoader extends ClassLoader implements IDomainClassLoader {
	
	private static final long serialVersionUID = -2371836345034347875L;
	
	@PojoField private String domainKey;
	@PojoField private String systemLoader = "loader";
	
	public DomainClassLoader() {
	}

	public DomainClassLoader(String domainKey, String loader) {
		super(loader);
		this.domainKey = domainKey;
	}

	public DomainClassLoader(String domainKey, String loader, String systemLoader) {
		super(loader);
		this.domainKey = domainKey;
		this.systemLoader = systemLoader;
	}

	@Override
	public String getDomainKey() {
		return domainKey;
	}

	@Override
	public void setDomainKey(String domainKey) {
		this.domainKey = domainKey;
	}

	@Override
	public String getSystemLoader() {
		return systemLoader;
	}

	@Override
	public void setSystemLoader(String systemLoader) {
		this.systemLoader = systemLoader;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(domainKey, systemLoader);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainClassLoader other = (DomainClassLoader) obj;
		return Objects.equals(domainKey, other.domainKey) && Objects.equals(systemLoader, other.systemLoader);
	}

}
