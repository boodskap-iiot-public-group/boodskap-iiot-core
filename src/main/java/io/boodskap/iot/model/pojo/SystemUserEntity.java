package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.ISystemUserEntity;

public class SystemUserEntity extends SystemEntity implements ISystemUserEntity {
	
	private static final long serialVersionUID = 2882976847537967569L;

	@PojoField private String userId;

	public SystemUserEntity() {
	}
	
	public SystemUserEntity(String userId, String entityType, String entityId) {
		super(entityType, entityId);
		this.userId = userId;
	}
	
	public SystemUserEntity(String domainKey, String userId, String entityType, String entityId) {
		super(domainKey, entityType, entityId);
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
