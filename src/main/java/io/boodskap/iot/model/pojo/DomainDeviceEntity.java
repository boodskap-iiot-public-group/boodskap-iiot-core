package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDomainDeviceEntity;

public class DomainDeviceEntity extends DomainEntity implements IDomainDeviceEntity{
	
	private static final long serialVersionUID = -855005903591254984L;
	
	@PojoField private String deviceId;

	public DomainDeviceEntity() {
	}

	public DomainDeviceEntity(String domainKey, String deviceId, String entityType, String entityId) {
		super(domainKey, entityType, entityId);
		this.deviceId = deviceId;
	}

	@Override
	public String getDeviceId() {
		return deviceId;
	}

	@Override
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainDeviceEntity other = (DomainDeviceEntity) obj;
		if (deviceId == null) {
			if (other.deviceId != null)
				return false;
		} else if (!deviceId.equals(other.deviceId))
			return false;
		return true;
	}

}
