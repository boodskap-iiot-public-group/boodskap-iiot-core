package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.ITomcatJDBCArgs;

public class TomcatJDBCArgs implements ITomcatJDBCArgs{

	private static final long serialVersionUID = 8209898967711385224L;
	
	@PojoField private String url = null;
	@PojoField private String username = null;
	@PojoField private String password = null;
	@PojoField private String driverClassName = null;
	@PojoField private Integer abandonWhenPercentageFull = null;
	@PojoField private Boolean accessToUnderlyingConnectionAllowed = null;
	@PojoField private Boolean alternateUsernameAllowed = null;
	@PojoField private Boolean commitOnReturn = null;
	@PojoField private Boolean defaultAutoCommit = null;
	@PojoField private String defaultCatalog = null;
	@PojoField private Boolean defaultReadOnly = null;
	@PojoField private Integer defaultTransactionIsolation = null;
	@PojoField private Boolean fairQueue = null;
	@PojoField private Boolean ignoreExceptionOnPreLoad = null;
	@PojoField private Integer initialSize = null;
	@PojoField private String initSQL = null;
	@PojoField private Boolean logAbandoned = null;
	@PojoField private Boolean logValidationErrors = null;
	@PojoField private Integer maxActive = null;
	@PojoField private Long maxAge = null;
	@PojoField private Integer maxIdle = null;
	@PojoField private Integer maxWait = null;
	@PojoField private Integer minEvictableIdleTimeMillis = null;
	@PojoField private Integer minIdle = null;
	@PojoField private Integer numTestsPerEvictionRun = null;
	@PojoField private Boolean propagateInterruptState = null;
	@PojoField private Boolean removeAbandoned = null;
	@PojoField private Integer removeAbandonedTimeout = null;
	@PojoField private Boolean rollbackOnReturn = null;
	@PojoField private Integer suspectTimeout = null;
	@PojoField private Boolean testOnBorrow = null;
	@PojoField private Boolean testOnConnect = null;
	@PojoField private Boolean testOnReturn = null;
	@PojoField private Boolean testWhileIdle = null;
	@PojoField private Integer timeBetweenEvictionRunsMillis = null;
	@PojoField private Boolean useDisposableConnectionFacade = null;
	@PojoField private Boolean useEquals = null;
	@PojoField private Boolean useLock = null;
	@PojoField private Boolean useStatementFacade = null;
	@PojoField private Long validationInterval = null;
	@PojoField private String validationQuery = null;
	@PojoField private Integer validationQueryTimeout = null;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public Integer getAbandonWhenPercentageFull() {
		return abandonWhenPercentageFull;
	}

	public void setAbandonWhenPercentageFull(Integer abandonWhenPercentageFull) {
		this.abandonWhenPercentageFull = abandonWhenPercentageFull;
	}

	public Boolean getAccessToUnderlyingConnectionAllowed() {
		return accessToUnderlyingConnectionAllowed;
	}

	public void setAccessToUnderlyingConnectionAllowed(Boolean accessToUnderlyingConnectionAllowed) {
		this.accessToUnderlyingConnectionAllowed = accessToUnderlyingConnectionAllowed;
	}

	public Boolean getAlternateUsernameAllowed() {
		return alternateUsernameAllowed;
	}

	public void setAlternateUsernameAllowed(Boolean alternateUsernameAllowed) {
		this.alternateUsernameAllowed = alternateUsernameAllowed;
	}

	public Boolean getCommitOnReturn() {
		return commitOnReturn;
	}

	public void setCommitOnReturn(Boolean commitOnReturn) {
		this.commitOnReturn = commitOnReturn;
	}

	public Boolean getDefaultAutoCommit() {
		return defaultAutoCommit;
	}

	public void setDefaultAutoCommit(Boolean defaultAutoCommit) {
		this.defaultAutoCommit = defaultAutoCommit;
	}

	public String getDefaultCatalog() {
		return defaultCatalog;
	}

	public void setDefaultCatalog(String defaultCatalog) {
		this.defaultCatalog = defaultCatalog;
	}

	public Boolean getDefaultReadOnly() {
		return defaultReadOnly;
	}

	public void setDefaultReadOnly(Boolean defaultReadOnly) {
		this.defaultReadOnly = defaultReadOnly;
	}

	public Integer getDefaultTransactionIsolation() {
		return defaultTransactionIsolation;
	}

	public void setDefaultTransactionIsolation(Integer defaultTransactionIsolation) {
		this.defaultTransactionIsolation = defaultTransactionIsolation;
	}

	public Boolean getFairQueue() {
		return fairQueue;
	}

	public void setFairQueue(Boolean fairQueue) {
		this.fairQueue = fairQueue;
	}

	public Boolean getIgnoreExceptionOnPreLoad() {
		return ignoreExceptionOnPreLoad;
	}

	public void setIgnoreExceptionOnPreLoad(Boolean ignoreExceptionOnPreLoad) {
		this.ignoreExceptionOnPreLoad = ignoreExceptionOnPreLoad;
	}

	public Integer getInitialSize() {
		return initialSize;
	}

	public void setInitialSize(Integer initialSize) {
		this.initialSize = initialSize;
	}

	public String getInitSQL() {
		return initSQL;
	}

	public void setInitSQL(String initSQL) {
		this.initSQL = initSQL;
	}

	public Boolean getLogAbandoned() {
		return logAbandoned;
	}

	public void setLogAbandoned(Boolean logAbandoned) {
		this.logAbandoned = logAbandoned;
	}

	public Boolean getLogValidationErrors() {
		return logValidationErrors;
	}

	public void setLogValidationErrors(Boolean logValidationErrors) {
		this.logValidationErrors = logValidationErrors;
	}

	public Integer getMaxActive() {
		return maxActive;
	}

	public void setMaxActive(Integer maxActive) {
		this.maxActive = maxActive;
	}

	public Long getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(Long maxAge) {
		this.maxAge = maxAge;
	}

	public Integer getMaxIdle() {
		return maxIdle;
	}

	public void setMaxIdle(Integer maxIdle) {
		this.maxIdle = maxIdle;
	}

	public Integer getMaxWait() {
		return maxWait;
	}

	public void setMaxWait(Integer maxWait) {
		this.maxWait = maxWait;
	}

	public Integer getMinEvictableIdleTimeMillis() {
		return minEvictableIdleTimeMillis;
	}

	public void setMinEvictableIdleTimeMillis(Integer minEvictableIdleTimeMillis) {
		this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
	}

	public Integer getMinIdle() {
		return minIdle;
	}

	public void setMinIdle(Integer minIdle) {
		this.minIdle = minIdle;
	}

	public Integer getNumTestsPerEvictionRun() {
		return numTestsPerEvictionRun;
	}

	public void setNumTestsPerEvictionRun(Integer numTestsPerEvictionRun) {
		this.numTestsPerEvictionRun = numTestsPerEvictionRun;
	}

	public Boolean getPropagateInterruptState() {
		return propagateInterruptState;
	}

	public void setPropagateInterruptState(Boolean propagateInterruptState) {
		this.propagateInterruptState = propagateInterruptState;
	}

	public Boolean getRemoveAbandoned() {
		return removeAbandoned;
	}

	public void setRemoveAbandoned(Boolean removeAbandoned) {
		this.removeAbandoned = removeAbandoned;
	}

	public Integer getRemoveAbandonedTimeout() {
		return removeAbandonedTimeout;
	}

	public void setRemoveAbandonedTimeout(Integer removeAbandonedTimeout) {
		this.removeAbandonedTimeout = removeAbandonedTimeout;
	}

	public Boolean getRollbackOnReturn() {
		return rollbackOnReturn;
	}

	public void setRollbackOnReturn(Boolean rollbackOnReturn) {
		this.rollbackOnReturn = rollbackOnReturn;
	}

	public Integer getSuspectTimeout() {
		return suspectTimeout;
	}

	public void setSuspectTimeout(Integer suspectTimeout) {
		this.suspectTimeout = suspectTimeout;
	}

	public Boolean getTestOnBorrow() {
		return testOnBorrow;
	}

	public void setTestOnBorrow(Boolean testOnBorrow) {
		this.testOnBorrow = testOnBorrow;
	}

	public Boolean getTestOnConnect() {
		return testOnConnect;
	}

	public void setTestOnConnect(Boolean testOnConnect) {
		this.testOnConnect = testOnConnect;
	}

	public Boolean getTestOnReturn() {
		return testOnReturn;
	}

	public void setTestOnReturn(Boolean testOnReturn) {
		this.testOnReturn = testOnReturn;
	}

	public Boolean getTestWhileIdle() {
		return testWhileIdle;
	}

	public void setTestWhileIdle(Boolean testWhileIdle) {
		this.testWhileIdle = testWhileIdle;
	}

	public Integer getTimeBetweenEvictionRunsMillis() {
		return timeBetweenEvictionRunsMillis;
	}

	public void setTimeBetweenEvictionRunsMillis(Integer timeBetweenEvictionRunsMillis) {
		this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
	}

	public Boolean getUseDisposableConnectionFacade() {
		return useDisposableConnectionFacade;
	}

	public void setUseDisposableConnectionFacade(Boolean useDisposableConnectionFacade) {
		this.useDisposableConnectionFacade = useDisposableConnectionFacade;
	}

	public Boolean getUseEquals() {
		return useEquals;
	}

	public void setUseEquals(Boolean useEquals) {
		this.useEquals = useEquals;
	}

	public Boolean getUseLock() {
		return useLock;
	}

	public void setUseLock(Boolean useLock) {
		this.useLock = useLock;
	}

	public Boolean getUseStatementFacade() {
		return useStatementFacade;
	}

	public void setUseStatementFacade(Boolean useStatementFacade) {
		this.useStatementFacade = useStatementFacade;
	}

	public Long getValidationInterval() {
		return validationInterval;
	}

	public void setValidationInterval(Long validationInterval) {
		this.validationInterval = validationInterval;
	}

	public String getValidationQuery() {
		return validationQuery;
	}

	public void setValidationQuery(String validationQuery) {
		this.validationQuery = validationQuery;
	}

	public Integer getValidationQueryTimeout() {
		return validationQueryTimeout;
	}

	public void setValidationQueryTimeout(Integer validationQueryTimeout) {
		this.validationQueryTimeout = validationQueryTimeout;
	}

}