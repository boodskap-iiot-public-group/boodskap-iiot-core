package io.boodskap.iot.model.pojo;


import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IUser;

public class User extends AbstractPerson implements IUser {

	private static final long serialVersionUID = 7791702980774707258L;
	
	@PojoField private String userId;

	public User(){
	}

	public User(String domainKey, String userId){
		super(domainKey);
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
