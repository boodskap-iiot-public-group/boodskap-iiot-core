package io.boodskap.iot.model.pojo;

import java.util.Date;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IClusterLicense;

public class ClusterLicense extends AbstractDomainObject implements IClusterLicense {
	
	private static final long serialVersionUID = -1039738320066232303L;
	
	@PojoField private String licenseKey;
	@PojoField private String clusterId;
	@PojoField private String targetDomainKey;
	@PojoField private LicenseType licenseType;
	@PojoField private LicenseStatus status;
	@PojoField private int maxDomains;
	@PojoField private int maxMachines;
	@PojoField private int maxUsers;
	@PojoField private int maxCores;
	@PojoField private int maxMachineCores;
	@PojoField private int maxDevices;
	@PojoField private int maxMessagesPerMinute;
	@PojoField private int maxDeviceMessagesPerMinute;
	@PojoField private int maxOrganizations;
	@PojoField private Date validFrom;
	@PojoField private Date validTo;
	@PojoField private int gracePeriod;

	public ClusterLicense() {
	}

	public ClusterLicense(String domainKey, String targetDomainKey, String clusterId, String licenseKey) {
		super(domainKey);
		this.licenseKey = licenseKey;
		this.clusterId = clusterId;
		this.targetDomainKey = targetDomainKey;
	}

	@Override
	public String getLicenseKey() {
		return licenseKey;
	}

	@Override
	public void setLicenseKey(String licenseKey) {
		this.licenseKey = licenseKey;
	}

	@Override
	public String getClusterId() {
		return clusterId;
	}

	@Override
	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	@Override
	public String getTargetDomainKey() {
		return targetDomainKey;
	}

	@Override
	public void setTargetDomainKey(String targetDomainKey) {
		this.targetDomainKey = targetDomainKey;
	}

	@Override
	public LicenseType getLicenseType() {
		return licenseType;
	}

	@Override
	public void setLicenseType(LicenseType licenseType) {
		this.licenseType = licenseType;
	}

	@Override
	public LicenseStatus getStatus() {
		return status;
	}

	@Override
	public void setStatus(LicenseStatus status) {
		this.status = status;
	}

	@Override
	public int getMaxDomains() {
		return maxDomains;
	}

	@Override
	public void setMaxDomains(int maxDomains) {
		this.maxDomains = maxDomains;
	}

	@Override
	public int getMaxMachines() {
		return maxMachines;
	}

	@Override
	public void setMaxMachines(int maxMachines) {
		this.maxMachines = maxMachines;
	}

	@Override
	public int getMaxUsers() {
		return maxUsers;
	}

	@Override
	public void setMaxUsers(int maxUsers) {
		this.maxUsers = maxUsers;
	}

	@Override
	public int getMaxCores() {
		return maxCores;
	}

	@Override
	public void setMaxCores(int maxCores) {
		this.maxCores = maxCores;
	}

	@Override
	public int getMaxMachineCores() {
		return maxMachineCores;
	}

	@Override
	public void setMaxMachineCores(int maxMachineCores) {
		this.maxMachineCores = maxMachineCores;
	}

	@Override
	public int getMaxDevices() {
		return maxDevices;
	}

	@Override
	public void setMaxDevices(int maxDevices) {
		this.maxDevices = maxDevices;
	}

	@Override
	public int getMaxMessagesPerMinute() {
		return maxMessagesPerMinute;
	}

	@Override
	public void setMaxMessagesPerMinute(int maxMessagesPerMinute) {
		this.maxMessagesPerMinute = maxMessagesPerMinute;
	}

	@Override
	public int getMaxDeviceMessagesPerMinute() {
		return maxDeviceMessagesPerMinute;
	}

	@Override
	public void setMaxDeviceMessagesPerMinute(int maxDeviceMessagesPerMinute) {
		this.maxDeviceMessagesPerMinute = maxDeviceMessagesPerMinute;
	}

	@Override
	public int getMaxOrganizations() {
		return maxOrganizations;
	}

	@Override
	public void setMaxOrganizations(int maxOrganizations) {
		this.maxOrganizations = maxOrganizations;
	}

	@Override
	public Date getValidFrom() {
		return validFrom;
	}

	@Override
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	@Override
	public Date getValidTo() {
		return validTo;
	}

	@Override
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	@Override
	public int getGracePeriod() {
		return gracePeriod;
	}

	@Override
	public void setGracePeriod(int gracePeriod) {
		this.gracePeriod = gracePeriod;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((clusterId == null) ? 0 : clusterId.hashCode());
		result = prime * result + gracePeriod;
		result = prime * result + ((licenseKey == null) ? 0 : licenseKey.hashCode());
		result = prime * result + ((licenseType == null) ? 0 : licenseType.hashCode());
		result = prime * result + maxCores;
		result = prime * result + maxDeviceMessagesPerMinute;
		result = prime * result + maxDevices;
		result = prime * result + maxDomains;
		result = prime * result + maxMachineCores;
		result = prime * result + maxMachines;
		result = prime * result + maxMessagesPerMinute;
		result = prime * result + maxOrganizations;
		result = prime * result + maxUsers;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((targetDomainKey == null) ? 0 : targetDomainKey.hashCode());
		result = prime * result + ((validFrom == null) ? 0 : validFrom.hashCode());
		result = prime * result + ((validTo == null) ? 0 : validTo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClusterLicense other = (ClusterLicense) obj;
		if (clusterId == null) {
			if (other.clusterId != null)
				return false;
		} else if (!clusterId.equals(other.clusterId))
			return false;
		if (gracePeriod != other.gracePeriod)
			return false;
		if (licenseKey == null) {
			if (other.licenseKey != null)
				return false;
		} else if (!licenseKey.equals(other.licenseKey))
			return false;
		if (licenseType != other.licenseType)
			return false;
		if (maxCores != other.maxCores)
			return false;
		if (maxDeviceMessagesPerMinute != other.maxDeviceMessagesPerMinute)
			return false;
		if (maxDevices != other.maxDevices)
			return false;
		if (maxDomains != other.maxDomains)
			return false;
		if (maxMachineCores != other.maxMachineCores)
			return false;
		if (maxMachines != other.maxMachines)
			return false;
		if (maxMessagesPerMinute != other.maxMessagesPerMinute)
			return false;
		if (maxOrganizations != other.maxOrganizations)
			return false;
		if (maxUsers != other.maxUsers)
			return false;
		if (status != other.status)
			return false;
		if (targetDomainKey == null) {
			if (other.targetDomainKey != null)
				return false;
		} else if (!targetDomainKey.equals(other.targetDomainKey))
			return false;
		if (validFrom == null) {
			if (other.validFrom != null)
				return false;
		} else if (!validFrom.equals(other.validFrom))
			return false;
		if (validTo == null) {
			if (other.validTo != null)
				return false;
		} else if (!validTo.equals(other.validTo))
			return false;
		return true;
	}

}
