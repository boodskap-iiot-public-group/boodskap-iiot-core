package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.ISystemEntity;

public class SystemEntity extends AbstractModel implements ISystemEntity {
	
	private static final long serialVersionUID = 2882976847537967569L;

	@PojoField private String entityType;
	@PojoField private String entityId;

	public SystemEntity() {
	}
	
	public SystemEntity(String entityType, String entityId) {
		this.entityType = entityType;
		this.entityId = entityId;
	}
	
	public SystemEntity(String domainKey, String entityType, String entityId) {
		super(domainKey);
		this.entityType = entityType;
		this.entityId = entityId;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
		result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemEntity other = (SystemEntity) obj;
		if (entityId == null) {
			if (other.entityId != null)
				return false;
		} else if (!entityId.equals(other.entityId))
			return false;
		if (entityType == null) {
			if (other.entityType != null)
				return false;
		} else if (!entityType.equals(other.entityType))
			return false;
		return true;
	}

}
