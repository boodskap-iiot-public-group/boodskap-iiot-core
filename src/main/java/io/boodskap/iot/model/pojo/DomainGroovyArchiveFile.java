package io.boodskap.iot.model.pojo;

import io.boodskap.iot.model.IDomainGroovyArchiveFile;

public class DomainGroovyArchiveFile extends JarFile implements IDomainGroovyArchiveFile {
	
	private static final long serialVersionUID = 1261676269196370819L;
	
	private String domainKey;
	
	public DomainGroovyArchiveFile() {
	}
	public DomainGroovyArchiveFile(String domainKey, String loader, String fileName) {
		super(loader, fileName);
		this.domainKey=domainKey;
	}

	@Override
	public String getDomainKey() {
		return domainKey;
	}

	@Override
	public void setDomainKey(String domainKey) {
		this.domainKey = domainKey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((domainKey == null) ? 0 : domainKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainGroovyArchiveFile other = (DomainGroovyArchiveFile) obj;
		if (domainKey == null) {
			if (other.domainKey != null)
				return false;
		} else if (!domainKey.equals(other.domainKey))
			return false;
		return true;
	}
	
}
