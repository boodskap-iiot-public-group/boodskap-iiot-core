package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDomainJarClass;

public class DomainJarClass extends JarClass implements IDomainJarClass {
	
	private static final long serialVersionUID = -8576784722761205441L;
	
	@PojoField private String domainKey;
	
	public DomainJarClass() {
	}
	
	public DomainJarClass(String domainKey, String loader, String fileName, String pkg, String name) {
		super(loader, fileName, pkg, name);
		this.domainKey=domainKey;
	}

	@Override
	public final String getDomainKey() {
		return domainKey;
	}

	@Override
	public final void setDomainKey(String domainKey) {
		this.domainKey = domainKey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((domainKey == null) ? 0 : domainKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainJarClass other = (DomainJarClass) obj;
		if (domainKey == null) {
			if (other.domainKey != null)
				return false;
		} else if (!domainKey.equals(other.domainKey))
			return false;
		return true;
	}

}
