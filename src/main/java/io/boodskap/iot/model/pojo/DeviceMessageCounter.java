package io.boodskap.iot.model.pojo;

import java.util.Date;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDeviceMessageCounter;

public class DeviceMessageCounter extends AbstractDomainObject implements IDeviceMessageCounter {
	
	private static final long serialVersionUID = 3580631715130618227L;
	
	@PojoField private String deviceId;
	@PojoField private String messageType;
	@PojoField private Date day;
	@PojoField private long count;
	
	public DeviceMessageCounter() {
	}

	public DeviceMessageCounter(String deviceId, String messageType, Date day, long count) {
		this.deviceId = deviceId;
		this.messageType = messageType;
		this.day = day;
		this.count = count;
	}

	@Override
	public String getDeviceId() {
		return deviceId;
	}

	@Override
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public String getMessageType() {
		return messageType;
	}

	@Override
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	@Override
	public Date getDay() {
		return day;
	}

	@Override
	public void setDay(Date day) {
		this.day = day;
	}

	@Override
	public long getCount() {
		return count;
	}

	@Override
	public void setCount(long count) {
		this.count = count;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (count ^ (count >>> 32));
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
		result = prime * result + ((messageType == null) ? 0 : messageType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeviceMessageCounter other = (DeviceMessageCounter) obj;
		if (count != other.count)
			return false;
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
		if (deviceId == null) {
			if (other.deviceId != null)
				return false;
		} else if (!deviceId.equals(other.deviceId))
			return false;
		if (messageType == null) {
			if (other.messageType != null)
				return false;
		} else if (!messageType.equals(other.messageType))
			return false;
		return true;
	}

}
