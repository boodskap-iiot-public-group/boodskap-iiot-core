package io.boodskap.iot.model.pojo;

import io.boodskap.iot.AuthType;
import io.boodskap.iot.PojoField;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.model.IAccessToken;

public class AccessToken extends AbstractEntity implements IAccessToken{
	
	private static final long serialVersionUID = -581325217377215244L;
	
	@PojoField private String token;
	@PojoField private boolean external;
	@PojoField private AuthType authType;
	@PojoField private String domainKey;
	@PojoField private String orgId;
	@PojoField private String deviceId;
	@PojoField private String userId;
	@PojoField private long expireIn;
	
	public AccessToken() {
	}

	public AccessToken(String token) {
		this.token = token;
	}

	@Override
	public void save() throws StorageException {
		IAccessToken.dao().createOrUpdate(this);
	}

	@Override
	public void copy(Object other) {
		super.copy(other);
	}

	@Override
	public String getToken() {
		return token;
	}

	@Override
	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public boolean isExternal() {
		return external;
	}

	@Override
	public void setExternal(boolean external) {
		this.external = external;
	}

	@Override
	public AuthType getAuthType() {
		return authType;
	}

	@Override
	public void setAuthType(AuthType authType) {
		this.authType = authType;
	}

	@Override
	public String getDomainKey() {
		return domainKey;
	}

	@Override
	public void setDomainKey(String domainKey) {
		this.domainKey = domainKey;
	}

	@Override
	public String getOrgId() {
		return orgId;
	}

	@Override
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	@Override
	public String getUserId() {
		return userId;
	}

	@Override
	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String getDeviceId() {
		return deviceId;
	}

	@Override
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public long getExpireIn() {
		return expireIn;
	}

	@Override
	public void setExpireIn(long expireIn) {
		this.expireIn = expireIn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((authType == null) ? 0 : authType.hashCode());
		result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
		result = prime * result + ((domainKey == null) ? 0 : domainKey.hashCode());
		result = prime * result + (int) (expireIn ^ (expireIn >>> 32));
		result = prime * result + (external ? 1231 : 1237);
		result = prime * result + ((orgId == null) ? 0 : orgId.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccessToken other = (AccessToken) obj;
		if (authType != other.authType)
			return false;
		if (deviceId == null) {
			if (other.deviceId != null)
				return false;
		} else if (!deviceId.equals(other.deviceId))
			return false;
		if (domainKey == null) {
			if (other.domainKey != null)
				return false;
		} else if (!domainKey.equals(other.domainKey))
			return false;
		if (expireIn != other.expireIn)
			return false;
		if (external != other.external)
			return false;
		if (orgId == null) {
			if (other.orgId != null)
				return false;
		} else if (!orgId.equals(other.orgId))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
