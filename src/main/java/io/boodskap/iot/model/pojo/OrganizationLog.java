package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IOrganizationLog;

public class OrganizationLog extends DomainLog implements IOrganizationLog{

	private static final long serialVersionUID = 4124539401464474204L;
	
	@PojoField private String orgId;

	public OrganizationLog() {
	}

	public OrganizationLog(String domainKey, String orgId, String id) {
		super(domainKey, id);
		this.orgId = orgId;
	}

	@Override
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	@Override
	public String getOrgId() {
		return orgId;
	}

}
