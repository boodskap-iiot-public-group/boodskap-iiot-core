package io.boodskap.iot.model.pojo;

import io.boodskap.iot.ConnectionPoolType;
import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IC3P0Args;
import io.boodskap.iot.model.IConnectionPool;
import io.boodskap.iot.model.IDBCPArgs;
import io.boodskap.iot.model.IHikariArgs;
import io.boodskap.iot.model.ITomcatJDBCArgs;

public class ConnectionPool extends AbstractDomainObject implements IConnectionPool {

	private static final long serialVersionUID = -6701435275975083083L;

	@PojoField private ConnectionPoolType type = null;
	@PojoField private IC3P0Args c3p0Args = null;
	@PojoField private IDBCPArgs dbcpArgs = null;
	@PojoField private IHikariArgs hikariArgs = null;
	@PojoField private ITomcatJDBCArgs tomcatArgs = null;
	
	public ConnectionPool() {
	}

	public ConnectionPool(String domainKey, String name) {
		super(domainKey, name);
	}

	@Override
	public ConnectionPoolType getType() {
		return type;
	}

	@Override
	public void setType(ConnectionPoolType type) {
		this.type = type;
	}

	@Override
	public IC3P0Args getC3p0Args() {
		return c3p0Args;
	}

	@Override
	public void setC3p0Args(IC3P0Args c3p0Args) {
		this.c3p0Args = c3p0Args;
	}

	@Override
	public IDBCPArgs getDbcpArgs() {
		return dbcpArgs;
	}

	@Override
	public void setDbcpArgs(IDBCPArgs dbcpArgs) {
		this.dbcpArgs = dbcpArgs;
	}

	@Override
	public IHikariArgs getHikariArgs() {
		return hikariArgs;
	}

	@Override
	public void setHikariArgs(IHikariArgs hikariArgs) {
		this.hikariArgs = hikariArgs;
	}

	@Override
	public ITomcatJDBCArgs getTomcatArgs() {
		return tomcatArgs;
	}

	@Override
	public void setTomcatArgs(ITomcatJDBCArgs tomcatArgs) {
		this.tomcatArgs = tomcatArgs;
	}

}
