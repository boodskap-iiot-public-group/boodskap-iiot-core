package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IOrganizationUserEntity;

public class OrganizationUserEntity extends OrganizationEntity implements IOrganizationUserEntity{
	
	private static final long serialVersionUID = -855005903591254984L;
	
	@PojoField private String userId;

	public OrganizationUserEntity() {
	}

	public OrganizationUserEntity(String domainKey, String orgId, String userId, String entityType, String entityId) {
		super(domainKey, orgId, entityType, entityId);
		this.userId = userId;
	}

	@Override
	public String getUserId() {
		return userId;
	}

	@Override
	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganizationUserEntity other = (OrganizationUserEntity) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
