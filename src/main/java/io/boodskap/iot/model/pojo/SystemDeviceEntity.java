package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.ISystemDeviceEntity;

public class SystemDeviceEntity extends SystemEntity implements ISystemDeviceEntity {
	
	private static final long serialVersionUID = 2882976847537967569L;

	@PojoField private String deviceId;

	public SystemDeviceEntity() {
	}
	
	public SystemDeviceEntity(String deviceId, String entityType, String entityId) {
		super(entityType, entityId);
		this.deviceId = deviceId;
	}
	
	public SystemDeviceEntity(String domainKey, String deviceId, String entityType, String entityId) {
		super(domainKey, entityType, entityId);
		this.deviceId = deviceId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

}
