package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDomainAssetGroupMember;

public class DomainAssetGroupMember extends AbstractGroupMember implements IDomainAssetGroupMember {

	private static final long serialVersionUID = -4347644491268639376L;

	@PojoField private String assetId;

	public DomainAssetGroupMember() {
	}

	public DomainAssetGroupMember(String domainKey, String groupId) {
		super(domainKey, groupId);
	}

	public DomainAssetGroupMember(String domainKey, String groupId, String assetId) {
		super(domainKey, groupId);
		this.assetId = assetId;
	}

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((assetId == null) ? 0 : assetId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainAssetGroupMember other = (DomainAssetGroupMember) obj;
		if (assetId == null) {
			if (other.assetId != null)
				return false;
		} else if (!assetId.equals(other.assetId))
			return false;
		return true;
	}
	
}
