package io.boodskap.iot.model.pojo;

import java.util.Objects;

import io.boodskap.iot.DataType;
import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IField;

public abstract class AbstractField extends AbstractDomainObject implements IField {

	private static final long serialVersionUID = -7398949474866288452L;
	
	@PojoField private String specId;
	@PojoField private DataType dataType;
	
	public AbstractField() {
	}

	public AbstractField(String domainKey, String specId, String name) {
		super(domainKey, name);
		setSpecId(specId);
	}

	@Override
	public String getSpecId() {
		return specId;
	}

	@Override
	public void setSpecId(String specId) {
		this.specId = specId;
	}

	@Override
	public final DataType getDataType() {
		return dataType;
	}

	@Override
	public final void setDataType(DataType type) {
		this.dataType = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(dataType, specId);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractField other = (AbstractField) obj;
		return dataType == other.dataType && Objects.equals(specId, other.specId);
	}

}
