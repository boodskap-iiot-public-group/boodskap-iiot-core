package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IOrganizationAssetEntity;

public class OrganizationAssetEntity extends OrganizationEntity implements IOrganizationAssetEntity{
	
	private static final long serialVersionUID = -855005903591254984L;
	
	@PojoField private String assetId;

	public OrganizationAssetEntity() {
	}

	public OrganizationAssetEntity(String domainKey, String orgId, String assetId, String entityType, String entityId) {
		super(domainKey, orgId, entityType, entityId);
		this.assetId = assetId;
	}

	@Override
	public String getAssetId() {
		return assetId;
	}

	@Override
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((assetId == null) ? 0 : assetId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganizationAssetEntity other = (OrganizationAssetEntity) obj;
		if (assetId == null) {
			if (other.assetId != null)
				return false;
		} else if (!assetId.equals(other.assetId))
			return false;
		return true;
	}

}
