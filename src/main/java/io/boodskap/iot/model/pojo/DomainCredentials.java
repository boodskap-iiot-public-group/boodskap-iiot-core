package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDomainCredentials;

public class DomainCredentials extends AbstractDomainObject implements IDomainCredentials {

	private static final long serialVersionUID = 2170935178193756635L;
	
	@PojoField private byte[] privateKeyData;
	@PojoField private byte[] publicKeyData;
	@PojoField private byte[] passwordSalt;
	@PojoField private String keyPairAlgorithm;
	@PojoField private String passwordAlgorithm;
	@PojoField private int iterations;
	@PojoField private int passwordKeyLength;

	public DomainCredentials() {
	}

	public DomainCredentials(String domainKey) {
		super(domainKey);
	}

	@Override
	public String getKeyPairAlgorithm() {
		return keyPairAlgorithm;
	}

	@Override
	public void setKeyPairAlgorithm(String keyPairAlgorithm) {
		this.keyPairAlgorithm = keyPairAlgorithm;
	}

	@Override
	public byte[] getPrivateKeyData() {
		return privateKeyData;
	}

	@Override
	public void setPrivateKeyData(byte[] privateKeyData) {
		this.privateKeyData = privateKeyData;
	}

	@Override
	public byte[] getPublicKeyData() {
		return publicKeyData;
	}

	@Override
	public void setPublicKeyData(byte[] publicKeyData) {
		this.publicKeyData = publicKeyData;
	}

	@Override
	public String getPasswordAlgorithm() {
		return passwordAlgorithm;
	}

	@Override
	public void setPasswordAlgorithm(String passwordAlgorithm) {
		this.passwordAlgorithm = passwordAlgorithm;
	}

	@Override
	public byte[] getPasswordSalt() {
		return passwordSalt;
	}

	@Override
	public void setPasswordSalt(byte[] passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	@Override
	public int getIterations() {
		return iterations;
	}

	@Override
	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	@Override
	public int getPasswordKeyLength() {
		return passwordKeyLength;
	}

	@Override
	public void setPasswordKeyLength(int passwordKeyLength) {
		this.passwordKeyLength = passwordKeyLength;
	}

}
