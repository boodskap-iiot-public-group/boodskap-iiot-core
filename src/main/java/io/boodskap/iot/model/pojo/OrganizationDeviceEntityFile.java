package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IOrganizationDeviceEntityFile;

public class OrganizationDeviceEntityFile extends OrganizationEntityFile implements IOrganizationDeviceEntityFile {

	private static final long serialVersionUID = -6219447926450733949L;
	
	@PojoField private String deviceId;
	
	public OrganizationDeviceEntityFile() {
	}
	
	public OrganizationDeviceEntityFile(String domainKey, String orgId, String deviceId, String entityType, String entityId, String fileId) {
		super(domainKey, orgId, entityType, entityId, fileId);
		this.deviceId = deviceId;
	}

	@Override
	public String getDeviceId() {
		return deviceId;
	}

	@Override
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganizationDeviceEntityFile other = (OrganizationDeviceEntityFile) obj;
		if (deviceId == null) {
			if (other.deviceId != null)
				return false;
		} else if (!deviceId.equals(other.deviceId))
			return false;
		return true;
	}

}
