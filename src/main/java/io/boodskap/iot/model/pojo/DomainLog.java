package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.RawDataType;
import io.boodskap.iot.model.IDomainLog;

public class DomainLog extends SystemLog implements IDomainLog{

	private static final long serialVersionUID = -743322479781689700L;
	
	@PojoField private String domainKey;
	@PojoField private RawDataType type;

	public DomainLog() {
	}

	public DomainLog(String domainKey, String id) {
		super(id);
		this.domainKey = domainKey;
	}

	@Override
	public String getDomainKey() {
		return domainKey;
	}

	@Override
	public void setDomainKey(String domainKey) {
		this.domainKey = domainKey;
	}

	@Override
	public void setType(RawDataType type) {
		this.type = type;
	}

	@Override
	public RawDataType getType() {
		return type;
	}

}
