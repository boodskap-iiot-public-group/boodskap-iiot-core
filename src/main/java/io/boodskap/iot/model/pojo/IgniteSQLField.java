package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.SQLFieldType;
import io.boodskap.iot.model.IIgniteSQLField;

public class IgniteSQLField extends AbstractModel implements IIgniteSQLField {

	private static final long serialVersionUID = 645577000976931387L;
	
	@PojoField private SQLFieldType type;
	
	public IgniteSQLField() {
	}

	public IgniteSQLField(String name, SQLFieldType type) {
		super(name);
		this.type = type;
	}

	public SQLFieldType getType() {
		return type;
	}

	public void setType(SQLFieldType type) {
		this.type = type;
	}

}
