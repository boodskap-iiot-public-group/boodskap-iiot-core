package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDomainEntityFile;

public class DomainEntityFile extends AbstractFile implements IDomainEntityFile {

	private static final long serialVersionUID = -6219447926450733949L;
	
	@PojoField private String entityType;
	@PojoField private String entityId;
	
	public DomainEntityFile() {
	}
	
	public DomainEntityFile(String domainKey, String entityType, String entityId, String fileId) {
		super(domainKey, fileId);
		this.entityType = entityType;
		this.entityId = entityId;
	}

	@Override
	public String getEntityType() {
		return entityType;
	}

	@Override
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@Override
	public String getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
		result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainEntityFile other = (DomainEntityFile) obj;
		if (entityId == null) {
			if (other.entityId != null)
				return false;
		} else if (!entityId.equals(other.entityId))
			return false;
		if (entityType == null) {
			if (other.entityType != null)
				return false;
		} else if (!entityType.equals(other.entityType))
			return false;
		return true;
	}

}
