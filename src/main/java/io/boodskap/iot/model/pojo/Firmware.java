package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IFirmware;

public class Firmware extends AbstractDomainObject implements IFirmware{

	private static final long serialVersionUID = 7075552397693223013L;
	
	@PojoField private String deviceModel;
	@PojoField private String version;
	@PojoField private String fileName;
	@PojoField private byte[] data;
	@PojoField private String mediaType;
	
	public Firmware() {
	}
	public Firmware(String domainKey, String deviceModel, String version) {
		super(domainKey);
		this.deviceModel=deviceModel;
		this.version=version;
	}

	@Override
	public String getDeviceModel() {
		return deviceModel;
	}

	@Override
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String getFileName() {
		return fileName;
	}

	@Override
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public byte[] getData() {
		return data;
	}

	@Override
	public void setData(byte[] data) {
		this.data = data;
	}

	@Override
	public String getMediaType() {
		return mediaType;
	}

	@Override
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

}
