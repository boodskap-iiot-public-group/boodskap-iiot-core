package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IHikariArgs;

public class HikariArgs implements IHikariArgs{

	private static final long serialVersionUID = 7633812728389675821L;
	
	@PojoField private String jdbcUrl = null;
	@PojoField private String username = null;
	@PojoField private String password = null;
	@PojoField private String dataSourceClassName = null;
	@PojoField private String driverClassName = null;
	@PojoField private Boolean allowPoolSuspension = null;
	@PojoField private Boolean autoCommit = null;
	@PojoField private String catalog = null;
	@PojoField private String connectionInitSql = null;
	@PojoField private String connectionTestQuery = null;
	@PojoField private Long connectionTimeout = null;
	@PojoField private Long idleTimeout = null;
	@PojoField private Long initializationFailTimeout = null;
	@PojoField private Boolean isolateInternalQueries = null;
	@PojoField private Long leakDetectionThreshold = null;
	@PojoField private Integer maximumPoolSize = null;
	@PojoField private Long maxLifetime = null;
	@PojoField private Integer minimumIdle = null;
	@PojoField private Boolean readOnly = null;
	@PojoField private String schema = null;
	@PojoField private String transactionIsolation = null;
	@PojoField private Long validationTimeout = null;

	public HikariArgs() {
	}

	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDataSourceClassName() {
		return dataSourceClassName;
	}

	public void setDataSourceClassName(String dataSourceClassName) {
		this.dataSourceClassName = dataSourceClassName;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public Boolean getAllowPoolSuspension() {
		return allowPoolSuspension;
	}

	public void setAllowPoolSuspension(Boolean allowPoolSuspension) {
		this.allowPoolSuspension = allowPoolSuspension;
	}

	public Boolean getAutoCommit() {
		return autoCommit;
	}

	public void setAutoCommit(Boolean autoCommit) {
		this.autoCommit = autoCommit;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getConnectionInitSql() {
		return connectionInitSql;
	}

	public void setConnectionInitSql(String connectionInitSql) {
		this.connectionInitSql = connectionInitSql;
	}

	public String getConnectionTestQuery() {
		return connectionTestQuery;
	}

	public void setConnectionTestQuery(String connectionTestQuery) {
		this.connectionTestQuery = connectionTestQuery;
	}

	public Long getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(Long connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public Long getIdleTimeout() {
		return idleTimeout;
	}

	public void setIdleTimeout(Long idleTimeout) {
		this.idleTimeout = idleTimeout;
	}

	public Long getInitializationFailTimeout() {
		return initializationFailTimeout;
	}

	public void setInitializationFailTimeout(Long initializationFailTimeout) {
		this.initializationFailTimeout = initializationFailTimeout;
	}

	public Boolean getIsolateInternalQueries() {
		return isolateInternalQueries;
	}

	public void setIsolateInternalQueries(Boolean isolateInternalQueries) {
		this.isolateInternalQueries = isolateInternalQueries;
	}

	public Long getLeakDetectionThreshold() {
		return leakDetectionThreshold;
	}

	public void setLeakDetectionThreshold(Long leakDetectionThreshold) {
		this.leakDetectionThreshold = leakDetectionThreshold;
	}

	public Integer getMaximumPoolSize() {
		return maximumPoolSize;
	}

	public void setMaximumPoolSize(Integer maximumPoolSize) {
		this.maximumPoolSize = maximumPoolSize;
	}

	public Long getMaxLifetime() {
		return maxLifetime;
	}

	public void setMaxLifetime(Long maxLifetime) {
		this.maxLifetime = maxLifetime;
	}

	public Integer getMinimumIdle() {
		return minimumIdle;
	}

	public void setMinimumIdle(Integer minimumIdle) {
		this.minimumIdle = minimumIdle;
	}

	public Boolean getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getTransactionIsolation() {
		return transactionIsolation;
	}

	public void setTransactionIsolation(String transactionIsolation) {
		this.transactionIsolation = transactionIsolation;
	}

	public Long getValidationTimeout() {
		return validationTimeout;
	}

	public void setValidationTimeout(Long validationTimeout) {
		this.validationTimeout = validationTimeout;
	}

}
