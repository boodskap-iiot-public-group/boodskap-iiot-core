package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IOrganizationEntityFile;

public class OrganizationEntityFile extends DomainEntityFile implements IOrganizationEntityFile {

	private static final long serialVersionUID = -6219447926450733949L;
	
	@PojoField private String orgId;
	
	public OrganizationEntityFile() {
	}
	
	public OrganizationEntityFile(String domainKey, String orgId, String entityType, String entityId, String fileId) {
		super(domainKey, entityType, entityId, fileId);
		this.orgId = orgId;
	}

	@Override
	public String getOrgId() {
		return orgId;
	}

	@Override
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((orgId == null) ? 0 : orgId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganizationEntityFile other = (OrganizationEntityFile) obj;
		if (orgId == null) {
			if (other.orgId != null)
				return false;
		} else if (!orgId.equals(other.orgId))
			return false;
		return true;
	}

}
