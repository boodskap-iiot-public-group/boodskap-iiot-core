package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDomainUserEntityFile;

public class DomainUserEntityFile extends DomainEntityFile implements IDomainUserEntityFile {

	private static final long serialVersionUID = -6219447926450733949L;
	
	@PojoField private String userId;
	
	public DomainUserEntityFile() {
	}
	
	public DomainUserEntityFile(String domainKey, String userId, String entityType, String entityId, String fileId) {
		super(domainKey, entityType, entityId, fileId);
		this.userId = userId;
	}

	@Override
	public String getUserId() {
		return userId;
	}

	@Override
	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainUserEntityFile other = (DomainUserEntityFile) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
