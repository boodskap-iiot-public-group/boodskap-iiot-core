package io.boodskap.iot.model.pojo;

import io.boodskap.iot.model.IGroovyArchiveFile;

public class GroovyArchiveFile extends JarFile implements IGroovyArchiveFile {
	
	private static final long serialVersionUID = 6701879076746071273L;

	public GroovyArchiveFile() {
	}

	public GroovyArchiveFile(String loader, String fileName) {
		super(loader, fileName);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
