package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IJarClass;

public class JarClass extends JavaClass implements IJarClass {

	private static final long serialVersionUID = 7481708273252679293L;
	
	@PojoField private String fileName;
	
	public JarClass() {
	}

	public JarClass(String loader, String pkg, String name) {
		super(loader, pkg, name);
	}
	
	public JarClass(String loader, String fileName, String pkg, String name) {
		super(loader, pkg, name);
		this.fileName=fileName;
	}
	@Override
	public final String getFileName() {
		return fileName;
	}

	@Override
	public final void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		JarClass other = (JarClass) obj;
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		} else if (!fileName.equals(other.fileName))
			return false;
		return true;
	}

}
