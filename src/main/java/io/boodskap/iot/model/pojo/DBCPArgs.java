package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDBCPArgs;

public class DBCPArgs implements IDBCPArgs {

	private static final long serialVersionUID = -8006178772336250745L;
	
	@PojoField private String url = null;
	@PojoField private String username = null;
	@PojoField private String password = null;
	@PojoField private String driverClassName = null;
	@PojoField private Boolean abandonedUsageTracking = null;
	@PojoField private Boolean accessToUnderlyingConnectionAllowed = null;
	@PojoField private Boolean cacheState = null;
	@PojoField private Boolean defaultAutoCommit = null;
	@PojoField private String defaultCatalog = null;
	@PojoField private Integer defaultQueryTimeout = null;
	@PojoField private Boolean defaultReadOnly = null;
	@PojoField private String defaultSchema = null;
	@PojoField private Integer defaultTransactionIsolation = null;
	@PojoField private Boolean enableAutoCommitOnReturn = null;
	@PojoField private Boolean fastFailValidation = null;
	@PojoField private Integer initialSize = null;
	@PojoField private Boolean lifo = null;
	@PojoField private Boolean logAbandoned = null;
	@PojoField private Boolean logExpiredConnections = null;
	@PojoField private Integer loginTimeout = null;
	@PojoField private Long maxConnLifetimeMillis = null;
	@PojoField private Integer maxIdle = null;
	@PojoField private Integer maxOpenPreparedStatements = null;
	@PojoField private Integer maxTotal = null;
	@PojoField private Long maxWaitMillis = null;
	@PojoField private Long minEvictableIdleTimeMillis = null;
	@PojoField private Integer minIdle = null;
	@PojoField private Integer numTestsPerEvictionRun = null;
	@PojoField private Boolean poolPreparedStatements = null;
	@PojoField private Boolean removeAbandonedOnBorrow = null;
	@PojoField private Boolean removeAbandonedOnMaintenance = null;
	@PojoField private Boolean rollbackOnReturn = null;
	@PojoField private Long softMinEvictableIdleTimeMillis = null;
	@PojoField private Boolean testOnBorrow = null;
	@PojoField private Boolean testOnCreate = null;
	@PojoField private Boolean testOnReturn = null;
	@PojoField private Boolean testWhileIdle = null;
	@PojoField private Long timeBetweenEvictionRunsMillis = null;
	@PojoField private String validationQuery = null;
	@PojoField private Integer validationQueryTimeout = null;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public Boolean getAbandonedUsageTracking() {
		return abandonedUsageTracking;
	}

	public void setAbandonedUsageTracking(Boolean abandonedUsageTracking) {
		this.abandonedUsageTracking = abandonedUsageTracking;
	}

	public Boolean getAccessToUnderlyingConnectionAllowed() {
		return accessToUnderlyingConnectionAllowed;
	}

	public void setAccessToUnderlyingConnectionAllowed(Boolean accessToUnderlyingConnectionAllowed) {
		this.accessToUnderlyingConnectionAllowed = accessToUnderlyingConnectionAllowed;
	}

	public Boolean getCacheState() {
		return cacheState;
	}

	public void setCacheState(Boolean cacheState) {
		this.cacheState = cacheState;
	}

	public Boolean getDefaultAutoCommit() {
		return defaultAutoCommit;
	}

	public void setDefaultAutoCommit(Boolean defaultAutoCommit) {
		this.defaultAutoCommit = defaultAutoCommit;
	}

	public String getDefaultCatalog() {
		return defaultCatalog;
	}

	public void setDefaultCatalog(String defaultCatalog) {
		this.defaultCatalog = defaultCatalog;
	}

	public Integer getDefaultQueryTimeout() {
		return defaultQueryTimeout;
	}

	public void setDefaultQueryTimeout(Integer defaultQueryTimeout) {
		this.defaultQueryTimeout = defaultQueryTimeout;
	}

	public Boolean getDefaultReadOnly() {
		return defaultReadOnly;
	}

	public void setDefaultReadOnly(Boolean defaultReadOnly) {
		this.defaultReadOnly = defaultReadOnly;
	}

	public String getDefaultSchema() {
		return defaultSchema;
	}

	public void setDefaultSchema(String defaultSchema) {
		this.defaultSchema = defaultSchema;
	}

	public Integer getDefaultTransactionIsolation() {
		return defaultTransactionIsolation;
	}

	public void setDefaultTransactionIsolation(Integer defaultTransactionIsolation) {
		this.defaultTransactionIsolation = defaultTransactionIsolation;
	}

	public Boolean getEnableAutoCommitOnReturn() {
		return enableAutoCommitOnReturn;
	}

	public void setEnableAutoCommitOnReturn(Boolean enableAutoCommitOnReturn) {
		this.enableAutoCommitOnReturn = enableAutoCommitOnReturn;
	}

	public Boolean getFastFailValidation() {
		return fastFailValidation;
	}

	public void setFastFailValidation(Boolean fastFailValidation) {
		this.fastFailValidation = fastFailValidation;
	}

	public Integer getInitialSize() {
		return initialSize;
	}

	public void setInitialSize(Integer initialSize) {
		this.initialSize = initialSize;
	}

	public Boolean getLifo() {
		return lifo;
	}

	public void setLifo(Boolean lifo) {
		this.lifo = lifo;
	}

	public Boolean getLogAbandoned() {
		return logAbandoned;
	}

	public void setLogAbandoned(Boolean logAbandoned) {
		this.logAbandoned = logAbandoned;
	}

	public Boolean getLogExpiredConnections() {
		return logExpiredConnections;
	}

	public void setLogExpiredConnections(Boolean logExpiredConnections) {
		this.logExpiredConnections = logExpiredConnections;
	}

	public Integer getLoginTimeout() {
		return loginTimeout;
	}

	public void setLoginTimeout(Integer loginTimeout) {
		this.loginTimeout = loginTimeout;
	}

	public Long getMaxConnLifetimeMillis() {
		return maxConnLifetimeMillis;
	}

	public void setMaxConnLifetimeMillis(Long maxConnLifetimeMillis) {
		this.maxConnLifetimeMillis = maxConnLifetimeMillis;
	}

	public Integer getMaxIdle() {
		return maxIdle;
	}

	public void setMaxIdle(Integer maxIdle) {
		this.maxIdle = maxIdle;
	}

	public Integer getMaxOpenPreparedStatements() {
		return maxOpenPreparedStatements;
	}

	public void setMaxOpenPreparedStatements(Integer maxOpenPreparedStatements) {
		this.maxOpenPreparedStatements = maxOpenPreparedStatements;
	}

	public Integer getMaxTotal() {
		return maxTotal;
	}

	public void setMaxTotal(Integer maxTotal) {
		this.maxTotal = maxTotal;
	}

	public Long getMaxWaitMillis() {
		return maxWaitMillis;
	}

	public void setMaxWaitMillis(Long maxWaitMillis) {
		this.maxWaitMillis = maxWaitMillis;
	}

	public Long getMinEvictableIdleTimeMillis() {
		return minEvictableIdleTimeMillis;
	}

	public void setMinEvictableIdleTimeMillis(Long minEvictableIdleTimeMillis) {
		this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
	}

	public Integer getMinIdle() {
		return minIdle;
	}

	public void setMinIdle(Integer minIdle) {
		this.minIdle = minIdle;
	}

	public Integer getNumTestsPerEvictionRun() {
		return numTestsPerEvictionRun;
	}

	public void setNumTestsPerEvictionRun(Integer numTestsPerEvictionRun) {
		this.numTestsPerEvictionRun = numTestsPerEvictionRun;
	}

	public Boolean getPoolPreparedStatements() {
		return poolPreparedStatements;
	}

	public void setPoolPreparedStatements(Boolean poolPreparedStatements) {
		this.poolPreparedStatements = poolPreparedStatements;
	}

	public Boolean getRemoveAbandonedOnBorrow() {
		return removeAbandonedOnBorrow;
	}

	public void setRemoveAbandonedOnBorrow(Boolean removeAbandonedOnBorrow) {
		this.removeAbandonedOnBorrow = removeAbandonedOnBorrow;
	}

	public Boolean getRemoveAbandonedOnMaintenance() {
		return removeAbandonedOnMaintenance;
	}

	public void setRemoveAbandonedOnMaintenance(Boolean removeAbandonedOnMaintenance) {
		this.removeAbandonedOnMaintenance = removeAbandonedOnMaintenance;
	}

	public Boolean getRollbackOnReturn() {
		return rollbackOnReturn;
	}

	public void setRollbackOnReturn(Boolean rollbackOnReturn) {
		this.rollbackOnReturn = rollbackOnReturn;
	}

	public Long getSoftMinEvictableIdleTimeMillis() {
		return softMinEvictableIdleTimeMillis;
	}

	public void setSoftMinEvictableIdleTimeMillis(Long softMinEvictableIdleTimeMillis) {
		this.softMinEvictableIdleTimeMillis = softMinEvictableIdleTimeMillis;
	}

	public Boolean getTestOnBorrow() {
		return testOnBorrow;
	}

	public void setTestOnBorrow(Boolean testOnBorrow) {
		this.testOnBorrow = testOnBorrow;
	}

	public Boolean getTestOnCreate() {
		return testOnCreate;
	}

	public void setTestOnCreate(Boolean testOnCreate) {
		this.testOnCreate = testOnCreate;
	}

	public Boolean getTestOnReturn() {
		return testOnReturn;
	}

	public void setTestOnReturn(Boolean testOnReturn) {
		this.testOnReturn = testOnReturn;
	}

	public Boolean getTestWhileIdle() {
		return testWhileIdle;
	}

	public void setTestWhileIdle(Boolean testWhileIdle) {
		this.testWhileIdle = testWhileIdle;
	}

	public Long getTimeBetweenEvictionRunsMillis() {
		return timeBetweenEvictionRunsMillis;
	}

	public void setTimeBetweenEvictionRunsMillis(Long timeBetweenEvictionRunsMillis) {
		this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
	}

	public String getValidationQuery() {
		return validationQuery;
	}

	public void setValidationQuery(String validationQuery) {
		this.validationQuery = validationQuery;
	}

	public Integer getValidationQueryTimeout() {
		return validationQueryTimeout;
	}

	public void setValidationQueryTimeout(Integer validationQueryTimeout) {
		this.validationQueryTimeout = validationQueryTimeout;
	}

}
