package io.boodskap.iot.model.pojo;

import java.util.Date;
import java.util.UUID;

import io.boodskap.iot.DataChannel;
import io.boodskap.iot.PojoField;
import io.boodskap.iot.RawDataType;
import io.boodskap.iot.model.IRawData;

public class RawData implements IRawData {

	private static final long serialVersionUID = 8770058772346142631L;
	
	@PojoField private String id = UUID.randomUUID().toString();	
	@PojoField private String domainKey;
	@PojoField private String deviceId;
	@PojoField private String nodeId;
	@PojoField private String nodeUid;
	@PojoField private String mqttTopic;
	@PojoField private String mqttServerId;
	@PojoField private String deviceModel;
	@PojoField private String firmwareVersion;
	@PojoField private String fileName;
	@PojoField private String contentType;
	@PojoField private String ipAddress;
	@PojoField private Integer port;
	@PojoField private int size;
	@PojoField private Date registeredStamp;
	@PojoField private Date updatedStamp;
	@PojoField private DataChannel channel;
	@PojoField private RawDataType rawDataType;
	@PojoField private State state = State.QUEUED;
	@PojoField private byte[] data;
	@PojoField private String extraProperties;
	@PojoField private String ruleId;
	@PojoField private String orgId;
	@PojoField private String trace;

	public RawData() {
	}

	public RawData(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public Date getRegisteredStamp() {
		return registeredStamp;
	}

	@Override
	public void setRegisteredStamp(Date registeredStamp) {
		this.registeredStamp = registeredStamp;
	}

	@Override
	public String getNodeId() {
		return nodeId;
	}

	@Override
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	@Override
	public String getNodeUid() {
		return nodeUid;
	}

	@Override
	public String getMqttServerId() {
		return mqttServerId;
	}

	@Override
	public void setMqttServerId(String mqttServerId) {
		this.mqttServerId = mqttServerId;
	}

	@Override
	public void setNodeUid(String nodeUid) {
		this.nodeUid = nodeUid;
	}

	@Override
	public String getIpAddress() {
		return ipAddress;
	}

	@Override
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Override
	public String getFileName() {
		return fileName;
	}

	@Override
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public byte[] getData() {
		return data;
	}

	@Override
	public void setData(byte[] data) {
		this.data = data;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public DataChannel getChannel() {
		return channel;
	}

	@Override
	public void setChannel(DataChannel channel) {
		this.channel = channel;
	}

	@Override
	public Integer getPort() {
		return port;
	}

	@Override
	public void setPort(Integer port) {
		this.port = port;
	}

	@Override
	public String getMqttTopic() {
		return mqttTopic;
	}

	@Override
	public void setMqttTopic(String mqttTopic) {
		this.mqttTopic = mqttTopic;
	}

	@Override
	public String getDomainKey() {
		return domainKey;
	}

	@Override
	public void setDomainKey(String domainKey) {
		this.domainKey = domainKey;
	}

	@Override
	public String getDeviceId() {
		return deviceId;
	}

	@Override
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public String getDeviceModel() {
		return deviceModel;
	}

	@Override
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	@Override
	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	@Override
	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	@Override
	public RawDataType getRawDataType() {
		return rawDataType;
	}

	@Override
	public void setRawDataType(RawDataType rawDataType) {
		this.rawDataType = rawDataType;
	}

	@Override
	public String getContentType() {
		return contentType;
	}

	@Override
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Override
	public State getState() {
		return state;
	}

	@Override
	public void setState(State state) {
		this.state = state;
	}

	@Override
	public Date getUpdatedStamp() {
		return updatedStamp;
	}

	@Override
	public void setUpdatedStamp(Date updatedStamp) {
		this.updatedStamp = updatedStamp;
	}

	@Override
	public String getExtraProperties() {
		return extraProperties;
	}

	@Override
	public void setExtraProperties(String extraProperties) {
		this.extraProperties = extraProperties;
	}

	@Override
	public String getRuleId() {
		return ruleId;
	}

	@Override
	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	@Override
	public String getOrgId() {
		return orgId;
	}

	@Override
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	@Override
	public String getTrace() {
		return trace;
	}

	@Override
	public void setTrace(String trace) {
		this.trace = trace;
	}

}
