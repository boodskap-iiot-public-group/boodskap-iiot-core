package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IGroovyClass;

public class GroovyClass extends JarClass implements IGroovyClass {

	private static final long serialVersionUID = 7481708273252679293L;
	
	@PojoField private String code;
	
	public GroovyClass() {
	}
	
	public GroovyClass(String loader, String pkg, String name) {
		super(loader, pkg, name);
	}

	@Override
	public final String getCode() {
		return code;
	}

	@Override
	public final void setCode(String code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroovyClass other = (GroovyClass) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

}
