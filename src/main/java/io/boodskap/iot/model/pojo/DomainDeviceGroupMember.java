package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IDomainDeviceGroupMember;

public class DomainDeviceGroupMember extends AbstractGroupMember implements IDomainDeviceGroupMember {

	private static final long serialVersionUID = -2319679190852676896L;
	
	@PojoField private String deviceId;
	
	public DomainDeviceGroupMember(){
	}
	
	public DomainDeviceGroupMember(String domainKey, String groupId, String deviceId){
		super(domainKey,groupId);
		this.deviceId=deviceId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainDeviceGroupMember other = (DomainDeviceGroupMember) obj;
		if (deviceId == null) {
			if (other.deviceId != null)
				return false;
		} else if (!deviceId.equals(other.deviceId))
			return false;
		return true;
	}
	
}
