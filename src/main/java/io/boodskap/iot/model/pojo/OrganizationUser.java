package io.boodskap.iot.model.pojo;

import io.boodskap.iot.PojoField;
import io.boodskap.iot.model.IOrganizationUser;

public class OrganizationUser extends User implements IOrganizationUser {
	
	private static final long serialVersionUID = -2018718809063224007L;
	
	@PojoField private String orgId;
	
	public OrganizationUser() {
	}

	public OrganizationUser(String domainKey, String orgId, String userId) {
		super(domainKey,userId);
		this.orgId = orgId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

}
