package io.boodskap.iot.model;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=IDomainWorkFlow.class)
public interface IDomainWorkFlow extends IDomainObject {

	//======================================
	// Default Methods
	//======================================
	
	public default boolean getCloneObjects() {
		return isCloneObjects();
	}
	
	public default boolean getTerminateOnError() {
		return isTerminateOnError();
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getId();
	
	public void setId(String id);
	
	public String getGroup();
	
	public void setGroup(String group);
	
	public String getTags();
	
	public void setTags(String tags);
	
	public boolean isCloneObjects();
	
	public void setCloneObjects(boolean cloneObjects);
	
	public boolean isTerminateOnError();
	
	public void setTerminateOnError(boolean terminateOnError);
	
	public List<? extends IWorkFlowProcess> getProcesses();
	
	public void setProcesses(List<? extends IWorkFlowProcess> processes);
	
	public List<? extends IWorkFlowTrigger> getTriggers();
	
	public void setTriggers(List<? extends IWorkFlowTrigger> triggers);
	
	public List<? extends IWorkFlowTriggerGroup> getTriggerGroups();
	
	public void setTriggerGroups(List<? extends IWorkFlowTriggerGroup> triggerGroups);
	
	public List<String> getMetaFields();
	
	public void setMetaFields(List<String> metaFields);
}
