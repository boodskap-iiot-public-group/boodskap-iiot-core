/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * static publichis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DeviceModelDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDeviceModel.class)
public interface IDeviceModel extends IDomainObject {

	//======================================
	// DAO Methods
	//======================================
	
	public static DeviceModelDAO<IDeviceModel> dao(){
		return DeviceModelDAO.get();
	}
	
	static public void createOrUpdate(IDeviceModel e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDeviceModel> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDeviceModel> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDeviceModel> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IDeviceModel> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDeviceModel> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IDeviceModel create(String domainKey, String modelId, String version) throws StorageException{
		return dao().create(domainKey, modelId, version);
	}

	static public IDeviceModel get(String domainKey, String modelId) throws StorageException{
		return dao().get(domainKey, modelId);
	}

	static public IDeviceModel get(String domainKey, String modelId, String version) throws StorageException{
		return dao().get(domainKey, modelId, version);
	}

	static public void delete(String domainKey, String modelId, String version) throws StorageException{
		dao().delete(domainKey, modelId, version);
	}

	static public void delete(String domainKey, String modelId) throws StorageException{
		dao().delete(domainKey, modelId);
	}

	static public long count(String domainKey, String modelId) throws StorageException{
		return dao().count(domainKey, modelId);
	}

	static public Collection<IDeviceModel> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}

	static public Collection<IDeviceModel> listNext(String domainKey, String modelId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, modelId, page, pageSize);
	}

	static public Collection<IDeviceModel> list(String domainKey, String modelId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, modelId, page, pageSize);
	}

	static public Collection<IDeviceModel> listNext(String domainKey, String modelId, String version, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, modelId, version, page, pageSize);
	}



	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDeviceModel.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IDeviceModel o = (IDeviceModel) other;
		
		setModelId(o.getModelId());
		setVersion(o.getVersion());
		
		IDomainObject.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getModelId();

	public void setModelId(String modelId);

	public String getVersion();

	public void setVersion(String version);

}
