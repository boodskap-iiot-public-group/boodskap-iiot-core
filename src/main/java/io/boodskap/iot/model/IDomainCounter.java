/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainCounterDAO;

@JsonSerialize(as=IDomainCounter.class)
public interface IDomainCounter extends IDomainObject {
	
	//======================================
	// DAO Methods
	//======================================

	public static DomainCounterDAO<IDomainCounter> dao(){
		return DomainCounterDAO.get();
	}
	
	static public IDomainCounter create(String domainKey){
		return dao().create(domainKey);
	}

	static public Class<? extends IDomainCounter> clazz(){
		return dao().clazz();
	}
	
	static public void init() throws StorageException{
		dao().init();
	}

	static public IDomainCounter find(String domainKey) throws StorageException{
		return dao().find(domainKey);
	}

	static public void incrementUsers(String domainKey) throws StorageException{
		dao().incrementUsers(domainKey);
	}

	static public void incrementDevices(String domainKey) throws StorageException{
		dao().incrementDevices(domainKey);
	}

	static public void incrementUdp(String domainKey) throws StorageException{
		dao().incrementUdp(domainKey);
	}

	static public void incrementLoRa(String domainKey) throws StorageException{
		dao().incrementLoRa(domainKey);
	}

	static public void incrementMqtt(String domainKey) throws StorageException{
		dao().incrementMqtt(domainKey);
	}

	static public void incrementHttp(String domainKey) throws StorageException{
		dao().incrementHttp(domainKey);
	}

	static public void incrementFcm(String domainKey) throws StorageException{
		dao().incrementFcm(domainKey);
	}

	static public void incrementCoAP(String domainKey) throws StorageException{
		dao().incrementCoAP(domainKey);
	}

	static public void incrementTcp(String domainKey) throws StorageException{
		dao().incrementTcp(domainKey);
	}

	static public void incrementCommands(String domainKey) throws StorageException{
		dao().incrementCommands(domainKey);
	}

	static public void incrementRecords(String domainKey) throws StorageException{
		dao().incrementRecords(domainKey);
	}
	
//	=============================
//			Default Methods
//	=============================
	
	@Override
	public default void save() {
		dao().init();
	}

	@Override
	public default void copy(Object other) {
		
		IDomainCounter o = (IDomainCounter) other;
		
		setUsers(o.getUsers());
		setDevices(o.getDevices());
		setUdpMessages(o.getUdpMessages());
		setMqttMessages(o.getMqttMessages());
		setHttpMessages(o.getHttpMessages());
		setFcmMessages(o.getFcmMessages());
		setCommands(o.getCommands());
		setCoapMessages(o.getCoapMessages());
		setTcpMessages(o.getTcpMessages());
		setLoraMessages(o.getLoraMessages());
		setRecords(o.getRecords());
		
		IDomainObject.super.copy(other);
	}

	public long getUsers();

	public void setUsers(long users);

	public long getDevices();

	public void setDevices(long devices);

	public long getUdpMessages();

	public void setUdpMessages(long udpMessages);

	public long getMqttMessages();

	public void setMqttMessages(long mqttMessages);

	public long getHttpMessages();

	public void setHttpMessages(long httpMessages);

	public long getFcmMessages();

	public void setFcmMessages(long fcmMessages);

	public long getCommands();

	public void setCommands(long commands);

	public long getCoapMessages();

	public void setCoapMessages(long coapMessages);

	public long getTcpMessages();

	public void setTcpMessages(long tcpMessages);

	public long getLoraMessages();

	public void setLoraMessages(long loraMessages);
	
	public long getRecords();

	public void setRecords(long records);

}
