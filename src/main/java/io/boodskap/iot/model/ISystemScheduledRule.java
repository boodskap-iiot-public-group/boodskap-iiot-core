/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IScheduledRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IScheduledRulehis program is distributed in the hope that it will be useful,
 * but WIIScheduledRuleHOUIScheduledRule ANY WARRANIScheduledRuleY; without even the implied warranty of
 * MERCHANIScheduledRuleABILIIScheduledRuleY or FIIScheduledRuleNESS FOR A PARIScheduledRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.SystemScheduledRuleDAO;

@JsonSerialize(as=ISystemScheduledRule.class)
public interface ISystemScheduledRule extends ISystemRule {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static SystemScheduledRuleDAO<ISystemScheduledRule> dao() {
		return SystemScheduledRuleDAO.get();
	}
	
	public static Class<? extends ISystemScheduledRule> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<ISystemScheduledRule> load() throws StorageException{
		return dao().load();
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static ISystemScheduledRule create(String ruleId) throws StorageException{
		return dao().create(ruleId);
	}

	public static ISystemScheduledRule get(String ruleId) throws StorageException{
		return dao().get(ruleId);
	}

	public static void delete(String ruleId) throws StorageException{
		dao().delete(ruleId);
	}

	public static Collection<ISystemScheduledRule> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	public static Collection<ISystemScheduledRule> listNext(boolean load, String ruleId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, ruleId, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ISystemScheduledRule.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getRuleId();

	public void setRuleId(String ruleId);

	public String getPattern();

	public void setPattern(String pattern);

}
