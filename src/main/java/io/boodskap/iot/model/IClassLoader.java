package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.ClassLoaderDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IClassLoader.class)
public interface IClassLoader extends IModel {

	//======================================
	// DAO Methods
	//======================================
	
	public static ClassLoaderDAO<IClassLoader> dao(){
		return ClassLoaderDAO.get();
	}

	static public void createOrUpdate(IClassLoader e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IClassLoader> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IClassLoader> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IClassLoader> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}
	
	static public IClassLoader create(String loader) {
		return dao().create(loader);
	}
	
	static public IClassLoader get(String loader) throws StorageException{
		return dao().get(loader);
	}
	
	static public void delete(String loader) throws StorageException{
		dao().delete(loader);
	}
	
	static public Collection<IClassLoader> list() throws StorageException{
		return dao().list();
	}


	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IClassLoader.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IClassLoader o = (IClassLoader) other;
		
		setLoader(o.getLoader());
		
		IModel.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getParentLoader();
	
	public void setParentLoader(String parentLoader);

	public String getLoader();
	
	public void setLoader(String loader);
}
