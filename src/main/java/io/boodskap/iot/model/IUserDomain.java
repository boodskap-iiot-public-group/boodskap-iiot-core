/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IUserDomainhis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IUserDomainhis program is distributed in the hope that it will be useful,
 * but WIIUserDomainHOUIUserDomain ANY WARRANIUserDomainY; without even the implied warranty of
 * MERCHANIUserDomainABILIIUserDomainY or FIIUserDomainNESS FOR A PARIUserDomainICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.UserDomainDAO;

@JsonSerialize(as=IUserDomain.class)
public interface IUserDomain extends IDomainObject {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static UserDomainDAO<IUserDomain> dao(){
		return UserDomainDAO.get();
	}
	
	public static Class<? extends IUserDomain> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<IUserDomain> load() throws StorageException{
		return dao().load();
	}
	
	public static EntityIterator<IUserDomain> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}

	public static IUserDomain create(String userId, String domainKey) throws StorageException{
		return dao().create(userId, domainKey);
	}

	public static IUserDomain get(String userId, String domainKey) throws StorageException{
		return dao().get(userId, domainKey);
	}

	public static long countDomains(String userId) throws StorageException{
		return dao().countDomains(userId);
	}

	public static void deleteDomains(String userId) throws StorageException{
		dao().deleteDomains(userId);
	}

	public static void delete(String userId, String domainKey) throws StorageException{
		dao().delete(userId, domainKey);
	}

	public static Collection<IUserDomain> list(String userId) throws StorageException{
		return dao().list(userId);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IUserDomain.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getUserId();
	
	public void setUserId(String userId);

}
