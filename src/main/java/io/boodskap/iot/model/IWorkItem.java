package io.boodskap.iot.model;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.WorkItemType;

@JsonSerialize(as=IWorkItem.class)
public interface IWorkItem extends Serializable {

	//======================================
	// Attributes
	//======================================
	
	public String getId();
	
	public void setId(String id);
	
	public WorkItemType getType();
	
	public void setType(WorkItemType type);
	
	public String getGroup();
	
	public void setGroup(String group);
	
}
