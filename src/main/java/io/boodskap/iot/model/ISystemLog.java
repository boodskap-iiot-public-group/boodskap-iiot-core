package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Date;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.BoodskapSystem;
import io.boodskap.iot.LogSeverity;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.SystemLogDAO;

@JsonSerialize(as=ISystemLog.class)
public interface ISystemLog extends IStorageObject {

	//======================================
	// DAO Methods
	//======================================
	
	public static SystemLogDAO<ISystemLog> dao() {
		return BoodskapSystem.storage().getSystemLogDAO();
	}
	
	public static Class<? extends ISystemLog> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<ISystemLog> load() throws StorageException{
		return dao().load();
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static SearchResult<ISystemLog> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	public static SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	public static JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	public static JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	public static ISystemLog create(String id) throws StorageException{
		return dao().create(id);
	}
	
	public static ISystemLog get(String id) throws StorageException{
		return dao().get(id);
	}
	
	public static Collection<ISystemLog> list(int page, int pageSize) throws StorageException{
		return dao().list(page, pageSize);
	}
	
	public static Collection<ISystemLog> listNext(String id, int page, int pageSize) throws StorageException{
		return dao().listNext(id, page, pageSize);
	}
	
	public static void delete(String id) throws StorageException{
		dao().delete(id);
	}

	public static void deleteBefore(Date date) throws StorageException{
		dao().deleteBefore(date);
	}

	public static void deleteAfter(Date date) throws StorageException{
		dao().deleteAfter(date);
	}

	public static void deleteBetween(Date from, Date to) throws StorageException{
		dao().deleteBetween(from, to);
	}
	
	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public void setId(String id);
	
	public String getId();
	
	public void setPath(String path);
	
	public String getPath();
	
	public void setSeverity(LogSeverity severity);
	
	public LogSeverity getSeverity();
	
	public void setMessage(String message);
	
	public String getMessage();
	
	public void setTrace(String trace);
	
	public String getTrace();
	
	public void setClassName(String className);
	
	public String getClassName();
	
	public void setFileName(String fileName);
	
	public String getFileName();
	
	public void setLineNumber(int lineNumber);
	
	public int getLineNumber();
}
