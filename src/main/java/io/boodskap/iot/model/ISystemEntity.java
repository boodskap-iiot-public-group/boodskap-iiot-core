/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.SystemEntityDAO;

@JsonSerialize(as=ISystemEntity.class)
public interface ISystemEntity extends IModel {

	//======================================
	// DAO Methods
	//======================================
	
	public static SystemEntityDAO<ISystemEntity> dao(){
		return SystemEntityDAO.get();
	}

	static public void createOrUpdate(ISystemEntity e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends ISystemEntity> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<ISystemEntity> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<ISystemEntity> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<ISystemEntity> load(String entityType) throws StorageException{
		return dao().load(entityType);
	}
	
	static public ISystemEntity create(String entityType, String entityId) throws StorageException{
		return dao().create(entityType, entityId);
	}
	
	static public ISystemEntity get(String entityType, String entityId) throws StorageException{
		return dao().get(entityType, entityId);
	}
	
	static public long count(String entityType) throws StorageException{
		return dao().count(entityType);
	}
	
	static public void delete(String enityType) throws StorageException{
		dao().delete(enityType);
	}
	
	static public void delete(String enityType, String entityId) throws StorageException{
		dao().delete(enityType, entityId);
	}
	
	static public Collection<ISystemEntity> list(String entityType, int page, int pageSize) throws StorageException{
		return dao().list(entityType, page, pageSize);
	}
	
	static public Collection<ISystemEntity> listNext(String entityType, String entityId, int page, int pageSize) throws StorageException{
		return dao().listNext(entityType, entityId, page, pageSize);
	}

	
	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ISystemEntity.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public default JSONObject getEntity() {
		return dynamicProperties();
	}
	
	public default void setEntity(JSONObject entity) {
		dynamicProperties(entity);
	}

	public String getEntityType();
	
	public void setEntityType(String entityType);
	
	public String getEntityId();
	
	public void setEntityId(String entityId);
	
}
