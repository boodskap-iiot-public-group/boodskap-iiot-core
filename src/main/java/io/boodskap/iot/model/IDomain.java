/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomain.class)
public interface IDomain extends IContact {

	//======================================
	// DAO Methods
	//======================================
	
	public static DomainDAO<IDomain> dao(){
		return DomainDAO.get();
	}
	
	static public Class<? extends IDomain> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomain> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomain> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IDomain create(String domainKey) throws StorageException{
		return dao().create(domainKey);
	}
	
	static public IDomain get(String domainKey) throws StorageException{
		return dao().get(domainKey);
	}
	
	static public Collection<IDomain> list(int page, int pageSize) throws StorageException{
		return dao().list(page, pageSize);
	}
	
	static public Collection<IDomain> listNext(String domainKey, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, page, pageSize);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}


	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDomain.dao().createOrUpdate(this);
	}

}
