/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IUserFilehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IUserFilehis program is distributed in the hope that it will be useful,
 * but WIIUserFileHOUIUserFile ANY WARRANIUserFileY; without even the implied warranty of
 * MERCHANIUserFileABILIIUserFileY or FIIUserFileNESS FOR A PARIUserFileICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.UserFileDAO;

@JsonSerialize(as=IUserFile.class)
public interface IUserFile extends IDomainFile {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static <T extends IUserFile> UserFileDAO<T> dao() {
		return UserFileDAO.get();
	}

	public static Class<? extends IUserFile> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<IUserFile> load() throws StorageException{
		return dao().load();
	}
	
	public static EntityIterator<IUserFile> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}

	public static IUserFile create(String domainKey, String userId, String fileId) throws StorageException{
		return dao().create(domainKey, userId, fileId);
	}

	public static IUserFile get(String domainKey, String userId, String fileId) throws StorageException{
		return dao().get(domainKey, userId, fileId);
	}

	public static IFileContent getContent(String domainKey, String userId, String fileId) throws StorageException{
		return dao().getContent(domainKey, userId, fileId);
	}

	public static boolean has(String domainKey, String userId, String fileId) throws StorageException{
		return dao().has(domainKey, userId, fileId);
	}

	public static void delete(String domainKey, String userId, String fileId) throws StorageException{
		dao().delete(domainKey, userId, fileId);
	}
	
	public static void update(String domainKey, String userId, String fileId, String tags, String description) throws StorageException{
		dao().update(domainKey, userId, fileId, tags, description);
	}

	public static void update(String domainKey, String userId, String fileId, byte[] data, String mediaType) throws StorageException{
		dao().update(domainKey, userId, fileId, fileId, mediaType);
	}

	public static Collection<IUserFile> list(boolean load, String domainKey, String userId, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, userId, page, pageSize);
	}

	public static Collection<IUserFile> listNext(boolean load, String domainKey, String userId, String fileId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, userId, fileId, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IUserFile.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getUserId();
	
	public void setUserId(String userId);
}
