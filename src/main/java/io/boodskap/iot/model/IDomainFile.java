/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainFileDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainFile.class)
public interface IDomainFile extends IPublicFile {

	//======================================
	// DAO Methods
	//======================================
	
	public static DomainFileDAO<IDomainFile> dao(){
		return DomainFileDAO.get();
	}
	
	static public void createOrUpdate(IDomainFile e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDomainFile> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainFile> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainFile> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IDomainFile> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainFile> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public Class<? extends IFileContent> contentClazz(){
		return dao().contentClazz();
	}
	
	static public IDomainFile create(String domainKey, String fileId) throws StorageException{
		return dao().create(domainKey, fileId);
	}

	static public IDomainFile get(String domainKey, String fileId) throws StorageException{
		return dao().get(domainKey, fileId);
	}

	static public IFileContent getContent(String domainKey, String fileId) throws StorageException{
		return dao().getContent(domainKey, fileId);
	}

	static public boolean has(String domainKey, String fileId) throws StorageException{
		return  dao().has(domainKey, fileId);
	}

	static public void delete(String domainKey, String fileId) throws StorageException{
		dao().delete(domainKey, fileId);
	}

	static public void update(String domainKey, String fileId, String tags, String description) throws StorageException{
		dao().update(domainKey, fileId, tags, description);
	}

	static public void update(String domainKey, String fileId, byte[] data, String mediaType) throws StorageException{
		dao().update(domainKey, fileId, data, mediaType);
	}

	static public Collection<IDomainFile> list(boolean load, String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, page, pageSize);
	}

	static public Collection<IDomainFile> listNext(boolean load, String domainKey, String fileId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, fileId, page, pageSize);
	}


	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDomainFile.dao().createOrUpdate(this);
	}
	
}
