/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OutgoingFCMDAO;

@JsonSerialize(as=IOutgoingFCM.class)
public interface IOutgoingFCM extends INotification {

	//======================================
	// DAO Methods
	//======================================
	
	public static OutgoingFCMDAO<IOutgoingFCM> dao() {
		return OutgoingFCMDAO.get();
	}

	static public void createOrUpdate(IOutgoingFCM e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IOutgoingFCM> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IOutgoingFCM> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IOutgoingFCM> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IOutgoingFCM> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IOutgoingFCM> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public IOutgoingFCM create(String domainKey, String notificationId) throws StorageException{
		return dao().create(domainKey, notificationId);
	}

	static public EntityIterator<String> loadIds() throws StorageException{
		return dao().loadIds();
	}

	static public IOutgoingFCM get(String notificationId) throws StorageException{
		return dao().get(notificationId);
	}

	static public IOutgoingFCM get(String domainKey, String notificationId) throws StorageException{
		return dao().get(domainKey, notificationId);
	}

	static public void delete(String domainKey, String notificationId) throws StorageException{
		dao().delete(domainKey, notificationId);
	}

	static public Collection<IOutgoingFCM> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}
	
	static public Collection<IOutgoingFCM> listNext(String domainKey, String notificationId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, notificationId, page, pageSize);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IOutgoingFCM.dao().createOrUpdate(this);
	}
}
