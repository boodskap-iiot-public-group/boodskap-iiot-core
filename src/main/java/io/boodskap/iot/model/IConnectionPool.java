package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.ConnectionPoolType;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.ConnectionPoolDAO;
import io.boodskap.iot.dao.EntityIterator;

/**
 * @author Jegan Vincent
 * @see 5.0.0-00
 */
@JsonSerialize(as=IConnectionPool.class)
public interface IConnectionPool extends IDomainObject {

	
	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static ConnectionPoolDAO<IConnectionPool> dao(){
		return ConnectionPoolDAO.get();
	}
	
	static public void createOrUpdate(IConnectionPool e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IConnectionPool> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IConnectionPool> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IConnectionPool> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}
	
	static public EntityIterator<IConnectionPool> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IConnectionPool> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey,query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey,what,how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey,query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey,what,how);
	}
	
	static public IConnectionPool create(String domainKey, String name) {
		return dao().create(domainKey,name);
	}

	static public IConnectionPool get(String domainKey, String name) throws StorageException{
		return dao().get(domainKey,name);
	}
	
	static public void delete(String domainKey, String name) throws StorageException{
		dao().delete(domainKey,name);
	}
	
	static public Collection<IConnectionPool> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey,page,pageSize);
	}
	
	static public Collection<IConnectionPool> listNext(String domainKey, String name, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey,name,page,pageSize);
	}
	
	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	@Override
	public default void save() {
		IConnectionPool.dao().createOrUpdate(this);
	}
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	@Override
	public default void copy(Object other) {
		
		IConnectionPool o = (IConnectionPool) other;
		
		setType(o.getType());
		setC3p0Args(o.getC3p0Args());
		setDbcpArgs(o.getDbcpArgs());
		setHikariArgs(o.getHikariArgs());
		setTomcatArgs(o.getTomcatArgs());
		
		IDomainObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public ConnectionPoolType getType();

	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setType(ConnectionPoolType type);

	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public IC3P0Args getC3p0Args();

	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setC3p0Args(IC3P0Args c3p0Args);

	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public IDBCPArgs getDbcpArgs();

	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setDbcpArgs(IDBCPArgs dbcpArgs);

	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public IHikariArgs getHikariArgs();

	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setHikariArgs(IHikariArgs hikariArgs);

	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public ITomcatJDBCArgs getTomcatArgs();

	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setTomcatArgs(ITomcatJDBCArgs tomcatArgs);

}
