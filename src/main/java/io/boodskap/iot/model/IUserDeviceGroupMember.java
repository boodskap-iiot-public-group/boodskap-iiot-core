/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IUserDeviceGroupMemberhis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IUserDeviceGroupMemberhis program is distributed in the hope that it will be useful,
 * but WIIUserDeviceGroupMemberHOUIUserDeviceGroupMember ANY WARRANIUserDeviceGroupMemberY; without even the implied warranty of
 * MERCHANIUserDeviceGroupMemberABILIIUserDeviceGroupMemberY or FIIUserDeviceGroupMemberNESS FOR A PARIUserDeviceGroupMemberICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.UserDeviceGroupMemberDAO;

@JsonSerialize(as=IUserDeviceGroupMember.class)
public interface IUserDeviceGroupMember extends IGroupMember {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static UserDeviceGroupMemberDAO<IUserDeviceGroupMember> dao(){
		return UserDeviceGroupMemberDAO.get();
	}

	public static Class<? extends IUserDeviceGroupMember> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<IUserDeviceGroupMember> load() throws StorageException{
		return dao().load();
	}
	
	public static EntityIterator<IUserDeviceGroupMember> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}

	public static IUserDeviceGroupMember create(String domainKey, String ownerUserId, String groupId, String memberDeviceId) throws StorageException{
		return dao().create(domainKey, ownerUserId, groupId, memberDeviceId);
	}
	
	public static long count(String domainKey, String ownerUserId, String groupId) throws StorageException{
		return dao().count(domainKey, ownerUserId, groupId);
	}

	public static IUserDeviceGroupMember get(String domainKey, String ownerUserId, String groupId, String memberDeviceId) throws StorageException{
		return dao().get(domainKey, ownerUserId, groupId, memberDeviceId);
	}
	
	public static void delete(String domainKey, String ownerUserId) throws StorageException{
		dao().delete(domainKey, ownerUserId);
	}
	
	public static void delete(String domainKey, String ownerUserId, String groupId) throws StorageException{
		dao().delete(domainKey, ownerUserId, groupId);
	}
	
	public static void delete(String domainKey, String ownerUserId, String groupId, String memberDeviceId) throws StorageException{
		dao().delete(domainKey, ownerUserId, groupId, memberDeviceId);
	}
	
	public static EntityIterator<String> iterateMembers(String domainKey, String ownerUserId, String groupId) throws StorageException{
		return dao().iterateMembers(domainKey, ownerUserId, groupId);
	}

	public static Collection<IUserDeviceGroupMember> list(String domainKey, String ownerUserId, String groupId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, ownerUserId, groupId, page, pageSize);
	}

	public static Collection<IUserDeviceGroupMember> listNext(String domainKey, String ownerUserId, String groupId, String memberDeviceId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, ownerUserId, groupId, memberDeviceId, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IUserDeviceGroupMember.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getOwnerUserId();

	public void setOwnerUserId(String ownerUserId);

}
