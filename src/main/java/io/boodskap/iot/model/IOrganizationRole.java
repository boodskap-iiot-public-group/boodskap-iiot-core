/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IOrganizationRolehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IOrganizationRolehis program is distributed in the hope that it will be useful,
 * but WIIOrganizationRoleHOUIOrganizationRole ANY WARRANIOrganizationRoleY; without even the implied warranty of
 * MERCHANIOrganizationRoleABILIIOrganizationRoleY or FIIOrganizationRoleNESS FOR A PARIOrganizationRoleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OrganizationRoleDAO;

@JsonSerialize(as=IOrganizationRole.class)
public interface IOrganizationRole extends IDomainRole, IOrgEntity {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static OrganizationRoleDAO<IOrganizationRole> dao(){
		return OrganizationRoleDAO.get();
	}
	
	static public void createOrUpdate(IOrganizationRole e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IOrganizationRole> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IOrganizationRole> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IOrganizationRole> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IOrganizationRole> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IOrganizationRole> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public EntityIterator<IOrganizationRole> load(String domainKey, String orgId) throws StorageException{
		return dao().load(domainKey, orgId);
	}
	
	static public SearchResult<IOrganizationRole> searchByQuery(String domainKey, String orgId, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, orgId, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String orgId, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, orgId, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String orgId, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, orgId, query);
	}

	static public JSONObject updateByQuery(String domainKey, String orgId, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, orgId, what, how);
	}

	static public IOrganizationRole create(String domainKey, String orgId, String name) throws StorageException{
		return dao().create(domainKey, orgId, name);
	}

	static public IOrganizationRole get(String domainKey, String orgId, String name) throws StorageException{
		return dao().get(domainKey, orgId, name);
	}

	static public long count(String domainKey, String orgId) throws StorageException{
		return dao().count(domainKey, orgId);
	}
	
	static public void delete(String domainKey, String orgId) throws StorageException{
		dao().delete(domainKey, orgId);
	}

	static public void delete(String domainKey, String orgId, String name) throws StorageException{
		dao().delete(domainKey, orgId, name);
	}

	static public Collection<IOrganizationRole> list(String domainKey, String orgId) throws StorageException{
		return dao().list(domainKey, orgId);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		IDomainRole.super.copy(other);
		IOrgEntity.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getOrgId();
	
	public void setOrgId(String orgId);
}
