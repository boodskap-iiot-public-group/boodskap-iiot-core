/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.NotificationChannel;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.EventRegistrationDAO;

@JsonSerialize(as=IEventRegistration.class)
public interface IEventRegistration extends IDomainObject {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static EventRegistrationDAO<IEventRegistration> dao(){
		return EventRegistrationDAO.get();
	}

	static public void createOrUpdate(IEventRegistration e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IEventRegistration> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IEventRegistration> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IEventRegistration> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IEventRegistration> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IEventRegistration> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IEventRegistration create(String domainKey, String eventId, NotificationChannel channel, String toAddress) throws StorageException{
		return dao().create(domainKey, eventId, channel, toAddress);
	}

	static public IEventRegistration get(String domainKey, String eventId, NotificationChannel channel, String toAddress) throws StorageException{
		return dao().get(domainKey, eventId, channel, toAddress);
	}

	static public EntityIterator<String> iterateReceipeints(String domainKey, String eventId, NotificationChannel channel) throws StorageException{
		return dao().iterateReceipeints(domainKey, eventId, channel);
	}

	static public void delete(String domainKey, String eventId) throws StorageException{
		dao().delete(domainKey, eventId);
	}

	static public void delete(String domainKey, String eventId, NotificationChannel channel) throws StorageException{
		dao().delete(domainKey, eventId, channel);
	}

	static public void delete(String domainKey, String eventId, NotificationChannel channel, String toAddress) throws StorageException{
		dao().delete(domainKey, eventId, channel, toAddress);
	}

	static public long count(String domainKey, String eventId) throws StorageException{
		return dao().count(domainKey, eventId);
	}

	static public long count(String domainKey, String eventId, NotificationChannel channel) throws StorageException{
		return dao().count(domainKey, eventId, channel);
	}

	static public Collection<String> listReceipeints(String domainKey, String eventId, NotificationChannel channel, int page, int pageSize) throws StorageException{
		return dao().listReceipeints(domainKey, eventId, channel, page, pageSize);
	}

	static public Collection<String> listReceipeintsNext(String domainKey, String eventId, NotificationChannel channel, String toAddress, int page, int pageSize) throws StorageException{
		return dao().listReceipeintsNext(domainKey, eventId, channel, toAddress, page, pageSize);
	}

	static public Collection<IEventRegistration> list(String domainKey, String eventId, NotificationChannel channel, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, eventId, channel, page, pageSize);
	}

	static public Collection<IEventRegistration> listNext(String domainKey, String eventId, NotificationChannel channel, String toAddress, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, eventId, channel, toAddress, page, pageSize);
	}

	


	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IEventRegistration.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public NotificationChannel getChannel();

	public void setChannel(NotificationChannel channel);

	public String getEventId();

	public void setEventId(String eventId);

	public String getToAddress();

	public void setToAddress(String toAddress);

}
