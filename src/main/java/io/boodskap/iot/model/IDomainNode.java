/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainNodeDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainNode.class)
public interface IDomainNode extends IDomainObject{

	//======================================
	// DAO Methods
	//======================================
	
	public static DomainNodeDAO<IDomainNode> dao(){
		return DomainNodeDAO.get();
	}
	
	static public void createOrUpdate(IDomainNode e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDomainNode> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainNode> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainNode> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IDomainNode> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainNode> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public IDomainNode create(String domainKey, String nodeId) throws StorageException{
		return dao().create(domainKey, nodeId);
	}
	
	static public boolean hasDomain(String domainKey) throws StorageException{
		return dao().hasDomain(domainKey);
	}

	static public IDomainNode get(String domainKey, String nodeId) throws StorageException{
		return dao().get(domainKey, nodeId);
	}

	static public void delete(String domainKey, String nodeId) throws StorageException{
		dao().delete(domainKey, nodeId);
	}

	static public EntityIterator<IDomainNode> loadByNodeId(String nodeId) throws StorageException{
		return dao().loadByNodeId(nodeId);
	}

	static public Collection<IDomainNode> list(String nodeId, int page, int pageSize) throws StorageException{
		return dao().list(nodeId, page, pageSize);
	}

	static public Collection<IDomainNode> listNext(String nodeId, String domainKey, int page, int pageSize) throws StorageException{
		return dao().listNext(nodeId, domainKey, page, pageSize);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDomainNode.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IDomainNode o = (IDomainNode) other;
		
		setNodeId(o.getNodeId());
		
		IDomainObject.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getNodeId();

	public void setNodeId(String nodeId);
	
}
