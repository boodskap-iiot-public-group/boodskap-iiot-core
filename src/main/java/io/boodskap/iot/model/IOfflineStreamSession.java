/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IOfflineStreamSessionhis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IOfflineStreamSessionhis program is distributed in the hope that it will be useful,
 * but WIIOfflineStreamSessionHOUIOfflineStreamSession ANY WARRANIOfflineStreamSessionY; without even the implied warranty of
 * MERCHANIOfflineStreamSessionABILIIOfflineStreamSessionY or FIIOfflineStreamSessionNESS FOR A PARIOfflineStreamSessionICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OfflineStreamSessionDAO;

public interface IOfflineStreamSession extends IDomainObject {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static OfflineStreamSessionDAO<IOfflineStreamSession> dao(){
		return OfflineStreamSessionDAO.get();
	}

	static public void createOrUpdate(IOfflineStreamSession e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IOfflineStreamSession> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IOfflineStreamSession> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IOfflineStreamSession> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IOfflineStreamSession> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IOfflineStreamSession> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IOfflineStreamSession create(String domainKey, String deviceId, String camera, String session) throws StorageException{
		return dao().create(domainKey, deviceId, camera, session);
	}

	static public IOfflineStreamSession get(String domainKey, String deviceId, String camera, String session) throws StorageException{
		return dao().get(domainKey, deviceId, camera, session);
	}

	static public long count(String domainKey, String deviceId) throws StorageException{
		return dao().count(domainKey, deviceId);
	}

	static public long count(String domainKey, String deviceId, String camera) throws StorageException{
		return dao().count(domainKey, deviceId, camera);
	}

	static public void delete(String domainKey, String deviceId) throws StorageException{
		dao().delete(domainKey, deviceId);
	}

	static public void delete(String domainKey, String deviceId, String camera) throws StorageException{
		dao().delete(domainKey, deviceId, camera);
	}

	static public void delete(String domainKey, String deviceId, String camera, String session) throws StorageException{
		dao().delete(domainKey, deviceId, camera, session);
	}
	
	static public Collection<String> listSessions(String domainKey, String deviceId, String camera, int page, int pageSize){
		return dao().listSessions(domainKey, deviceId, camera, page, pageSize);
	}

	static public Collection<String> listSessionsNext(String domainKey, String deviceId, String camera, String session, int page, int pageSize){
		return dao().listSessionsNext(domainKey, deviceId, camera, session, page, pageSize);
	}

	static public Collection<IOfflineStreamSession> list(String domainKey, String deviceId, String camera, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, deviceId, camera, page, pageSize);
	}

	static public Collection<IOfflineStreamSession> listNext(String domainKey, String deviceId, String camera, String session, int frame, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, deviceId, camera, session, frame, page, pageSize);
	}

	


	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		IOfflineStreamSession.dao().createOrUpdate(this);
	}
	
	@Override
	public default void copy(Object other) {
		
		IOfflineStreamSession o = (IOfflineStreamSession) other;

		setDeviceId(o.getDeviceId());
		setCamera(o.getCamera());
		setSession(o.getSession());
		
		IDomainObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getDeviceId();

	public void setDeviceId(String deviceId);

	public String getCamera();

	public void setCamera(String camera);

	public String getSession();

	public void setSession(String session);

}
