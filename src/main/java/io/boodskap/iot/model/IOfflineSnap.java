/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IOfflineSnaphis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IOfflineSnaphis program is distributed in the hope that it will be useful,
 * but WIIOfflineSnapHOUIOfflineSnap ANY WARRANIOfflineSnapY; without even the implied warranty of
 * MERCHANIOfflineSnapABILIIOfflineSnapY or FIIOfflineSnapNESS FOR A PARIOfflineSnapICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OfflineSnapDAO;

@JsonSerialize(as=IOfflineSnap.class)
public interface IOfflineSnap extends IRawContent, IDomainObject{
	
	//======================================
	// DAO Methods
	//======================================
	
	public static OfflineSnapDAO<IOfflineSnap> dao(){
		return OfflineSnapDAO.get();
	}

	static public void createOrUpdate(IOfflineSnap e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IOfflineSnap> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IOfflineSnap> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IOfflineSnap> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IOfflineSnap> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IOfflineSnap> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IOfflineSnap create(String domainKey, String deviceId, String camera, String id) throws StorageException{
		return dao().create(domainKey, deviceId, camera, id);
	}
	
	static public IOfflineSnap get(String domainKey, String deviceId, String camera, String id) throws StorageException{
		return dao().get(domainKey, deviceId, camera, id);
	}
	
	static public void delete(String domainKey, String deviceId) throws StorageException{
		dao().delete(domainKey, deviceId);
	}
	
	static public void delete(String domainKey, String deviceId, String camera) throws StorageException{
		dao().delete(domainKey, deviceId, camera);
	}
	
	static public void delete(String domainKey, String deviceId, String camera, String id) throws StorageException{
		dao().delete(domainKey, deviceId, camera, id);
	}
	
	static public long count(String domainKey, String deviceId) throws StorageException{
		return dao().count(domainKey, deviceId);
	}
	
	static public long count(String domainKey, String deviceId, String camera) throws StorageException{
		return dao().count(domainKey, deviceId, camera);
	}
	
	static public Collection<IOfflineSnap> list(boolean load, String domainKey, String deviceId, String camera, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, deviceId, camera, page, pageSize);
	}
	
	static public Collection<IOfflineSnap> listNext(boolean load, String domainKey, String deviceId, String camera, String id, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, deviceId, camera, id, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		IOfflineSnap.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IOfflineSnap o = (IOfflineSnap) other;
		
		setDeviceId(o.getDeviceId());
		setMime(o.getMime());
		setData(o.getData());
		setCamera(o.getCamera());
		
		IDomainObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getId();
	
	public void setId(String id);
	
	public String getDeviceId();

	public void setDeviceId(String deviceId);

	public String getMime();

	public void setMime(String mime);

	public String getCamera();

	public void setCamera(String camera);

}
