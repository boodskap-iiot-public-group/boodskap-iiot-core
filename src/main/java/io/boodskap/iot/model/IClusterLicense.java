package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Date;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.ClusterLicenseDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IClusterLicense.class)
public interface IClusterLicense extends IDomainObject {
	
	public static enum LicenseType{
		INTERNAL,
		DEVELOPER,
		PERSONAL,
		CORE,
		CUSTOM,
		PERPETUAL,
		EDGE,
		EDUCATIONAL
	}
	
	public static enum LicenseStatus{
		ACTIVE,
		DISABLED,
		SUSPENDED,
		TERMINATED,
		UNACTIVATED,
	}
	
	//======================================
	// DAO Methods
	//======================================
	
	public static ClusterLicenseDAO<IClusterLicense> dao(){
		return ClusterLicenseDAO.get();
	}
	
	static public void createOrUpdate(IClusterLicense e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IClusterLicense> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IClusterLicense> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IClusterLicense> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}
	
	static public IClusterLicense create(String domainKey, String targetDomainKey, String clusterId, String licenseKey) {
		return dao().create(domainKey,targetDomainKey,clusterId,licenseKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public long count(String domainKey, String targetDomainKey) throws StorageException{
		return dao().count(domainKey,targetDomainKey);
	}
	
	static public long count(String domainKey, String targetDomainKey, String clusterId) throws StorageException{
		return dao().count(domainKey,targetDomainKey,clusterId);
	}
	
	static public IClusterLicense get(String domainKey, String targetDomainKey, String clusterId, String licenseKey) throws StorageException{
		return dao().get(domainKey,targetDomainKey,clusterId,licenseKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}

	static public void delete(String domainKey, String targetDomainKey) throws StorageException{
		dao().delete(domainKey,targetDomainKey);
	}

	static public void delete(String domainKey, String targetDomainKey, String clusterId) throws StorageException{
		dao().delete(domainKey,targetDomainKey,clusterId);
	}

	static public void delete(String domainKey, String targetDomainKey, String clusterId, String licenseKey) throws StorageException{
		dao().delete(domainKey,targetDomainKey,clusterId,licenseKey);
	}

	static public Collection<IClusterLicense> list(String domainKey, String targetDomainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey,targetDomainKey,page,pageSize);
	}
	
	static public Collection<IClusterLicense> listNext(String domainKey, String targetDomainKey, String clusterId, String licenseKey, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey,targetDomainKey,clusterId,licenseKey,page,pageSize);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IClusterLicense.dao().createOrUpdate(this);
	}
	
	@Override
	public default void copy(Object other) {
		
		IClusterLicense o = (IClusterLicense) other;
		
		setLicenseKey(o.getLicenseKey());
		setClusterId(o.getClusterId());
		setTargetDomainKey(o.getTargetDomainKey());
		setLicenseType(o.getLicenseType());
		setStatus(o.getStatus());
		setMaxDomains(o.getMaxDomains());
		setMaxMachineCores(o.getMaxMachines());
		setMaxUsers(o.getMaxUsers());
		setMaxCores(o.getMaxCores());
		setMaxMachineCores(o.getMaxMachineCores());
		setMaxDevices(o.getMaxDevices());
		setMaxMessagesPerMinute(o.getMaxMessagesPerMinute());
		setMaxOrganizations(o.getMaxOrganizations());
		setValidFrom(o.getValidFrom());
		setValidTo(o.getValidTo());
		setGracePeriod(o.getGracePeriod());
		
		IDomainObject.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getLicenseKey();
	
	public void setLicenseKey(String licenseKey);

	public String getClusterId();
	
	public void setClusterId(String clusterId);
	
	public String getTargetDomainKey();

	public void setTargetDomainKey(String targetDomainKey);

	public LicenseType getLicenseType();

	public void setLicenseType(LicenseType licenseType);

	public LicenseStatus getStatus();

	public void setStatus(LicenseStatus status);

	public int getMaxDomains();

	public void setMaxDomains(int maxDomains);

	public int getMaxMachines();

	public void setMaxMachines(int maxMachines);

	public int getMaxUsers();

	public void setMaxUsers(int maxUsers);

	public int getMaxCores();

	public void setMaxCores(int maxCores);

	public int getMaxMachineCores();

	public void setMaxMachineCores(int maxMachineCores);

	public int getMaxDevices();

	public void setMaxDevices(int maxDevices);

	public int getMaxMessagesPerMinute();

	public void setMaxMessagesPerMinute(int maxMessagesPerMinute);

	public int getMaxDeviceMessagesPerMinute();

	public void setMaxDeviceMessagesPerMinute(int maxDeviceMessagesPerMinute);

	public int getMaxOrganizations();

	public void setMaxOrganizations(int maxOrganizations);

	public Date getValidFrom();

	public void setValidFrom(Date validFrom);

	public Date getValidTo();

	public void setValidTo(Date validTo);

	public int getGracePeriod();
	
	public void setGracePeriod(int gracePeriod);
}
