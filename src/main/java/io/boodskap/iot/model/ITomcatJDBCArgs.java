package io.boodskap.iot.model;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Jegan Vincent
 * @see 5.0.0-00
 */
@JsonSerialize(as=ITomcatJDBCArgs.class)
public interface ITomcatJDBCArgs extends Serializable{

	//======================================
	// Attributes
	//======================================
	
	public String getUrl();

	public void setUrl(String url);

	public String getUsername();

	public void setUsername(String username);

	public String getPassword();

	public void setPassword(String password);

	public String getDriverClassName();

	public void setDriverClassName(String driverClassName);

	public Integer getAbandonWhenPercentageFull();

	public void setAbandonWhenPercentageFull(Integer abandonWhenPercentageFull);

	public Boolean getAccessToUnderlyingConnectionAllowed();

	public void setAccessToUnderlyingConnectionAllowed(Boolean accessToUnderlyingConnectionAllowed);

	public Boolean getAlternateUsernameAllowed();

	public void setAlternateUsernameAllowed(Boolean alternateUsernameAllowed);

	public Boolean getCommitOnReturn();

	public void setCommitOnReturn(Boolean commitOnReturn);

	public Boolean getDefaultAutoCommit();

	public void setDefaultAutoCommit(Boolean defaultAutoCommit);

	public String getDefaultCatalog();

	public void setDefaultCatalog(String defaultCatalog);

	public Boolean getDefaultReadOnly();

	public void setDefaultReadOnly(Boolean defaultReadOnly);

	public Integer getDefaultTransactionIsolation();

	public void setDefaultTransactionIsolation(Integer defaultTransactionIsolation);

	public Boolean getFairQueue();

	public void setFairQueue(Boolean fairQueue);

	public Boolean getIgnoreExceptionOnPreLoad();

	public void setIgnoreExceptionOnPreLoad(Boolean ignoreExceptionOnPreLoad);

	public Integer getInitialSize();

	public void setInitialSize(Integer initialSize);

	public String getInitSQL();

	public void setInitSQL(String initSQL);

	public Boolean getLogAbandoned();

	public void setLogAbandoned(Boolean logAbandoned);

	public Boolean getLogValidationErrors();

	public void setLogValidationErrors(Boolean logValidationErrors);

	public Integer getMaxActive();

	public void setMaxActive(Integer maxActive);

	public Long getMaxAge();

	public void setMaxAge(Long maxAge);

	public Integer getMaxIdle();

	public void setMaxIdle(Integer maxIdle);

	public Integer getMaxWait();

	public void setMaxWait(Integer maxWait);

	public Integer getMinEvictableIdleTimeMillis();

	public void setMinEvictableIdleTimeMillis(Integer minEvictableIdleTimeMillis);

	public Integer getMinIdle();

	public void setMinIdle(Integer minIdle);

	public Integer getNumTestsPerEvictionRun();

	public void setNumTestsPerEvictionRun(Integer numTestsPerEvictionRun);

	public Boolean getPropagateInterruptState();

	public void setPropagateInterruptState(Boolean propagateInterruptState);

	public Boolean getRemoveAbandoned();

	public void setRemoveAbandoned(Boolean removeAbandoned);

	public Integer getRemoveAbandonedTimeout();

	public void setRemoveAbandonedTimeout(Integer removeAbandonedTimeout);

	public Boolean getRollbackOnReturn();

	public void setRollbackOnReturn(Boolean rollbackOnReturn);

	public Integer getSuspectTimeout();

	public void setSuspectTimeout(Integer suspectTimeout);

	public Boolean getTestOnBorrow();

	public void setTestOnBorrow(Boolean testOnBorrow);

	public Boolean getTestOnConnect();

	public void setTestOnConnect(Boolean testOnConnect);

	public Boolean getTestOnReturn();

	public void setTestOnReturn(Boolean testOnReturn);

	public Boolean getTestWhileIdle();

	public void setTestWhileIdle(Boolean testWhileIdle);

	public Integer getTimeBetweenEvictionRunsMillis();

	public void setTimeBetweenEvictionRunsMillis(Integer timeBetweenEvictionRunsMillis);

	public Boolean getUseDisposableConnectionFacade();

	public void setUseDisposableConnectionFacade(Boolean useDisposableConnectionFacade);

	public Boolean getUseEquals();

	public void setUseEquals(Boolean useEquals);

	public Boolean getUseLock();

	public void setUseLock(Boolean useLock);

	public Boolean getUseStatementFacade();

	public void setUseStatementFacade(Boolean useStatementFacade);

	public Long getValidationInterval();

	public void setValidationInterval(Long validationInterval);

	public String getValidationQuery();

	public void setValidationQuery(String validationQuery);

	public Integer getValidationQueryTimeout();

	public void setValidationQueryTimeout(Integer validationQueryTimeout);

}