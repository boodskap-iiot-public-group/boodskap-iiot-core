package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.JarClassDAO;

@JsonSerialize(as=IJarClass.class)
public interface IJarClass extends IJavaClass{

	//======================================
	// DAO Methods
	//======================================
	
	public static JarClassDAO<IJarClass> dao(){
		return JarClassDAO.get();
	}
	
	static public void createOrUpdate(IJarClass e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IJarClass> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IJarClass> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IJarClass> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IJarClass create(String loader, String fileName, String pkg, String name){
		return dao().create(loader, fileName, pkg, name);
	}
	
	static public long count(String loader) throws StorageException{
		return dao().count(loader);
	}
	
	static public long count(String loader, String fileName) throws StorageException{
		return dao().count(loader, fileName);
	}
	
	static public long count(String loader, String fileName, String pkg) throws StorageException{
		return dao().count(loader, fileName, pkg);
	}
	
	static public long countPackage(String loader, String pkg) throws StorageException{
		return dao().countPackage(loader, pkg);
	}
	
	static public void delete(String loader) throws StorageException{
		dao().delete(loader);
	}

	static public void delete(String loader, String fileName) throws StorageException{
		dao().delete(loader, fileName);
	}
	
	static public EntityIterator<IJarClass> load(String loader) throws StorageException{
		return dao().load(loader);
	}
	
	static public Collection<IJarClass> listClasses(String loader, int page, int pageSize) throws StorageException{
		return dao().listClasses(loader, page, pageSize);
	}
	
	static public Collection<IJarClass> listNextClasses(String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextClasses(loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<IJarClass> listPackageClasses(String loader, String pkg, int page, int pageSize) throws StorageException{
		return dao().listPackageClasses(loader, pkg, page, pageSize);
	}
	
	static public Collection<IJarClass> listNextPackageClasses(String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextPackageClasses(loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<IJarClass> listArchiveClasses(String loader, String fileName, int page, int pageSize) throws StorageException{
		return dao().listArchiveClasses(loader, fileName, page, pageSize);
	}
	
	static public Collection<IJarClass> listNextArchiveClasses(String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextArchiveClasses(loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<IJarClass> listArchivePackageClasses(String loader, String fileName, String pkg, int page, int pageSize) throws StorageException{
		return dao().listArchivePackageClasses(loader, fileName, pkg, page, pageSize);
	}
	
	static public Collection<IJarClass> listNextArchivePackageClasses(String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextArchivePackageClasses(loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<String> listPackages(String loader, int page, int pageSize) throws StorageException{
		return dao().listPackages(loader, page, pageSize);
	}
	
	static public Collection<String> listNextPackages(String loader, String fileName, String pkg, int page, int pageSize) throws StorageException{
		return dao().listNextPackages(loader, fileName, pkg, page, pageSize);
	}
	
	static public Collection<String> listArchivePackages(String loader, String fileName, int page, int pageSize) throws StorageException{
		return dao().listArchivePackages(loader, fileName, page, pageSize);
	}
	
	static public Collection<String> listNextArchivePackages(String loader, String fileName, String pkg, int page, int pageSize) throws StorageException{
		return dao().listNextArchivePackages(loader, fileName, pkg, page, pageSize);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getFileName();
	
	public void setFileName(String fileName);
	
}
