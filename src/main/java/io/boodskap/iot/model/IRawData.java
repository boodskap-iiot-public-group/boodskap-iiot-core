/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.DataChannel;
import io.boodskap.iot.RawDataType;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.RawDataDAO;

@JsonSerialize(as=IRawData.class)
public interface IRawData extends IDomainData, IRawContent {
	
	public static enum State{
		QUEUED,
		REQUEUED,
		PROCESSING,
		PROCESSED,
		SKIPPED,
		FAILED
	}
	
	//======================================
	// DAO Methods
	//======================================
	
	public static RawDataDAO<IRawData> dao() {
		return  RawDataDAO.get();
	}
	
	static public void createOrUpdate(IRawData e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IRawData> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IRawData> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IRawData> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IRawData> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IRawData> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public IRawData create(String id) throws StorageException{
		return dao().create(id);
	}
	
	static public void update(IRawData e) throws StorageException{
		dao().update(e);
	}
	
	static public void updateState(IRawData e) throws StorageException{
		dao().updateState(e);
	}
	
	static public IRawData get(String id) throws StorageException{
		return dao().get(id);
	}
	
	static public void deleteById(String id) throws StorageException{
		dao().deleteById(id);
	}
	
	static public Collection<IRawData> list(boolean load, String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, page, pageSize);
	}

	static public Collection<IRawData> listNext(boolean load, String domainKey, String id, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, id, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		RawDataDAO.get().createOrUpdate(this);
	}
	
	public default void setProcessing() throws StorageException {
		updateState(State.PROCESSING);
	}

	public default void setProcessed() throws StorageException {
		updateState(State.PROCESSED);
	}

	public default void setSkipped() throws StorageException {
		updateState(State.SKIPPED);
	}

	public default void setFailed() throws StorageException {
		updateState(State.FAILED);
	}

	public default void updateState(State state) throws StorageException {
		setState(state);
		IRawData.dao().updateState(this);
	}

	public default void remove() throws StorageException {
		IRawData.dao().deleteById(getId());
	}

	//======================================
	// Attributes
	//======================================
	
	public String getId();

	public void setId(String id);

	public String getNodeId();

	public void setNodeId(String nodeId);

	public String getNodeUid();

	public void setNodeUid(String nodeUid);

	public String getIpAddress();

	public void setIpAddress(String ipAddress);

	public int getSize();

	public void setSize(int size);

	public DataChannel getChannel();

	public void setChannel(DataChannel channel);

	public Integer getPort();

	public void setPort(Integer port);

	public String getMqttServerId();

	public void setMqttServerId(String mqttServerId);

	public String getMqttTopic();

	public void setMqttTopic(String mqttTopic);

	public String getDeviceId();

	public void setDeviceId(String deviceId);

	public String getDeviceModel();

	public void setDeviceModel(String deviceModel);

	public String getFirmwareVersion();

	public void setFirmwareVersion(String firmwareVersion);

	public RawDataType getRawDataType();

	public void setRawDataType(RawDataType rawDataType);

	public String getContentType();

	public void setContentType(String contentType);

	public State getState();
	
	public void setState(State state);
	
	public void setFileName(String fileName);
	
	public String getFileName();

	public String getRuleId();
	
	public void setRuleId(String ruleId);
	
	public void setOrgId(String orgId);
	
	public String getOrgId();
	
	public void setTrace(String trace);
	
	public String getTrace();
	
}
