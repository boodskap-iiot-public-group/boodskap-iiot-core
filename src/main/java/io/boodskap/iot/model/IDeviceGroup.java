/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DeviceGroupDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDeviceGroup.class)
public interface IDeviceGroup extends IGroup {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static DeviceGroupDAO<IDeviceGroup> dao(){
		return DeviceGroupDAO.get();
	}
	

	static public void createOrUpdate(IDeviceGroup e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDeviceGroup> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDeviceGroup> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDeviceGroup> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IDeviceGroup> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDeviceGroup> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IDeviceGroup create(String domainKey, String ownerDeviceId, String groupId) {
		return dao().create(domainKey, ownerDeviceId, groupId);
	}

	static public IDeviceGroup get(String domainKey, String ownerDeviceId, String groupId) throws StorageException{
		return dao().get(domainKey, ownerDeviceId, groupId);
	}

	static public void delete(String domainKey, String ownerDeviceId, String groupId) throws StorageException{
		dao().delete(domainKey, ownerDeviceId, groupId);
	}
	

	static public void delete(String domainKey, String ownerDeviceId) throws StorageException{
		dao().delete(domainKey, ownerDeviceId);
	}

	static public long count(String domainKey, String ownerDeviceId) throws StorageException{
		return dao().count(domainKey, ownerDeviceId);
	}

	static public Collection<IDeviceGroup> list(String domainKey, String ownerDeviceId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, ownerDeviceId, page, pageSize);
	}

	static public Collection<IDeviceGroup> listNext(String domainKey, String ownerDeviceId, String groupId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, ownerDeviceId, groupId, page, pageSize);
	}
	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDeviceGroup.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IDeviceGroup o = (IDeviceGroup) other;
		
		setOwnerDeviceId(o.getOwnerDeviceId());
		
		IGroup.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getOwnerDeviceId();

	public void setOwnerDeviceId(String ownerDeviceId);

}
