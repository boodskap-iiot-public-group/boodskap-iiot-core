package io.boodskap.iot.model;

import io.boodskap.iot.SQLFieldType;
import io.boodskap.iot.StorageException;

public interface IIgniteSQLField extends IModel {

	//======================================
	// Default Methods
	//======================================
	
	@Override
	default void save() throws StorageException {
		throw new StorageException("Use the ISQLTable.save() instead");
	}

	//======================================
	// Attributes
	//======================================
	
	public SQLFieldType getType();
	
	public void setType(SQLFieldType type);
}
