package io.boodskap.iot.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=IConditionalProcess.class)
public interface IConditionalProcess extends Serializable{

	
	//======================================
	// Attributes
	//======================================
	
	public String getField();
	
	public void setField(String field);
	
	public String getExpression();
	
	public void setExpression(String expression);
	
	public List<? extends IWorkItem> getOnTrueWorkItems();
	
	public void setOnTrueWorkItems(List<? extends IWorkItem> onTrueWorkItems);
	
	public List<? extends IWorkItem> getOnFalseWorkItems();
	
	public void setOnFalseWorkItems(List<? extends IWorkItem> onFalseWorkItems);
	
	public Map<String, Object> getOnTrueUpdateSession();
	
	public void setOnTrueUpdateSession(Map<String, Object> onTrueUpdateSession);
	
	public Map<String, Object> getOnTrueUpdateInput();
	
	public void setOnTrueUpdateInput(Map<String, Object> onTrueUpdateInput);
	
	public Map<String, Object> getOnTrueUpdateOutput();
	
	public void setOnTrueUpdateOutputt(Map<String, Object> onTrueUpdateOutput);

	public Map<String, Object> getOnFalseUpdateSession();
	
	public void setOnFalseUpdateSession(Map<String, Object> onFalseUpdateSession);
	
	public Map<String, Object> getOnFalseUpdateInput();
	
	public void setOnFalseUpdateInput(Map<String, Object> onFalseUpdateInput);
	
	public Map<String, Object> getOnFalseUpdateOutput();
	
	public void setOnFalseUpdateOutputt(Map<String, Object> onFalseUpdateOutput);

	public Map<String, Object> getOnErrorUpdateSession();
	
	public void setOnErrorUpdateSession(Map<String, Object> onErrorUpdateSession);
	
	public Map<String, Object> getOnErrorUpdateInput();
	
	public void setOnErrorUpdateInput(Map<String, Object> onErrorUpdateInput);
	
	public Map<String, Object> getOnErrorUpdateOutput();
	
	public void setOnErrorUpdateOutputt(Map<String, Object> onErrorUpdateOutput);

}
