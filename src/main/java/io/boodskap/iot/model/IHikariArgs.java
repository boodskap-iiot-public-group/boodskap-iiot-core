package io.boodskap.iot.model;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Jegan Vincent
 * @see 5.0.0-00
 */
@JsonSerialize(as=IHikariArgs.class)
public interface IHikariArgs extends Serializable {

	public String getJdbcUrl();

	public void setJdbcUrl(String jdbcUrl);

	public String getUsername();

	public void setUsername(String username);

	public String getPassword();

	public void setPassword(String password);

	public String getDataSourceClassName();

	public void setDataSourceClassName(String dataSourceClassName);

	public String getDriverClassName();

	public void setDriverClassName(String driverClassName);

	public Boolean getAllowPoolSuspension();

	public void setAllowPoolSuspension(Boolean allowPoolSuspension);

	public Boolean getAutoCommit();

	public void setAutoCommit(Boolean autoCommit);

	public String getCatalog();

	public void setCatalog(String catalog);

	public String getConnectionInitSql();

	public void setConnectionInitSql(String connectionInitSql);

	public String getConnectionTestQuery();

	public void setConnectionTestQuery(String connectionTestQuery);

	public Long getConnectionTimeout();

	public void setConnectionTimeout(Long connectionTimeout);

	public Long getIdleTimeout();

	public void setIdleTimeout(Long idleTimeout);

	public Long getInitializationFailTimeout();

	public void setInitializationFailTimeout(Long initializationFailTimeout);

	public Boolean getIsolateInternalQueries();

	public void setIsolateInternalQueries(Boolean isolateInternalQueries);

	public Long getLeakDetectionThreshold();

	public void setLeakDetectionThreshold(Long leakDetectionThreshold);

	public Integer getMaximumPoolSize();

	public void setMaximumPoolSize(Integer maximumPoolSize);

	public Long getMaxLifetime();

	public void setMaxLifetime(Long maxLifetime);

	public Integer getMinimumIdle();

	public void setMinimumIdle(Integer minimumIdle);

	public Boolean getReadOnly();

	public void setReadOnly(Boolean readOnly);

	public String getSchema();

	public void setSchema(String schema);

	public String getTransactionIsolation();

	public void setTransactionIsolation(String transactionIsolation);

	public Long getValidationTimeout();

	public void setValidationTimeout(Long validationTimeout);

}
