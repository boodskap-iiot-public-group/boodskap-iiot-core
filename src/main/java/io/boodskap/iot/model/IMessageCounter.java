package io.boodskap.iot.model;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.MessageCounterDAO;

@JsonSerialize(as=ICounter.class)
public interface IMessageCounter extends IStorageObject {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static MessageCounterDAO<IMessageCounter> dao() {
		return MessageCounterDAO.get();
	}
	
	public static Class<? extends IMessageCounter> clazz(){
		return dao().clazz();
	}
	
	public static void increment() throws StorageException{
		dao().increment();
	}
	
	public static long countYearly() throws StorageException{
		return dao().countYearly();
	}

	public static long countMonthly() throws StorageException{
		return dao().countMonthly();
	}

	public static long countDaily() throws StorageException{
		return dao().countDaily();
	}

	public static long countHourly() throws StorageException{
		return dao().countHourly();
	}

	public static long countMinutely() throws StorageException{
		return dao().countMonthly();
	}

	public static long countSecondly() throws StorageException{
		return dao().countSecondly();
	}

	public static long countBy(Date date) throws StorageException{
		return dao().countBy(date);
	}

	public static long countBetween(Date from, Date to) throws StorageException{
		return dao().countBetween(from, to);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() throws StorageException {
		dao().increment();
	}

	@Override
	public default void copy(Object other) {
		
		IMessageCounter o = (IMessageCounter) other;

		setId(o.getId());
		setCount(o.getCount());
		
		IStorageObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public Date getId();
	
	public void setId(Date id);
	
	public long getCount();
	
	public void setCount(long count);
}
