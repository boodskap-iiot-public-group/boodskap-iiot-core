/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.SystemBinaryRuleDAO;

@JsonSerialize(as=ISystemBinaryRule.class)
public interface ISystemBinaryRule extends ISystemRule {

	//======================================
	// DAO Methods
	//======================================
	
	public static SystemBinaryRuleDAO<ISystemBinaryRule> dao(){
		return SystemBinaryRuleDAO.get();
	}

	static public void createOrUpdate(ISystemBinaryRule e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends ISystemBinaryRule> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<ISystemBinaryRule> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<ISystemBinaryRule> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public ISystemBinaryRule create(String type){
		return dao().create(type);
	}

	static public ISystemBinaryRule get(String type) throws StorageException{
		return dao().get(type);
	}

	static public void delete(String type) throws StorageException{
		dao().delete(type);
	}

	static public Collection<ISystemBinaryRule> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	static public Collection<ISystemBinaryRule> listNext(boolean load, String type, int page, int pageSize) throws StorageException{
		return dao().listNext(load, type, page, pageSize);
	}

	static public Collection<String> listTypes(int page, int pageSize) throws StorageException{
		return dao().listTypes(page, pageSize);
	}

	static public Collection<String> listTypesNext(String type, int page, int pageSize) throws StorageException{
		return dao().listTypesNext(type, page, pageSize);
		
	}

	
	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ISystemBinaryRule.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		ISystemBinaryRule o = (ISystemBinaryRule) other;
		
		setType(o.getType());
		
		ISystemRule.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getType() ;

	public void setType(String type) ;
}
