/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainDeviceEntityDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainDeviceEntity.class)
public interface IDomainDeviceEntity extends IDomainEntity {

	//======================================
	// DAO Methods
	//======================================
	
	public static DomainDeviceEntityDAO<IDomainDeviceEntity> dao(){
		return DomainDeviceEntityDAO.get();
	}

	static public void createOrUpdate(IDomainDeviceEntity e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDomainDeviceEntity> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainDeviceEntity> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainDeviceEntity> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IDomainDeviceEntity> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainDeviceEntity> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IDomainDeviceEntity create(String domainKey, String deviceId, String entityType, String entityId) throws StorageException{
		return dao().create(domainKey, deviceId, entityType, entityId);
	}
	
	static public IDomainDeviceEntity get(String domainKey, String deviceId, String entityType, String entityId) throws StorageException{
		return dao().get(domainKey, deviceId, entityType, entityId);
	}
	
	static public void delete(String domainKey, String deviceId) throws StorageException{
		dao().delete(domainKey, deviceId);
	}
	
	static public void delete(String domainKey, String deviceId, String enityType) throws StorageException{
		dao().delete(domainKey, deviceId, enityType);
	}
	
	static public void delete(String domainKey, String deviceId, String enityType, String entityId) throws StorageException{
		dao().delete(domainKey, deviceId, enityType, entityId);
	}
	
	static public Collection<IDomainDeviceEntity> list(String domainKey, String deviceId, String entityType, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, deviceId, entityType, page, pageSize);
	}
	
	static public Collection<IDomainDeviceEntity> listNext(String domainKey, String deviceId, String entityType, String entityId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, deviceId, entityType, entityId, page, pageSize);
	}


	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDomainDeviceEntity.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getDeviceId();
	
	public void setDeviceId(String deviceId);
}
