/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IUserGrouphis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IUserGrouphis program is distributed in the hope that it will be useful,
 * but WIIUserGroupHOUIUserGroup ANY WARRANIUserGroupY; without even the implied warranty of
 * MERCHANIUserGroupABILIIUserGroupY or FIIUserGroupNESS FOR A PARIUserGroupICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.UserGroupDAO;

@JsonSerialize(as=IUserGroup.class)
public interface IUserGroup extends IGroup {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static <T extends IUserGroup> UserGroupDAO<T> dao() {
		return UserGroupDAO.get();
	}

	public static Class<? extends IUserGroup> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<IUserGroup> load() throws StorageException{
		return dao().load();
	}
	
	public static EntityIterator<IUserGroup> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	public static IUserGroup create(String domainKey, String ownerUserId, String groupId) throws StorageException{
		return dao().create(domainKey, ownerUserId, groupId);
	}

	public static IUserGroup get(String domainKey, String ownerUserId, String groupId) throws StorageException{
		return dao().get(domainKey, ownerUserId, groupId);
	}

	public static void delete(String domainKey, String ownerUserId, String groupId) throws StorageException{
		dao().delete(domainKey, ownerUserId, groupId);
	}

	public static void delete(String domainKey, String ownerUserId) throws StorageException{
		dao().delete(domainKey, ownerUserId);
	}

	public static long count(String domainKey, String ownerUserId) throws StorageException{
		return dao().count(domainKey, ownerUserId);
	}

	public static Collection<IUserGroup> list(String domainKey, String ownerUserId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, ownerUserId, page, pageSize);
	}

	public static Collection<IUserGroup> listNext(String domainKey, String ownerUserId, String groupId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, ownerUserId, groupId, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IUserGroup.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getOwnerUserId();

	public void setOwnerUserId(String ownerUserId);
}
