package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.JarFileDAO;

@JsonSerialize(as=IJarFile.class)
public interface IJarFile extends IRawContent, IModel{

	//======================================
	// DAO Methods
	//======================================
	
	public static JarFileDAO<IJarFile> dao(){
		return JarFileDAO.get();
	}

	static public Class<? extends IJarFile> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IJarFile> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IJarFile> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IJarFile create(String loader, String fileName){
		return dao().create(loader, fileName);
	}
	
	static public void createOrUpdate(IJarFile e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public IJarFile get(String loader, String fileName, boolean loadContent) throws StorageException{
		return dao().get(loader, fileName, loadContent);
	}
	
	static public long count(String loader) throws StorageException{
		return dao().count(loader);
	}
	
	static public void delete(String loader) throws StorageException{
		dao().delete(loader);
	}

	static public void delete(String loader, String fileName) throws StorageException{
		dao().delete(loader, fileName);
	}
	
	static public EntityIterator<IJarFile> load(String loader) throws StorageException{
		return dao().load(loader);
	}
	
	static public Collection<IJarFile> list(String loader, int page, int pageSize, boolean loadContent) throws StorageException{
		return dao().list(loader, page, pageSize, loadContent);
	}
	
	static public Collection<IJarFile> listNext(String loader, String fileName, int page, int pageSize, boolean loadContent) throws StorageException{
		return dao().listNext(loader, fileName, page, pageSize, loadContent);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		IJarFile.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IJarFile o = (IJarFile) other;
		
		setLoader(o.getLoader());
		setFileName(o.getFileName());
		setData(o.getData());
		
		IModel.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getLoader();
	
	public void setLoader(String loader);
	
	public String getFileName();
	
	public void setFileName(String fileName);
	
	public byte[] getData();
	
	public void setData(byte[] data);

}
