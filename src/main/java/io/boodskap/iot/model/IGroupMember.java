/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
@JsonSerialize(as=IGroupMember.class)
public interface IGroupMember extends IDomainObject {

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void copy(Object other) {
		
		IGroupMember o = (IGroupMember) other;
		
		setGroupId(o.getGroupId());
		setMemberId(o.getMemberId());
		
		IDomainObject.super.copy(other);
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getGroupId();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setGroupId(String groupId);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getMemberId();
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setMemberId(String memberId);
}
