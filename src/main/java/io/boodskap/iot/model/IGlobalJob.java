/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * INamedRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * INamedRulehis program is distributed in the hope that it will be useful,
 * but WIINamedRuleHOUINamedRule ANY WARRANINamedRuleY; without even the implied warranty of
 * MERCHANINamedRuleABILIINamedRuleY or FIINamedRuleNESS FOR A PARINamedRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.GlobalJobDAO;

@JsonSerialize(as=IGlobalJob.class)
public interface IGlobalJob extends ISystemJob  {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static GlobalJobDAO<IGlobalJob> dao(){
		return GlobalJobDAO.get();
	}

	public static Class<? extends IGlobalJob> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<IGlobalJob> load() throws StorageException{
		return dao().load();
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static IGlobalJob create(String id) throws StorageException{
		return dao().create(id);
	}

	public static IGlobalJob get(String id) throws StorageException{
		return dao().get(id);
	}

	public static boolean has(String id) throws StorageException{
		return dao().has(id);
	}

	public static void delete(String id) throws StorageException{
		dao().delete(id);
	}

	public static Collection<IGlobalJob> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	public static Collection<IGlobalJob> listNext(boolean load, String id, int page, int pageSize) throws StorageException{
		return dao().listNext(load, id, page, pageSize);
	}

	public static Collection<String> listIds(int page, int pageSize) throws StorageException{
		return dao().listIds(page, pageSize);
	}

	public static Collection<String> listIdsNext(String id, int page, int pageSize) throws StorageException{
		return dao().listIdsNext(id, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IGlobalJob.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
}


