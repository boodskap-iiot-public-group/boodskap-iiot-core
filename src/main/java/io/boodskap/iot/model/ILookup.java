/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.DataType;
import io.boodskap.iot.PropertyTarget;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.ThreadContext;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.LookupDAO;

@JsonSerialize(as=ILookup.class)
public interface ILookup extends IDomainObject{

	//======================================
	// DAO Methods
	//======================================
	
	public static LookupDAO<ILookup> dao() {
		return LookupDAO.get();
	}
	
	public static Class<? extends ILookup> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<ILookup> load() throws StorageException{
		return dao().load();
	}
	
	public static EntityIterator<ILookup> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}

	public static ILookup create(String domainKey, PropertyTarget target, String targetId, String name) throws StorageException{
		return dao().create(domainKey, target, targetId, name);
	}
	
	public static ILookup get(String domainKey, PropertyTarget target, String targetId, String name) throws StorageException{
		return dao().get(domainKey, target, targetId, name);
	}
	
	public static long count(String domainKey, PropertyTarget target) throws StorageException{
		return dao().count(domainKey, target);
	}

	public static void set(String domainKey, PropertyTarget target, String targetId, DataType type, String name, String value) throws StorageException{
		dao().set(domainKey, target, targetId, type, name, value);
	}

	public static void delete(String domainKey, PropertyTarget target) throws StorageException{
		dao().delete(domainKey, target);
	}

	public static void delete(String domainKey, PropertyTarget target, String targetId) throws StorageException{
		dao().delete(domainKey, target, targetId);
	}

	public static void delete(String domainKey, PropertyTarget target, String targetId, String name) throws StorageException{
		dao().delete(domainKey, target, targetId, name);
	}

	public static Collection<ILookup> list(String domainKey, PropertyTarget target, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, target, page, pageSize);
	}

	public static Collection<ILookup> listNext(String domainKey, PropertyTarget target, String name, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, target, name, page, pageSize);
	}

	public static void set(String domainKey, PropertyTarget target, String targetId, String name, Map<String, Object> value) throws StorageException{
		set(domainKey, target, targetId, DataType.JSON, name, ThreadContext.mapToJson(value));
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, UUID value) throws StorageException{
		set(domainKey, target, targetId, DataType.UUID, name, value.toString());
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, String value) throws StorageException{
		set(domainKey, target, targetId, DataType.STRING, name, value);
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, Byte[] value) throws StorageException{
		set(domainKey, target, targetId, DataType.BLOB, name, Base64.encodeBase64String(ArrayUtils.toPrimitive(value)));
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, byte[] value) throws StorageException{
		set(domainKey, target, targetId, DataType._blob, name, Base64.encodeBase64String(value));
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, Byte value) throws StorageException{
		set(domainKey, target, targetId, DataType.BYTE, name, value.toString());
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, byte value) throws StorageException{
		set(domainKey, target, targetId, DataType._byte, name, String.valueOf(value));
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, Short value) throws StorageException{
		set(domainKey, target, targetId, DataType.SHORT, name, value.toString());
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, short value) throws StorageException{
		set(domainKey, target, targetId, DataType._short, name, String.valueOf(value));
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, Integer value) throws StorageException{
		set(domainKey, target, targetId, DataType.INT, name, value.toString());
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, int value) throws StorageException{
		set(domainKey, target, targetId, DataType._int, name, String.valueOf(value));
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, Long value) throws StorageException{
		set(domainKey, target, targetId, DataType.LONG, name, value.toString());
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, long value) throws StorageException{
		set(domainKey, target, targetId, DataType._long, name, String.valueOf(value));
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, Float value) throws StorageException{
		set(domainKey, target, targetId, DataType.FLOAT, name, value.toString());
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, float value) throws StorageException{
		set(domainKey, target, targetId, DataType._float, name, String.valueOf(value));
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, Double value) throws StorageException{
		set(domainKey, target, targetId, DataType.DOUBLE, name, value.toString());
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, double value) throws StorageException{
		set(domainKey, target, targetId, DataType._double, name, String.valueOf(value));
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, Boolean value) throws StorageException{
		set(domainKey, target, targetId, DataType.BOOLEAN, name, value.toString());
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, boolean value) throws StorageException{
		set(domainKey, target, targetId, DataType._boolean, name, String.valueOf(value));
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, Character value) throws StorageException{
		set(domainKey, target, targetId, DataType.CHAR, name, value.toString());
	}
	
	public static void set(String domainKey, PropertyTarget target, String targetId, String name, char value) throws StorageException{
		set(domainKey, target, targetId, DataType._char, name, String.valueOf(value));
	}
	
	public static Object getValue(String domainKey, PropertyTarget target, String targetId, String name) throws StorageException {
		
		ILookup val = get(domainKey, target, targetId, name);
		
		if(null != val) {
			return val.get();
		}
		
		return null;
		
	}
	

	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		LookupDAO.get().createOrUpdate(this);
	}
	
	public default Object get() {
		
		try {
			
			switch(getType()) {
			case BOOLEAN:
				return Boolean.valueOf(getValue());
			case _boolean:
				return Boolean.valueOf(getValue()).booleanValue();
			case BYTE:
				return Byte.valueOf(getValue());
			case _byte:
				return Byte.valueOf(getValue()).byteValue();
			case CHAR:
				return Character.valueOf(getValue().charAt(0));
			case _char:
				return Character.valueOf(getValue().charAt(0)).charValue();
			case DOUBLE:
				return Double.valueOf(getValue());
			case _double:
				return Double.valueOf(getValue()).doubleValue();
			case FLOAT:
				return Float.valueOf(getValue());
			case _float:
				return Float.valueOf(getValue()).floatValue();
			case INT:
				return Double.valueOf(getValue());
			case _int:
				return Double.valueOf(getValue()).intValue();
			case LONG:
				return Long.valueOf(getValue());
			case _long:
				return Long.valueOf(getValue()).longValue();
			case SHORT:
				return Short.valueOf(getValue());
			case _short:
				return Short.valueOf(getValue()).shortValue();
			case BLOB:
				return ArrayUtils.toObject(Base64.decodeBase64(getValue()));
			case _blob:
				return Base64.decodeBase64(getValue());
			case UUID:
				return UUID.fromString(getValue());
			case JSON:
				return ThreadContext.jsonToMap(getValue());
			default:
			case STRING:
				return getValue();
			}
		}catch(Exception ex) {
			throw new StorageException(ex);
		}
		
	}

	@Override
	public default void copy(Object other) {
		
		ILookup o = (ILookup) other;
		
		setTarget(o.getTarget());
		setTargetId(o.getTargetId());
		setValue(o.getValue());
		setType(o.getType());
		
		IDomainObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public PropertyTarget getTarget();

	public void setTarget(PropertyTarget target);

	public String getTargetId();

	public void setTargetId(String targetId);

	public String getValue();

	public void setValue(String value);

	public DataType getType();

	public void setType(DataType type);
	
}
