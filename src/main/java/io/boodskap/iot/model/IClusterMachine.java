package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.ClusterMachineDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IClusterMachine.class)
public interface IClusterMachine extends IDomainObject {
	
	public static enum MachineStatus{
		INACTIVE,
		ACTIVE,
		DISABLED,
		SUSPENDED,
		TERMINATED,
	}
	
	//======================================
	// DAO Methods
	//======================================
	
	public static ClusterMachineDAO<IClusterMachine> dao(){
		return ClusterMachineDAO.get();
	}
	
	static public void createOrUpdate(IClusterMachine e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IClusterMachine> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IClusterMachine> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IClusterMachine> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}
	
	static public IClusterMachine create(String domainKey, String targetDomainKey, String clusterId, String machineId) {
		return dao().create(domainKey,targetDomainKey,clusterId,machineId);
	}

	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public long count(String domainKey, String targetDomainKey) throws StorageException{
		return dao().count(domainKey,targetDomainKey);
	}
	
	static public long count(String domainKey, String targetDomainKey, String clusterId) throws StorageException{
		return dao().count(domainKey,targetDomainKey,clusterId);
	}
	
	static public long countCpuCores() throws StorageException{
		return dao().countCpuCores();
	}
	
	static public long countCpuCores(String domainKey) throws StorageException{
		return dao().countCpuCores(domainKey);
	}
	
	static public long countCpuCores(String domainKey, String targetDomainKey) throws StorageException{
		return dao().countCpuCores(domainKey,targetDomainKey);
	}
	
	static public long countCpuCores(String domainKey, String targetDomainKey, String clusterId) throws StorageException{
		return dao().countCpuCores(domainKey,targetDomainKey,clusterId);
	}
	
	static public IClusterMachine get(String domainKey, String targetDomainKey, String clusterId, String machineId) throws StorageException{
		return dao().get(domainKey,targetDomainKey,clusterId,machineId);
	}
	
	static public void touch(String domainKey, String targetDomainKey, String clusterId, String machineId) throws StorageException{
		dao().touch(domainKey,targetDomainKey,clusterId,machineId);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}

	static public void delete(String domainKey, String targetDomainKey) throws StorageException{
		dao().delete(domainKey,targetDomainKey);
	}

	static public void delete(String domainKey, String targetDomainKey, String clusterId) throws StorageException{
		dao().delete(domainKey,targetDomainKey,clusterId);
	}

	static public void delete(String domainKey, String targetDomainKey, String clusterId, String machineId) throws StorageException{
		dao().delete(domainKey,targetDomainKey,clusterId,machineId);
	}

	static public Collection<IClusterMachine> list(String domainKey, String targetDomainKey, String clusterId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey,targetDomainKey,clusterId,page,pageSize);
	}
	
	static public Collection<IClusterMachine> listNext(String domainKey, String targetDomainKey, String clusterId, String machineId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey,targetDomainKey,machineId,clusterId,page,pageSize);
	}
	

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IClusterMachine.dao().createOrUpdate(this);
	}
	
	public default boolean isActive() {
		return MachineStatus.ACTIVE == getStatus();
	}
	
	@Override
	public default void copy(Object other) {
		
		IClusterMachine o = (IClusterMachine) other;
		
		setClusterId(o.getClusterId());
		setTargetDomainKey(o.getTargetDomainKey());
		setMachineId(o.getMachineId());
		setStatus(o.getStatus());
		setCpuSlots(o.getCpuSlots());
		setCpuCores(o.getCpuCores());
		setProperties(o.getProperties());
		
		IDomainObject.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public INameValuePair createNameValuePair(String name, String value);
	
	public String getClusterId();
	
	public void setClusterId(String clusterId);

	public String getTargetDomainKey();

	public void setTargetDomainKey(String targetDomainKey);

	public String getMachineId();
	
	public void setMachineId(String machineId);

	public MachineStatus getStatus();

	public void setStatus(MachineStatus status);
	
	public int getCpuSlots();
	
	public void setCpuSlots(int cpuSlots);

	public int getCpuCores();
	
	public void setCpuCores(int cpuCores);

	public <T extends INameValuePair> Collection<T> getProperties();

	public void setProperties(Collection<? extends INameValuePair> properties);
	
}
