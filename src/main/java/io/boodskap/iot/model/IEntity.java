/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
@JsonSerialize(as = IEntity.class)
public interface IEntity extends IStorageObject {

	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void copy(Object other) {
		
		IEntity o = (IEntity) other;
		
		setDescription(o.getDescription());
		setCreatedBy(o.getCreatedBy());
		setUpdatedBy(o.getUpdatedBy());
		setEntity(o.getEntity());

		IStorageObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void setEntity(Object entity) {
		if(null == entity) {
			setExtraProperties("{}");
			return;
		}
		dynamicProperties(entity);
	};

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@JsonIgnore
	public default Object getEntity() {
		return dynamicProperties();
	};

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getDescription();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setDescription(String description);
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getCreatedBy();
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setCreatedBy(String createdBy);
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getUpdatedBy();
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setUpdatedBy(String updatedBy);
	
}
