package io.boodskap.iot.model;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=IWorkFlowTriggerGroup.class)
public interface IWorkFlowTriggerGroup extends Serializable{

	//======================================
	// Attributes
	//======================================
	
}
