package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainJarClassDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainJarClass.class)
public interface IDomainJarClass extends IJarClass {

	//======================================
	// DAO Methods
	//======================================
	
	public static DomainJarClassDAO<IDomainJarClass> dao(){
		return DomainJarClassDAO.get();
	}
	
	static public void createOrUpdate(IDomainJarClass e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDomainJarClass> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainJarClass> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainJarClass> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IDomainJarClass> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainJarClass> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey,query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IDomainJarClass create(String domainKey, String loader, String fileName, String pkg, String name) throws StorageException{
		return dao().create(domainKey, loader, fileName, pkg, name);
	}
	
	static public IDomainJarClass get(String domainKey, String loader, String fileName, String pkg, String name) throws StorageException{
		return dao().get(domainKey, loader, fileName, pkg, name);
	}
	
	static public long count(String domainKey, String loader) throws StorageException{
		return dao().count(domainKey, loader);
	}
	
	static public long count(String domainKey, String loader, String fileName) throws StorageException{
		return dao().count(domainKey, loader, fileName);
	}
	
	static public long count(String domainKey, String loader, String fileName, String pkg) throws StorageException{
		return dao().count(domainKey, loader, fileName, pkg);
	}
	
	static public long countPackage(String domainKey, String loader, String pkg) throws StorageException{
		return dao().countPackage(domainKey, loader, pkg);
	}
	
	static public void delete(String domainKey, String loader) throws StorageException{
		dao().delete(domainKey, loader);
	}
	
	static public void delete(String domainKey, String loader, String fileName) throws StorageException{
		dao().delete(domainKey, loader, fileName);
	}
	
	static public Collection<IDomainJarClass> listClasses(String domainKey, String loader, int page, int pageSize) throws StorageException{
		return dao().listClasses(domainKey, loader, page, pageSize);
	}
	
	static public Collection<IDomainJarClass> listNextClasses(String domainKey, String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextClasses(domainKey, loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<IDomainJarClass> listPackageClasses(String domainKey, String loader, String pkg, int page, int pageSize) throws StorageException{
		return dao().listPackageClasses(domainKey, loader, pkg, page, pageSize);
	}
	
	static public Collection<IDomainJarClass> listNextPackageClasses(String domainKey, String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextPackageClasses(domainKey, loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<IDomainJarClass> listArchiveClasses(String domainKey, String loader, String fileName, int page, int pageSize) throws StorageException{
		return dao().listArchiveClasses(domainKey, loader, fileName, page, pageSize);
	}
	
	static public Collection<IDomainJarClass> listNextArchiveClasses(String domainKey, String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextArchiveClasses(domainKey, loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<IDomainJarClass> listArchivePackageClasses(String domainKey, String loader, String fileName, String pkg, int page, int pageSize) throws StorageException{
		return dao().listArchivePackageClasses(domainKey, loader, fileName, pkg, page, pageSize);
	}
	
	static public Collection<IDomainJarClass> listNextArchivePackageClasses(String domainKey, String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextArchivePackageClasses(domainKey, loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<String> listPackages(String domainKey, String loader, int page, int pageSize) throws StorageException{
		return dao().listPackages(domainKey, loader, page, pageSize);
	}
	
	static public Collection<String> listNextPackages(String domainKey, String loader, String fileName, String pkg, int page, int pageSize) throws StorageException{
		return dao().listNextPackages(domainKey, loader, fileName, pkg, page, pageSize);
	}
	
	static public Collection<String> listArchivePackages(String domainKey, String loader, String fileName, int page, int pageSize) throws StorageException{
		return dao().listArchivePackages(domainKey, loader, fileName, page, pageSize);
	}
	
	static public Collection<String> listNextArchivePackages(String domainKey, String loader, String fileName, String pkg, int page, int pageSize) throws StorageException{
		return dao().listNextArchivePackages(domainKey, loader, fileName, pkg, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		IDomainJarClass.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IDomainJarClass o = (IDomainJarClass) other;
		
		setDomainKey(o.getDomainKey());
		
		IJarClass.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getDomainKey();
	
	public void setDomainKey(String domainKey);
}
