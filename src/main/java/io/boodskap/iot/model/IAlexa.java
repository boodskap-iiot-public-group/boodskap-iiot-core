/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.AlexaDAO;
import io.boodskap.iot.dao.EntityIterator;

/**
 * @author Jegan Vincent
 * @see 5.0.0-00
 */
@JsonSerialize(as=IAlexa.class)
public interface IAlexa extends IDomainObject {
	
	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static AlexaDAO<IAlexa> dao(){
		return AlexaDAO.get();
	}
	
	static public void createOrUpdate(IAlexa e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IAlexa> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IAlexa> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IAlexa> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IAlexa> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IAlexa> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public IAlexa get(String domainKey, String alexaId) throws StorageException{
		return dao().get(domainKey, alexaId);
	}
	
	static public void delete(String domainKey, String alexaId) throws StorageException{
		dao().delete(domainKey, alexaId);
	}
	
	static public Collection<IAlexa> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}
	
	static public Collection<IAlexa> listNext(String domainKey, String alexaId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, alexaId, page, pageSize);
	}
	
	static public IAlexa create(String domainKey, String alexaId) {
		return dao().create(domainKey, alexaId);
	}

	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	@Override
	public default void save() {
		IAlexa.dao().createOrUpdate(this);
	}
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	@Override
	public default void copy(Object other) {
		
		IAlexa o = (IAlexa) other;
		
		setAlexaId(o.getAlexaId());
		setRuleType(o.getRuleType());
		setRuleName(o.getRuleName());
		setIntentName(o.getIntentName());
		setErrorResponse(o.getErrorResponse());
		
		IDomainObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public String getAlexaId();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setAlexaId(String alexaId);
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public String getRuleType();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setRuleType(String ruleType);
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public String getRuleName();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setRuleName(String ruleName);
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public String getIntentName();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setIntentName(String intentName);
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public String getErrorResponse();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setErrorResponse(String errorResponse);

}
