package io.boodskap.iot.model;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
@JsonSerialize(as=IDomainObject.class)
public interface IDomainContent extends Serializable{

	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void copy(Object other) {
		
		IDomainContent o = (IDomainContent) other;
		
		setDomainKey(o.getDomainKey());
		
	}
	
	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getDomainKey();
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setDomainKey(String domainKey);
}
