package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Date;

import org.json.JSONObject;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.BillingInvoiceDAO;
import io.boodskap.iot.dao.EntityIterator;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
public interface IBillingInvoice extends IDomainObject {

	//======================================
	// DAO Methods
	//======================================

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public static BillingInvoiceDAO<IBillingInvoice> dao(){
		return BillingInvoiceDAO.get();
	}
	
	static public void createOrUpdate(IBillingInvoice e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IBillingInvoice> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IBillingInvoice> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IBillingInvoice> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}
	
	static public EntityIterator<IBillingInvoice> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}

	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IBillingInvoice> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey,query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey,what,how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey,query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey,what,how);
	}
	
	static public IBillingInvoice create(String domainKey, String targetDomain, String invoiceId) {
		return dao().create(domainKey,targetDomain,invoiceId);
	}
	
	static public IBillingInvoice get(String domainKey, String targetDomain, String ivoiceId) throws StorageException{
		return dao().get(domainKey,targetDomain,ivoiceId);
	}
	
	static public void delete(String domainKey, String targetDomain) throws StorageException{
		dao().delete(domainKey,targetDomain);
	}
	
	static public void delete(String domainKey, String targetDomain, String ivoiceId) throws StorageException{
		dao().delete(domainKey,targetDomain,ivoiceId);
	}
	
	static public Collection<IBillingInvoice> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey,page,pageSize);
	}
	
	static public Collection<IBillingInvoice> listNext(String domainKey, String targetDomain, String ivoiceId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey,targetDomain,ivoiceId,page,pageSize);
	}

	
	//======================================
	// Default Methods
	//======================================

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void save() {
		IBillingInvoice.dao().createOrUpdate(this);
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void copy(Object other) {
		
		IBillingInvoice o = (IBillingInvoice) other;
		
		setTargetDomain(o.getTargetDomain());
		setInvoicename(o.getInvoicename());
		setInvoiceno(o.getInvoiceno());
		setFrequency(o.getFrequency());
		setFile(o.getFile());
		setGrandtotal(o.getGrandtotal());
		setStartdate(o.getStartdate());
		setEnddate(o.getEnddate());
		setObj(o.getObj());
		setInvoicetype(o.getInvoicetype());
		
		IDomainObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getTargetDomain();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setTargetDomain(String targetDomain);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getInvoicename();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setInvoicename(String invoicename);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getInvoiceno();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setInvoiceno(String invoiceno);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getFrequency();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setFrequency(String frequency);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getFile();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setFile(String file);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public double getGrandtotal();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setGrandtotal(double grandtotal);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public Date getStartdate();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setStartdate(Date startdate);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public Date getEnddate();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setEnddate(Date enddate);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getObj();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setObj(String obj);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getInvoicetype();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setInvoicetype(String invoicetype);

}
