/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IScheduledRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IScheduledRulehis program is distributed in the hope that it will be useful,
 * but WIIScheduledRuleHOUIScheduledRule ANY WARRANIScheduledRuleY; without even the implied warranty of
 * MERCHANIScheduledRuleABILIIScheduledRuleY or FIIScheduledRuleNESS FOR A PARIScheduledRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.ScheduledRuleDAO;

@JsonSerialize(as=IScheduledRule.class)
public interface IScheduledRule extends IRule {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static ScheduledRuleDAO<IScheduledRule> dao() {
		return ScheduledRuleDAO.get();
	}
	
	static public void createOrUpdate(IScheduledRule e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IScheduledRule> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IScheduledRule> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IScheduledRule> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IScheduledRule> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
		
	static public SearchResult<IScheduledRule> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public IScheduledRule create(String domainKey, String ruleId) throws StorageException{
		return dao().create(domainKey, ruleId);
	}

	static public IScheduledRule get(String domainKey, String ruleId) throws StorageException{
		return dao().get(domainKey, ruleId);
	}

	static public void delete(String domainKey, String ruleId) throws StorageException{
		dao().delete(domainKey, ruleId);
	}

	static public Collection<IScheduledRule> list(boolean load, String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, page, pageSize);
	}

	static public Collection<IScheduledRule> listNext(boolean load, String domainKey, String ruleId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, ruleId, page, pageSize);
	}


	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IScheduledRule.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getRuleId();

	public void setRuleId(String ruleId);

	public String getPattern();

	public void setPattern(String pattern);

}
