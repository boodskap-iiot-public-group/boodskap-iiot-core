/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainAssetGroupMemberDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainAssetGroupMember.class)
public interface IDomainAssetGroupMember extends IGroupMember {

	//======================================
	// DAO Methods
	//======================================
	
	public static DomainAssetGroupMemberDAO<IDomainAssetGroupMember> dao(){
		return DomainAssetGroupMemberDAO.get();
	}
	
	static public void createOrUpdate(IDomainAssetGroupMember e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDomainAssetGroupMember> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainAssetGroupMember> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainAssetGroupMember> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IDomainAssetGroupMember> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainAssetGroupMember> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IDomainAssetGroupMember create(String domainKey, String groupId, String assetId) {
		return dao().create(domainKey, groupId, assetId);
	}
	
	static public IDomainAssetGroupMember get(String domainKey, String groupId, String assetId) {
		return dao().get(domainKey, groupId, assetId);
	}
	
	static public void delete(String domainKey, String groupId) throws StorageException{
		dao().delete(domainKey, groupId);
	}
	
	static public void delete(String domainKey, String groupId, String assetId) throws StorageException{
		dao().delete(domainKey, groupId, assetId);
	}
	
	static public long count(String domainKey, String groupId) throws StorageException{
		return dao().count(domainKey, groupId);
	}

	static public EntityIterator<String> iterateMembers(String domainKey, String groupId) throws StorageException{
		return dao().iterateMembers(domainKey, groupId);
	}

	static public Collection<IDomainAssetGroupMember> list(String domainKey, String groupId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, groupId, page, pageSize);
	}

	static public Collection<IDomainAssetGroupMember> listNext(String domainKey, String groupId, String assetId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, groupId, assetId, page, pageSize);
	}


	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDomainAssetGroupMember.dao().createOrUpdate(this);
	}

}
