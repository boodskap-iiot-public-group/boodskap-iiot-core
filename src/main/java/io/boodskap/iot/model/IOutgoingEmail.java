/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OutgoingEmailDAO;

@JsonSerialize(as=IOutgoingEmail.class)
public interface IOutgoingEmail extends INotification{
	
	//======================================
	// DAO Methods
	//======================================
	
	public static OutgoingEmailDAO<IOutgoingEmail> dao() {
		return OutgoingEmailDAO.get();
	}

	static public void createOrUpdate(IOutgoingEmail e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IOutgoingEmail> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IOutgoingEmail> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IOutgoingEmail> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IOutgoingEmail> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IOutgoingEmail> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public IOutgoingEmail create(String domainKey, String id) throws StorageException{
		return dao().create(domainKey, id);
	}

	static public EntityIterator<String> loadIds() throws StorageException{
		return dao().loadIds();
	}

	static public IOutgoingEmail get(String domainKey, String id) throws StorageException{
		return dao().get(domainKey, id);
	}

	static public IOutgoingEmail get(String id) throws StorageException{
		return dao().get(id);
	}

	static public void delete(String domainKey, String id) throws StorageException{
		dao().delete(domainKey, id);
	}

	static public Collection<IOutgoingEmail> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}
	
	static public Collection<IOutgoingEmail> listNext(String domainKey, String id, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, id, page, pageSize);
	}

	
	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IOutgoingEmail.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getSubject();

	public void setSubject(String subject);

	public String getHtmlContent();

	public void setHtmlContent(String htmlContent);

	public String getDsResolrver();

	public void setDsResolrver(String dsResolrver);

}
