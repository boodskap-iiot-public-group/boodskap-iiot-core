/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * INamedRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * INamedRulehis program is distributed in the hope that it will be useful,
 * but WIINamedRuleHOUINamedRule ANY WARRANINamedRuleY; without even the implied warranty of
 * MERCHANINamedRuleABILIINamedRuleY or FIINamedRuleNESS FOR A PARINamedRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Map;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainProcessDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainProcess.class)
public interface IDomainProcess extends IRule  {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static DomainProcessDAO<IDomainProcess> dao(){
		return DomainProcessDAO.get();
	}

	static public void createOrUpdate(IDomainProcess e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDomainProcess> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainProcess> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainProcess> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IDomainProcess> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count();
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainProcess> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IDomainProcess create(String domainKey, String id) throws StorageException{
		return dao().create(domainKey, id);
	}

	static public IDomainProcess get(String domainKey, String id) throws StorageException{
		return dao().get(domainKey, id);
	}

	static public boolean has(String domainKey, String id) throws StorageException{
		return dao().has(domainKey, id);
	}

	static public void delete(String domainKey, String id) throws StorageException{
		dao().delete(domainKey, id);
	}

	static public Collection<IDomainProcess> list(boolean load, String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, page, pageSize);
	}

	static public Collection<IDomainProcess> listNext(boolean load, String domainKey, String id, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, id, page, pageSize);
	}

	static public Collection<String> listIds(String domainKey, int page, int pageSize) throws StorageException{
		return dao().listIds(domainKey, page, pageSize);
	}

	static public Collection<String> listIdsNext(String domainKey, String id, int page, int pageSize) throws StorageException{
		return dao().listIdsNext(domainKey, id, page, pageSize);
	}


	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDomainProcess.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getId();
	
	public void setId(String id);
	
	public Map<String, Object> getInput();
	
	public void setInput(Map<String, Object> input);
	
	public Map<String, Object> getOutput();
	
	public void setOutput(Map<String, Object> output);
}


