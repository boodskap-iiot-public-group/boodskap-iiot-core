/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.OTABatchState;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OTAModelBatchDAO;

@JsonSerialize(as=IOTAModelBatch.class)
public interface IOTAModelBatch extends IOTABatch {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static OTAModelBatchDAO<IOTAModelBatch> dao() {
		return OTAModelBatchDAO.get();
	}
	
	static public void createOrUpdate(IOTAModelBatch e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IOTAModelBatch> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IOTAModelBatch> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IOTAModelBatch> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IOTAModelBatch> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IOTAModelBatch> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IOTAModelBatch create(String domainKey, String batchId, String fromModel) throws StorageException{
		return dao().create(domainKey, batchId, fromModel);
	}

	static public IOTAModelBatch get(String domainKey, String batchId, String fromModel) throws StorageException{
		return dao().get(domainKey, batchId, fromModel);
	}

	static public void delete(String domainKey, String batchId) throws StorageException{
		dao().delete(domainKey, batchId);
	}

	static public long count(String domainKey, OTABatchState[] states) throws StorageException{
		return dao().count(domainKey, states);
	}

	static public Collection<IOTAModelBatch> list(String domainKey, OTABatchState[] states, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, states, page, pageSize);
	}

	static public Collection<IOTAModelBatch> listNext(String domainKey, OTABatchState[] states, String batchId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, states, batchId, page, pageSize);
	}

	static public Collection<IOTAModelBatch> search(String domainKey, OTABatchState[] states, String query, int pageSize) throws StorageException{
		return dao().search(domainKey, states, query, pageSize);
	}


	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IOTAModelBatch.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getFromModel();

	public void setFromModel(String fromModel);

}
