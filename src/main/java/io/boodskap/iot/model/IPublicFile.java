/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.PublicFileDAO;

@JsonSerialize(as=IPublicFile.class)
public interface IPublicFile extends IFile {

	//======================================
	// DAO Methods
	//======================================
	
	public static PublicFileDAO<IPublicFile> dao() {
		return  PublicFileDAO.get();
	}
	
	static public void createOrUpdate(IPublicFile e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IPublicFile> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IPublicFile> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IPublicFile> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public Class<? extends IFileContent> contentClazz(){
		return dao().contentClazz();
	}
	
	static public IPublicFile create(String fileId) throws StorageException{
		return dao().create(fileId);
	}

	static public IPublicFile get(String fileId) throws StorageException{
		return dao().get(fileId);
	}

	static public IFileContent getContent(String fileId) throws StorageException{
		return dao().getContent(fileId);
	}

	static public boolean has(String fileId) throws StorageException{
		return dao().has(fileId);
	}

	static public void deleteById(String fileId) throws StorageException{
		dao().deleteById(fileId);
	}

	static public void update(String fileId, String tags, String description) throws StorageException{
		dao().update(fileId, tags, description);
	}
	
	static public void update(String fileId, byte[] data, String mediaType)throws StorageException{
		dao().update(fileId, data, mediaType);
	}

	static public Collection<IPublicFile> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}
	
	static public Collection<IPublicFile> listNext(boolean load, String fileId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, fileId, page, pageSize);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IPublicFile.dao().createOrUpdate(this);
	}
}
