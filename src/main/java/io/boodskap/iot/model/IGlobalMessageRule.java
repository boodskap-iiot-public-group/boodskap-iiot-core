/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IMessageRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IMessageRulehis program is distributed in the hope that it will be useful,
 * but WIIMessageRuleHOUIMessageRule ANY WARRANIMessageRuleY; without even the implied warranty of
 * MERCHANIMessageRuleABILIIMessageRuleY or FIIMessageRuleNESS FOR A PARIMessageRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.GlobalMessageRuleDAO;

@JsonSerialize(as=IGlobalMessageRule.class)
public interface IGlobalMessageRule extends IRule {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static GlobalMessageRuleDAO<IGlobalMessageRule> dao() {
		return GlobalMessageRuleDAO.get();
	}
	
	static public void createOrUpdate(IGlobalMessageRule e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IGlobalMessageRule> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IGlobalMessageRule> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IGlobalMessageRule> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IGlobalMessageRule create(String specId) throws StorageException{
		return dao().create(specId);
	}

	static public IGlobalMessageRule get(String specId) throws StorageException{
		return dao().get(specId);
	}

	static public boolean has(String specId) throws StorageException{
		return dao().has(specId);
	}

	static public void delete(String specId) throws StorageException{
		dao().delete(specId);
	}

	static public Collection<String> listSpecs(int page, int pageSize) throws StorageException{
		return dao().listSpecs(page, pageSize);
	}

	static public Collection<String> listSpecsNext(String specId, int page, int pageSize) throws StorageException{
		return dao().listSpecsNext(specId, page, pageSize);
	}

	static public Collection<IGlobalMessageRule> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	static public Collection<IGlobalMessageRule> listNext(boolean load, String specId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, specId, page, pageSize);	
	}

	

	//======================================
	// Default Methods
	//===================================
	public default void save() {
		IGlobalMessageRule.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IGlobalMessageRule o = (IGlobalMessageRule) other;
		
		setSpecId(o.getSpecId());
		
		IRule.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getSpecId();

	public void setSpecId(String specId);

}
