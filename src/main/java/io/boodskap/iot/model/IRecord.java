package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Map;

import org.json.JSONObject;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.RecordDAO;

public interface IRecord {

	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static RecordDAO dao(){
		return RecordDAO.get();
	}
	
	static public EntityIterator<JSONObject> load(String domainKey, String specId) throws StorageException{
		return dao().load(domainKey, specId);
	}
	
	static public long count(String domainKey, String specId) throws StorageException{
		return dao().count(domainKey, specId);
	}
	
	static public void create(String domainKey, String specId, String recordId, String json) throws StorageException{
		createOrUpdate(domainKey, specId, recordId, new JSONObject(json));
	}
	
	static public void create(String domainKey, String specId, String recordId, Map<String, Object> map) throws StorageException{
		createOrUpdate(domainKey, specId, recordId, new JSONObject(map));
	}
	
	static public void create(String domainKey, String specId, String recordId, JSONObject e) throws StorageException{
		createOrUpdate(domainKey, specId, recordId, e);
	}
	
	static public void createOrUpdate(String domainKey, String specId, String recordId, JSONObject e) throws StorageException{
		dao().createOrUpdate(domainKey, specId, recordId, e);
	}
	
	static public JSONObject get(String domainKey, String specId, String recordId) throws StorageException{
		return dao().get(domainKey, specId, recordId);
	}
	
	static public void delete(String domainKey, String specId, String recordId) throws StorageException{
		dao().deleteByQuery(domainKey, specId, recordId);
	}
	
	static public void delete(String domainKey, String specId) throws StorageException{
		dao().delete(domainKey, specId);
	}
	
	static public Collection<JSONObject> list(String domainKey, String specId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, specId, page, pageSize);
	}

	static public Collection<JSONObject> listNext(String domainKey, String specId, String id, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, specId, id, page, pageSize);
	}

	static public SearchResult<JSONObject> searchByQuery(String domainKey, String specId, String sql, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, specId, sql, page, pageSize);
	}

	static public JSONObject selectByQuery(String domainKey, String specId, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, specId, what, how);
	}

	static public JSONObject deleteByQuery(String domainKey, String specId, String sql) throws StorageException{
		return dao().deleteByQuery(domainKey, specId, sql);
	}

	static public JSONObject updateByQuery(String domainKey, String specId, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, specId, what, how);
	}

}
