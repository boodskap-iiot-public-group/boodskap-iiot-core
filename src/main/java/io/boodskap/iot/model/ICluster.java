package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.ClusterDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=ICluster.class)
public interface ICluster extends IDomainObject {
	
	public static enum ClusterStatus{
		INACTIVE,
		ACTIVE,
		DISABLED,
		SUSPENDED,
		TERMINATED,
	}
	
	//======================================
	// DAO Methods
	//======================================
	
	public static ClusterDAO<ICluster> dao(){
		return ClusterDAO.get();
	}
	
	static public void createOrUpdate(ICluster e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends ICluster> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<ICluster> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<ICluster> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}

	static public ICluster create(String domainKey, String targetDomainKey, String clusterId) {
		return dao().create(domainKey,targetDomainKey,clusterId);
	}

	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public long count(String domainKey, String targetDomainKey) throws StorageException{
		return dao().count(domainKey,targetDomainKey);
	}
	
	static public ICluster get(String domainKey, String targetDomainKey, String clusterId) throws StorageException{
		return dao().get(domainKey,targetDomainKey,clusterId);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public void delete(String domainKey, String targetDomainKey) throws StorageException{
		dao().delete(domainKey,targetDomainKey);
	}
	
	static public void delete(String domainKey, String targetDomainKey, String clusterId) throws StorageException{
		dao().delete(domainKey,targetDomainKey,clusterId);
	}
	
	static public void touch(String domainKey, String targetDomainKey, String clusterId) throws StorageException{
		dao().touch(domainKey,targetDomainKey,clusterId);
	}
	
	static public Collection<ICluster> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey,page,pageSize);
	}
	
	static public Collection<ICluster> listNext(String domainKey, String targetDomainKey, String clusterId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey,targetDomainKey,clusterId,page,pageSize);
	}
		
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ICluster.dao().createOrUpdate(this);
	}
	
	@Override
	public default void copy(Object other) {
		
		ICluster o = (ICluster) other;
		
		setTargetDomainKey(o.getTargetDomainKey());
		setClusterId(o.getClusterId());
		setStatus(o.getStatus());
		setMachines(o.getMachines());
		setUsers(o.getUsers());
		setCores(o.getCores());
		setDevices(o.getDevices());
		setOrganizations(o.getOrganizations());
		
		IDomainObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getTargetDomainKey();

	public void setTargetDomainKey(String targetDomainKey);

	public String getClusterId();
	
	public void setClusterId(String clusterId);

	public ClusterStatus getStatus();

	public void setStatus(ClusterStatus status);

	public int getMachines();

	public void setMachines(int machines);

	public int getUsers();

	public void setUsers(int users);

	public int getCores();

	public void setCores(int cores);

	public int getDevices();

	public void setDevices(int devices);

	public int getOrganizations();

	public void setOrganizations(int organizations);
}
