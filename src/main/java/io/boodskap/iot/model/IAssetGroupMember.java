/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.AssetGroupMemberDAO;
import io.boodskap.iot.dao.EntityIterator;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
@JsonSerialize(as=IAssetGroupMember.class)
public interface IAssetGroupMember extends IGroupMember {

	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public static AssetGroupMemberDAO<IAssetGroupMember> dao(){
		return AssetGroupMemberDAO.get();
	}
	
	static public void createOrUpdate(IAssetGroupMember e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IAssetGroupMember> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IAssetGroupMember> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IAssetGroupMember> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize);

	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}
	
	static public EntityIterator<IAssetGroupMember> load(String domainKey) throws StorageException{
		return dao().load(domainKey);

	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IAssetGroupMember> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey,query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey,what,how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey,query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey,what,how);
	}
	
	static public IAssetGroupMember create(String domainKey, String ownerAssetId, String groupId, String memberId) {
		return dao().create(domainKey,ownerAssetId,groupId,memberId);

	}

	static public IAssetGroupMember get(String domainKey, String ownerAssetId, String groupId, String memberId) throws StorageException{
		return dao().get(domainKey,ownerAssetId,groupId,memberId);
	}
	
	static public long count(String domainKey, String ownerAssetId, String groupId) throws StorageException{
		return dao().count(domainKey,ownerAssetId,groupId);
	}

	static public void delete(String domainKey, String ownerAssetId) throws StorageException{
		dao().delete(domainKey,ownerAssetId);
	}

	static public void delete(String domainKey, String ownerAssetId, String groupId) throws StorageException{
		dao().delete(domainKey,ownerAssetId,groupId);
	}

	static public void delete(String domainKey, String ownerAssetId, String groupId, String assetId) throws StorageException{
		dao().delete(domainKey,ownerAssetId,groupId,assetId);
	}
	
	static public EntityIterator<String> iterateMembers(String domainKey, String ownerAssetId, String groupId) throws StorageException{
		return dao().iterateMembers(domainKey,ownerAssetId,groupId);
	}

	static public Collection<IAssetGroupMember> list(String domainKey, String ownerAssetId, String groupId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey,ownerAssetId,groupId,page,pageSize);
	}

	static public Collection<IAssetGroupMember> listNext(String domainKey, String ownerAssetId, String groupId, String assetId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey,ownerAssetId,groupId,assetId,page,pageSize);
	}


	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void save() {
		IAssetGroupMember.dao().createOrUpdate(this);
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void copy(Object other) {
		
		IAssetGroupMember o = (IAssetGroupMember) other;
		
		setOwnerAssetId(o.getOwnerAssetId());
		
		IGroupMember.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getOwnerAssetId();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setOwnerAssetId(String ownerAssetId);

}
