/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.MessageSpecDAO;

public interface IMessageSpecification extends IDomainObject {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static MessageSpecDAO<IMessageSpecification> dao() {
		return  MessageSpecDAO.get();
	}
	
	public static boolean canAddField(){
		return dao().canAddField();
	}
	
	public static boolean canDropField() {
		return dao().canDropField();
	}
	
	public static boolean canModifyField() {
		return dao().canModifyField();
	}
	
	public static IMessageSpecification create(String domainKey, String specId) throws StorageException{
		return dao().create(domainKey, specId);
	}

	static public void createOrUpdate(IMessageSpecification e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IMessageSpecification> clazz(){
		return dao().clazz();
	}
	
	static public Class<? extends IMessageField> fieldClazz(){
		return dao().fieldClazz();
	}
	
	public static IMessageField createField(String domainKey, String specId, String name) throws StorageException{
		return dao().createField(domainKey, specId, name);
	}

	public static IMessageSpecification get(String domainKey, String specId) throws StorageException{
		return dao().get(domainKey, specId);
	}

	public static void delete(String domainKey, String specId) throws StorageException{
		dao().delete(domainKey, specId);
	}

	public static Collection<IMessageSpecification> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}

	public static Collection<IMessageSpecification> listNext(String domainKey, String specId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, specId, page, pageSize);
	}

	static public EntityIterator<IMessageSpecification> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IMessageSpecification> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IMessageSpecification> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IMessageSpecification> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IMessageSpecification.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IMessageSpecification o = (IMessageSpecification) other;
		
		setSpecId(o.getSpecId());
		setFields(o.getFields());
		
		IDomainObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getSpecId();

	public void setSpecId(String specId);

	public Collection<? extends IMessageField> getFields();
	
	public void setFields(Collection<? extends IMessageField> fields);

	//======================================
	// Helpers
	//======================================
	
	public void addField(IMessageField field) throws StorageException;
	
	public void modifyField(IMessageField field) throws StorageException;
	
	public void removeField(String name) throws StorageException;

}
