/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IScheduledRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IScheduledRulehis program is distributed in the hope that it will be useful,
 * but WIIScheduledRuleHOUIScheduledRule ANY WARRANIScheduledRuleY; without even the implied warranty of
 * MERCHANIScheduledRuleABILIIScheduledRuleY or FIIScheduledRuleNESS FOR A PARIScheduledRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.GlobalScheduledRuleDAO;

@JsonSerialize(as=IGlobalScheduledRule.class)
public interface IGlobalScheduledRule extends ISystemRule {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static GlobalScheduledRuleDAO<IGlobalScheduledRule> dao() {
		return GlobalScheduledRuleDAO.get();
	}
	
	static public void createOrUpdate(IGlobalScheduledRule e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IGlobalScheduledRule> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IGlobalScheduledRule> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IGlobalScheduledRule> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IGlobalScheduledRule create(String ruleId) throws StorageException{
		return dao().create(ruleId);
	}

	static public IGlobalScheduledRule get(String ruleId) throws StorageException{
		return dao().get(ruleId);
	}

	static public void delete(String ruleId) throws StorageException{
		dao().delete(ruleId);
	}

	static public Collection<IGlobalScheduledRule> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	static public Collection<IGlobalScheduledRule> listNext(boolean load, String ruleId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, ruleId, page, pageSize);
	}


	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IGlobalScheduledRule.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getRuleId();

	public void setRuleId(String ruleId);

	public String getPattern();

	public void setPattern(String pattern);

}
