/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IUserAssetGroupMemberhis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IUserAssetGroupMemberhis program is distributed in the hope that it will be useful,
 * but WIIUserAssetGroupMemberHOUIUserAssetGroupMember ANY WARRANIUserAssetGroupMemberY; without even the implied warranty of
 * MERCHANIUserAssetGroupMemberABILIIUserAssetGroupMemberY or FIIUserAssetGroupMemberNESS FOR A PARIUserAssetGroupMemberICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.UserAssetGroupMemberDAO;

@JsonSerialize(as=IUserAssetGroupMember.class)
public interface IUserAssetGroupMember extends IGroupMember {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static UserAssetGroupMemberDAO<IUserAssetGroupMember> dao(){
		return UserAssetGroupMemberDAO.get();
	}

	public static Class<? extends IUserAssetGroupMember> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<IUserAssetGroupMember> load() throws StorageException{
		return dao().load();
	}
	
	public static EntityIterator<IUserAssetGroupMember> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}

	public static IUserAssetGroupMember create(String domainKey, String ownerUserId, String groupId, String memberAssetId) throws StorageException{
		return dao().create(domainKey, ownerUserId, groupId, memberAssetId);
	}

	public static IUserAssetGroupMember get(String domainKey, String ownerUserId, String groupId, String memberAssetId) throws StorageException{
		return dao().get(domainKey, ownerUserId, groupId, memberAssetId);
	}

	public static void delete(String domainKey, String ownerUserId, String groupId) throws StorageException{
		dao().delete(domainKey, ownerUserId, groupId);
	}

	public static void delete(String domainKey, String ownerUserId) throws StorageException{
		dao().delete(domainKey, ownerUserId);
	}

	public static void delete(String domainKey, String ownerUserId, String groupId, String memberAssetId) throws StorageException{
		dao().delete(domainKey, ownerUserId, groupId, memberAssetId);
	}

	public static long count(String domainKey, String ownerUserId, String groupId) throws StorageException{
		return dao().count(domainKey, ownerUserId, groupId);
	}

	public static EntityIterator<String> iterateMembers(String domainKey, String ownerUserId, String groupId) throws StorageException{
		return dao().iterateMembers(domainKey, ownerUserId, groupId);
	}

	public static Collection<IUserAssetGroupMember> list(String domainKey, String ownerUserId, String groupId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, ownerUserId, groupId, page, pageSize);
	}

	public static Collection<IUserAssetGroupMember> listNext(String domainKey, String ownerUserId, String groupId, String memberAssetId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, ownerUserId, groupId, memberAssetId, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IUserAssetGroupMember.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getOwnerUserId();

	public void setOwnerUserId(String ownerUserId);

}
