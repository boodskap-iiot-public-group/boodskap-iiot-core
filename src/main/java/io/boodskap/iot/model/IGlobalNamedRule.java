/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * INamedRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * INamedRulehis program is distributed in the hope that it will be useful,
 * but WIINamedRuleHOUINamedRule ANY WARRANINamedRuleY; without even the implied warranty of
 * MERCHANINamedRuleABILIINamedRuleY or FIINamedRuleNESS FOR A PARINamedRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.GlobalNamedRuleDAO;

@JsonSerialize(as=IGlobalNamedRule.class)
public interface IGlobalNamedRule extends ISystemRule  {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static GlobalNamedRuleDAO<IGlobalNamedRule> dao(){
		return GlobalNamedRuleDAO.get();
	}

	static public void createOrUpdate(IGlobalNamedRule e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IGlobalNamedRule> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IGlobalNamedRule> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IGlobalNamedRule> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public IGlobalNamedRule create(String name) throws StorageException{
		return dao().create(name);
	}

	static public IGlobalNamedRule get(String name) throws StorageException{
		return dao().get(name);
	}

	static public boolean has(String name) throws StorageException{
		return dao().has(name);
	}

	static public void delete(String name) throws StorageException{
		dao().delete(name);
	}

	static public Collection<IGlobalNamedRule> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	static public Collection<IGlobalNamedRule> listNext(boolean load, String name, int page, int pageSize) throws StorageException{
		return dao().listNext(load, name, page, pageSize);
	}

	static public Collection<String> listNames(int page, int pageSize) throws StorageException{
		return dao().listNames(page, pageSize);
	}

	static public Collection<String> listNamesNext(String name, int page, int pageSize) throws StorageException{
		return dao().listNamesNext(name, page, pageSize);
	}

	

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IGlobalNamedRule.dao().createOrUpdate(this);
	}
	
}


