package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainGroovyArchiveFileDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainGroovyArchiveFile.class)
public interface IDomainGroovyArchiveFile extends IJarFile {

	//======================================
	// DAO Methods
	//======================================
	
	public static DomainGroovyArchiveFileDAO<IDomainGroovyArchiveFile> dao(){
		return DomainGroovyArchiveFileDAO.get();
	}
	
	static public void createOrUpdate(IDomainGroovyArchiveFile e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDomainGroovyArchiveFile> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainGroovyArchiveFile> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainGroovyArchiveFile> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IDomainGroovyArchiveFile> load(String domainKey) throws StorageException{
		return dao().load();
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainGroovyArchiveFile> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IDomainGroovyArchiveFile create(String domainKey, String loader, String fileName){
		return dao().create(domainKey, loader, fileName);
	}
	
	static public IDomainGroovyArchiveFile get(String domainKey, String loader, String fileName, boolean loadContent) throws StorageException{
		return dao().get(domainKey, loader, fileName, loadContent);
	}
	
	static public long count(String domainKey, String loader) throws StorageException{
		return dao().count(domainKey, loader);
	}
	
	static public void delete(String domainKey, String loader) throws StorageException{
		dao().delete(domainKey, loader);
	}
	
	static public void delete(String domainKey, String loader, String fileName) throws StorageException{
		dao().delete(domainKey, loader, fileName);
	}
	
	static public Collection<IDomainGroovyArchiveFile> list(String domainKey, String loader, int page, int pageSize, boolean loadContent) throws StorageException{
		return dao().list(domainKey, loader, page, pageSize, loadContent);
	}
	
	static public Collection<IDomainGroovyArchiveFile> listNext(String domainKey, String loader, String fileName, int page, int pageSize, boolean loadContent) throws StorageException{
		return dao().listNext(domainKey, loader, fileName, page, pageSize, loadContent);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		dao().createOrUpdate(this);
	}
	
	@Override
	public default void copy(Object other) {
		
		IDomainGroovyArchiveFile o = (IDomainGroovyArchiveFile) other;
		
		setDomainKey(o.getDomainKey());
		
		IJarFile.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getDomainKey();
	
	public void setDomainKey(String domainKey);

}
