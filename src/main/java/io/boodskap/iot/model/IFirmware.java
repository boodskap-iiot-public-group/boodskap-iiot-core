/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IFirmwarehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IFirmwarehis program is distributed in the hope that it will be useful,
 * but WIIFirmwareHOUIFirmware ANY WARRANIFirmwareY; without even the implied warranty of
 * MERCHANIFirmwareABILIIFirmwareY or FIIFirmwareNESS FOR A PARIFirmwareICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.FirmwareDAO;

@JsonSerialize(as=IFirmware.class)
public interface IFirmware extends IFileContent, IDomainObject{

	//======================================
	// DAO Methods
	//======================================
	
	public static FirmwareDAO<IFirmware> dao(){
		return FirmwareDAO.get();
	}

	static public void createOrUpdate(IFirmware e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IFirmware> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IFirmware> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IFirmware> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IFirmware> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IFirmware> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IFirmware create(String domainKey, String deviceModel, String version) throws StorageException{
		return dao().create(domainKey, deviceModel, version);
	}

	static public IFirmware get(String domainKey, String deviceModel, String version, boolean loadContent) throws StorageException{
		return dao().get(domainKey, deviceModel, version, loadContent);
	}

	static public void delete(String domainKey, String deviceModel) throws StorageException{
		dao().delete(domainKey, deviceModel);
	}

	static public void delete(String domainKey, String deviceModel, String version) throws StorageException{
		dao().delete(domainKey, deviceModel, version);
	}

	static public long count(String domainKey, String deviceModel) throws StorageException{
		return dao().count(domainKey, deviceModel);
	}
	
	static public Collection<IFirmware> list(String domainKey, String deviceModel, int page, int pageSize, boolean loadContent) throws StorageException{
		return dao().list(domainKey, deviceModel, page, pageSize, loadContent);
	}

	static public Collection<IFirmware> listNext( String domainKey, String deviceModel, String version, int page, int pageSize, boolean loadContent) throws StorageException{
		return dao().listNext(domainKey, deviceModel, version, page, pageSize, loadContent);
	}

	//======================================
	// Default methods
	//======================================
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void save() throws StorageException{
		IFirmware.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getDeviceModel();
	
	public void setDeviceModel(String deviceModel);

	public String getVersion();

	public void setVersion(String version);

	public String getFileName();

	public void setFileName(String fileName);

}
