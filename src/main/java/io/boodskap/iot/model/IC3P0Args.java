package io.boodskap.iot.model;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Jegan Vincent
 * @see 5.0.0-00
 */
@JsonSerialize(as=IC3P0Args.class)
public interface IC3P0Args extends Serializable{

	public String getJdbcUrl();

	public void setJdbcUrl(String jdbcUrl);

	public String getUser();

	public void setUser(String user);

	public String getPassword();

	public void setPassword(String password);

	public String getDriverClass();

	public void setDriverClass(String driverClass);

	public Integer getAcquireIncrement();

	public void setAcquireIncrement(Integer acquireIncrement);

	public Integer getAcquireRetryAttempts();

	public void setAcquireRetryAttempts(Integer acquireRetryAttempts);

	public Integer getAcquireRetryDelay();

	public void setAcquireRetryDelay(Integer acquireRetryDelay);

	public Boolean getAutoCommitOnClose();

	public void setAutoCommitOnClose(Boolean autoCommitOnClose);

	public String getAutomaticTestTable();

	public void setAutomaticTestTable(String automaticTestTable);

	public Boolean getBreakAfterAcquireFailure();

	public void setBreakAfterAcquireFailure(Boolean breakAfterAcquireFailure);

	public Integer getCheckoutTimeout();

	public void setCheckoutTimeout(Integer checkoutTimeout);

	public Boolean getDebugUnreturnedConnectionStackTraces();

	public void setDebugUnreturnedConnectionStackTraces(Boolean debugUnreturnedConnectionStackTraces);

	public Boolean getForceIgnoreUnresolvedTransactions();

	public void setForceIgnoreUnresolvedTransactions(Boolean forceIgnoreUnresolvedTransactions);

	public Boolean getForceSynchronousCheckins();

	public void setForceSynchronousCheckins(Boolean forceSynchronousCheckins);

	public Boolean getForceUseNamedDriverClass();

	public void setForceUseNamedDriverClass(Boolean forceUseNamedDriverClass);

	public String getIdentityToken();

	public void setIdentityToken(String identityToken);

	public Integer getIdleConnectionTestPeriod();

	public void setIdleConnectionTestPeriod(Integer idleConnectionTestPeriod);

	public Integer getInitialPoolSize();

	public void setInitialPoolSize(Integer initialPoolSize);

	public Integer getLoginTimeout();

	public void setLoginTimeout(Integer loginTimeout);

	public Integer getMaxAdministrativeTaskTime();

	public void setMaxAdministrativeTaskTime(Integer maxAdministrativeTaskTime);

	public Integer getMaxConnectionAge();

	public void setMaxConnectionAge(Integer maxConnectionAge);

	public Integer getMaxIdleTime();

	public void setMaxIdleTime(Integer maxIdleTime);

	public Integer getMaxIdleTimeExcessConnections();

	public void setMaxIdleTimeExcessConnections(Integer maxIdleTimeExcessConnections);

	public Integer getMaxPoolSize();

	public void setMaxPoolSize(Integer maxPoolSize);

	public Integer getMaxStatements();

	public void setMaxStatements(Integer maxStatements);

	public Integer getMaxStatementsPerConnection();

	public void setMaxStatementsPerConnection(Integer maxStatementsPerConnection);

	public Integer getMinPoolSize();

	public void setMinPoolSize(Integer minPoolSize);

	public Integer getNumHelperThreads();

	public void setNumHelperThreads(Integer numHelperThreads);

	public String getOverrideDefaultPassword();

	public void setOverrideDefaultPassword(String overrideDefaultPassword);

	public String getOverrideDefaultUser();

	public void setOverrideDefaultUser(String overrideDefaultUser);

	public String getPreferredTestQuery();

	public void setPreferredTestQuery(String preferredTestQuery);

	public Boolean getPrivilegeSpawnedThreads();

	public void setPrivilegeSpawnedThreads(Boolean privilegeSpawnedThreads);

	public Integer getPropertyCycle();

	public void setPropertyCycle(Integer propertyCycle);

	public Integer getStatementCacheNumDeferredCloseThreads();

	public void setStatementCacheNumDeferredCloseThreads(Integer statementCacheNumDeferredCloseThreads);

	public Boolean getTestConnectionOnCheckin();

	public void setTestConnectionOnCheckin(Boolean testConnectionOnCheckin);

	public Boolean getTestConnectionOnCheckout();

	public void setTestConnectionOnCheckout(Boolean testConnectionOnCheckout);

	public Integer getUnreturnedConnectionTimeout();

	public void setUnreturnedConnectionTimeout(Integer unreturnedConnectionTimeout);

	public String getUserOverridesAsString();

	public void setUserOverridesAsString(String userOverridesAsString);

	public Boolean getUsesTraditionalReflectiveProxies();

	public void setUsesTraditionalReflectiveProxies(Boolean usesTraditionalReflectiveProxies);

}
