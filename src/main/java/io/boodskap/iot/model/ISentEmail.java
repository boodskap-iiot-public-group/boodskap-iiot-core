/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * ISentEmailhis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * ISentEmailhis program is distributed in the hope that it will be useful,
 * but WIISentEmailHOUISentEmail ANY WARRANISentEmailY; without even the implied warranty of
 * MERCHANISentEmailABILIISentEmailY or FIISentEmailNESS FOR A PARISentEmailICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.SentEmailDAO;

@JsonSerialize(as=ISentEmail.class)
public interface ISentEmail extends ISentNotification {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static SentEmailDAO<ISentEmail> dao() {
		return SentEmailDAO.get();
	}
	
	static public void createOrUpdate(ISentEmail e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends ISentEmail> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<ISentEmail> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<ISentEmail> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<ISentEmail> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<ISentEmail> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public Class<? extends IProgress> progressClazz(){
		return dao().progressClazz();
	}
	
	static public Class<? extends IResponse> responseClazz(){
		return dao().responseClazz();
	}
	
	static public ISentEmail create(String domainKey, String notificationId) throws StorageException{
		return dao().create(domainKey, notificationId);
	}

	static public ISentEmail get(String notificationId) throws StorageException{
		return dao().get(notificationId);
	}

	static public ISentEmail get(String domainKey, String notificationId) throws StorageException{
		return dao().get(domainKey, notificationId);
	}

	static public void delete(String domainKey, String notificationId) throws StorageException{
		dao().delete(domainKey, notificationId);
	}

	static public Collection<ISentEmail> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}

	static public Collection<ISentEmail> listNext(String domainKey, String notificationId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, notificationId, page, pageSize);
	}

	static public Collection<? extends IProgress> listProgresses(String domainKey, String notificationId) throws StorageException{
		return dao().listProgresses(domainKey, notificationId);
	}

	static public Collection<? extends IResponse> listResponses(String domainKey, String notificationId) throws StorageException{
		return dao().listResponses(domainKey, notificationId);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ISentEmail.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getSendor();

	public void setSendor(String sendor);

	public String getHtmlContent();

	public void setHtmlContent(String htmlContent);

	public String getDsResolrver();

	public void setDsResolrver(String dsResolrver);

	//======================================
	// Helpers
	//======================================
	
	public IProgress createProgress(String content) throws StorageException;
	
	public IResponse createResponse(String content) throws StorageException;

}
