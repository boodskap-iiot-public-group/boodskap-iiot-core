/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.SystemAssetEntityDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=ISystemAssetEntity.class)
public interface ISystemAssetEntity extends ISystemEntity {

	//======================================
	// DAO Methods
	//======================================
	
	public static SystemAssetEntityDAO<ISystemAssetEntity> dao(){
		return SystemAssetEntityDAO.get();
	}

	static public void createOrUpdate(ISystemAssetEntity e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends ISystemAssetEntity> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<ISystemAssetEntity> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<ISystemAssetEntity> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<ISystemAssetEntity> load(String assetId) throws StorageException{
		return dao().load(assetId);
	}
	
	static public ISystemAssetEntity create(String assetId, String entityType, String entityId) throws StorageException{
		return dao().create(assetId, entityType, entityId);
	}
	
	static public ISystemAssetEntity get(String assetId, String entityType, String entityId) throws StorageException{
		return dao().get(assetId, entityType, entityId);
	}
	
	static public long count(String assetId) throws StorageException{
		return dao().count(assetId);
	}
	
	static public void delete(String assetId) throws StorageException{
		dao().delete(assetId);
	}
	
	static public void delete(String assetId, String entityType) throws StorageException{
		dao().delete(assetId, entityType);
	}
	
	static public void delete(String assetId, String entityType, String entityId) throws StorageException{
		dao().delete(assetId, entityType, entityId);
	}
	
	static public Collection<ISystemAssetEntity> list(String assetId, String entityType, int page, int pageSize) throws StorageException{
		return dao().list(assetId, entityType, page, pageSize);
	}
	
	static public Collection<ISystemAssetEntity> listNext(String assetId, String entityType, String entityId, int page, int pageSize) throws StorageException{
		return dao().listNext(assetId, entityType, entityId, page, pageSize);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ISystemAssetEntity.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		ISystemAssetEntity o = (ISystemAssetEntity) other;
		
		setAssetId(o.getAssetId());
		
		ISystemEntity.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getAssetId();
	
	public void setAssetId(String assetId);
	
}
