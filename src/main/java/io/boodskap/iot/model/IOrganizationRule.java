package io.boodskap.iot.model;

public interface IOrganizationRule extends IRule {

	//======================================
	// Attributes
	//======================================
	
	public String getOrgId();
	
	public void setOrgId(String orgId);
	
}
