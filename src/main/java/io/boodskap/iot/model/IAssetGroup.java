/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.AssetGroupDAO;
import io.boodskap.iot.dao.EntityIterator;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
@JsonSerialize(as=IAssetGroup.class)
public interface IAssetGroup extends IGroup {

	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public static AssetGroupDAO<IAssetGroup> dao(){
		return AssetGroupDAO.get();
	}
	
	static public void createOrUpdate(IAssetGroup e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IAssetGroup> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IAssetGroup> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IAssetGroup> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}
	
	static public EntityIterator<IAssetGroup> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IAssetGroup> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey,query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey,what,how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey,query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey,what,how);
	}
	
	static public IAssetGroup create(String domainKey, String ownerAssetId, String groupId) {
		return dao().create(domainKey,ownerAssetId,groupId);
	}
	
	static public IAssetGroup get(String domainKey, String ownerAssetId, String groupId) throws StorageException{
		return dao().get(domainKey,ownerAssetId,groupId);
	}

	static public void delete(String domainKey, String ownerAssetId, String groupId) throws StorageException{
		dao().delete(domainKey,ownerAssetId,groupId);
	}

	static public void delete(String domainKey, String ownerAssetId) throws StorageException{
		dao().delete(domainKey,ownerAssetId);
	}

	static public long count(String domainKey, String ownerAssetId) throws StorageException{
		return dao().count(domainKey,ownerAssetId);
	}

	static public Collection<IAssetGroup> list(String domainKey, String ownerAssetId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey,ownerAssetId,page,pageSize);
	}

	static public Collection<IAssetGroup> listNext(String domainKey, String ownerAssetId, String groupId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey,ownerAssetId,groupId,page,pageSize);
	}



	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void save() {
		IAssetGroup.dao().createOrUpdate(this);
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void copy(Object other) {
		
		IAssetGroup o = (IAssetGroup) other;
		
		setOwnerAssetId(o.getOwnerAssetId());
		
		IGroup.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getOwnerAssetId();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setOwnerAssetId(String ownerAssetId);

}
