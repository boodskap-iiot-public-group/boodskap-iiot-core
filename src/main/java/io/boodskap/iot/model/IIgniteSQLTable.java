package io.boodskap.iot.model;

import java.util.Collection;
import java.util.List;

import org.json.JSONObject;

import io.boodskap.iot.CacheAtomicityMode;
import io.boodskap.iot.CacheMode;
import io.boodskap.iot.CacheWriteSynchronizationMode;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.IgniteSQLTableDAO;

public interface IIgniteSQLTable extends IDomainObject {

	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static IgniteSQLTableDAO<IIgniteSQLTable> dao(){
		return IgniteSQLTableDAO.get();
	}
	
	static public void createOrUpdate(IIgniteSQLTable e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IIgniteSQLTable> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IIgniteSQLTable> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IIgniteSQLTable> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IIgniteSQLTable> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IIgniteSQLTable> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public IIgniteSQLTable create(String domainKey, String name){
		return dao().create(domainKey, name);
	}
	
	static public IIgniteSQLTable get(String domainKey, String name) throws StorageException{
		return dao().get(domainKey, name);
	}
	
	static public void delete(String domainKey, String name) throws StorageException{
		dao().delete(domainKey, name);
	}
	
	static public Collection<IIgniteSQLTable> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}
	
	static public Collection<IIgniteSQLTable> listNext(String domainKey, String name, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, name, page, pageSize);
	}

	

	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	@Override
	public default void save() {
		IIgniteSQLTable.dao().createOrUpdate(this);
	}
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	@Override
	public default void copy(Object other) {
		
		IIgniteSQLTable o = (IIgniteSQLTable) other;
		
		setFields(o.getFields());
		setPrimaryKeyFields(o.getPrimaryKeyFields());
		setIndexFields(o.getIndexFields());
		setAffinityField(o.getAffinityField());
		setBackups(o.getBackups());
		setCacheMode(o.getCacheMode());
		setCacheAtomicityMode(o.getCacheAtomicityMode());
		setCacheWriteSynchronizationMode(o.getCacheWriteSynchronizationMode());
		
		IDomainObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public Collection<? extends IIgniteSQLField> getFields();

	public void setFields(Collection<? extends IIgniteSQLField> fields);

	public List<String> getPrimaryKeyFields();

	public void setPrimaryKeyFields(List<String> primaryKeyFields);

	public List<String> getIndexFields();

	public void setIndexFields(List<String> indexFields);
	
	public String getAffinityField();
	
	public void setAffinityField(String affinityField);
	
	public Integer getBackups();
	
	public void setBackups(Integer backups);
	
	public CacheMode getCacheMode();
	
	public void setCacheMode(CacheMode cacheMode);
	
	public CacheAtomicityMode getCacheAtomicityMode();

	public void setCacheAtomicityMode(CacheAtomicityMode cacheAtomicityMode);
	
	public CacheWriteSynchronizationMode getCacheWriteSynchronizationMode();
	
	public void setCacheWriteSynchronizationMode(CacheWriteSynchronizationMode cacheWriteSynchronizationMode);

}
