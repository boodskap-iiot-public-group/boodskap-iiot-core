/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DeviceFriendDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDeviceFriend.class)
public interface IDeviceFriend extends IDomainObject {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static DeviceFriendDAO<IDeviceFriend> dao(){
		return DeviceFriendDAO.get();
	}
	
	static public void createOrUpdate(IDeviceFriend e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDeviceFriend> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDeviceFriend> load() throws StorageException{
		return dao().load();
	}
		
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDeviceFriend> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IDeviceFriend> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDeviceFriend> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IDeviceFriend create(String domainKey, String deviceId, String friendId) {
		return dao().create(domainKey, deviceId, friendId);
	}

	static public IDeviceFriend get(String domainKey, String deviceId, String friendId) throws StorageException{
		return dao().get(domainKey, deviceId, friendId);
	}

	static public void delete(String domainKey, String deviceId, String friendId) throws StorageException{
		dao().delete(domainKey, deviceId, friendId);
	}

	static public void delete(String domainKey, String deviceId) throws StorageException{
		dao().delete(domainKey, deviceId);
	}

	static public long count(String domainKey, String deviceId) throws StorageException{
		return dao().count(domainKey, deviceId);
	}

	static public EntityIterator<IDeviceFriend> load(String domainKey, String deviceId) throws StorageException{
		return dao().load(domainKey, deviceId);
	}

	static public Collection<IDeviceFriend> list(String domainKey, String deviceId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, deviceId, page, pageSize);
	}

	static public Collection<IDeviceFriend> listNext(String domainKey, String deviceId, String friendId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, deviceId,friendId, page, pageSize);
	}


	
	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDeviceFriend.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IDeviceFriend o = (IDeviceFriend) other;
		
		setDeviceId(o.getDeviceId());
		setFriendId(o.getFriendId());
		
		IDomainObject.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getDeviceId();

	public void setDeviceId(String deviceId);

	public String getFriendId();

	public void setFriendId(String friendId);
	
}
