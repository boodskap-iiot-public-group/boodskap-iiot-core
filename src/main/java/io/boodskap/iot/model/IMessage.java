package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.MessageDAO;

public interface IMessage {

	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static MessageDAO dao(){
		return MessageDAO.get();
	}
	
	public static EntityIterator<JSONObject> load(String domainKey, String specId) throws StorageException{
		return dao().load(domainKey, specId);
	}
	
	public static long count(String domainKey, String specId) throws StorageException{
		return dao().count(domainKey, specId);
	}
	
	public static void createOrUpdate(String domainKey, String specId, String messageId, JSONObject e) throws StorageException{
		dao().createOrUpdate(domainKey, specId, messageId, e);
	}
	
	public static JSONObject get(String domainKey, String specId, String messageId) throws StorageException{
		return dao().get(domainKey, specId, messageId);
	}
	
	public static void delete(String domainKey, String specId, String messageId) throws StorageException{
		dao().delete(domainKey, specId, messageId);
	}
	
	public static void delete(String domainKey, String specId) throws StorageException{
		dao().delete(domainKey, specId);
	}
	
	public static Collection<JSONObject> list(String domainKey, String specId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, specId, page, pageSize);
	}

	public static Collection<JSONObject> listNext(String domainKey, String specId, String messageId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, specId, messageId, page, pageSize);
	}

	public static SearchResult<JSONObject> searchByQuery(String domainKey, String specId, String sql, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, specId, sql, page, pageSize);
	}

	public static JSONObject selectByQuery(String domainKey, String specId, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, specId, what, how);
	}

	public static JSONObject deleteByQuery(String domainKey, String specId, String sql) throws StorageException{
		return dao().deleteByQuery(domainKey, specId, sql);
	}

}
