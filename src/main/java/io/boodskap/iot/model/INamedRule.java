/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * INamedRulehis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * INamedRulehis program is distributed in the hope that it will be useful,
 * but WIINamedRuleHOUINamedRule ANY WARRANINamedRuleY; without even the implied warranty of
 * MERCHANINamedRuleABILIINamedRuleY or FIINamedRuleNESS FOR A PARINamedRuleICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.NamedRuleDAO;

@JsonSerialize(as=INamedRule.class)
public interface INamedRule extends IRule  {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static NamedRuleDAO<INamedRule> dao(){
		return NamedRuleDAO.get();
	}

	static public void createOrUpdate(INamedRule e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends INamedRule> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<INamedRule> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<INamedRule> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<INamedRule> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<INamedRule> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public INamedRule create(String domainKey, String name) throws StorageException{
		return dao().create(domainKey, name);
	}

	static public INamedRule get(String domainKey, String name) throws StorageException{
		return dao().get(domainKey, name);
	}

	static public boolean has(String domainKey, String name) throws StorageException{
		return dao().has(domainKey, name);
	}

	static public void delete(String domainKey, String name) throws StorageException{
		dao().delete(domainKey, name);
	}

	static public Collection<INamedRule> list(boolean load, String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, page, pageSize);
	}

	static public Collection<INamedRule> listNext(boolean load, String domainKey, String name, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, name, page, pageSize);
	}

	static public Collection<String> listNames(String domainKey, int page, int pageSize) throws StorageException{
		return dao().listNames(domainKey, page, pageSize);
	}

	static public Collection<String> listNamesNext(String domainKey, String name, int page, int pageSize) throws StorageException{
		return dao().listNamesNext(domainKey, name, page, pageSize);
	}



	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		INamedRule.dao().createOrUpdate(this);
	}
	
}


