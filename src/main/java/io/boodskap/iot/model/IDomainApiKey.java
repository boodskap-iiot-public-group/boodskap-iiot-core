/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainApiKeyDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainApiKey.class)
public interface IDomainApiKey extends IDomainObject{
	
	//======================================
	// DAO Methods
	//======================================
	
	public static DomainApiKeyDAO<IDomainApiKey> dao(){
		return DomainApiKeyDAO.get();
	}
	
	static public void createOrUpdate(IDomainApiKey e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDomainApiKey> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainApiKey> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainApiKey> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IDomainApiKey> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainApiKey> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what,how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey,query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IDomainApiKey create(String domainKey, String apiKey) throws StorageException{
		return dao().create(domainKey, apiKey);
	}
	
	static public IDomainApiKey get(String domainKey, String apiKey) throws StorageException{
		return dao().get(domainKey, apiKey);
	}
	
	static public IDomainApiKey get(String domainKey) throws StorageException{
		return dao().get(domainKey);
	}

	static public Collection<IDomainApiKey> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}

	static public Collection<IDomainApiKey> listNext(String domainKey, String apiKey, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, apiKey, page, pageSize);
	}

	


	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IDomainApiKey o = (IDomainApiKey) other;
		
		setApiKey(o.getApiKey());
		
		IDomainObject.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getApiKey();

	public void setApiKey(String apiKey);

}
