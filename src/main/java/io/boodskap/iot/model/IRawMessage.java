/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.DataChannel;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.RawMessageDAO;

@JsonSerialize(as=IRawMessage.class)
public interface IRawMessage extends IDomainData{
	
	//======================================
	// DAO Methods
	//======================================
	
	public static RawMessageDAO<IRawMessage> dao() {
		return  RawMessageDAO.get();
	}
	
	static public void createOrUpdate(IRawMessage e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IRawMessage> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IRawMessage> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IRawMessage> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{ 
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IRawMessage> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IRawMessage> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IRawMessage create(String domainKey, String rawId) throws StorageException{
		return dao().create(domainKey, rawId);
	}

	static public IRawMessage get(String domainKey, String rawId) throws StorageException{
		return dao().get(domainKey, rawId);
	}

	static public long count(String domainKey, String deviceId) throws StorageException{
		return dao().count(domainKey, deviceId);
	}

	static public long count(String domainKey, String deviceId, String specId) throws StorageException{
		return dao().count(domainKey, deviceId, specId);
	}

	static public void delete(String domainKey, String rawId) throws StorageException{
		dao().delete(domainKey, rawId);
		IRawData.deleteById(rawId);
	}

	static public Collection<IRawMessage> list(boolean load, String domainKey, String deviceId, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, deviceId, page, pageSize);
	}

	static public Collection<IRawMessage> listNext(boolean load, String domainKey, String deviceId, String rawId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, deviceId, rawId, page, pageSize);
	}

	static public Collection<IRawMessage> list(boolean load, String domainKey, String deviceId, String messageId, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, deviceId, messageId, page, pageSize);
	}
	
	static public Collection<IRawMessage> listNext(boolean load, String domainKey, String deviceId, String messageId, String rawId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, deviceId, messageId, rawId, page, pageSize);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IRawMessage.dao().createOrUpdate(this);
	}
	
	@Override
	public default void copy(Object other) {
		
		IRawMessage o = (IRawMessage) other;
		
		setRawId(o.getRawId());
		setChannel(o.getChannel());
		setSpecId(o.getSpecId());
		setDeviceId(o.getDeviceId());
		setDeviceModel(o.getDeviceModel());
		setFirmwareVersion(o.getFirmwareVersion());
		setNodeId(o.getNodeId());
		setNodeUid(o.getNodeUid());
		setPort(o.getPort());
		setIpAddress(o.getIpAddress());
		setHeader(o.getHeader());
		setData(o.getData());
		
		IDomainData.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getRawId();

	public void setRawId(String rawId);

	public DataChannel getChannel();

	public void setChannel(DataChannel channel);

	public String getSpecId();

	public void setSpecId(String specId);

	public String getDeviceId();

	public void setDeviceId(String deviceId);

	public String getDeviceModel();

	public void setDeviceModel(String deviceModel);

	public String getFirmwareVersion();

	public void setFirmwareVersion(String firmwareVersion);

	public String getNodeId();

	public void setNodeId(String nodeId);

	public String getNodeUid();

	public void setNodeUid(String nodeUid);

	public int getPort();

	public void setPort(int port);

	public String getIpAddress();

	public void setIpAddress(String ipAddress);

	public String getHeader();

	public void setHeader(String header);

	public String getData();

	public void setData(String data);

}
