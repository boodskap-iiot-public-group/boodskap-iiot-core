/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainEntityFileDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainEntityFile.class)
public interface IDomainEntityFile extends IDomainFile {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static DomainEntityFileDAO<IDomainEntityFile> dao(){
		return DomainEntityFileDAO.get();
	}

	static public void createOrUpdate(IDomainEntityFile e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDomainEntityFile> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainEntityFile> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainEntityFile> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IDomainEntityFile> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainEntityFile> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public Class<? extends IFileContent> contentClazz(){
		return dao().contentClazz();
	}
	
	static public IDomainEntityFile create(String domainKey, String entityType, String entityId, String fileId) throws StorageException{
		return dao().create(domainKey, entityType, entityId, fileId);
	}

	static public IDomainEntityFile get(String domainKey, String entityType, String entityId, String fileId, boolean load) throws StorageException{
		return dao().get(domainKey, entityType, entityId, fileId, load);
	}

	static public IFileContent getContent(String domainKey, String entityType, String entityId, String fileId) throws StorageException{
		return dao().getContent(domainKey, entityType, entityId, fileId);
	}

	static public boolean has(String domainKey, String entityType, String entityId, String fileId) throws StorageException{
		return dao().has(domainKey, entityType, entityId, fileId);
	}

	static public void delete(String domainKey, String entityType) throws StorageException{
		dao().delete(domainKey, entityType);
	}

	static public void delete(String domainKey, String entityType, String entityId) throws StorageException{
		dao().delete(domainKey, entityType, entityId);
	}

	static public void delete(String domainKey, String entityType, String entityId, String fileId) throws StorageException{
		dao().delete(domainKey, entityType, entityId, fileId);
	}

	static public void update(String domainKey, String entityType, String entityId, String fileId, String tags, String description) throws StorageException{
		dao().update(domainKey, entityType, entityId, fileId, tags, description);
	}

	static public void update(String domainKey, String entityType, String entityId, String fileId, byte[] data, String mediaType) throws StorageException{
		dao().update(domainKey, entityType, entityId, fileId, data, mediaType);
	}
		
	static public Collection<IDomainEntityFile> list(boolean load, String domainKey, String entityType, String entityId, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, entityType, entityId, page, pageSize);
	}

	static public Collection<IDomainEntityFile> listNext(boolean load, String domainKey, String entityType, String entityId, String fileId, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, entityType, entityId, fileId, page, pageSize);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDomainEntityFile.dao().createOrUpdate(this);
	}
	
	@Override
	default void copy(Object other) {
		
		IDomainEntityFile o = (IDomainEntityFile) other;
		
		setEntity(o.getEntityType());
		setEntityId(o.getEntityId());
		setEntity(o.getEntity());
		
		IDomainFile.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getEntityType();
	
	public void setEntityType(String entityType);

	public String getEntityId();
	
	public void setEntityId(String entityId);

}

