/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.AssetFileDAO;
import io.boodskap.iot.dao.EntityIterator;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
@JsonSerialize(as=IAssetFile.class)
public interface IAssetFile extends IDomainFile {

	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public static AssetFileDAO<IAssetFile> dao(){
		return AssetFileDAO.get();
	}
	
	static public void createOrUpdate(IAssetFile e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IAssetFile> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IAssetFile> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IAssetFile> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize);	
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}
	
	static public EntityIterator<IAssetFile> load(String domainKey) throws StorageException{
		return dao().load(domainKey);

	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IAssetFile> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey,query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey,what,how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey,query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey,what,how);
	}
	
	static public long count(String domainKey, String assetId) throws StorageException{
		return dao().count(domainKey,assetId);
	}
	
	static public IAssetFile create(String domainKey, String assetId, String fileId) {
		return dao().create(domainKey,assetId,fileId);
	}

	static public IAssetFile get(String domainKey, String assetId, String fileId) throws StorageException{
		return dao().get(domainKey,assetId,fileId);
	}
	
	static public IFileContent getContent(String domainKey, String assetId, String fileId) throws StorageException{
		return dao().getContent(domainKey,assetId,fileId);
	}

	static public boolean has(String domainKey, String assetId, String fileId) throws StorageException{
		return dao().has(domainKey,assetId,fileId);
	}

	static public void delete(String domainKey, String assetId) throws StorageException{
		dao().delete(domainKey,assetId);
	}

	static public void delete(String domainKey, String assetId, String fileId) throws StorageException{
		dao().delete(domainKey,assetId,fileId);	
	}

	static public void update(String domainKey, String assetId, String fileId, String tags, String description) throws StorageException{
		dao().update(domainKey,assetId,fileId,tags,description);	
	}

	static public void update(String domainKey, String assetId, String fileId, byte[] data, String mediaType) throws StorageException{
		dao().update(domainKey,assetId,fileId,data,mediaType);	
	}

	static public Collection<IAssetFile> list(boolean load, String domainKey, String assetId, int page, int pageSize) throws StorageException{
		return dao().list(load,domainKey,assetId,page,pageSize);
	}

	static public Collection<IAssetFile> listNext(boolean load, String domainKey, String assetId, String fileId, int page, int pageSize) throws StorageException{
		return dao().listNext(load,domainKey,assetId,fileId,page,pageSize);
	}

	

	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void save() {
		IAssetFile.dao().createOrUpdate(this);
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void copy(Object other) {
		
		IAssetFile o = (IAssetFile) other;
		
		setAssetId(o.getAssetId());
		
		IDomainFile.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getAssetId();
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setAssetId(String assetId);
}
