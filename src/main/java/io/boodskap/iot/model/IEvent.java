/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.EventDAO;

@JsonSerialize(as=IEvent.class)
public interface IEvent extends IDomainObject {

	//======================================
	// DAO Methods
	//======================================
	
	public static EventDAO<IEvent> dao(){
		return EventDAO.get();
	}

	static public void createOrUpdate(IEvent e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IEvent> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IEvent> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IEvent> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IEvent> load(String domainKey) throws StorageException{
		return dao().load();
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IEvent> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IEvent create(String domainKey, String eventId) throws StorageException{
		return dao().create(domainKey, eventId);
	}

	static public IEvent get(String domainKey, String eventId) throws StorageException{
		return dao().get(domainKey, eventId);
	}

	static public Collection<String> listIds(String domainKey) throws StorageException{
		return dao().listIds(domainKey);
	}

	static public void delete(String domainKey, String eventId) throws StorageException{
		dao().delete(domainKey, eventId);
	}

	static public Collection<IEvent> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}

	static public Collection<IEvent> listNext(String domainKey, String eventId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, eventId, page, pageSize);
	}



	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IEvent.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getEventId();

	public void setEventId(String eventId);

	public String getContent();

	public void setContent(String content);

	public String getSubject();

	public void setSubject(String subject);

	public String getContentTemplate();

	public void setContentTemplate(String contentTemplate);

	public String getSubjectTemplate();

	public void setSubjectTemplate(String subjectTemplate);

}
