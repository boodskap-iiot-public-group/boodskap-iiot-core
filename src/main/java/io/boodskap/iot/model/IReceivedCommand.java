package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Date;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.ReceivedCommandDAO;

@JsonSerialize(as=IReceivedCommand.class)
public interface IReceivedCommand extends IStorageObject {

	public static enum EncodingFormat {
		BASE64, HEX, TEXT
	}

	public static enum Status {
		QUEUED, PROCESSING, PROCESSED, FAILED
	}

	public static enum CommandType {
		COMMAND, RAW_COMMAND, BROADCAST_COMMAND, BROADCAST_RAW_COMMAND, GROUP_COMMAND, GROUP_RAW_COMMAND, GROUP_BROADCAST_COMMAND, TEMPLATE_COMMAND, TEMPLATE_BROADCAST_COMMAND, GROUP_TEMPLATE_COMMAND, GROUP_TEMPLATE_BROADCAST_COMMAND
	}
	
	//======================================
	// DAO Methods
	//======================================
	
	public static ReceivedCommandDAO<IReceivedCommand> dao() {
		return ReceivedCommandDAO.get();
	}
	
	static public void createOrUpdate(IReceivedCommand e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IReceivedCommand> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IReceivedCommand> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IReceivedCommand> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IReceivedCommand> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IReceivedCommand> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public Class<? extends INameValuePair> nvPairClazz(){
		return dao().nvPairClazz();
	}

	static public IReceivedCommand create(String domainKey, String requestId){
		return dao().create(domainKey, requestId);
	}
	
	static public IReceivedCommand get(String domainKey, String requestId) throws StorageException{
		return dao().get(domainKey, requestId);
	}
	
	static public void delete(String domainKey, String requestId) throws StorageException{
		dao().delete(domainKey, requestId);
	}
	
	static public Collection<IReceivedCommand> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}
	
	static public Collection<IReceivedCommand> listNext(String domainKey, String requestId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, requestId, page, pageSize);
	}

	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IReceivedCommand.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getRequestId();

	public void setRequestId(String requestId);

	public Collection<String> getDeviceIds();

	public void setDeviceIds(Collection<String> deviceIds);

	public Collection<? extends INameValuePair> getData();

	public void setData(Collection<? extends INameValuePair> data);

	public String getRawData();

	public void setRawData(String rawData);

	public EncodingFormat getEncodingFormat();

	public void setEncodingFormat(EncodingFormat encodingFormat);

	public Integer getCommandId();

	public void setCommandId(Integer commandId);

	public String getDeviceId();

	public void setDeviceId(String deviceId);

	public String getGroupId();

	public void setGroupId(String groupId);

	public String getTemplateId();

	public void setTemplateId(String templateId);

	public Boolean getSystem();

	public void setSystem(Boolean system);

	public Collection<? extends INameValuePair> getMergeContent();

	public void setMergeContent(Collection<? extends INameValuePair> mergeContent);

	public String getDeviceModel();

	public void setDeviceModel(String deviceModel);

	public String getDeviceVersion();

	public void setDeviceVersion(String deviceVersion);

	public Integer getTotalCount();

	public void setTotalCount(Integer totalCount);

	public Integer getSentCount();

	public void setSentCount(Integer sentCount);

	public Integer getFailedCount();

	public void setFailedCount(Integer failedCount);

	public Date getQueuedStamp();

	public void setQueuedStamp(Date queuedStamp);

	public Date getCompletedStamp();

	public void setCompletedStamp(Date completedStamp);

	public Status getStatus();

	public void setStatus(Status status);

	public CommandType getCommandType();

	public void setCommandType(CommandType commandType);

	//======================================
	// Helpers
	//======================================
	
	public INameValuePair createPair(String name, String value);

}
