/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IUserAssetGrouphis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IUserAssetGrouphis program is distributed in the hope that it will be useful,
 * but WIIUserAssetGroupHOUIUserAssetGroup ANY WARRANIUserAssetGroupY; without even the implied warranty of
 * MERCHANIUserAssetGroupABILIIUserAssetGroupY or FIIUserAssetGroupNESS FOR A PARIUserAssetGroupICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.UserAssetGroupDAO;

@JsonSerialize(as=IUserAssetGroup.class)
public interface IUserAssetGroup extends IGroup {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static UserAssetGroupDAO<IUserAssetGroup> dao(){
		return UserAssetGroupDAO.get();
	}

	public static Class<? extends IUserAssetGroup> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<IUserAssetGroup> load() throws StorageException{
		return dao().load();
	}
	
	public static EntityIterator<IUserAssetGroup> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}

	public static IUserAssetGroup create(String domainKey, String ownerUserId, String groupId) throws StorageException{
		return dao().create(domainKey, ownerUserId, groupId);
	}

	public static IUserAssetGroup get(String domainKey, String ownerUserId, String groupId) throws StorageException{
		return dao().get(domainKey, ownerUserId, groupId);
	}

	public static void delete(String domainKey, String ownerUserId, String groupId) throws StorageException{
		dao().delete(domainKey, ownerUserId, groupId);
	}

	public static void delete(String domainKey, String ownerUserId) throws StorageException{
		dao().delete(domainKey, ownerUserId);
	}

	public static long count(String domainKey, String ownerUserId) throws StorageException{
		return dao().count(domainKey, ownerUserId);
	}

	public static Collection<IUserAssetGroup> list(String domainKey, String ownerUserId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, ownerUserId, page, pageSize);
	}

	public static Collection<IUserAssetGroup> listNext(String domainKey, String ownerUserId, String groupId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, ownerUserId, groupId, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IUserAssetGroup.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getOwnerUserId();

	public void setOwnerUserId(String ownerUserId);

}
