package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.GroovyClassDAO;

@JsonSerialize(as=IGroovyClass.class)
public interface IGroovyClass extends IJarClass{

	//======================================
	// DAO Methods
	//======================================
	
	public static GroovyClassDAO<IGroovyClass> dao(){
		return GroovyClassDAO.get();
	}
	
	static public void createOrUpdate(IGroovyClass e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IGroovyClass> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IGroovyClass> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IGroovyClass> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static  public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public IGroovyClass create(String loader, String pkg, String name){
		return dao().create(loader, pkg, name);
	}
	
	static public IGroovyClass get(String loader, String pkg, String name) throws StorageException{
		return dao().get(loader, pkg, name);
	}
	
	static public long count(String loader) throws StorageException{
		return dao().count(loader);
	}
	
	static public long count(String loader, String pkg) throws StorageException{
		return dao().count(loader, pkg);
	}
	
	static public long countArchive(String loader, String fileName) throws StorageException{
		return dao().countArchive(loader, fileName);
	}
	
	static public long countArchive(String loader, String fileName, String pkg) throws StorageException{
		return dao().countArchive(loader, fileName, pkg);
	}
	
	static public void delete(String loader) throws StorageException{
		dao().delete(loader);
	}

	static public void delete(String loader, String pkg) throws StorageException{
		dao().delete(loader, pkg);
	}
	
	static public void delete(String loader, String pkg, String name) throws StorageException{
		dao().delete(loader,pkg, name);
	}
	
	static public void deleteArchive(String loader, String fileName) throws StorageException{
		dao().deleteArchive(loader, fileName);
	}
	
	static public EntityIterator<IGroovyClass> load(String loader) throws StorageException{
		return dao().load(loader);
	}
	
	static public Collection<IGroovyClass> listClasses(String loader, int page, int pageSize) throws StorageException{
		return dao().listClasses(loader, page, pageSize);
	}
	
	static public Collection<IGroovyClass> listNextClasses(String loader, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextClasses(loader, pkg, name, page, pageSize);
	}
	
	static public Collection<IGroovyClass> listPackageClasses(String loader, String pkg, int page, int pageSize) throws StorageException{
		return dao().listPackageClasses(loader, pkg, page, pageSize);
	}
	
	static public Collection<IGroovyClass> listNextPackageClasses(String loader, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextPackageClasses(loader, pkg, name, page, pageSize);
	}
	
	static public Collection<IGroovyClass> listArchiveClasses(String loader, String fileName, int page, int pageSize) throws StorageException{
		return dao().listArchiveClasses(loader, fileName, page, pageSize);
	}
	
	static public Collection<IGroovyClass> listNextArchiveClasses(String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextArchiveClasses(loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<IGroovyClass> listArchivePackageClasses(String loader, String fileName, String pkg, int page, int pageSize) throws StorageException{
		return dao().listArchivePackageClasses(loader, fileName, pkg, page, pageSize);
	}
	
	static public Collection<IGroovyClass> listNextArchivePackageClasses(String loader, String fileName, String pkg, String name, int page, int pageSize) throws StorageException{
		return dao().listNextArchivePackageClasses(loader, fileName, pkg, name, page, pageSize);
	}
	
	static public Collection<String> listPackages(String loader, int page, int pageSize) throws StorageException{
		return dao().listPackages(loader, page, pageSize);
	}
	
	static public Collection<String> listNextPackages(String loader, String pkg, int page, int pageSize) throws StorageException{
		return dao().listNextPackages(loader, pkg, page, pageSize);
	}
	
	static public Collection<String> listArchivePackages(String loader, String fileName, int page, int pageSize) throws StorageException{
		return dao().listArchivePackages(loader, fileName, page, pageSize);
	}
	
	static public Collection<String> listNextArchivePackages(String loader, String fileName, String pkg, int page, int pageSize) throws StorageException{
		return dao().listNextArchivePackages(loader, fileName, pkg, page, pageSize);
	}

	
	
	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		IGroovyClass.dao().createOrUpdate(this);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getCode();
	
	public void setCode(String code);
}
