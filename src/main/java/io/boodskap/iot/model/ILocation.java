/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.EntityType;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.LocationDAO;

@JsonSerialize(as=ILocation.class)
public interface ILocation extends IDomainObject {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static LocationDAO<ILocation> dao(){
		return LocationDAO.get();
	}

	static public void createOrUpdate(ILocation e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends ILocation> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<ILocation> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<ILocation> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<ILocation> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<ILocation> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public ILocation create(String domainKey, EntityType entityType, String entityId) throws StorageException{
		return dao().create(domainKey, entityType, entityId);
	}

	static public ILocation get(String domainKey, EntityType entityType, String entityId) throws StorageException{
		return dao().get(domainKey, entityType, entityId);
	}

	static public long count(String domainKey, EntityType entityType) throws StorageException{
		return dao().count(domainKey, entityType);
	}

	static public void delete(String domainKey, EntityType entityType) throws StorageException{
		dao().delete(domainKey, entityType);
	}

	static public void delete(String domainKey, EntityType entityType, String entityId) throws StorageException{
		dao().delete(domainKey, entityType, entityId);
	}

	static public Collection<ILocation> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}

	static public Collection<ILocation> listNext(String domainKey, String entityId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, entityId, page, pageSize);
	}

	static public Collection<ILocation> list(String domainKey, EntityType entityType, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}

	static public Collection<ILocation> listNext(String domainKey, EntityType entityType, String entityId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, entityType, entityId, page, pageSize);
		
	}


	
	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ILocation.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		ILocation o = (ILocation) other;
		
		setEntityId(o.getEntityId());
		setEntityType(o.getEntityType());
		setLatitude(o.getLatitude());
		setLongitude(o.getLongitude());
		
		IDomainObject.super.copy(other);
	}
	
	//======================================
	// DAO Methods
	//======================================
	
	public String getEntityId();

	public void setEntityId(String entityId);

	public EntityType getEntityType();

	public void setEntityType(EntityType locationType);

	public double getLatitude();
	
	public void setLatitude(double latitude);
	
	public double getLongitude();
	
	public void setLongitude(double longitude);
}
