package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.BillingContactDAO;
import io.boodskap.iot.dao.EntityIterator;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
@JsonSerialize(as=IBillingContact.class)
public interface IBillingContact extends IContact {

	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public static BillingContactDAO<IBillingContact> dao(){
		return BillingContactDAO.get();
	}
	
	static public void createOrUpdate(IBillingContact e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IBillingContact> clazz(){
		return dao().clazz();

	}
	
	static public EntityIterator<IBillingContact> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IBillingContact> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what,how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what,how);
	}

	static public EntityIterator<IBillingContact> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IBillingContact> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey,query,page,pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey,what,how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey,query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey,what,how);
	}
	
	static public IBillingContact create(String domainKey, String targetDomain, String contactId) {
		return dao().create(domainKey,targetDomain,contactId);
	}

	static public IBillingContact get(String domainKey, String targetDomain, String contactId) throws StorageException{
		return dao().get(domainKey,targetDomain,contactId);
	}
	
	static public void delete(String domainKey, String targetDomain) throws StorageException{
		dao().delete(domainKey,targetDomain);
	}
	
	static public void delete(String domainKey, String targetDomain, String contactId) throws StorageException{
		dao().delete(domainKey,targetDomain,contactId);
	}
	
	static public Collection<IBillingContact> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey,page,pageSize);
	}
	
	static public Collection<IBillingContact> listNext(String domainKey, String targetDomain, String contactId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey,targetDomain,contactId,page,pageSize);
	}
	


	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void save() {
		IBillingContact.dao().createOrUpdate(this);
	}
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void copy(Object other) {
		
		IBillingContact o = (IBillingContact) other;
		
		setTargetDomain(o.getTargetDomain());
		setContactId(o.getContactId());
		setType(o.getType());
		setLogo(o.getLogo());
		setObj(o.getObj());
		setDepartment(o.getDepartment());
		
		IContact.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getTargetDomain();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setTargetDomain(String targetDomain);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getContactId();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setContactId(String contactId);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getType();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setType(String type);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getLogo();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setLogo(String logo);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getObj();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setObj(String obj);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getDepartment();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setDepartment(String department);
}
