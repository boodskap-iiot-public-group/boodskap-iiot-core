/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IUserEntityhis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IUserEntityhis program is distributed in the hope that it will be useful,
 * but WIIUserEntityHOUIUserEntity ANY WARRANIUserEntityY; without even the implied warranty of
 * MERCHANIUserEntityABILIIUserEntityY or FIIUserEntityNESS FOR A PARIUserEntityICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.SystemUserEntityDAO;

@JsonSerialize(as=ISystemUserEntity.class)
public interface ISystemUserEntity extends ISystemEntity {

	//======================================
	// DAO Methods
	//======================================
	
	public static SystemUserEntityDAO<ISystemUserEntity> dao(){
		return SystemUserEntityDAO.get();
	}
	
	public static Class<? extends ISystemUserEntity> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<ISystemUserEntity> load() throws StorageException{
		return dao().load();
	}
	
	public static EntityIterator<ISystemUserEntity> load(String userId) throws StorageException{
		return dao().load(userId);
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static long count(String userId) throws StorageException{
		return dao().count(userId);
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static ISystemUserEntity create(String userId, String entityType, String entityId) throws StorageException{
		return dao().create(userId, entityType, entityId);
	}
	
	public static ISystemUserEntity get(String userId, String entityType, String entityId) throws StorageException{
		return dao().get(userId, entityType, entityId);
	}
	
	public static void delete(String userId) throws StorageException{
		dao().delete(userId);
	}
	
	public static void delete(String userId, String enityType) throws StorageException{
		dao().delete(userId, enityType);
	}
	
	public static void delete(String userId, String entityType, String entityId) throws StorageException{
		dao().delete(userId, entityType, entityId);
	}
	
	public static Collection<ISystemUserEntity> list(String userId, String entityType, int page, int pageSize) throws StorageException{
		return dao().list(userId, entityType, page, pageSize);
	}
	
	public static Collection<ISystemUserEntity> listNext(String userId, String entityType, String entityId, int page, int pageSize) throws StorageException{
		return dao().listNext(userId, entityType, entityId, page, pageSize);
	}
	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ISystemUserEntity.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		ISystemUserEntity o = (ISystemUserEntity) other;
		
		setUserId(o.getUserId());
		
		ISystemEntity.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getUserId();
	
	public void setUserId(String userId);
	
}
