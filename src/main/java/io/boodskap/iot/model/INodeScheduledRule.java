/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.NodeScheduledRuleDAO;

@JsonSerialize(as=INodeScheduledRule.class)
public interface INodeScheduledRule extends IDomainObject{
	
	//======================================
	// DAO Methods
	//======================================
	
	public static NodeScheduledRuleDAO<INodeScheduledRule> dao(){
		return NodeScheduledRuleDAO.get();
	}

	static public void createOrUpdate(INodeScheduledRule e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends INodeScheduledRule> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<INodeScheduledRule> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<INodeScheduledRule> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<INodeScheduledRule> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<INodeScheduledRule> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public INodeScheduledRule create(String domainKey, String ruleId, String nodeId) throws StorageException{
		return dao().create(domainKey, ruleId, nodeId);
	}

	static public EntityIterator<String> ruleIdsByNode(String nodeId) throws StorageException{
		return dao().ruleIdsByNode(nodeId);
	}
	
	static public long countByNodeId(String nodeId) throws StorageException{
		return dao().countByNodeId(nodeId);
	}

	static public INodeScheduledRule get(String domainKey, String ruleId) throws StorageException{
		return dao().get(domainKey, ruleId);
	}

	static public INodeScheduledRule get(String domainKey, String ruleId, String nodeId) throws StorageException{
		return dao().get(domainKey, ruleId, nodeId);
	}

	static public void delete(String domainKey, String ruleId) throws StorageException{
		dao().delete(domainKey, ruleId);
	}

	static public void delete(String domainKey, String ruleId, String nodeId) throws StorageException{
		dao().delete(domainKey, ruleId, nodeId);
	}



	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		INodeScheduledRule.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		INodeScheduledRule o = (INodeScheduledRule) other;

		setRuleId(o.getRuleId());
		setNodeId(o.getNodeId());
		
		IDomainObject.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getRuleId();

	public void setRuleId(String ruleId);

	public String getNodeId();

	public void setNodeId(String nodeId);

}
