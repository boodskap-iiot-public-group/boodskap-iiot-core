/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Date;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.CommandStatus;
import io.boodskap.iot.CommandType;
import io.boodskap.iot.DataChannel;
import io.boodskap.iot.LoRaTarget;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OutgoingCommandDAO;

@JsonSerialize(as=IOutgoingCommand.class)
public interface IOutgoingCommand extends IDomainObject {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static OutgoingCommandDAO<IOutgoingCommand> dao() {
		return OutgoingCommandDAO.get();
	}
	
	
	static public void createOrUpdate(IOutgoingCommand e) throws StorageException{
		dao().createOrUpdate(e);
	}
	
	static public Class<? extends IOutgoingCommand> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IOutgoingCommand> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IOutgoingCommand> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}
	
	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}
	
	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}
	
	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	static public EntityIterator<IOutgoingCommand> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IOutgoingCommand> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public IOutgoingCommand create(String domainKey, String requestId, String deviceId, long corrId) throws StorageException{
		return dao().create(domainKey, requestId, deviceId, corrId);
	}
	
	static public IOutgoingCommand get(String domainKey, String requestId, String deviceId, long corrId) throws StorageException{
		return dao().get(domainKey, requestId, deviceId, corrId);
	}
	
	static public void delete(String domainKey, String requestId) throws StorageException{
		dao().delete(domainKey, requestId);
	}

	static public void delete(String domainKey, String requestId, String deviceId) throws StorageException{
		dao().delete(domainKey, requestId, deviceId);
	}

	static public void delete(String domainKey, String requestId, String deviceId, long corrId) throws StorageException{
		dao().delete(domainKey, requestId, deviceId, corrId);
	}

	static public long countByRequest(String domainKey, String requestId) throws StorageException{
		return dao().countByRequest(domainKey, requestId);
	}
	
	static public long countByDevice(String domainKey, String deviceId) throws StorageException{
		return dao().countByDevice(domainKey, deviceId);
	}
	
	static public Collection<IOutgoingCommand> listByRequest(String domainKey, String requestId, int page, int pageSize) throws StorageException{
		return dao().listByRequest(domainKey, requestId, page, pageSize);
	}
	
	static public Collection<IOutgoingCommand> listByReqestNext(String domainKey, String requestId, String deviceId, long corrId, int page, int pageSize) throws StorageException{
		return dao().listByReqestNext(domainKey, requestId, deviceId, corrId, page, pageSize);
	}
	
	static public Collection<IOutgoingCommand> list(String domainKey, String deviceId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, deviceId, page, pageSize);
	}
	
	static public Collection<IOutgoingCommand> listNext(String domainKey, String deviceId, long corrId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, deviceId, corrId, page, pageSize);
	}



	
	//======================================
	// DAO Methods
	//======================================
	
	public default void save() {
		IOutgoingCommand.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getDeviceId();

	public void setDeviceId(String deviceId);

	public String getRequestId();

	public void setRequestId(String requestId);

	public long getCorrId();

	public void setCorrId(long corrId);

	public Date getQueuedStamp();

	public void setQueuedStamp(Date queuedStamp);

	public Date getSentStamp();

	public void setSentStamp(Date sentStamp);

	public Date getAckedStamp();

	public void setAckedStamp(Date ackedStamp);

	public CommandStatus getStatus();

	public void setStatus(CommandStatus status);

	public CommandType getType();

	public void setType(CommandType type);

	public byte[] getData();

	public void setData(byte[] data);

	public DataChannel getChannel();

	public void setChannel(DataChannel channel);

	public String getNodeId();

	public void setNodeId(String nodeId);

	public String getNodeUid();

	public void setNodeUid(String nodeUid);

	public String getReportedIp();

	public void setReportedIp(String reportedIp);

	public int getReportedPort();

	public void setReportedPort(int reportedPort);

	public int getNackCode();

	public void setNackCode(int nackCode);

	public LoRaTarget getLoraTarget();

	public void setLoraTarget(LoRaTarget loraTarget);

	public long getLoraBaseStationId();

	public void setLoraBaseStationId(long loraBaseStationId);

	public long getLoraTransmitterId();

	public void setLoraTransmitterId(long loraTransmitterId);

	public long getLoraReceiverId();

	public void setLoraReceiverId(long loraReceiverId);

	public long getLoraTransceiverId();

	public void setLoraTransceiverId(long loraTransceiverId);

	public long getLoraModemId();

	public void setLoraModemId(long loraModemId);
	
	public int getMqttQos();
	
	public void setMqttQos(int mqttQos);

}
