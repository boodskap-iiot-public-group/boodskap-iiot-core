package io.boodskap.iot.model;

import java.util.Collection;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.BillingTemplateDAO;
import io.boodskap.iot.dao.EntityIterator;

/**
 * @since 5.0.0-00 
 * @author Jegan Vincent
 */
public interface IBillingTemplate extends IModel {

	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public static BillingTemplateDAO<IBillingTemplate> dao(){
		return BillingTemplateDAO.get();
	}
	
	static public Class<? extends IBillingTemplate> clazz(){
		return dao().clazz();
	}
	
	static public IBillingTemplate create(String templatename, String itemname) {
		return dao().create(templatename,itemname);
	}
	
	static public void createOrUpdate(IBillingTemplate e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public EntityIterator<IBillingTemplate> load() throws StorageException{
		return dao().load();
	}
	
	static public EntityIterator<IBillingTemplate> load(String templatename) throws StorageException{
		return dao().load(templatename);
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public long count(String templatename) throws StorageException{
		return dao().count(templatename);
	}
	
	static public IBillingTemplate get(String templateName, String itemname) throws StorageException{
		return dao().get(templateName,itemname);
	}

	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public void delete(String templatename) throws StorageException{
		dao().delete(templatename);
	}
	
	static public void delete(String templateName, String itemname) throws StorageException{
		dao().delete(templateName,itemname);
	}
	
	static public Collection<IBillingTemplate> list(int page, int pageSize) throws StorageException{
		return dao().list(page,pageSize);
	}
	
	static public Collection<IBillingTemplate> listNext(String templateName, int page, int pageSize) throws StorageException{
		return dao().listNext(templateName,page,pageSize);
	}
	
	static public Collection<IBillingTemplate> listItems(String templateName, int page, int pageSize) throws StorageException{
		return dao().listItems(templateName,page,pageSize);
	}
	
	static public Collection<IBillingTemplate> listNextItems(String templateName, String itemName, int page, int pageSize) throws StorageException{
		return dao().listNextItems(templateName,itemName,page,pageSize);
	}

	
	
	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public default void save() {
		IBillingTemplate.dao().createOrUpdate(this);
	}

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	@Override
	public default void copy(Object other) {
		
		IBillingTemplate o = (IBillingTemplate) other;
		
		setTemplatecode(o.getTemplatename());
		setItemname(o.getItemname());
		setDescription(o.getDescription());
		setUnitprice(o.getUnitprice());
		setTax(o.getTax());
		setTemplatecode(o.getTemplatecode());
		
		IModel.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getTemplatename();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setTemplatename(String templatename);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getItemname();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setItemname(String itemname);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getDescription();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setDescription(String description);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public double getUnitprice();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setUnitprice(double unitprice);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public float getTax();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setTax(float tax);

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public String getTemplatecode();

	/**
	 * @since 5.0.0-00 
	 * @author Jegan Vincent
	 */
	public void setTemplatecode(String templatecode);

}
