/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.GlobalBinaryRuleDAO;

@JsonSerialize(as=IGlobalBinaryRule.class)
public interface IGlobalBinaryRule extends ISystemRule {

	//======================================
	// DAO Methods
	//======================================
	
	public static GlobalBinaryRuleDAO<IGlobalBinaryRule> dao(){
		return GlobalBinaryRuleDAO.get();
	}

	static public void createOrUpdate(IGlobalBinaryRule e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IGlobalBinaryRule> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IGlobalBinaryRule> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IGlobalBinaryRule> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public IGlobalBinaryRule create(String type){
		return dao().create(type);
	}

	static public IGlobalBinaryRule get(String type) throws StorageException{
		return dao().get(type);
	}

	static public void delete(String type) throws StorageException{
		dao().delete();
	}

	static public Collection<IGlobalBinaryRule> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	static public Collection<IGlobalBinaryRule> listNext(boolean load, String type, int page, int pageSize) throws StorageException{
		return dao().listNext(load, type, page, pageSize);
	}

	static public Collection<String> listTypes(int page, int pageSize) throws StorageException{
		return dao().listTypes(page, pageSize);
	}

	static public Collection<String> listTypesNext(String type, int page, int pageSize) throws StorageException{
		return dao().listTypesNext(type, page, pageSize);
	}


	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IGlobalBinaryRule.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IGlobalBinaryRule o = (IGlobalBinaryRule) other;
		
		setType(o.getType());
		
		ISystemRule.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getType() ;

	public void setType(String type) ;
}
