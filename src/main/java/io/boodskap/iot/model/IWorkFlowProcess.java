package io.boodskap.iot.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=IWorkFlowProcess.class)
public interface IWorkFlowProcess extends Serializable {

	//======================================
	// Attributes
	//======================================

	public String getId();
	
	public void setId(String id);
	
	public String getProcessId();
	
	public void setProcessId(String processId);
	
	public IWorkItem getChainedProcess();
	
	public void setChainedProcess(IWorkItem chainedProcess);
	
	public List<? extends IWorkItem> getOnErrorProcesses();
	
	public void setOnErrorProcesses(List<? extends IWorkItem> onErrorProcesses);
	
	public List<? extends IWorkItem> getOnCompleteProcesses();
	
	public void setOnCompleteProcesses(List<? extends IWorkItem> onCompleteProcesses);
	
	public List<? extends IWorkItem> getOnNextProcesses();
	
	public void setOnNextProcesses(List<? extends IWorkItem> onNextProcesses);
	
	public List<? extends IConditionalProcess> getConditionalProcesses();
	
	public void setConditionalProcess(List<? extends IConditionalProcess> conditionalProcesses);
	
}
