/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainCredentialsDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainCredentials.class)
public interface IDomainCredentials extends IDomainObject{
	
	//======================================
	// DAO Methods
	//======================================
	
	public static DomainCredentialsDAO<IDomainCredentials> dao(){
		return DomainCredentialsDAO.get();
	}
	
	public static Class<? extends IDomainCredentials> clazz() {
		return dao().clazz();
	}

	public static IDomainCredentials create(String domainKey) throws StorageException{
		IDomainCredentials e = dao().create(domainKey);
		e.setKeyPairAlgorithm("RSA");
		return e;
	}
		
	static public void createOrUpdate(IDomainCredentials e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public EntityIterator<IDomainCredentials> load() throws StorageException{
		return dao().load();
	}
	
	static public IDomainCredentials get(String domainKey) throws StorageException{
		return dao().get(domainKey);
	}

	static public void delete() throws StorageException{
		dao().delete();
	}

	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}

	static public Collection<IDomainCredentials> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	static public Collection<IDomainCredentials> listNext(boolean load, String domainKey, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, page, pageSize);
	}


	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getKeyPairAlgorithm();
	
	public void setKeyPairAlgorithm(String keyPairAlgorithm);
	
	public byte[] getPrivateKeyData();
	
	public void setPrivateKeyData(byte[] privateKeyData);

	public byte[] getPublicKeyData();

	public void setPublicKeyData(byte[] publicKeyData);

	public String getPasswordAlgorithm();
	
	public void setPasswordAlgorithm(String passwordAlgorithm);
	
	public byte[] getPasswordSalt();

	public void setPasswordSalt(byte[] passwordSalt);
	
	public int getIterations();
	
	public void setIterations(int iterations);

	public int getPasswordKeyLength();
	
	public void setPasswordKeyLength(int passwordKeyLength);

}
