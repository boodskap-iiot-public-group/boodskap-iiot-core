package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainClassLoaderDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainClassLoader.class)
public interface IDomainClassLoader extends IClassLoader{
	
	//======================================
	// DAO Methods
	//======================================


	public static DomainClassLoaderDAO<IDomainClassLoader> dao(){
		return DomainClassLoaderDAO.get();
	}

	static public void createOrUpdate(IDomainClassLoader e) throws StorageException{
		dao().createOrUpdate(null);
	}

	static public Class<? extends IDomainClassLoader> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainClassLoader> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainClassLoader> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IDomainClassLoader> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainClassLoader> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public IDomainClassLoader create(String domainKey, String loader){
		return dao().create(domainKey, loader);
	}
	
	static public IDomainClassLoader get(String domainKey, String loader) throws StorageException{
		return dao().get(domainKey, loader);
	}
	
	static public void delete(String domainKey, String loader) throws StorageException{
		dao().delete(domainKey, loader);
	}
	
	static public Collection<IDomainClassLoader> list(String domainKey) throws StorageException{
		return dao().list(domainKey);
	}

//	========================
//			Default methods
//	========================
	
	public default void save() {
		dao().createOrUpdate(this);
	}
	
	@Override
	public default void copy(Object other) {
		
		IDomainClassLoader o = (IDomainClassLoader) other;
		
		setDomainKey(o.getDomainKey());
		
		IClassLoader.super.copy(other);
	}
	
	public String getSystemLoader();
	
	public void setSystemLoader(String systemLoader);

	public String getDomainKey();
	
	public void setDomainKey(String domainKey);
}
