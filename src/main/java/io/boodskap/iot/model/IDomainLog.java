package io.boodskap.iot.model;

import java.util.Collection;
import java.util.Date;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.RawDataType;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainLogDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainLog.class)
public interface IDomainLog extends ISystemLog, IDomainContent {

	//======================================
	// DAO Methods
	//======================================
	
	public static DomainLogDAO<IDomainLog> dao() {
		return DomainLogDAO.get();
	}

	public static Class<? extends IDomainLog> clazz(){
		return dao().clazz();
	}
	
	public static EntityIterator<IDomainLog> load() throws StorageException{
		return dao().load();
	}
	
	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}
	
	public static SearchResult<IDomainLog> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	public static SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	public static JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	public static JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}
	
	public static EntityIterator<IDomainLog> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	public static long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	public static void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	public static SearchResult<IDomainLog> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	public static SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	public static JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	public static JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	public static IDomainLog create(String domainKey, String id) throws StorageException{
		return dao().create(domainKey, id);
	}
	
	public static IDomainLog get(String domainKey, String id) throws StorageException{
		return dao().get(domainKey, id);
	}
	
	public static Collection<IDomainLog> list(String domainKey, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, page, pageSize);
	}
	
	public static Collection<IDomainLog> listNext(String domainKey, String id, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, id, page, pageSize);
	}
	
	public static void delete(String domainKey, String id) throws StorageException{
		dao().delete(domainKey, id);
	}

	public static void deleteBefore(String domainKey, Date date) throws StorageException{
		dao().deleteBefore(domainKey, date);
	}

	public static void deleteAfter(String domainKey, Date date) throws StorageException{
		dao().deleteAfter(domainKey, date);
	}

	public static void deleteBetween(String domainKey, Date from, Date to) throws StorageException{
		dao().deleteBetween(domainKey, from, to);
	}

	public static void deleteBefore(Date date) throws StorageException{
		dao().deleteBefore(date);
	}

	public static void deleteAfter(Date date) throws StorageException{
		dao().deleteAfter(date);
	}

	public static void deleteBetween(Date from, Date to) throws StorageException{
		dao().deleteBetween(from, to);
	}

	//======================================
	// Default Methods
	//======================================
	
	@Override
	default void save() {
		dao().createOrUpdate(this);
	}
	
	@Override
	default void copy(Object other) {
		ISystemLog.super.copy(other);
		IDomainContent.super.copy(other);
	}

	//======================================
	// Attributes
	//======================================

	public void setType(RawDataType type);
	
	public RawDataType getType();
}
