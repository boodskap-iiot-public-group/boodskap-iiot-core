/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.StorageException;
import io.boodskap.iot.TemplateScriptLanguage;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.SystemTemplateDAO;

@JsonSerialize(as=ISystemTemplate.class)
public interface ISystemTemplate extends IModel {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static SystemTemplateDAO<ISystemTemplate> dao() {
		return SystemTemplateDAO.get();
	}
	
	public static Class<? extends ISystemTemplate> clazz(){
		return dao().clazz();
	}
	
	public static ISystemTemplate create(String name) {
		return dao().create(name);
	}
	
	public static ISystemTemplate get(String name) throws StorageException{
		return dao().get(name);
	}

	public static long count() throws StorageException{
		return dao().count();
	}
	
	public static void delete() throws StorageException{
		dao().delete();
	}

	public static void delete(String name) throws StorageException{
		dao().delete(name);
	}

	public static EntityIterator<ISystemTemplate> load() throws StorageException{
		return dao().load();
	}
	
	public static Collection<ISystemTemplate> list(boolean load, int page, int pageSize) throws StorageException{
		return dao().list(load, page, pageSize);
	}

	public static Collection<ISystemTemplate> listNext(boolean load, String name, int page, int pageSize) throws StorageException{
		return dao().listNext(load, name, page, pageSize);
	}

	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		ISystemTemplate.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public TemplateScriptLanguage getLanguage();

	public void setLanguage(TemplateScriptLanguage language);

	public String getCode();

	public void setCode(String code);

}
