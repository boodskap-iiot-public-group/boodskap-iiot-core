/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * IOfflineStreamhis program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * IOfflineStreamhis program is distributed in the hope that it will be useful,
 * but WIIOfflineStreamHOUIOfflineStream ANY WARRANIOfflineStreamY; without even the implied warranty of
 * MERCHANIOfflineStreamABILIIOfflineStreamY or FIIOfflineStreamNESS FOR A PARIOfflineStreamICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OfflineStreamDAO;

@JsonSerialize(as=IOfflineStream.class)
public interface IOfflineStream extends IOfflineSnap{
	
	//======================================
	// DAO Methods
	//======================================
	
	public static OfflineStreamDAO<IOfflineStream> dao(){
		return OfflineStreamDAO.get();
	}

	static public void createOrUpdate(IOfflineStream e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IOfflineStream> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IOfflineStream> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IOfflineStream> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IOfflineStream> load(String domainKey) throws StorageException{
		return dao().load(domainKey);
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IOfflineStream> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}
	
	static public IOfflineStream create(String domainKey, String deviceId, String camera, String session, int frame) throws StorageException{
		return dao().create(domainKey, deviceId, camera, session, frame);
	}

	static public IOfflineStream get(String domainKey, String deviceId, String camera, String session, int frame) throws StorageException{
		return dao().get(domainKey, deviceId, camera, session, frame);
	}

	static	public long count(String domainKey, String deviceId) throws StorageException{
		return dao().count(domainKey, deviceId);
	};

	static public long count(String domainKey, String deviceId, String camera) throws StorageException{
		return dao().count(domainKey, deviceId, camera);
	}

	static public long count(String domainKey, String deviceId, String camera, String session) throws StorageException{
		return dao().count(domainKey, deviceId, camera, session);
	}

	static public void delete(String domainKey, String deviceId) throws StorageException{
		dao().delete(domainKey, deviceId);
	}

	static public void delete(String domainKey, String deviceId, String camera) throws StorageException{
		dao().delete(domainKey, deviceId, camera);
	}

	static public void delete(String domainKey, String deviceId, String camera, String session) throws StorageException{
		dao().delete(domainKey, deviceId, camera, session);
	}
	
	static public void delete(String domainKey, String deviceId, String camera, String session, int frame) throws StorageException{
		dao().delete(domainKey, deviceId, camera, session, frame);
	}
	
	static public Collection<IOfflineStream> list(boolean load, String domainKey, String deviceId, String camera, String session, int page, int pageSize) throws StorageException{
		return dao().list(load, domainKey, deviceId, camera, session, page, pageSize);
	}

	static public Collection<IOfflineStream> listNext(boolean load, String domainKey, String deviceId, String camera, String session, int frame, int page, int pageSize) throws StorageException{
		return dao().listNext(load, domainKey, deviceId, camera, session, frame, page, pageSize);
	}

	static public SearchResult<IOfflineStream> search(boolean isContext, boolean load, String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().search(isContext, load, domainKey, query, page, pageSize);
	}

	
	
	
	//======================================
	// Default Methods
	//======================================
	
	@JsonIgnore
	@Override
	public default String getId() {
		return null;
	}

	@Override
	public default void setId(String id) {
	}


	@Override
	public default void save() {
		IOfflineStream.dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		
		IOfflineStream o = (IOfflineStream) other;

		setSession(o.getSession());
		setFrame(o.getFrame());
		
		IOfflineSnap.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getSession();

	public void setSession(String session);

	public int getFrame();

	public void setFrame(int frame);
	
}
