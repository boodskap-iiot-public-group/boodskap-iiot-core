/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.EntityIterator;
import io.boodskap.iot.dao.OrganizationDeviceDAO;

@JsonSerialize(as=IOrganizationDevice.class)
public interface IOrganizationDevice extends IDevice, IOrgEntity {
	
	//======================================
	// DAO Methods
	//======================================
	
	public static OrganizationDeviceDAO<IOrganizationDevice> dao(){
		return OrganizationDeviceDAO.get();
	}

	static public void createOrUpdate(IOrganizationDevice e) throws StorageException{
		dao().createOrUpdate(null);
	}

	static public Class<? extends IOrganizationDevice> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IOrganizationDevice> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IOrganizationDevice> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IOrganizationDevice> load(String domainKey) throws StorageException{
		return dao().load();
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count(domainKey);
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IOrganizationDevice> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public EntityIterator<IOrganizationDevice> load(String domainKey, String orgId) throws StorageException{
		return dao().load(domainKey, orgId);
	}
	
	static public SearchResult<IOrganizationDevice> searchByQuery(String domainKey, String orgId, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String orgId, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, orgId, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String orgId, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, orgId, query);
	}

	static public JSONObject updateByQuery(String domainKey, String orgId, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, orgId, what, how);
	}

	static public IOrganizationDevice create(String domainKey, String orgId, String deviceId) throws StorageException{
		return dao().create(domainKey, orgId, deviceId);
	}

	static public IOrganizationDevice get(String domainKey, String orgId, String deviceId) throws StorageException{
		return dao().get(domainKey, orgId, deviceId);
	}

	static public void delete(String domainKey, String orgId, String deviceId) throws StorageException{
		dao().delete(domainKey, orgId, deviceId);
	}

	static public void delete(String domainKey, String orgId) throws StorageException{
		dao().delete(domainKey, orgId);
	}

	static public long count(String domainKey, String orgId) throws StorageException{
		return dao().count(domainKey, orgId);
	}

	static public Collection<IOrganizationDevice> list(String domainKey, String orgId, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, orgId, page, pageSize);
	}

	static public Collection<IOrganizationDevice> listNext(String domainKey, String orgId, String deviceId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, orgId, deviceId, page, pageSize);
	}

	
	
	//======================================
	// Default Methods
	//======================================
	
	@Override
	public default void save() {
		dao().createOrUpdate(this);
	}

	@Override
	public default void copy(Object other) {
		IDevice.super.copy(other);
		IOrgEntity.super.copy(other);
	}
	
	//======================================
	// Attributes
	//======================================
	
	public String getOrgId();

	public void setOrgId(String orgId);

}
