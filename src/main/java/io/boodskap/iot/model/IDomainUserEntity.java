/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.model;

import java.util.Collection;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.DomainUserEntityDAO;
import io.boodskap.iot.dao.EntityIterator;

@JsonSerialize(as=IDomainUserEntity.class)
public interface IDomainUserEntity extends IDomainEntity {

	//======================================
	// DAO Methods
	//======================================
	
	public static DomainUserEntityDAO<IDomainUserEntity> dao(){
		return DomainUserEntityDAO.get();
	}

	static public void createOrUpdate(IDomainUserEntity e) throws StorageException{
		dao().createOrUpdate(e);
	}

	static public Class<? extends IDomainUserEntity> clazz(){
		return dao().clazz();
	}
	
	static public EntityIterator<IDomainUserEntity> load() throws StorageException{
		return dao().load();
	}
	
	static public long count() throws StorageException{
		return dao().count();
	}
	
	static public void delete() throws StorageException{
		dao().delete();
	}
	
	static public SearchResult<IDomainUserEntity> searchByQuery(String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		return dao().selectByQuery(what, how);
	}

	static public JSONObject deleteByQuery(String query) throws StorageException{
		return dao().deleteByQuery(query);
	}

	static public JSONObject updateByQuery(String what, String how) throws StorageException{
		return dao().updateByQuery(what, how);
	}

	static public EntityIterator<IDomainUserEntity> load(String domainKey) throws StorageException{
		return dao().load();
	}
	
	static public long count(String domainKey) throws StorageException{
		return dao().count();
	}
	
	static public void delete(String domainKey) throws StorageException{
		dao().delete(domainKey);
	}
	
	static public SearchResult<IDomainUserEntity> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		return dao().searchByQuery(domainKey, query, page, pageSize);
	}

	static public SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().selectByQuery(domainKey, what, how);
	}
	
	static public JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		return dao().deleteByQuery(domainKey, query);
	}

	static public JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		return dao().updateByQuery(domainKey, what, how);
	}

	static public IDomainUserEntity create(String domainKey, String userId, String entityType, String entityId) throws StorageException{
		return dao().create(domainKey, userId, entityType, entityId);
	}
	
	static public IDomainUserEntity get(String domainKey, String userId, String entityType, String entityId) throws StorageException{
		return dao().get(domainKey, userId, entityType, entityId);
	}
	
	static public void delete(String domainKey, String userId) throws StorageException{
		dao().delete(domainKey, userId);
	}
	
	static public void delete(String domainKey, String userId, String entityType) throws StorageException{
		dao().delete(domainKey, userId, entityType);
	}
	
	static public void delete(String domainKey, String userId, String entityType, String entityId) throws StorageException{
		dao().delete(domainKey, userId, entityType, entityId);
	}
	
	static public Collection<IDomainUserEntity> list(String domainKey, String userId, String entityType, int page, int pageSize) throws StorageException{
		return dao().list(domainKey, userId, entityType, page, pageSize);
	}
	
	static public Collection<IDomainUserEntity> listNext(String domainKey, String userId, String entityType, String entityId, int page, int pageSize) throws StorageException{
		return dao().listNext(domainKey, userId, entityType, entityId, page, pageSize);
	}

	
	
	//======================================
	// Default Methods
	//======================================
	
	public default void save() {
		IDomainUserEntity.dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	public String getUserId();
	
	public void setUserId(String userId);
}
