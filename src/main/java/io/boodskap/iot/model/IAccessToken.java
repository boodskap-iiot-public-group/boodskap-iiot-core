package io.boodskap.iot.model;

import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.boodskap.iot.AuthType;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.dao.AccessTokenDAO;

/**
 * @author Jegan Vincent
 * @since 5.0.0-00
 */
@JsonSerialize(as=IAccessToken.class)
public interface IAccessToken extends IEntity {
	
	//======================================
	// DAO Methods
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static AccessTokenDAO<IAccessToken> dao(){
		return AccessTokenDAO.get();
	}
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static Class<? extends IAccessToken> clazz() {
		return dao().clazz();
	}

	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static IAccessToken create(String token) {
		return dao().create(token);
	}
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static IAccessToken get(String token) throws StorageException{
		return dao().get(token);
	}
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static void delete(String token) throws StorageException{
		dao().delete(token);
	}

	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static Collection<IAccessToken> listPlatformTokens() throws StorageException{
		return dao().listPlatformTokens();
	}
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static Collection<IAccessToken> listDomainTokens(String domainKey) throws StorageException{
		return dao().listDomainTokens(domainKey);
	}
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static Collection<IAccessToken> listUserTokens(String domainKey, String userId) throws StorageException{
		return dao().listUserTokens(domainKey, userId);
	}
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static Collection<IAccessToken> listDeviceTokens(String domainKey, String deviceId) throws StorageException{
		return dao().listDeviceTokens(domainKey, deviceId);
	}
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static Collection<IAccessToken> listOrgUserTokens(String domainKey, String orgId, String userId) throws StorageException{
		return dao().listOrgUserTokens(domainKey, orgId, userId);
	}
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public static Collection<IAccessToken> listOrgDeviceTokens(String domainKey, String orgId, String deviceId) throws StorageException{
		return dao().listOrgDeviceTokens(domainKey, orgId, deviceId);
	}
	
	//======================================
	// Default Methods
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public default void copy(Object other) {
		
		IAccessToken o = (IAccessToken) other;
		
		setToken(o.getToken());
		setExternal(o.isExternal());
		setAuthType(o.getAuthType());
		setUserId(o.getUserId());
		setDeviceId(o.getDeviceId());
		setOrgId(o.getOrgId());
		setExpireIn(o.getExpireIn());
		setDomainKey(o.getDomainKey());
		
		IEntity.super.copy(other);
		
	}

	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public default void save() throws StorageException {
		dao().createOrUpdate(this);
	}

	//======================================
	// Attributes
	//======================================
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public String getToken();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setToken(String token);
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public boolean isExternal();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setExternal(boolean external);
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public AuthType getAuthType();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setAuthType(AuthType authType);
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public String getDomainKey();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setDomainKey(String domainKey);
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public String getOrgId();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setOrgId(String orgId);
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public String getUserId();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setUserId(String userId);
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public String getDeviceId();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setDeviceId(String deviceId);
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public long getExpireIn();
	
	/**
	 * @author Jegan Vincent
	 * @since 5.0.0-00
	 */
	public void setExpireIn(long expireIn);

}
