package io.boodskap.iot;

import java.io.Serializable;
import java.util.concurrent.Callable;

public interface PlatformCallable<V> extends Callable<V>, Serializable {

}
