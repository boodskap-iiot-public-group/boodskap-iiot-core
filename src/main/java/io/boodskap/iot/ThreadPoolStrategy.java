package io.boodskap.iot;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=ThreadPoolStrategy.class)
public enum ThreadPoolStrategy {
	FIXED,
	CACHED
}
