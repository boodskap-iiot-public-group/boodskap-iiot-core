package io.boodskap.iot;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=AuthType.class)
public enum AuthType{
	
	PLATFORM,
	DOMAIN,
	ORG,
	USER,
	ORGUSER,
	DEVICE,
	ORGDEVICE
}