package io.boodskap.iot;

public class SQLConfiguration implements IConfig {

	private static final long serialVersionUID = -1421200687725440828L;
	
	private String jdbcUrl = "jdbc:ignite:thin://127.0.0.1";
	private String userName = "";
	private String password = "";
	private String cachePrepStmts = "true";
	private String prepStmtCacheSize = "2500";
	private String prepStmtCacheSqlLimit = "25000";
	
	public SQLConfiguration() {
	}

	@Override
	public int getStartPriority() {
		return 0;
	}

	@Override
	public void setDefaults() {
	}

	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCachePrepStmts() {
		return cachePrepStmts;
	}

	public void setCachePrepStmts(String cachePrepStmts) {
		this.cachePrepStmts = cachePrepStmts;
	}

	public String getPrepStmtCacheSize() {
		return prepStmtCacheSize;
	}

	public void setPrepStmtCacheSize(String prepStmtCacheSize) {
		this.prepStmtCacheSize = prepStmtCacheSize;
	}

	public String getPrepStmtCacheSqlLimit() {
		return prepStmtCacheSqlLimit;
	}

	public void setPrepStmtCacheSqlLimit(String prepStmtCacheSqlLimit) {
		this.prepStmtCacheSqlLimit = prepStmtCacheSqlLimit;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cachePrepStmts == null) ? 0 : cachePrepStmts.hashCode());
		result = prime * result + ((jdbcUrl == null) ? 0 : jdbcUrl.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((prepStmtCacheSize == null) ? 0 : prepStmtCacheSize.hashCode());
		result = prime * result + ((prepStmtCacheSqlLimit == null) ? 0 : prepStmtCacheSqlLimit.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SQLConfiguration other = (SQLConfiguration) obj;
		if (cachePrepStmts == null) {
			if (other.cachePrepStmts != null)
				return false;
		} else if (!cachePrepStmts.equals(other.cachePrepStmts))
			return false;
		if (jdbcUrl == null) {
			if (other.jdbcUrl != null)
				return false;
		} else if (!jdbcUrl.equals(other.jdbcUrl))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (prepStmtCacheSize == null) {
			if (other.prepStmtCacheSize != null)
				return false;
		} else if (!prepStmtCacheSize.equals(other.prepStmtCacheSize))
			return false;
		if (prepStmtCacheSqlLimit == null) {
			if (other.prepStmtCacheSqlLimit != null)
				return false;
		} else if (!prepStmtCacheSqlLimit.equals(other.prepStmtCacheSqlLimit))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

}
