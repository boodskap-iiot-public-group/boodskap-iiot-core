package io.boodskap.iot.sql;

import java.sql.Connection;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import io.boodskap.iot.BoodskapSystem;
import io.boodskap.iot.SQLConfiguration;

public class DataBase {
	
	private static DataBase instance;
	
	private HikariDataSource dataSource;

	private DataBase() {	
	}
	
	public static final DataBase get() {
		
		if(null == instance) {
			
			try {
				
				SQLConfiguration cfg = BoodskapSystem.config().getIgniteSql();
				HikariConfig config = new HikariConfig();
				config.setJdbcUrl(cfg.getJdbcUrl());
				config.setUsername(cfg.getUserName());
				config.setPassword(cfg.getPassword());
				config.addDataSourceProperty("cachePrepStmts", cfg.getCachePrepStmts());
				config.addDataSourceProperty("prepStmtCacheSize", cfg.getPrepStmtCacheSize());
				config.addDataSourceProperty("prepStmtCacheSqlLimit", cfg.getPrepStmtCacheSqlLimit());

				HikariDataSource dataSource = new HikariDataSource(config);
				
				Connection conn = dataSource.getConnection();
				conn.close();
				
				instance = new DataBase();
				instance.dataSource = dataSource;
				
			}catch(Exception ex) {
				throw new RuntimeException(ex);
			}
		}
		
		return instance;
	}
	
	public static final void close() {
		
		if(null != instance && null != instance.dataSource && !instance.dataSource.isClosed()) {
			instance.dataSource.close();
			instance.dataSource = null;
			instance = null;
		}
	}

	public HikariDataSource getDataSource() {
		return dataSource;
	}

}
