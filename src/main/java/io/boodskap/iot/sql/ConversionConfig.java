package io.boodskap.iot.sql;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import io.boodskap.iot.model.IIgniteSQLTable;

public class ConversionConfig {
	
	public static enum ConversionType{
		UUID,
		DATE,
		TIME,
		TIMESTAMP,
	};

	private ConversionConfig() {
	}

    private static final Map<String, Map<String,ConversionType>> types = new HashMap<>();
    
    public static final void setType(String tableName, String fieldName, ConversionType type) {
    	Map<String,ConversionType> ctypes = types.get(tableName);
    	if(null == ctypes) {
    		ctypes = new HashMap<>();
    		types.put(tableName.toLowerCase(), ctypes);
    	}
    	ctypes.put(fieldName.toLowerCase(), type);
    }
    
    public static final ConversionType getType(String tableName, String fieldName) {
    	Map<String,ConversionType> ctypes = types.get(tableName.toLowerCase());
    	if(null != ctypes) {
    		return ctypes.get(fieldName.toLowerCase());
    	}
    	return null;
    }

    public static final ConversionType removeType(String tableName, String fieldName) {
    	Map<String,ConversionType> ctypes = types.get(tableName.toLowerCase());
    	if(null != ctypes) {
    		return ctypes.remove(fieldName.toLowerCase());
    	}
    	return null;
    }
    
    public static final void clearTypes(String tableName) {
    	types.remove(tableName.toLowerCase());
    }
    
    public static boolean hasTypes(String tableName) {
    	return types.containsKey(tableName.toLowerCase());
    }
    
    public static void initTypes(IIgniteSQLTable table) {
    	
    	table.getFields().forEach(f -> {
    		switch(f.getType()) {
			case UUID:
				ConversionConfig.setType(table.getName(), f.getName(), ConversionType.UUID);
				break;
			case DATE:
				ConversionConfig.setType(table.getName(), f.getName(), ConversionType.DATE);
				break;
			case TIME:
				ConversionConfig.setType(table.getName(), f.getName(), ConversionType.TIME);
				break;
			case TIMESTAMP:
				ConversionConfig.setType(table.getName(), f.getName(), ConversionType.TIMESTAMP);
				break;
			default:
				break;
    		}
    	});
    	
    	if(!hasTypes(table.getName())) {
    		types.put(table.getName().toLowerCase(), new HashMap<>());
    	}
    }
    
    public static String getGuidFromByteArray(byte[] bytes)
    {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        UUID uuid = new UUID(bb.getLong(), bb.getLong());
        return uuid.toString();
    }

}
