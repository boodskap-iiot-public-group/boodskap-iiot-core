package io.boodskap.iot.sql;

import com.mchange.v2.c3p0.impl.NewProxyResultSet;

public class C3P0ResultSetSerializer extends JdbcResultSetSerializer<NewProxyResultSet> {

	@Override
	public Class<NewProxyResultSet> handledType() {
		return NewProxyResultSet.class;
	}

}