package io.boodskap.iot.sql;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import io.boodskap.iot.model.IIgniteSQLTable;
import io.boodskap.iot.sql.ConversionConfig.ConversionType;

public class IgniteResultSetSerializer extends JsonSerializer<ResultSet> {
	
    public static class ResultSetSerializerException extends JsonProcessingException{
        private static final long serialVersionUID = -914957626413580734L;

        public ResultSetSerializerException(Throwable cause){
            super(cause);
        }

		public ResultSetSerializerException(String msg) {
			super(msg);
		}
    }
    
    @Override
    public Class<ResultSet> handledType() {
        return ResultSet.class;
    }
    
    @Override
    public void serialize(ResultSet rs, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {

        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int numColumns = rsmd.getColumnCount();
            String[] columnNames = new String[numColumns];
            String[] tableNames = new String[numColumns];
           
            
            for (int i = 0; i < columnNames.length; i++) {
                
            	columnNames[i] = rsmd.getColumnLabel(i + 1).toLowerCase();
                tableNames[i] = rsmd.getTableName(i+1).toLowerCase();

                if(tableNames[i].indexOf("_") != -1 && !ConversionConfig.hasTypes(tableNames[i])) {
            		int endIndex = tableNames[i].indexOf("_");

            		IIgniteSQLTable table = IIgniteSQLTable.get(tableNames[i].substring(0, endIndex), tableNames[i]);

            		if(null != table) {
            			ConversionConfig.initTypes(table);
            		}
            	}

            }

            jgen.writeStartArray();

            while (rs.next()) {

                jgen.writeStartObject();

                for (int i = 0; i < columnNames.length; i++) {
                	
                	ConversionType convert = ConversionConfig.getType(tableNames[i], columnNames[i]);
                	
                    jgen.writeFieldName(columnNames[i]);
                    
                	Object value = rs.getObject(i+1);
                	
                	if(rs.wasNull() || null == value) {
                		jgen.writeNull();
                	}else {
                		
                		
                		if (value instanceof Integer) {
                            jgen.writeNumber((Integer) value);
                        } else if (value instanceof String) {
                            jgen.writeString((String) value);                
                        } else if (value instanceof UUID) {
                            jgen.writeString(((UUID)value).toString());                
                        } else if (value instanceof Boolean) {
                        	jgen.writeBoolean((Boolean) value);           
                        } else if (value instanceof Date) {
                        	jgen.writeNumber(((Date) value).getTime());                
                        } else if (value instanceof Time) {
                        	jgen.writeNumber(((Time) value).getTime());                
                        } else if (value instanceof Timestamp) {
                        	jgen.writeNumber(((Timestamp) value).getTime());                
                        } else if (value instanceof Long) {
                        	jgen.writeNumber((Long) value);                
                        } else if (value instanceof Double) {
                        	jgen.writeNumber((Double) value);                
                        } else if (value instanceof Float) {
                        	jgen.writeNumber((Float) value);                
                        } else if (value instanceof BigDecimal) {
                        	jgen.writeNumber((BigDecimal) value);
                        } else if (value instanceof Byte) {
                        	jgen.writeNumber((Byte) value);
                        } else if (value instanceof byte[]) {
                        	if(null == convert) {
                        		jgen.writeBinary((byte[]) value);
                        	}else {
                        		jgen.writeString(ConversionConfig.getGuidFromByteArray((byte[]) value));
                        	}
                        } else if (value instanceof Blob) {
                        	Blob blob = (Blob)value;
                        	byte[] bytes = blob.getBytes(1, (int) blob.length());
                        	if(null == convert) {
                        		jgen.writeBinary(bytes);
                        	}else {
                        		jgen.writeString(ConversionConfig.getGuidFromByteArray(bytes));
                        	}
                        	jgen.writeBinary(bytes);                
                        } else if (value instanceof Clob) {
                        	Clob clob = (Clob)value;
                        	char[] chars = new char[(int)clob.length()];
                        	clob.getCharacterStream().read(chars);
                        	jgen.writeString(chars, 0, chars.length);           
                        } else {
                            throw new ResultSetSerializerException("Unmappable object type: " + value.getClass());
                        }
                		
                	}
                	
                }

                jgen.writeEndObject();
            }

            jgen.writeEndArray();

        } catch (SQLException e) {
            throw new ResultSetSerializerException(e);
        }
    }

}