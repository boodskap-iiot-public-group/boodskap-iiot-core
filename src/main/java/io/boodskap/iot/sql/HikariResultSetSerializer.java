package io.boodskap.iot.sql;

import com.zaxxer.hikari.pool.HikariProxyResultSet;

public class HikariResultSetSerializer extends JdbcResultSetSerializer<HikariProxyResultSet> {

	@Override
	public Class<HikariProxyResultSet> handledType() {
		return HikariProxyResultSet.class;
	}

}