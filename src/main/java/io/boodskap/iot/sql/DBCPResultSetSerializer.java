package io.boodskap.iot.sql;

import org.apache.commons.dbcp2.DelegatingResultSet;

public class DBCPResultSetSerializer extends JdbcResultSetSerializer<DelegatingResultSet> {

	@Override
	public Class<DelegatingResultSet> handledType() {
		return DelegatingResultSet.class;
	}

}