package io.boodskap.iot.sql;

import java.sql.ResultSet;

public class TomcatResultSetSerializer extends JdbcResultSetSerializer<ResultSet> {

	@Override
	public Class<ResultSet> handledType() {
		return ResultSet.class;
	}

}