package io.boodskap.iot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONObject;

public class SearchResult<T> implements Serializable{

	private static final long serialVersionUID = 5767474740509444835L;
	
	private Collection<T> result;
	private Object custom;

	public SearchResult() {
	}

	public SearchResult(Collection<T> result) {
		this.result = result;
	}

	public SearchResult(Collection<T> result, Object custom) {
		this.result = result;
		this.custom = custom;
	}

	public Collection<T> getResult() {
		return result;
	}

	public void setResult(Collection<T> result) {
		this.result = result;
	}

	public Object getCustom() {
		return custom;
	}

	public void setCustom(Object custom) {
		this.custom = custom;
	}
	
	public JSONObject toJSON() {
		JSONObject j = new JSONObject();
		
		j.put("result", result != null ? result : new ArrayList<>());
		if(null != custom) j.put("custom", custom);
		
		return j;
	}

}
