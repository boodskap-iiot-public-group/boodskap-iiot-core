package io.boodskap.iot;

public enum JobState {
	ENABLED,
	DISABLED,
}
