/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.spi.grid;

import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

import io.boodskap.iot.GridException;
import io.boodskap.iot.PlatformCallable;
import io.boodskap.iot.PlatformRunnable;

public interface IGrid {
	
	public default <T> List<Future<T>> broadcast(PlatformCallable<T> task) throws GridException{
		return broadcast(task, true);
	}
	
	public default List<Future<?>> broadcast(PlatformRunnable task) throws GridException{
		return broadcast(task, true);
	}
	
	public INode thisNode() throws GridException;
	
	public List<INode> listNodes() throws GridException;
	
	public INode getNode(Object id) throws GridException;
	
	public Lock lock(String id) throws GridException;

	public <T> Future<T> submit(PlatformCallable<T> task) throws GridException;
	
	public Future<?> submit(PlatformRunnable task) throws GridException;
	
	public <T> List<Future<T>> broadcast(PlatformCallable<T> task, boolean includeThis) throws GridException;
	
	public List<Future<?>> broadcast(PlatformRunnable task, boolean includeThis) throws GridException;
	
	public void run(Runnable task) throws GridException;
	
	public <T> T call(PlatformCallable<T> task) throws GridException;
	
	public void addListener(INodeListener listener) throws GridException;

	public void removeListener(INodeListener listener) throws GridException;
	
	public boolean isMaster() throws GridException;

	public Future<?> submitNodeSingleton(PlatformRunnable task) throws GridException;
	
	public Future<?> submitClusterSingleton(PlatformRunnable task) throws GridException;
	
}
