package io.boodskap.iot.spi.service;

public class VoidServiceConfig implements IServiceConfig {

	private static final long serialVersionUID = -3975905770472780910L;

	@Override
	public int getStartPriority() {
		return 0;
	}

	@Override
	public void setDefaults() {
	}

}
