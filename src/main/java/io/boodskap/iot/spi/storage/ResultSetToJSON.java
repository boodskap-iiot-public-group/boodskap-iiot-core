package io.boodskap.iot.spi.storage;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import io.boodskap.iot.StorageException;

public class ResultSetToJSON {
	
	private final ResultSet rs;
	private ResultSetMetaData rsmd;
	private String[] columnNames;
	private String[] tableNames;

	public ResultSetToJSON(ResultSet rs) {
		this.rs = rs;
	}
	
	public boolean hasNext() {
		try {
			return rs.next();
		} catch (SQLException e) {
			throw new StorageException(e);
		}
	}

	public static JSONObject deepMerge(JSONObject source, JSONObject target) throws JSONException {
	    for (String key: JSONObject.getNames(source)) {
	            Object value = source.get(key);
	            if (!target.has(key)) {
	                // new value for "key":
	                target.put(key, value);
	            } else {
	                // existing value for "key" - recursively deep merge:
	                if (value instanceof JSONObject) {
	                    JSONObject valueJson = (JSONObject)value;
	                    deepMerge(valueJson, target.getJSONObject(key));
	                } else {
	                    target.put(key, value);
	                }
	            }
	    }
	    return target;
	}
	
	public JSONObject next() throws StorageException {
		
		try {
			
			if(null == rsmd) {
		        rsmd = rs.getMetaData();
		        
	            int numColumns = rsmd.getColumnCount();
	            columnNames = new String[numColumns];
	            tableNames = new String[numColumns];
	           
	            
	            for (int i = 0; i < columnNames.length; i++) {
	            	columnNames[i] = rsmd.getColumnLabel(i + 1).replaceAll("\"", "");
	                tableNames[i] = rsmd.getTableName(i+1).replaceAll("\"", "");
	            }
		        
			}
						
			JSONObject j = new JSONObject();
            JSONObject unknown = null;
			
	        for (int i = 0; i < columnNames.length; i++) {
	        	
	        	Object value = rs.getObject(i+1);
	        	final String key = columnNames[i];
	        	
            	if(null != value && "unknown".equals(key)){
            		unknown = new JSONObject(value.toString());
            		continue;
            	}

            	if(rs.wasNull() || null == value) {
	        		continue;
	        	}
	        	
	    		if (value instanceof Integer) {
	            	j.put(key, value);
	            } else if (value instanceof String) {
	            	j.put(key, value);
	            } else if (value instanceof Boolean) {
	            	j.put(key, value);
	            } else if (value instanceof Date) {
	            	j.put(key, ((Date) value).getTime());                
	            } else if (value instanceof Time) {
	            	j.put(key, ((Time) value).getTime());                
	            } else if (value instanceof Timestamp) {
	            	j.put(key, ((Timestamp) value).getTime());                
	            } else if (value instanceof Long) {
	            	j.put(key, value);
	            } else if (value instanceof Double) {
	            	j.put(key, value);
	            } else if (value instanceof Float) {
	            	j.put(key, value);
	            } else if (value instanceof BigDecimal) {
	            	j.put(key, ((BigDecimal)value).toEngineeringString());
	            } else if (value instanceof Byte) {
	            	j.put(key, ((Byte) value).intValue());                
	            } else if (value instanceof UUID) {
	                j.put(key, ((UUID)value).toString());                
	            } else if (value instanceof byte[]) {
	            	j.put(key, Base64.encodeBase64String((byte[])value));
	            } else if (value instanceof Blob) {
	            	Blob blob = (Blob)value;
	            	byte[] bytes = blob.getBytes(1, (int) blob.length());
	            	j.put(key, Base64.encodeBase64String(bytes));
	            } else if (value instanceof Clob) {
	            	Clob clob = (Clob)value;
	            	char[] chars = new char[(int)clob.length()];
	            	clob.getCharacterStream().read(chars);
	            	j.put(key, new String(chars));
	            } else if("oracle.sql.DATE".equals(value.getClass().getName())) {
	            	Method m = value.getClass().getMethod("dateValue");
	            	m.setAccessible(true);
	            	value = m.invoke(value);
	            	j.put(key, ((Date) value).getTime());                
	            } else if("oracle.sql.TIMESTAMP".equals(value.getClass().getName())) {
	            	Method m = value.getClass().getMethod("timestampValue");
	            	m.setAccessible(true);
	            	value = m.invoke(value);
	            	j.put(key, ((Timestamp) value).getTime());                
	            } else if("oracle.sql.TIMESTAMPTZ".equals(value.getClass().getName())) {
	            	j.put(key, rs.getTimestamp(i+1).getTime());
	            } else {
	                throw new StorageException("Unmappable object type: " + value.getClass());
	            }
	        	
	        }
			
            if(null != unknown && !unknown.keySet().isEmpty()) {
            	deepMerge(unknown, j);
            }
            
            j.remove("specId");
            j.remove("messageId");
            j.remove("recordId");
            
			return j;
			
		}catch(StorageException ex) {
			throw ex;
		}catch(Exception ex) {
			throw new StorageException(ex);
		}
		
		
	}
}
