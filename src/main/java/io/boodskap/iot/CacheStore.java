/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.boodskap.iot.dao.MessageSpecDAO;
import io.boodskap.iot.dao.RecordSpecDAO;
import io.boodskap.iot.model.IDomain;
import io.boodskap.iot.model.IMessageSpecification;
import io.boodskap.iot.model.IOrganization;
import io.boodskap.iot.model.IRecordSpecification;
import io.boodskap.iot.spi.cache.ICache;

public class CacheStore implements Serializable {

	private static final long serialVersionUID = -8860662658126337896L;

	private static final Logger LOG = LoggerFactory.getLogger(CacheStore.class);
	
	private static final CacheStore instance = new CacheStore();
	
	private Map<String, DomainSettings> settingsCache;
	private Map<String, OrganizationSettings> orgSettingsCache;
	private Map<String, IMessageSpecification> msgSpecCache; // domainKey.specId
	private Map<String, IRecordSpecification> recSpecCache; // domainKey.specId
	private Map<String, Long> deviceUpdateCache; //domainKey.deviceId
	private BlockingQueue<String> incomingMessages;
	private BlockingQueue<LogRecord> logs;
	private BlockingQueue<String> taskQueue;
	
	private int logOfferTimeout = BoodskapSystem.getConfig().getLogOfferTimeout();

	private CacheStore() {
	}
	
	public static CacheStore get() {
		return instance;
	}

	@SuppressWarnings("unchecked")
	protected synchronized void load() {
		
		ICache cache = BoodskapSystem.cache();
		
		{
			LOG.info("Initing domain settings cache");
			settingsCache = cache.getCache(String.class, DomainSettings.class, "DOM_SETTINGS_CACHE");
		}

		{
			LOG.info("Initing organization settings cache");
			orgSettingsCache = cache.getCache(String.class, OrganizationSettings.class, "ORG_SETTINGS_CACHE");
		}

		{
			LOG.info("Initing device update cache");
			deviceUpdateCache = cache.getCache(String.class, Long.class, "DEVICE_UPDATE_CACHE");
		}

		{
			LOG.info("Initing message spec cache");
			msgSpecCache = (Map<String, IMessageSpecification>) cache.getCache(String.class, IMessageSpecification.clazz(), "MSG_SPEC_CACHE");
		}

		{
			LOG.info("Initing record spec cache");
			recSpecCache = (Map<String, IRecordSpecification>) cache.getCache(String.class, IRecordSpecification.clazz(), "REC_SPEC_CACHE");
		}

		{
			LOG.info("Initing log Q");
			logs = cache.createOrGetQueue(LogRecord.class, "LOG_RECORDS");
		}

		{
			LOG.info("Initing task Q");
			taskQueue = cache.createOrGetQueue(String.class, "PLATFORM_TASKS");
		}

		{
			LOG.info("Initing incoming messages Q"); // Load this at last after all caches
			incomingMessages = cache.createOrGetQueue(String.class, "INCOMING_MESSAGES");
		}

		LOG.info("Loaded/Initialized all caches successfully");
		
	}
	
	public void set(IMessageSpecification entity) {
		msgSpecCache.put(String.format("%s.%s", entity.getDomainKey(), entity.getSpecId()), entity);
	}

	public void set(IRecordSpecification entity) {
		recSpecCache.put(String.format("%s.%s", entity.getDomainKey(), entity.getSpecId()), entity);
	}

	public boolean log(LogRecord rec) throws InterruptedException {
		return logs.offer(rec, logOfferTimeout, TimeUnit.SECONDS);
	}

	public IMessageSpecification loadOrGetMessageSpec(String domainKey, String specId) {
		
		String key = String.format("%s.%s", domainKey, specId);
		IMessageSpecification val = msgSpecCache.get(key);
		
		if(null != val) return val;
		
		if (null == val) {
			
			val = MessageSpecDAO.get().get(domainKey, specId);
			
			if (null != val) {
				msgSpecCache.put(key, val);
			}
		}
		
		return val;
	}

	public IRecordSpecification loadOrGetRecordSpec(String domainKey, String specId) {
		
		String key = String.format("%s.%s", domainKey, specId);
		IRecordSpecification val = recSpecCache.get(key);
		
		if(null != val) return val;
		
		key = String.format("%s.%s", domainKey, specId);
		val = recSpecCache.get(key);
		
		if (null == val) {
			
			val = RecordSpecDAO.get().get(domainKey, specId);
			
			if (null != val) {
				recSpecCache.put(key, val);
			}
		}
		
		return val;
	}

	public Map<String, IMessageSpecification> getMsgSpecCache() {
		return msgSpecCache;
	}

	public Map<String, IRecordSpecification> getRecSpecCache() {
		return recSpecCache;
	}

	public Map<String, Long> getDeviceUpdateCache() {
		return deviceUpdateCache;
	}

	public BlockingQueue<String> getIncomingMessages() {
		return incomingMessages;
	}

	public BlockingQueue<String> getTaskQueue() {
		return taskQueue;
	}

	public int getLogOfferTimeout() {
		return logOfferTimeout;
	}

	public DomainSettings getSettings(String domainKey) {
		
		if(null == IDomain.get(domainKey)) throw new StorageException(String.format("Domain %s not found", domainKey));
		
		DomainSettings s =  settingsCache.get(domainKey);
		if(null == s) {
			s = new DomainSettings();
			settingsCache.put(domainKey, s);
		}
		return s;
	}
	
	public void setSettings(DomainSettings settings) {
		settingsCache.put(settings.getDomainKey(), settings);
	}
	
	
	public OrganizationSettings getOrgSettings(String domainKey, String organizationId) {
		
		if(null == IDomain.get(domainKey)) throw new StorageException(String.format("Domain %s not found", domainKey));
		if(null == IOrganization.get(domainKey, organizationId)) throw new StorageException(String.format("Organization %s.%s not found", domainKey, organizationId));		
		
		OrganizationSettings s =  orgSettingsCache.get(String.format("%s.%s", domainKey, organizationId));
		if(null == s) {
			s = new OrganizationSettings();
		}
		return s;
	}
	
	public void setOrgSettings(OrganizationSettings settings) {
		settingsCache.put(String.format("%s.%s", settings.getDomainKey(), settings.getOrganizationId()), settings);
	}
}
