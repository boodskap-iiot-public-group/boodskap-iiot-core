package io.boodskap.iot;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=SQLFieldType.class)
public enum SQLFieldType {

	BOOLEAN,
	
	INT,

	TINYINT,

	SMALLINT,

	BIGINT,

	DECIMAL,

	DOUBLE,

	REAL,

	TIME,

	DATE,

	TIMESTAMP,

	VARCHAR,

	CHAR,

	UUID,

	BINARY,

	GEOMETRY;

}
