package io.boodskap.iot;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=ConnectionPoolType.class)
public enum ConnectionPoolType {
	C3P0,
	DBCP,
	HIKARI,
	TOMCAT
}
