package io.boodskap.iot;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Cache rebalance mode. When rebalancing is enabled (i.e. has value other than {@link #NONE}), distributed caches
 * will attempt to rebalance all necessary values from other grid nodes. This enumeration is used to configure
 * rebalancing via {@link org.apache.ignite.configuration.CacheConfiguration#getRebalanceMode()} configuration property. If not configured
 * explicitly, then {@link org.apache.ignite.configuration.CacheConfiguration#DFLT_REBALANCE_MODE} is used.
 * <p>
 * Replicated caches will try to load the full set of cache entries from other nodes (or as defined by
 * pluggable {@link AffinityFunction}), while partitioned caches will only load the entries for which
 * current node is primary or back up.
 * <p>
 * Note that rebalance mode only makes sense for {@link CacheMode#REPLICATED} and {@link CacheMode#PARTITIONED}
 * caches. Caches with {@link CacheMode#LOCAL} mode are local by definition and therefore cannot rebalance
 * any values from neighboring nodes.
 */
@JsonSerialize(as=CacheRebalanceMode.class)
public enum CacheRebalanceMode {
    /**
     * Synchronous rebalance mode. Distributed caches will not start until all necessary data
     * is loaded from other available grid nodes.
     */
    SYNC,

    /**
     * Asynchronous rebalance mode. Distributed caches will start immediately and will load all necessary
     * data from other available grid nodes in the background.
     */
    ASYNC,

    /**
     * In this mode no rebalancing will take place which means that caches will be either loaded on
     * demand from persistent store whenever data is accessed, or will be populated explicitly.
     */
    NONE;

}