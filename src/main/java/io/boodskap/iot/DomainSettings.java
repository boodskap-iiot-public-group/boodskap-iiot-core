package io.boodskap.iot;

import java.io.Serializable;

public class DomainSettings implements Serializable {

	private static final long serialVersionUID = -4710000257522379249L;
	
	private String domainKey;
	
	//
	// Common
	//
	
	private boolean autoCreateDevices = true;
	
	//
	// User TOKEN
	//

	private boolean canUserCreateUser = true;
	private boolean canUserCreateDevice = true;
	private boolean canUserCreateAsset = true;
	private boolean canUserCreateEntity = true;
	private boolean canUserCreateUserEntity = true;
	private boolean canUserCreateDeviceEntity = true;
	private boolean canUserCreateAssetEntity = true;
	private boolean canUserCreateOrgUser = false;
	private boolean canUserCreateOrgDevice = false;
	private boolean canUserCreateOrgAsset = false;
	private boolean canUserCreateOrgEntity = false;
	private boolean canUserCreateOrgUserEntity = false;
	private boolean canUserCreateOrgDeviceEntity = false;
	private boolean canUserCreateOrgAssetEntity = false;
	
	
	private boolean canUserDeleteUser = false;
	private boolean canUserDeleteDevice = false;
	private boolean canUserDeleteAsset = false;
	private boolean canUserDeleteEntity = false;
	private boolean canUserDeleteUserEntity = false;
	private boolean canUserDeleteDeviceEntity = false;
	private boolean canUserDeleteAssetEntity = false;
	private boolean canUserDeleteOrgUser = false;
	private boolean canUserDeleteOrgDevice = false;
	private boolean canUserDeleteOrgAsset = false;
	private boolean canUserDeleteOrgEntity = false;
	private boolean canUserDeleteOrgUserEntity = false;
	private boolean canUserDeleteOrgDeviceEntity = false;
	private boolean canUserDeleteOrgAssetEntity = false;

	private boolean canUserSearchByQuery = true;	
	private boolean canUserSelectByQuery = true;
	private boolean canUserUpdateByQuery = true;
	private boolean canUserDeleteByQuery = true;
	private boolean canUserSearchByOrgQuery = false;	
	private boolean canUserSelectByOrgQuery = false;
	private boolean canUserUpdateByOrgQuery = false;
	private boolean canUserDeleteByOrgQuery = false;


	//
	// Device TOKEN
	//

	private boolean canDeviceCreateUser = true;
	private boolean canDeviceCreateDevice = true;
	private boolean canDeviceCreateAsset = true;
	private boolean canDeviceCreateEntity = true;
	private boolean canDeviceCreateUserEntity = true;
	private boolean canDeviceCreateDeviceEntity = true;
	private boolean canDeviceCreateAssetEntity = true;
	private boolean canDeviceCreateOrgUser = false;
	private boolean canDeviceCreateOrgDevice = false;
	private boolean canDeviceCreateOrgAsset = false;
	private boolean canDeviceCreateOrgEntity = false;
	private boolean canDeviceCreateOrgUserEntity = false;
	private boolean canDeviceCreateOrgDeviceEntity = false;
	private boolean canDeviceCreateOrgAssetEntity = false;
	
	private boolean canDeviceDeleteUser = false;
	private boolean canDeviceDeleteDevice = false;
	private boolean canDeviceDeleteAsset = false;
	private boolean canDeviceDeleteEntity = false;
	private boolean canDeviceDeleteUserEntity = false;
	private boolean canDeviceDeleteDeviceEntity = false;
	private boolean canDeviceDeleteAssetEntity = false;
	private boolean canDeviceDeleteOrgUser = false;
	private boolean canDeviceDeleteOrgDevice = false;
	private boolean canDeviceDeleteOrgAsset = false;
	private boolean canDeviceDeleteOrgEntity = false;
	private boolean canDeviceDeleteOrgUserEntity = false;
	private boolean canDeviceDeleteOrgDeviceEntity = false;
	private boolean canDeviceDeleteOrgAssetEntity = false;

	private boolean canDeviceSearchByQuery = true;	
	private boolean canDeviceSelectByQuery = true;
	private boolean canDeviceUpdateByQuery = true;
	private boolean canDeviceDeleteByQuery = true;	
	private boolean canDeviceSearchByOrgQuery = false;	
	private boolean canDeviceSelectByOrgQuery = false;
	private boolean canDeviceUpdateByOrgQuery = false;
	private boolean canDeviceDeleteByOrgQuery = false;	
		
	//
	// Organization User TOKEN
	//
	
	private boolean canOrgUserCreateUser = false;
	private boolean canOrgUserCreateDevice = false;
	private boolean canOrgUserCreateAsset = false;
	private boolean canOrgUserCreateEntity = false;
	private boolean canOrgUserCreateOrgUser = true;
	private boolean canOrgUserCreateOrgDevice = true;
	private boolean canOrgUserCreateOrgAsset = true;
	private boolean canOrgUserCreateOrgEntity = true;
	private boolean canOrgUserCreateOrgUserEntity = true;
	private boolean canOrgUserCreateOrgDeviceEnntity = true;
	private boolean canOrgUserCreateOrgAssetEntity = true;
	
	private boolean canOrgUserDeleteUser = false;
	private boolean canOrgUserDeleteDevice = false;
	private boolean canOrgUserDeleteAsset = false;
	private boolean canOrgUserDeleteEntity = false;
	private boolean canOrgUserDeleteOrgUser = true;
	private boolean canOrgUserDeleteOrgDevice = true;
	private boolean canOrgUserDeleteOrgAsset = true;
	private boolean canOrgUserDeleteOrgEntity = true;
	private boolean canOrgUserDeleteOrgUserEntity = true;
	private boolean canOrgUserDeleteOrgDeviceEntity = true;
	private boolean canOrgUserDeleteOrgAssetEntity = true;
	
	private boolean canOrgUserSearchByQuery = false;	
	private boolean canOrgUserSelectByQuery = false;
	private boolean canOrgUserUpdateByQuery = false;
	private boolean canOrgUserDeleteByQuery = false;
	private boolean canOrgUserSearchByOrgQuery = true;	
	private boolean canOrgUserSelectByOrgQuery = true;
	private boolean canOrgUserUpdateByOrgQuery = true;
	private boolean canOrgUserDeleteByOrgQuery = true;

	//
	// Organization TOKEN
	//
	
	private boolean canOrgCreateUser = false;
	private boolean canOrgCreateDevice = false;
	private boolean canOrgCreateAsset = false;
	private boolean canOrgCreateEntity = false;
	private boolean canOrgCreateUserEntity = false;
	private boolean canOrgCreateDeviceEntity = false;
	private boolean canOrgCreateAssetEntity = false;
	private boolean canOrgCreateOrgUser = true;
	private boolean canOrgCreateOrgDevice = true;
	private boolean canOrgCreateOrgAsset = true;
	private boolean canOrgCreateOrgEntity = true;
	private boolean canOrgCreateOrgUserEntity = true;
	private boolean canOrgCreateOrgDeviceEnntity = true;
	private boolean canOrgCreateOrgAssetEntity = true;

	private boolean canOrgDeleteUser = false;
	private boolean canOrgDeleteDevice = false;
	private boolean canOrgDeleteAsset = false;
	private boolean canOrgDeleteEntity = false;
	private boolean canOrgDeleteUserEntity = false;
	private boolean canOrgDeleteDeviceEntity = false;
	private boolean canOrgDeleteAssetEntity = false;
	private boolean canOrgDeleteOrgUser = true;
	private boolean canOrgDeleteOrgDevice = true;
	private boolean canOrgDeleteOrgAsset = true;
	private boolean canOrgDeleteOrgEntity = true;
	private boolean canOrgDeleteOrgUserEntity = true;
	private boolean canOrgDeleteOrgDeviceEntity = true;
	private boolean canOrgDeleteOrgAssetEntity = true;

	private boolean canOrgSearchByQuery = false;	
	private boolean canOrgSelectByQuery = false;
	private boolean canOrgUpdateByQuery = false;
	private boolean canOrgDeleteByQuery = false;
	private boolean canOrgSearchByOrgQuery = true;	
	private boolean canOrgSelectByOrgQuery = true;
	private boolean canOrgUpdateByOrgQuery = true;
	private boolean canOrgDeleteByOrgQuery = true;

	//
	// Organization Device TOKEN
	//

	private boolean canOrgDeviceCreateUser = false;
	private boolean canOrgDeviceCreateDevice = true;
	private boolean canOrgDeviceCreateAsset = true;
	private boolean canOrgDeviceCreateEntity = true;
	private boolean canOrgDeviceCreateUserEntity = false;
	private boolean canOrgDeviceCreateDeviceEntity = true;
	private boolean canOrgDeviceCreateAssetEntity = true;
	private boolean canOrgDeviceCreateOrgUser = false;
	private boolean canOrgDeviceCreateOrgDevice = false;
	private boolean canOrgDeviceCreateOrgAsset = false;
	private boolean canOrgDeviceCreateOrgEntity = false;
	private boolean canOrgDeviceCreateOrgUserEntity = false;
	private boolean canOrgDeviceCreateOrgDeviceEntity = false;
	private boolean canOrgDeviceCreateOrgAssetEntity = false;
	
	private boolean canOrgDeviceDeleteUser = false;
	private boolean canOrgDeviceDeleteDevice = false;
	private boolean canOrgDeviceDeleteAsset = false;
	private boolean canOrgDeviceDeleteEntity = false;
	private boolean canOrgDeviceDeleteUserEntity = false;
	private boolean canOrgDeviceDeleteDeviceEntity = false;
	private boolean canOrgDeviceDeleteAssetEntity = false;
	private boolean canOrgDeviceDeleteOrgUser = false;
	private boolean canOrgDeviceDeleteOrgDevice = false;
	private boolean canOrgDeviceDeleteOrgAsset = false;
	private boolean canOrgDeviceDeleteOrgEntity = false;
	private boolean canOrgDeviceDeleteOrgUserEntity = false;
	private boolean canOrgDeviceDeleteOrgDeviceEntity = false;
	private boolean canOrgDeviceDeleteOrgAssetEntity = false;

	private boolean canOrgDeviceSearchByQuery = false;	
	private boolean canOrgDeviceSelectByQuery = false;
	private boolean canOrgDeviceUpdateByQuery = false;
	private boolean canOrgDeviceDeleteByQuery = false;	
	private boolean canOrgDeviceSearchByOrgQuery = true;	
	private boolean canOrgDeviceSelectByOrgQuery = true;
	private boolean canOrgDeviceUpdateByOrgQuery = true;
	private boolean canOrgDeviceDeleteByOrgQuery = true;	
	
	public DomainSettings() {
	}

	public DomainSettings(String domainKey) {
		this.domainKey = domainKey;
	}

	public String getDomainKey() {
		return domainKey;
	}

	public void setDomainKey(String domainKey) {
		this.domainKey = domainKey;
	}

	public boolean isCanUserCreateUser() {
		return canUserCreateUser;
	}

	public void setCanUserCreateUser(boolean canUserCreateUser) {
		this.canUserCreateUser = canUserCreateUser;
	}

	public boolean isCanUserCreateDevice() {
		return canUserCreateDevice;
	}

	public void setCanUserCreateDevice(boolean canUserCreateDevice) {
		this.canUserCreateDevice = canUserCreateDevice;
	}

	public boolean isCanUserCreateAsset() {
		return canUserCreateAsset;
	}

	public void setCanUserCreateAsset(boolean canUserCreateAsset) {
		this.canUserCreateAsset = canUserCreateAsset;
	}

	public boolean isCanUserCreateEntity() {
		return canUserCreateEntity;
	}

	public void setCanUserCreateEntity(boolean canUserCreateEntity) {
		this.canUserCreateEntity = canUserCreateEntity;
	}

	public boolean isCanUserCreateUserEntity() {
		return canUserCreateUserEntity;
	}

	public void setCanUserCreateUserEntity(boolean canUserCreateUserEntity) {
		this.canUserCreateUserEntity = canUserCreateUserEntity;
	}

	public boolean isCanUserCreateDeviceEntity() {
		return canUserCreateDeviceEntity;
	}

	public void setCanUserCreateDeviceEntity(boolean canUserCreateDeviceEntity) {
		this.canUserCreateDeviceEntity = canUserCreateDeviceEntity;
	}

	public boolean isCanUserCreateAssetEntity() {
		return canUserCreateAssetEntity;
	}

	public void setCanUserCreateAssetEntity(boolean canUserCreateAssetEntity) {
		this.canUserCreateAssetEntity = canUserCreateAssetEntity;
	}

	public boolean isCanUserCreateOrgUser() {
		return canUserCreateOrgUser;
	}

	public void setCanUserCreateOrgUser(boolean canUserCreateOrgUser) {
		this.canUserCreateOrgUser = canUserCreateOrgUser;
	}

	public boolean isCanUserCreateOrgDevice() {
		return canUserCreateOrgDevice;
	}

	public void setCanUserCreateOrgDevice(boolean canUserCreateOrgDevice) {
		this.canUserCreateOrgDevice = canUserCreateOrgDevice;
	}

	public boolean isCanUserCreateOrgAsset() {
		return canUserCreateOrgAsset;
	}

	public void setCanUserCreateOrgAsset(boolean canUserCreateOrgAsset) {
		this.canUserCreateOrgAsset = canUserCreateOrgAsset;
	}

	public boolean isCanUserCreateOrgEntity() {
		return canUserCreateOrgEntity;
	}

	public void setCanUserCreateOrgEntity(boolean canUserCreateOrgEntity) {
		this.canUserCreateOrgEntity = canUserCreateOrgEntity;
	}

	public boolean isCanUserCreateOrgUserEntity() {
		return canUserCreateOrgUserEntity;
	}

	public void setCanUserCreateOrgUserEntity(boolean canUserCreateOrgUserEntity) {
		this.canUserCreateOrgUserEntity = canUserCreateOrgUserEntity;
	}

	public boolean isCanUserCreateOrgDeviceEntity() {
		return canUserCreateOrgDeviceEntity;
	}

	public void setCanUserCreateOrgDeviceEntity(boolean canUserCreateOrgDeviceEntity) {
		this.canUserCreateOrgDeviceEntity = canUserCreateOrgDeviceEntity;
	}

	public boolean isCanUserCreateOrgAssetEntity() {
		return canUserCreateOrgAssetEntity;
	}

	public void setCanUserCreateOrgAssetEntity(boolean canUserCreateOrgAssetEntity) {
		this.canUserCreateOrgAssetEntity = canUserCreateOrgAssetEntity;
	}

	public boolean isCanUserDeleteUser() {
		return canUserDeleteUser;
	}

	public void setCanUserDeleteUser(boolean canUserDeleteUser) {
		this.canUserDeleteUser = canUserDeleteUser;
	}

	public boolean isCanUserDeleteDevice() {
		return canUserDeleteDevice;
	}

	public void setCanUserDeleteDevice(boolean canUserDeleteDevice) {
		this.canUserDeleteDevice = canUserDeleteDevice;
	}

	public boolean isCanUserDeleteAsset() {
		return canUserDeleteAsset;
	}

	public void setCanUserDeleteAsset(boolean canUserDeleteAsset) {
		this.canUserDeleteAsset = canUserDeleteAsset;
	}

	public boolean isCanUserDeleteEntity() {
		return canUserDeleteEntity;
	}

	public void setCanUserDeleteEntity(boolean canUserDeleteEntity) {
		this.canUserDeleteEntity = canUserDeleteEntity;
	}

	public boolean isCanUserDeleteUserEntity() {
		return canUserDeleteUserEntity;
	}

	public void setCanUserDeleteUserEntity(boolean canUserDeleteUserEntity) {
		this.canUserDeleteUserEntity = canUserDeleteUserEntity;
	}

	public boolean isCanUserDeleteDeviceEntity() {
		return canUserDeleteDeviceEntity;
	}

	public void setCanUserDeleteDeviceEntity(boolean canUserDeleteDeviceEntity) {
		this.canUserDeleteDeviceEntity = canUserDeleteDeviceEntity;
	}

	public boolean isCanUserDeleteAssetEntity() {
		return canUserDeleteAssetEntity;
	}

	public void setCanUserDeleteAssetEntity(boolean canUserDeleteAssetEntity) {
		this.canUserDeleteAssetEntity = canUserDeleteAssetEntity;
	}

	public boolean isCanUserDeleteOrgUser() {
		return canUserDeleteOrgUser;
	}

	public void setCanUserDeleteOrgUser(boolean canUserDeleteOrgUser) {
		this.canUserDeleteOrgUser = canUserDeleteOrgUser;
	}

	public boolean isCanUserDeleteOrgDevice() {
		return canUserDeleteOrgDevice;
	}

	public void setCanUserDeleteOrgDevice(boolean canUserDeleteOrgDevice) {
		this.canUserDeleteOrgDevice = canUserDeleteOrgDevice;
	}

	public boolean isCanUserDeleteOrgAsset() {
		return canUserDeleteOrgAsset;
	}

	public void setCanUserDeleteOrgAsset(boolean canUserDeleteOrgAsset) {
		this.canUserDeleteOrgAsset = canUserDeleteOrgAsset;
	}

	public boolean isCanUserDeleteOrgEntity() {
		return canUserDeleteOrgEntity;
	}

	public void setCanUserDeleteOrgEntity(boolean canUserDeleteOrgEntity) {
		this.canUserDeleteOrgEntity = canUserDeleteOrgEntity;
	}

	public boolean isCanUserDeleteOrgUserEntity() {
		return canUserDeleteOrgUserEntity;
	}

	public void setCanUserDeleteOrgUserEntity(boolean canUserDeleteOrgUserEntity) {
		this.canUserDeleteOrgUserEntity = canUserDeleteOrgUserEntity;
	}

	public boolean isCanUserDeleteOrgDeviceEntity() {
		return canUserDeleteOrgDeviceEntity;
	}

	public void setCanUserDeleteOrgDeviceEntity(boolean canUserDeleteOrgDeviceEntity) {
		this.canUserDeleteOrgDeviceEntity = canUserDeleteOrgDeviceEntity;
	}

	public boolean isCanUserDeleteOrgAssetEntity() {
		return canUserDeleteOrgAssetEntity;
	}

	public void setCanUserDeleteOrgAssetEntity(boolean canUserDeleteOrgAssetEntity) {
		this.canUserDeleteOrgAssetEntity = canUserDeleteOrgAssetEntity;
	}

	public boolean isCanOrgUserCreateUser() {
		return canOrgUserCreateUser;
	}

	public void setCanOrgUserCreateUser(boolean canOrgUserCreateUser) {
		this.canOrgUserCreateUser = canOrgUserCreateUser;
	}

	public boolean isCanOrgUserCreateDevice() {
		return canOrgUserCreateDevice;
	}

	public void setCanOrgUserCreateDevice(boolean canOrgUserCreateDevice) {
		this.canOrgUserCreateDevice = canOrgUserCreateDevice;
	}

	public boolean isCanOrgUserCreateAsset() {
		return canOrgUserCreateAsset;
	}

	public void setCanOrgUserCreateAsset(boolean canOrgUserCreateAsset) {
		this.canOrgUserCreateAsset = canOrgUserCreateAsset;
	}

	public boolean isCanOrgUserCreateEntity() {
		return canOrgUserCreateEntity;
	}

	public void setCanOrgUserCreateEntity(boolean canOrgUserCreateEntity) {
		this.canOrgUserCreateEntity = canOrgUserCreateEntity;
	}

	public boolean isCanOrgUserCreateOrgUser() {
		return canOrgUserCreateOrgUser;
	}

	public void setCanOrgUserCreateOrgUser(boolean canOrgUserCreateOrgUser) {
		this.canOrgUserCreateOrgUser = canOrgUserCreateOrgUser;
	}

	public boolean isCanOrgUserCreateOrgDevice() {
		return canOrgUserCreateOrgDevice;
	}

	public void setCanOrgUserCreateOrgDevice(boolean canOrgUserCreateOrgDevice) {
		this.canOrgUserCreateOrgDevice = canOrgUserCreateOrgDevice;
	}

	public boolean isCanOrgUserCreateOrgAsset() {
		return canOrgUserCreateOrgAsset;
	}

	public void setCanOrgUserCreateOrgAsset(boolean canOrgUserCreateOrgAsset) {
		this.canOrgUserCreateOrgAsset = canOrgUserCreateOrgAsset;
	}

	public boolean isCanOrgUserCreateOrgEntity() {
		return canOrgUserCreateOrgEntity;
	}

	public void setCanOrgUserCreateOrgEntity(boolean canOrgUserCreateOrgEntity) {
		this.canOrgUserCreateOrgEntity = canOrgUserCreateOrgEntity;
	}

	public boolean isCanOrgUserCreateOrgUserEntity() {
		return canOrgUserCreateOrgUserEntity;
	}

	public void setCanOrgUserCreateOrgUserEntity(boolean canOrgUserCreateOrgUserEntity) {
		this.canOrgUserCreateOrgUserEntity = canOrgUserCreateOrgUserEntity;
	}

	public boolean isCanOrgUserCreateOrgDeviceEnntity() {
		return canOrgUserCreateOrgDeviceEnntity;
	}

	public void setCanOrgUserCreateOrgDeviceEnntity(boolean canOrgUserCreateOrgDeviceEnntity) {
		this.canOrgUserCreateOrgDeviceEnntity = canOrgUserCreateOrgDeviceEnntity;
	}

	public boolean isCanOrgUserCreateOrgAssetEntity() {
		return canOrgUserCreateOrgAssetEntity;
	}

	public void setCanOrgUserCreateOrgAssetEntity(boolean canOrgUserCreateOrgAssetEntity) {
		this.canOrgUserCreateOrgAssetEntity = canOrgUserCreateOrgAssetEntity;
	}

	public boolean isCanOrgUserDeleteUser() {
		return canOrgUserDeleteUser;
	}

	public void setCanOrgUserDeleteUser(boolean canOrgUserDeleteUser) {
		this.canOrgUserDeleteUser = canOrgUserDeleteUser;
	}

	public boolean isCanOrgUserDeleteDevice() {
		return canOrgUserDeleteDevice;
	}

	public void setCanOrgUserDeleteDevice(boolean canOrgUserDeleteDevice) {
		this.canOrgUserDeleteDevice = canOrgUserDeleteDevice;
	}

	public boolean isCanOrgUserDeleteAsset() {
		return canOrgUserDeleteAsset;
	}

	public void setCanOrgUserDeleteAsset(boolean canOrgUserDeleteAsset) {
		this.canOrgUserDeleteAsset = canOrgUserDeleteAsset;
	}

	public boolean isCanOrgUserDeleteEntity() {
		return canOrgUserDeleteEntity;
	}

	public void setCanOrgUserDeleteEntity(boolean canOrgUserDeleteEntity) {
		this.canOrgUserDeleteEntity = canOrgUserDeleteEntity;
	}

	public boolean isCanOrgUserDeleteOrgUser() {
		return canOrgUserDeleteOrgUser;
	}

	public void setCanOrgUserDeleteOrgUser(boolean canOrgUserDeleteOrgUser) {
		this.canOrgUserDeleteOrgUser = canOrgUserDeleteOrgUser;
	}

	public boolean isCanOrgUserDeleteOrgDevice() {
		return canOrgUserDeleteOrgDevice;
	}

	public void setCanOrgUserDeleteOrgDevice(boolean canOrgUserDeleteOrgDevice) {
		this.canOrgUserDeleteOrgDevice = canOrgUserDeleteOrgDevice;
	}

	public boolean isCanOrgUserDeleteOrgAsset() {
		return canOrgUserDeleteOrgAsset;
	}

	public void setCanOrgUserDeleteOrgAsset(boolean canOrgUserDeleteOrgAsset) {
		this.canOrgUserDeleteOrgAsset = canOrgUserDeleteOrgAsset;
	}

	public boolean isCanOrgUserDeleteOrgEntity() {
		return canOrgUserDeleteOrgEntity;
	}

	public void setCanOrgUserDeleteOrgEntity(boolean canOrgUserDeleteOrgEntity) {
		this.canOrgUserDeleteOrgEntity = canOrgUserDeleteOrgEntity;
	}

	public boolean isCanOrgUserDeleteOrgUserEntity() {
		return canOrgUserDeleteOrgUserEntity;
	}

	public void setCanOrgUserDeleteOrgUserEntity(boolean canOrgUserDeleteOrgUserEntity) {
		this.canOrgUserDeleteOrgUserEntity = canOrgUserDeleteOrgUserEntity;
	}

	public boolean isCanOrgUserDeleteOrgDeviceEntity() {
		return canOrgUserDeleteOrgDeviceEntity;
	}

	public void setCanOrgUserDeleteOrgDeviceEntity(boolean canOrgUserDeleteOrgDeviceEntity) {
		this.canOrgUserDeleteOrgDeviceEntity = canOrgUserDeleteOrgDeviceEntity;
	}

	public boolean isCanOrgUserDeleteOrgAssetEntity() {
		return canOrgUserDeleteOrgAssetEntity;
	}

	public void setCanOrgUserDeleteOrgAssetEntity(boolean canOrgUserDeleteOrgAssetEntity) {
		this.canOrgUserDeleteOrgAssetEntity = canOrgUserDeleteOrgAssetEntity;
	}

	public boolean isCanOrgCreateUser() {
		return canOrgCreateUser;
	}

	public void setCanOrgCreateUser(boolean canOrgCreateUser) {
		this.canOrgCreateUser = canOrgCreateUser;
	}

	public boolean isCanOrgCreateDevice() {
		return canOrgCreateDevice;
	}

	public void setCanOrgCreateDevice(boolean canOrgCreateDevice) {
		this.canOrgCreateDevice = canOrgCreateDevice;
	}

	public boolean isCanOrgCreateAsset() {
		return canOrgCreateAsset;
	}

	public void setCanOrgCreateAsset(boolean canOrgCreateAsset) {
		this.canOrgCreateAsset = canOrgCreateAsset;
	}

	public boolean isCanOrgCreateEntity() {
		return canOrgCreateEntity;
	}

	public void setCanOrgCreateEntity(boolean canOrgCreateEntity) {
		this.canOrgCreateEntity = canOrgCreateEntity;
	}

	public boolean isCanOrgCreateUserEntity() {
		return canOrgCreateUserEntity;
	}

	public void setCanOrgCreateUserEntity(boolean canOrgCreateUserEntity) {
		this.canOrgCreateUserEntity = canOrgCreateUserEntity;
	}

	public boolean isCanOrgCreateDeviceEntity() {
		return canOrgCreateDeviceEntity;
	}

	public void setCanOrgCreateDeviceEntity(boolean canOrgCreateDeviceEntity) {
		this.canOrgCreateDeviceEntity = canOrgCreateDeviceEntity;
	}

	public boolean isCanOrgCreateAssetEntity() {
		return canOrgCreateAssetEntity;
	}

	public void setCanOrgCreateAssetEntity(boolean canOrgCreateAssetEntity) {
		this.canOrgCreateAssetEntity = canOrgCreateAssetEntity;
	}

	public boolean isCanOrgCreateOrgUser() {
		return canOrgCreateOrgUser;
	}

	public void setCanOrgCreateOrgUser(boolean canOrgCreateOrgUser) {
		this.canOrgCreateOrgUser = canOrgCreateOrgUser;
	}

	public boolean isCanOrgCreateOrgDevice() {
		return canOrgCreateOrgDevice;
	}

	public void setCanOrgCreateOrgDevice(boolean canOrgCreateOrgDevice) {
		this.canOrgCreateOrgDevice = canOrgCreateOrgDevice;
	}

	public boolean isCanOrgCreateOrgAsset() {
		return canOrgCreateOrgAsset;
	}

	public void setCanOrgCreateOrgAsset(boolean canOrgCreateOrgAsset) {
		this.canOrgCreateOrgAsset = canOrgCreateOrgAsset;
	}

	public boolean isCanOrgCreateOrgEntity() {
		return canOrgCreateOrgEntity;
	}

	public void setCanOrgCreateOrgEntity(boolean canOrgCreateOrgEntity) {
		this.canOrgCreateOrgEntity = canOrgCreateOrgEntity;
	}

	public boolean isCanOrgCreateOrgUserEntity() {
		return canOrgCreateOrgUserEntity;
	}

	public void setCanOrgCreateOrgUserEntity(boolean canOrgCreateOrgUserEntity) {
		this.canOrgCreateOrgUserEntity = canOrgCreateOrgUserEntity;
	}

	public boolean isCanOrgCreateOrgDeviceEnntity() {
		return canOrgCreateOrgDeviceEnntity;
	}

	public void setCanOrgCreateOrgDeviceEnntity(boolean canOrgCreateOrgDeviceEnntity) {
		this.canOrgCreateOrgDeviceEnntity = canOrgCreateOrgDeviceEnntity;
	}

	public boolean isCanOrgCreateOrgAssetEntity() {
		return canOrgCreateOrgAssetEntity;
	}

	public void setCanOrgCreateOrgAssetEntity(boolean canOrgCreateOrgAssetEntity) {
		this.canOrgCreateOrgAssetEntity = canOrgCreateOrgAssetEntity;
	}

	public boolean isCanOrgDeleteUser() {
		return canOrgDeleteUser;
	}

	public void setCanOrgDeleteUser(boolean canOrgDeleteUser) {
		this.canOrgDeleteUser = canOrgDeleteUser;
	}

	public boolean isCanOrgDeleteDevice() {
		return canOrgDeleteDevice;
	}

	public void setCanOrgDeleteDevice(boolean canOrgDeleteDevice) {
		this.canOrgDeleteDevice = canOrgDeleteDevice;
	}

	public boolean isCanOrgDeleteAsset() {
		return canOrgDeleteAsset;
	}

	public void setCanOrgDeleteAsset(boolean canOrgDeleteAsset) {
		this.canOrgDeleteAsset = canOrgDeleteAsset;
	}

	public boolean isCanOrgDeleteEntity() {
		return canOrgDeleteEntity;
	}

	public void setCanOrgDeleteEntity(boolean canOrgDeleteEntity) {
		this.canOrgDeleteEntity = canOrgDeleteEntity;
	}

	public boolean isCanOrgDeleteUserEntity() {
		return canOrgDeleteUserEntity;
	}

	public void setCanOrgDeleteUserEntity(boolean canOrgDeleteUserEntity) {
		this.canOrgDeleteUserEntity = canOrgDeleteUserEntity;
	}

	public boolean isCanOrgDeleteDeviceEntity() {
		return canOrgDeleteDeviceEntity;
	}

	public void setCanOrgDeleteDeviceEntity(boolean canOrgDeleteDeviceEntity) {
		this.canOrgDeleteDeviceEntity = canOrgDeleteDeviceEntity;
	}

	public boolean isCanOrgDeleteAssetEntity() {
		return canOrgDeleteAssetEntity;
	}

	public void setCanOrgDeleteAssetEntity(boolean canOrgDeleteAssetEntity) {
		this.canOrgDeleteAssetEntity = canOrgDeleteAssetEntity;
	}

	public boolean isCanOrgDeleteOrgUser() {
		return canOrgDeleteOrgUser;
	}

	public void setCanOrgDeleteOrgUser(boolean canOrgDeleteOrgUser) {
		this.canOrgDeleteOrgUser = canOrgDeleteOrgUser;
	}

	public boolean isCanOrgDeleteOrgDevice() {
		return canOrgDeleteOrgDevice;
	}

	public void setCanOrgDeleteOrgDevice(boolean canOrgDeleteOrgDevice) {
		this.canOrgDeleteOrgDevice = canOrgDeleteOrgDevice;
	}

	public boolean isCanOrgDeleteOrgAsset() {
		return canOrgDeleteOrgAsset;
	}

	public void setCanOrgDeleteOrgAsset(boolean canOrgDeleteOrgAsset) {
		this.canOrgDeleteOrgAsset = canOrgDeleteOrgAsset;
	}

	public boolean isCanOrgDeleteOrgEntity() {
		return canOrgDeleteOrgEntity;
	}

	public void setCanOrgDeleteOrgEntity(boolean canOrgDeleteOrgEntity) {
		this.canOrgDeleteOrgEntity = canOrgDeleteOrgEntity;
	}

	public boolean isCanOrgDeleteOrgUserEntity() {
		return canOrgDeleteOrgUserEntity;
	}

	public void setCanOrgDeleteOrgUserEntity(boolean canOrgDeleteOrgUserEntity) {
		this.canOrgDeleteOrgUserEntity = canOrgDeleteOrgUserEntity;
	}

	public boolean isCanOrgDeleteOrgDeviceEntity() {
		return canOrgDeleteOrgDeviceEntity;
	}

	public void setCanOrgDeleteOrgDeviceEntity(boolean canOrgDeleteOrgDeviceEntity) {
		this.canOrgDeleteOrgDeviceEntity = canOrgDeleteOrgDeviceEntity;
	}

	public boolean isCanOrgDeleteOrgAssetEntity() {
		return canOrgDeleteOrgAssetEntity;
	}

	public void setCanOrgDeleteOrgAssetEntity(boolean canOrgDeleteOrgAssetEntity) {
		this.canOrgDeleteOrgAssetEntity = canOrgDeleteOrgAssetEntity;
	}

	public boolean isCanDeviceCreateUser() {
		return canDeviceCreateUser;
	}

	public void setCanDeviceCreateUser(boolean canDeviceCreateUser) {
		this.canDeviceCreateUser = canDeviceCreateUser;
	}

	public boolean isCanDeviceCreateDevice() {
		return canDeviceCreateDevice;
	}

	public void setCanDeviceCreateDevice(boolean canDeviceCreateDevice) {
		this.canDeviceCreateDevice = canDeviceCreateDevice;
	}

	public boolean isCanDeviceCreateAsset() {
		return canDeviceCreateAsset;
	}

	public void setCanDeviceCreateAsset(boolean canDeviceCreateAsset) {
		this.canDeviceCreateAsset = canDeviceCreateAsset;
	}

	public boolean isCanDeviceCreateEntity() {
		return canDeviceCreateEntity;
	}

	public void setCanDeviceCreateEntity(boolean canDeviceCreateEntity) {
		this.canDeviceCreateEntity = canDeviceCreateEntity;
	}

	public boolean isCanDeviceCreateUserEntity() {
		return canDeviceCreateUserEntity;
	}

	public void setCanDeviceCreateUserEntity(boolean canDeviceCreateUserEntity) {
		this.canDeviceCreateUserEntity = canDeviceCreateUserEntity;
	}

	public boolean isCanDeviceCreateDeviceEntity() {
		return canDeviceCreateDeviceEntity;
	}

	public void setCanDeviceCreateDeviceEntity(boolean canDeviceCreateDeviceEntity) {
		this.canDeviceCreateDeviceEntity = canDeviceCreateDeviceEntity;
	}

	public boolean isCanDeviceCreateAssetEntity() {
		return canDeviceCreateAssetEntity;
	}

	public void setCanDeviceCreateAssetEntity(boolean canDeviceCreateAssetEntity) {
		this.canDeviceCreateAssetEntity = canDeviceCreateAssetEntity;
	}

	public boolean isCanDeviceCreateOrgUser() {
		return canDeviceCreateOrgUser;
	}

	public void setCanDeviceCreateOrgUser(boolean canDeviceCreateOrgUser) {
		this.canDeviceCreateOrgUser = canDeviceCreateOrgUser;
	}

	public boolean isCanDeviceCreateOrgDevice() {
		return canDeviceCreateOrgDevice;
	}

	public void setCanDeviceCreateOrgDevice(boolean canDeviceCreateOrgDevice) {
		this.canDeviceCreateOrgDevice = canDeviceCreateOrgDevice;
	}

	public boolean isCanDeviceCreateOrgAsset() {
		return canDeviceCreateOrgAsset;
	}

	public void setCanDeviceCreateOrgAsset(boolean canDeviceCreateOrgAsset) {
		this.canDeviceCreateOrgAsset = canDeviceCreateOrgAsset;
	}

	public boolean isCanDeviceCreateOrgEntity() {
		return canDeviceCreateOrgEntity;
	}

	public void setCanDeviceCreateOrgEntity(boolean canDeviceCreateOrgEntity) {
		this.canDeviceCreateOrgEntity = canDeviceCreateOrgEntity;
	}

	public boolean isCanDeviceCreateOrgUserEntity() {
		return canDeviceCreateOrgUserEntity;
	}

	public void setCanDeviceCreateOrgUserEntity(boolean canDeviceCreateOrgUserEntity) {
		this.canDeviceCreateOrgUserEntity = canDeviceCreateOrgUserEntity;
	}

	public boolean isCanDeviceCreateOrgDeviceEntity() {
		return canDeviceCreateOrgDeviceEntity;
	}

	public void setCanDeviceCreateOrgDeviceEntity(boolean canDeviceCreateOrgDeviceEntity) {
		this.canDeviceCreateOrgDeviceEntity = canDeviceCreateOrgDeviceEntity;
	}

	public boolean isCanDeviceCreateOrgAssetEntity() {
		return canDeviceCreateOrgAssetEntity;
	}

	public void setCanDeviceCreateOrgAssetEntity(boolean canDeviceCreateOrgAssetEntity) {
		this.canDeviceCreateOrgAssetEntity = canDeviceCreateOrgAssetEntity;
	}

	public boolean isCanDeviceDeleteUser() {
		return canDeviceDeleteUser;
	}

	public void setCanDeviceDeleteUser(boolean canDeviceDeleteUser) {
		this.canDeviceDeleteUser = canDeviceDeleteUser;
	}

	public boolean isCanDeviceDeleteDevice() {
		return canDeviceDeleteDevice;
	}

	public void setCanDeviceDeleteDevice(boolean canDeviceDeleteDevice) {
		this.canDeviceDeleteDevice = canDeviceDeleteDevice;
	}

	public boolean isCanDeviceDeleteAsset() {
		return canDeviceDeleteAsset;
	}

	public void setCanDeviceDeleteAsset(boolean canDeviceDeleteAsset) {
		this.canDeviceDeleteAsset = canDeviceDeleteAsset;
	}

	public boolean isCanDeviceDeleteEntity() {
		return canDeviceDeleteEntity;
	}

	public void setCanDeviceDeleteEntity(boolean canDeviceDeleteEntity) {
		this.canDeviceDeleteEntity = canDeviceDeleteEntity;
	}

	public boolean isCanDeviceDeleteUserEntity() {
		return canDeviceDeleteUserEntity;
	}

	public void setCanDeviceDeleteUserEntity(boolean canDeviceDeleteUserEntity) {
		this.canDeviceDeleteUserEntity = canDeviceDeleteUserEntity;
	}

	public boolean isCanDeviceDeleteDeviceEntity() {
		return canDeviceDeleteDeviceEntity;
	}

	public void setCanDeviceDeleteDeviceEntity(boolean canDeviceDeleteDeviceEntity) {
		this.canDeviceDeleteDeviceEntity = canDeviceDeleteDeviceEntity;
	}

	public boolean isCanDeviceDeleteAssetEntity() {
		return canDeviceDeleteAssetEntity;
	}

	public void setCanDeviceDeleteAssetEntity(boolean canDeviceDeleteAssetEntity) {
		this.canDeviceDeleteAssetEntity = canDeviceDeleteAssetEntity;
	}

	public boolean isCanDeviceDeleteOrgUser() {
		return canDeviceDeleteOrgUser;
	}

	public void setCanDeviceDeleteOrgUser(boolean canDeviceDeleteOrgUser) {
		this.canDeviceDeleteOrgUser = canDeviceDeleteOrgUser;
	}

	public boolean isCanDeviceDeleteOrgDevice() {
		return canDeviceDeleteOrgDevice;
	}

	public void setCanDeviceDeleteOrgDevice(boolean canDeviceDeleteOrgDevice) {
		this.canDeviceDeleteOrgDevice = canDeviceDeleteOrgDevice;
	}

	public boolean isCanDeviceDeleteOrgAsset() {
		return canDeviceDeleteOrgAsset;
	}

	public void setCanDeviceDeleteOrgAsset(boolean canDeviceDeleteOrgAsset) {
		this.canDeviceDeleteOrgAsset = canDeviceDeleteOrgAsset;
	}

	public boolean isCanDeviceDeleteOrgEntity() {
		return canDeviceDeleteOrgEntity;
	}

	public void setCanDeviceDeleteOrgEntity(boolean canDeviceDeleteOrgEntity) {
		this.canDeviceDeleteOrgEntity = canDeviceDeleteOrgEntity;
	}

	public boolean isCanDeviceDeleteOrgUserEntity() {
		return canDeviceDeleteOrgUserEntity;
	}

	public void setCanDeviceDeleteOrgUserEntity(boolean canDeviceDeleteOrgUserEntity) {
		this.canDeviceDeleteOrgUserEntity = canDeviceDeleteOrgUserEntity;
	}

	public boolean isCanDeviceDeleteOrgDeviceEntity() {
		return canDeviceDeleteOrgDeviceEntity;
	}

	public void setCanDeviceDeleteOrgDeviceEntity(boolean canDeviceDeleteOrgDeviceEntity) {
		this.canDeviceDeleteOrgDeviceEntity = canDeviceDeleteOrgDeviceEntity;
	}

	public boolean isCanDeviceDeleteOrgAssetEntity() {
		return canDeviceDeleteOrgAssetEntity;
	}

	public void setCanDeviceDeleteOrgAssetEntity(boolean canDeviceDeleteOrgAssetEntity) {
		this.canDeviceDeleteOrgAssetEntity = canDeviceDeleteOrgAssetEntity;
	}

	public boolean isCanOrgDeviceCreateUser() {
		return canOrgDeviceCreateUser;
	}

	public void setCanOrgDeviceCreateUser(boolean canOrgDeviceCreateUser) {
		this.canOrgDeviceCreateUser = canOrgDeviceCreateUser;
	}

	public boolean isCanOrgDeviceCreateDevice() {
		return canOrgDeviceCreateDevice;
	}

	public void setCanOrgDeviceCreateDevice(boolean canOrgDeviceCreateDevice) {
		this.canOrgDeviceCreateDevice = canOrgDeviceCreateDevice;
	}

	public boolean isCanOrgDeviceCreateAsset() {
		return canOrgDeviceCreateAsset;
	}

	public void setCanOrgDeviceCreateAsset(boolean canOrgDeviceCreateAsset) {
		this.canOrgDeviceCreateAsset = canOrgDeviceCreateAsset;
	}

	public boolean isCanOrgDeviceCreateEntity() {
		return canOrgDeviceCreateEntity;
	}

	public void setCanOrgDeviceCreateEntity(boolean canOrgDeviceCreateEntity) {
		this.canOrgDeviceCreateEntity = canOrgDeviceCreateEntity;
	}

	public boolean isCanOrgDeviceCreateUserEntity() {
		return canOrgDeviceCreateUserEntity;
	}

	public void setCanOrgDeviceCreateUserEntity(boolean canOrgDeviceCreateUserEntity) {
		this.canOrgDeviceCreateUserEntity = canOrgDeviceCreateUserEntity;
	}

	public boolean isCanOrgDeviceCreateDeviceEntity() {
		return canOrgDeviceCreateDeviceEntity;
	}

	public void setCanOrgDeviceCreateDeviceEntity(boolean canOrgDeviceCreateDeviceEntity) {
		this.canOrgDeviceCreateDeviceEntity = canOrgDeviceCreateDeviceEntity;
	}

	public boolean isCanOrgDeviceCreateAssetEntity() {
		return canOrgDeviceCreateAssetEntity;
	}

	public void setCanOrgDeviceCreateAssetEntity(boolean canOrgDeviceCreateAssetEntity) {
		this.canOrgDeviceCreateAssetEntity = canOrgDeviceCreateAssetEntity;
	}

	public boolean isCanOrgDeviceCreateOrgUser() {
		return canOrgDeviceCreateOrgUser;
	}

	public void setCanOrgDeviceCreateOrgUser(boolean canOrgDeviceCreateOrgUser) {
		this.canOrgDeviceCreateOrgUser = canOrgDeviceCreateOrgUser;
	}

	public boolean isCanOrgDeviceCreateOrgDevice() {
		return canOrgDeviceCreateOrgDevice;
	}

	public void setCanOrgDeviceCreateOrgDevice(boolean canOrgDeviceCreateOrgDevice) {
		this.canOrgDeviceCreateOrgDevice = canOrgDeviceCreateOrgDevice;
	}

	public boolean isCanOrgDeviceCreateOrgAsset() {
		return canOrgDeviceCreateOrgAsset;
	}

	public void setCanOrgDeviceCreateOrgAsset(boolean canOrgDeviceCreateOrgAsset) {
		this.canOrgDeviceCreateOrgAsset = canOrgDeviceCreateOrgAsset;
	}

	public boolean isCanOrgDeviceCreateOrgEntity() {
		return canOrgDeviceCreateOrgEntity;
	}

	public void setCanOrgDeviceCreateOrgEntity(boolean canOrgDeviceCreateOrgEntity) {
		this.canOrgDeviceCreateOrgEntity = canOrgDeviceCreateOrgEntity;
	}

	public boolean isCanOrgDeviceCreateOrgUserEntity() {
		return canOrgDeviceCreateOrgUserEntity;
	}

	public void setCanOrgDeviceCreateOrgUserEntity(boolean canOrgDeviceCreateOrgUserEntity) {
		this.canOrgDeviceCreateOrgUserEntity = canOrgDeviceCreateOrgUserEntity;
	}

	public boolean isCanOrgDeviceCreateOrgDeviceEntity() {
		return canOrgDeviceCreateOrgDeviceEntity;
	}

	public void setCanOrgDeviceCreateOrgDeviceEntity(boolean canOrgDeviceCreateOrgDeviceEntity) {
		this.canOrgDeviceCreateOrgDeviceEntity = canOrgDeviceCreateOrgDeviceEntity;
	}

	public boolean isCanOrgDeviceCreateOrgAssetEntity() {
		return canOrgDeviceCreateOrgAssetEntity;
	}

	public void setCanOrgDeviceCreateOrgAssetEntity(boolean canOrgDeviceCreateOrgAssetEntity) {
		this.canOrgDeviceCreateOrgAssetEntity = canOrgDeviceCreateOrgAssetEntity;
	}

	public boolean isCanOrgDeviceDeleteUser() {
		return canOrgDeviceDeleteUser;
	}

	public void setCanOrgDeviceDeleteUser(boolean canOrgDeviceDeleteUser) {
		this.canOrgDeviceDeleteUser = canOrgDeviceDeleteUser;
	}

	public boolean isCanOrgDeviceDeleteDevice() {
		return canOrgDeviceDeleteDevice;
	}

	public void setCanOrgDeviceDeleteDevice(boolean canOrgDeviceDeleteDevice) {
		this.canOrgDeviceDeleteDevice = canOrgDeviceDeleteDevice;
	}

	public boolean isCanOrgDeviceDeleteAsset() {
		return canOrgDeviceDeleteAsset;
	}

	public void setCanOrgDeviceDeleteAsset(boolean canOrgDeviceDeleteAsset) {
		this.canOrgDeviceDeleteAsset = canOrgDeviceDeleteAsset;
	}

	public boolean isCanOrgDeviceDeleteEntity() {
		return canOrgDeviceDeleteEntity;
	}

	public void setCanOrgDeviceDeleteEntity(boolean canOrgDeviceDeleteEntity) {
		this.canOrgDeviceDeleteEntity = canOrgDeviceDeleteEntity;
	}

	public boolean isCanOrgDeviceDeleteUserEntity() {
		return canOrgDeviceDeleteUserEntity;
	}

	public void setCanOrgDeviceDeleteUserEntity(boolean canOrgDeviceDeleteUserEntity) {
		this.canOrgDeviceDeleteUserEntity = canOrgDeviceDeleteUserEntity;
	}

	public boolean isCanOrgDeviceDeleteDeviceEntity() {
		return canOrgDeviceDeleteDeviceEntity;
	}

	public void setCanOrgDeviceDeleteDeviceEntity(boolean canOrgDeviceDeleteDeviceEntity) {
		this.canOrgDeviceDeleteDeviceEntity = canOrgDeviceDeleteDeviceEntity;
	}

	public boolean isCanOrgDeviceDeleteAssetEntity() {
		return canOrgDeviceDeleteAssetEntity;
	}

	public void setCanOrgDeviceDeleteAssetEntity(boolean canOrgDeviceDeleteAssetEntity) {
		this.canOrgDeviceDeleteAssetEntity = canOrgDeviceDeleteAssetEntity;
	}

	public boolean isCanOrgDeviceDeleteOrgUser() {
		return canOrgDeviceDeleteOrgUser;
	}

	public void setCanOrgDeviceDeleteOrgUser(boolean canOrgDeviceDeleteOrgUser) {
		this.canOrgDeviceDeleteOrgUser = canOrgDeviceDeleteOrgUser;
	}

	public boolean isCanOrgDeviceDeleteOrgDevice() {
		return canOrgDeviceDeleteOrgDevice;
	}

	public void setCanOrgDeviceDeleteOrgDevice(boolean canOrgDeviceDeleteOrgDevice) {
		this.canOrgDeviceDeleteOrgDevice = canOrgDeviceDeleteOrgDevice;
	}

	public boolean isCanOrgDeviceDeleteOrgAsset() {
		return canOrgDeviceDeleteOrgAsset;
	}

	public void setCanOrgDeviceDeleteOrgAsset(boolean canOrgDeviceDeleteOrgAsset) {
		this.canOrgDeviceDeleteOrgAsset = canOrgDeviceDeleteOrgAsset;
	}

	public boolean isCanOrgDeviceDeleteOrgEntity() {
		return canOrgDeviceDeleteOrgEntity;
	}

	public void setCanOrgDeviceDeleteOrgEntity(boolean canOrgDeviceDeleteOrgEntity) {
		this.canOrgDeviceDeleteOrgEntity = canOrgDeviceDeleteOrgEntity;
	}

	public boolean isCanOrgDeviceDeleteOrgUserEntity() {
		return canOrgDeviceDeleteOrgUserEntity;
	}

	public void setCanOrgDeviceDeleteOrgUserEntity(boolean canOrgDeviceDeleteOrgUserEntity) {
		this.canOrgDeviceDeleteOrgUserEntity = canOrgDeviceDeleteOrgUserEntity;
	}

	public boolean isCanOrgDeviceDeleteOrgDeviceEntity() {
		return canOrgDeviceDeleteOrgDeviceEntity;
	}

	public void setCanOrgDeviceDeleteOrgDeviceEntity(boolean canOrgDeviceDeleteOrgDeviceEntity) {
		this.canOrgDeviceDeleteOrgDeviceEntity = canOrgDeviceDeleteOrgDeviceEntity;
	}

	public boolean isCanOrgDeviceDeleteOrgAssetEntity() {
		return canOrgDeviceDeleteOrgAssetEntity;
	}

	public void setCanOrgDeviceDeleteOrgAssetEntity(boolean canOrgDeviceDeleteOrgAssetEntity) {
		this.canOrgDeviceDeleteOrgAssetEntity = canOrgDeviceDeleteOrgAssetEntity;
	}

	public boolean isAutoCreateDevices() {
		return autoCreateDevices;
	}

	public void setAutoCreateDevices(boolean autoCreateDevices) {
		this.autoCreateDevices = autoCreateDevices;
	}

	public boolean isCanUserSearchByQuery() {
		return canUserSearchByQuery;
	}

	public void setCanUserSearchByQuery(boolean canUserSearchByQuery) {
		this.canUserSearchByQuery = canUserSearchByQuery;
	}

	public boolean isCanUserSelectByQuery() {
		return canUserSelectByQuery;
	}

	public void setCanUserSelectByQuery(boolean canUserSelectByQuery) {
		this.canUserSelectByQuery = canUserSelectByQuery;
	}

	public boolean isCanUserUpdateByQuery() {
		return canUserUpdateByQuery;
	}

	public void setCanUserUpdateByQuery(boolean canUserUpdateByQuery) {
		this.canUserUpdateByQuery = canUserUpdateByQuery;
	}

	public boolean isCanUserDeleteByQuery() {
		return canUserDeleteByQuery;
	}

	public void setCanUserDeleteByQuery(boolean canUserDeleteByQuery) {
		this.canUserDeleteByQuery = canUserDeleteByQuery;
	}

	public boolean isCanUserSearchByOrgQuery() {
		return canUserSearchByOrgQuery;
	}

	public void setCanUserSearchByOrgQuery(boolean canUserSearchByOrgQuery) {
		this.canUserSearchByOrgQuery = canUserSearchByOrgQuery;
	}

	public boolean isCanUserSelectByOrgQuery() {
		return canUserSelectByOrgQuery;
	}

	public void setCanUserSelectByOrgQuery(boolean canUserSelectByOrgQuery) {
		this.canUserSelectByOrgQuery = canUserSelectByOrgQuery;
	}

	public boolean isCanUserUpdateByOrgQuery() {
		return canUserUpdateByOrgQuery;
	}

	public void setCanUserUpdateByOrgQuery(boolean canUserUpdateByOrgQuery) {
		this.canUserUpdateByOrgQuery = canUserUpdateByOrgQuery;
	}

	public boolean isCanUserDeleteByOrgQuery() {
		return canUserDeleteByOrgQuery;
	}

	public void setCanUserDeleteByOrgQuery(boolean canUserDeleteByOrgQuery) {
		this.canUserDeleteByOrgQuery = canUserDeleteByOrgQuery;
	}

	public boolean isCanDeviceSearchByQuery() {
		return canDeviceSearchByQuery;
	}

	public void setCanDeviceSearchByQuery(boolean canDeviceSearchByQuery) {
		this.canDeviceSearchByQuery = canDeviceSearchByQuery;
	}

	public boolean isCanDeviceSelectByQuery() {
		return canDeviceSelectByQuery;
	}

	public void setCanDeviceSelectByQuery(boolean canDeviceSelectByQuery) {
		this.canDeviceSelectByQuery = canDeviceSelectByQuery;
	}

	public boolean isCanDeviceUpdateByQuery() {
		return canDeviceUpdateByQuery;
	}

	public void setCanDeviceUpdateByQuery(boolean canDeviceUpdateByQuery) {
		this.canDeviceUpdateByQuery = canDeviceUpdateByQuery;
	}

	public boolean isCanDeviceDeleteByQuery() {
		return canDeviceDeleteByQuery;
	}

	public void setCanDeviceDeleteByQuery(boolean canDeviceDeleteByQuery) {
		this.canDeviceDeleteByQuery = canDeviceDeleteByQuery;
	}

	public boolean isCanDeviceSearchByOrgQuery() {
		return canDeviceSearchByOrgQuery;
	}

	public void setCanDeviceSearchByOrgQuery(boolean canDeviceSearchByOrgQuery) {
		this.canDeviceSearchByOrgQuery = canDeviceSearchByOrgQuery;
	}

	public boolean isCanDeviceSelectByOrgQuery() {
		return canDeviceSelectByOrgQuery;
	}

	public void setCanDeviceSelectByOrgQuery(boolean canDeviceSelectByOrgQuery) {
		this.canDeviceSelectByOrgQuery = canDeviceSelectByOrgQuery;
	}

	public boolean isCanDeviceUpdateByOrgQuery() {
		return canDeviceUpdateByOrgQuery;
	}

	public void setCanDeviceUpdateByOrgQuery(boolean canDeviceUpdateByOrgQuery) {
		this.canDeviceUpdateByOrgQuery = canDeviceUpdateByOrgQuery;
	}

	public boolean isCanDeviceDeleteByOrgQuery() {
		return canDeviceDeleteByOrgQuery;
	}

	public void setCanDeviceDeleteByOrgQuery(boolean canDeviceDeleteByOrgQuery) {
		this.canDeviceDeleteByOrgQuery = canDeviceDeleteByOrgQuery;
	}

	public boolean isCanOrgUserSearchByQuery() {
		return canOrgUserSearchByQuery;
	}

	public void setCanOrgUserSearchByQuery(boolean canOrgUserSearchByQuery) {
		this.canOrgUserSearchByQuery = canOrgUserSearchByQuery;
	}

	public boolean isCanOrgUserSelectByQuery() {
		return canOrgUserSelectByQuery;
	}

	public void setCanOrgUserSelectByQuery(boolean canOrgUserSelectByQuery) {
		this.canOrgUserSelectByQuery = canOrgUserSelectByQuery;
	}

	public boolean isCanOrgUserUpdateByQuery() {
		return canOrgUserUpdateByQuery;
	}

	public void setCanOrgUserUpdateByQuery(boolean canOrgUserUpdateByQuery) {
		this.canOrgUserUpdateByQuery = canOrgUserUpdateByQuery;
	}

	public boolean isCanOrgUserDeleteByQuery() {
		return canOrgUserDeleteByQuery;
	}

	public void setCanOrgUserDeleteByQuery(boolean canOrgUserDeleteByQuery) {
		this.canOrgUserDeleteByQuery = canOrgUserDeleteByQuery;
	}

	public boolean isCanOrgUserSearchByOrgQuery() {
		return canOrgUserSearchByOrgQuery;
	}

	public void setCanOrgUserSearchByOrgQuery(boolean canOrgUserSearchByOrgQuery) {
		this.canOrgUserSearchByOrgQuery = canOrgUserSearchByOrgQuery;
	}

	public boolean isCanOrgUserSelectByOrgQuery() {
		return canOrgUserSelectByOrgQuery;
	}

	public void setCanOrgUserSelectByOrgQuery(boolean canOrgUserSelectByOrgQuery) {
		this.canOrgUserSelectByOrgQuery = canOrgUserSelectByOrgQuery;
	}

	public boolean isCanOrgUserUpdateByOrgQuery() {
		return canOrgUserUpdateByOrgQuery;
	}

	public void setCanOrgUserUpdateByOrgQuery(boolean canOrgUserUpdateByOrgQuery) {
		this.canOrgUserUpdateByOrgQuery = canOrgUserUpdateByOrgQuery;
	}

	public boolean isCanOrgUserDeleteByOrgQuery() {
		return canOrgUserDeleteByOrgQuery;
	}

	public void setCanOrgUserDeleteByOrgQuery(boolean canOrgUserDeleteByOrgQuery) {
		this.canOrgUserDeleteByOrgQuery = canOrgUserDeleteByOrgQuery;
	}

	public boolean isCanOrgSearchByQuery() {
		return canOrgSearchByQuery;
	}

	public void setCanOrgSearchByQuery(boolean canOrgSearchByQuery) {
		this.canOrgSearchByQuery = canOrgSearchByQuery;
	}

	public boolean isCanOrgSelectByQuery() {
		return canOrgSelectByQuery;
	}

	public void setCanOrgSelectByQuery(boolean canOrgSelectByQuery) {
		this.canOrgSelectByQuery = canOrgSelectByQuery;
	}

	public boolean isCanOrgUpdateByQuery() {
		return canOrgUpdateByQuery;
	}

	public void setCanOrgUpdateByQuery(boolean canOrgUpdateByQuery) {
		this.canOrgUpdateByQuery = canOrgUpdateByQuery;
	}

	public boolean isCanOrgDeleteByQuery() {
		return canOrgDeleteByQuery;
	}

	public void setCanOrgDeleteByQuery(boolean canOrgDeleteByQuery) {
		this.canOrgDeleteByQuery = canOrgDeleteByQuery;
	}

	public boolean isCanOrgSearchByOrgQuery() {
		return canOrgSearchByOrgQuery;
	}

	public void setCanOrgSearchByOrgQuery(boolean canOrgSearchByOrgQuery) {
		this.canOrgSearchByOrgQuery = canOrgSearchByOrgQuery;
	}

	public boolean isCanOrgSelectByOrgQuery() {
		return canOrgSelectByOrgQuery;
	}

	public void setCanOrgSelectByOrgQuery(boolean canOrgSelectByOrgQuery) {
		this.canOrgSelectByOrgQuery = canOrgSelectByOrgQuery;
	}

	public boolean isCanOrgUpdateByOrgQuery() {
		return canOrgUpdateByOrgQuery;
	}

	public void setCanOrgUpdateByOrgQuery(boolean canOrgUpdateByOrgQuery) {
		this.canOrgUpdateByOrgQuery = canOrgUpdateByOrgQuery;
	}

	public boolean isCanOrgDeleteByOrgQuery() {
		return canOrgDeleteByOrgQuery;
	}

	public void setCanOrgDeleteByOrgQuery(boolean canOrgDeleteByOrgQuery) {
		this.canOrgDeleteByOrgQuery = canOrgDeleteByOrgQuery;
	}

	public boolean isCanOrgDeviceSearchByQuery() {
		return canOrgDeviceSearchByQuery;
	}

	public void setCanOrgDeviceSearchByQuery(boolean canOrgDeviceSearchByQuery) {
		this.canOrgDeviceSearchByQuery = canOrgDeviceSearchByQuery;
	}

	public boolean isCanOrgDeviceSelectByQuery() {
		return canOrgDeviceSelectByQuery;
	}

	public void setCanOrgDeviceSelectByQuery(boolean canOrgDeviceSelectByQuery) {
		this.canOrgDeviceSelectByQuery = canOrgDeviceSelectByQuery;
	}

	public boolean isCanOrgDeviceUpdateByQuery() {
		return canOrgDeviceUpdateByQuery;
	}

	public void setCanOrgDeviceUpdateByQuery(boolean canOrgDeviceUpdateByQuery) {
		this.canOrgDeviceUpdateByQuery = canOrgDeviceUpdateByQuery;
	}

	public boolean isCanOrgDeviceDeleteByQuery() {
		return canOrgDeviceDeleteByQuery;
	}

	public void setCanOrgDeviceDeleteByQuery(boolean canOrgDeviceDeleteByQuery) {
		this.canOrgDeviceDeleteByQuery = canOrgDeviceDeleteByQuery;
	}

	public boolean isCanOrgDeviceSearchByOrgQuery() {
		return canOrgDeviceSearchByOrgQuery;
	}

	public void setCanOrgDeviceSearchByOrgQuery(boolean canOrgDeviceSearchByOrgQuery) {
		this.canOrgDeviceSearchByOrgQuery = canOrgDeviceSearchByOrgQuery;
	}

	public boolean isCanOrgDeviceSelectByOrgQuery() {
		return canOrgDeviceSelectByOrgQuery;
	}

	public void setCanOrgDeviceSelectByOrgQuery(boolean canOrgDeviceSelectByOrgQuery) {
		this.canOrgDeviceSelectByOrgQuery = canOrgDeviceSelectByOrgQuery;
	}

	public boolean isCanOrgDeviceUpdateByOrgQuery() {
		return canOrgDeviceUpdateByOrgQuery;
	}

	public void setCanOrgDeviceUpdateByOrgQuery(boolean canOrgDeviceUpdateByOrgQuery) {
		this.canOrgDeviceUpdateByOrgQuery = canOrgDeviceUpdateByOrgQuery;
	}

	public boolean isCanOrgDeviceDeleteByOrgQuery() {
		return canOrgDeviceDeleteByOrgQuery;
	}

	public void setCanOrgDeviceDeleteByOrgQuery(boolean canOrgDeviceDeleteByOrgQuery) {
		this.canOrgDeviceDeleteByOrgQuery = canOrgDeviceDeleteByOrgQuery;
	}

}
