package io.boodskap.iot;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

public class KeyGenerator {
	
	public static final String newDomainKey(){
		String key = null;
		do{
			key = RandomStringUtils.random(12, true, false).toUpperCase();
		}while(!StringUtils.isAlpha(String.valueOf(key.charAt(0))));
		return key;
	}
	
	public static final String newApiKey(){
		return RandomStringUtils.random(14, true, true);
	}
	
	public static final String newAccessToken(){
		return RandomStringUtils.random(14, true, true);
	}
	
	public static final String newToken(){
		String key = null;
		do{
			key = RandomStringUtils.random(18, true, true);
		}while(!StringUtils.isAlpha(String.valueOf(key.charAt(0))) && !StringUtils.isAlpha(String.valueOf(key.charAt(17))));
		return key;
	}
	
}
