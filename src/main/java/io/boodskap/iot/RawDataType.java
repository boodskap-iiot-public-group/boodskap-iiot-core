/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=RawDataType.class)
public enum RawDataType {
	
	MESSAGE_RULE,
	BINARY_RULE,
	OFFLINE_SNAP,
	OFFLINE_STREAM,
	FILE_RULE,
	PROCESS_RULE,
	JOB_RULE,
	API_RULE,
	ORG_MESSAGE_RULE,
	ORG_BINARY_RULE,
	ORG_OFFLINE_SNAP,
	ORG_OFFLINE_STREAM,
	ORG_FILE_RULE,
	ORG_PROCESS_RULE,
	ORG_JOB_RULE,
	ORG_API_RULE,
}
