package io.boodskap.iot;

public enum WorkItemType {

    WORKFLOW_PROCESS,
    
    PROCESS,
    
    TRIGGER,
    
    TRIGGER_GROUP,
    
    WORKFLOW,
    
    STOP,
    
    TERMINATE,
    
    PAUSE;
}
