package io.boodskap.iot;

import java.io.File;
import java.util.concurrent.ThreadFactory;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.tinylog.Logger;

import io.boodskap.iot.spi.cache.ICache;
import io.boodskap.iot.spi.cache.ICacheFactory;
import io.boodskap.iot.spi.grid.IGrid;
import io.boodskap.iot.spi.grid.IGridFactory;
import io.boodskap.iot.spi.storage.IDynamicStorage;
import io.boodskap.iot.spi.storage.IDynamicStorageFactory;
import io.boodskap.iot.spi.storage.IRawStorage;
import io.boodskap.iot.spi.storage.IRawStorageFactory;
import io.boodskap.iot.spi.storage.IStorage;
import io.boodskap.iot.spi.storage.IStorageFactory;

public class BoodskapSystem {

	private static final BoodskapSystem instance = new BoodskapSystem();
	
	private static boolean testMode;

	private File homeDir;
	private File configFolder;
	private File configFile;
	private File dataDir;
	private File sharedDir;
	
	private IStorageFactory<?> storageManager;
	private IRawStorageFactory<?> rawStorageManager;
	private IDynamicStorageFactory<?> dynamicStorageManager;

	private JSONObject configJson;
	private BoodskapConfiguration config;
	private IGridFactory<?> gridManager;
	private ICacheFactory<?> cacheManager;
	private ThreadFactory platformThreadFactory;
	
	static {
		System.setProperty("IGNITE_NO_ASCII", "true");
		System.setProperty("IGNITE_QUIET", "true");
	}

	private BoodskapSystem() {
	}

	public static final BoodskapSystem get() {
		return instance;
	}
	
	protected void dispose() {
		
		try{if(null != gridManager) gridManager.dispose();}catch(Exception ex) {Logger.warn(ex, "Error while disposing grid");}
		try{if(null != cacheManager) cacheManager.dispose();}catch(Exception ex) {Logger.warn(ex, "Error while disposing cache");}
		try{if(null != storageManager) storageManager.dispose();}catch(Exception ex) {Logger.warn(ex, "Error while disposing storage");}
		try{if(null != rawStorageManager) rawStorageManager.dispose();}catch(Exception ex) {Logger.warn(ex, "Error while disposing raw storage");}
		try{if(null != dynamicStorageManager) dynamicStorageManager.dispose();}catch(Exception ex) {Logger.warn(ex, "Error while disposing dynamic storage");}
		
		gridManager = null;
		cacheManager = null;
		storageManager = null;
		rawStorageManager = null;
		dynamicStorageManager = null;
	}

	public static boolean isTestMode() {
		return testMode;
	}

	protected static void setTestMode(boolean testMode) {
		BoodskapSystem.testMode = testMode;
	}

	public static ICacheFactory<?> getCacheManager() {
		return instance.cacheManager;
	}

	public static IGridFactory<?> getGridManager() {
		return instance.gridManager;
	}

	public static IStorageFactory<?> getStorageManager() {
		return instance.storageManager;
	}

	public static IRawStorageFactory<?> getRawStorageManager() {
		return instance.rawStorageManager;
	}

	public static IDynamicStorageFactory<?> getDynamicStorageManager() {
		return instance.dynamicStorageManager;
	}

	public static BoodskapConfiguration getConfig() {
		return instance.config;
	}

	public static JSONObject getConfigJson() {
		return instance.configJson;
	}

	public static BoodskapConfiguration config() {
		return instance.config;
	}
	
	public static IGrid grid() throws GridException {
		return instance.gridManager.getGrid();
	}

	public static ICache cache() throws CacheException {
		return instance.cacheManager.getCache();
	}

	public static IStorage storage() throws StorageException {
		return instance.storageManager.getStorage();
	}

	public static IRawStorage rawStorage() throws StorageException {
		return instance.rawStorageManager.getRawStorage();
	}
	
	public static IDynamicStorage dynamicStorage() throws StorageException{
		return instance.dynamicStorageManager.getDynamicStorage();
	}
	
	protected void setStorageManager(IStorageFactory<?> storageManager) {
		this.storageManager = storageManager;
	}

	protected void setRawStorageManager(IRawStorageFactory<?> rawStorageManager) {
		this.rawStorageManager = rawStorageManager;
	}

	protected void setDynamicStorageManager(IDynamicStorageFactory<?> dynamicStorageManager) {
		this.dynamicStorageManager = dynamicStorageManager;
	}

	protected void setConfigJson(JSONObject configJson) {
		this.configJson = configJson;
	}

	protected void setConfig(BoodskapConfiguration config) {
		
		this.config = config;
		
		if(StringUtils.isNotBlank(config.getDataDir())) {
			setDataDir(new File(config.getDataDir()));
		}
		
		if(StringUtils.isNotBlank(config.getSharedDir())) {
			setSharedDir(new File(config.getSharedDir()));
		}
		
	}

	protected void setGridManager(IGridFactory<?> gridManager) {
		this.gridManager = gridManager;
	}

	protected void setCacheManager(ICacheFactory<?> cacheManager) {
		this.cacheManager = cacheManager;
	}

	public ThreadFactory getPlatformThreadFactory() {
		return platformThreadFactory;
	}

	protected void setPlatformThreadFactory(ThreadFactory platformThreadFactory) {
		this.platformThreadFactory = platformThreadFactory;
	}

	public File getHomeDir() {
		return homeDir;
	}

	protected void setHomeDir(File homeDir) {
		this.homeDir = homeDir;
	}

	public File getConfigFolder() {
		return configFolder;
	}

	protected void setConfigFolder(File configFolder) {
		this.configFolder = configFolder;
	}

	public File getConfigFile() {
		return configFile;
	}

	protected void setConfigFile(File configFile) {
		this.configFile = configFile;
	}

	public File getDataDir() {
		return dataDir;
	}

	protected void setDataDir(File dataDir) {
		this.dataDir = dataDir;
	}

	public File getSharedDir() {
		return sharedDir;
	}

	protected void setSharedDir(File sharedDir) {
		this.sharedDir = sharedDir;
	}

}
