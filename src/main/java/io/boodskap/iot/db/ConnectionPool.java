package io.boodskap.iot.db;

import java.sql.Connection;

public abstract class ConnectionPool extends BasicConnectionPool {
	
	public abstract String getDomainKey();

	public abstract String getId();
	
	public boolean execute(String sql) throws Exception {
		
		Connection con = getConnection();
	
		try {
			
			return con.createStatement().execute(sql);
			
		}finally {
			con.close();
		}
	}
}
