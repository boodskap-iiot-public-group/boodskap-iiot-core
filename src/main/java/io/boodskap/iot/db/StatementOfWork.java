package io.boodskap.iot.db;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.zaxxer.hikari.pool.HikariProxyResultSet;

import io.boodskap.iot.ThreadContext;

public class StatementOfWork implements AutoCloseable {

	private final ConnectionPool pool;
	private final Connection connection;
	private Set<Statement> statements = new HashSet<>();

	public StatementOfWork(String domainKey, String poolId) throws Exception {

		pool = ConnectionPoolManager.get(domainKey, poolId);
		
		if(null == pool) {
			throw new IllegalArgumentException(String.format("Connection pool %s not found", poolId));
		}
		
		this.connection = pool.getConnection();
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public ConnectionPool getPool() {
		return pool;
	}
	
	public String nativeSQL(String sql) throws SQLException {
		return connection.nativeSQL(sql);
	}

	public void commit() throws SQLException {
		connection.commit();
	}

	public DatabaseMetaData getMetaData() throws SQLException {
		return connection.getMetaData();
	}

	public String getCatalog() throws SQLException {
		return connection.getCatalog();
	}

	public int getTransactionIsolation() throws SQLException {
		return connection.getTransactionIsolation();
	}

	public void clearWarnings() throws SQLException {
		connection.clearWarnings();
	}

	public Statement createStatement() throws SQLException {
		Statement s =  connection.createStatement();
		statements.add(s);
		return s;
	}

	public PreparedStatement prepareStatement(String sql) throws SQLException {
		PreparedStatement s = connection.prepareStatement(sql);
		statements.add(s);
		return s;
	}

	public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
		Statement s = connection.createStatement(resultSetType, resultSetConcurrency);
		statements.add(s);
		return s;
	}

	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
		PreparedStatement s = connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
		statements.add(s);
		return s;
	}

	public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)throws SQLException {
		Statement s = connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
		statements.add(s);
		return s;
	}

	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
		PreparedStatement s = connection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
		statements.add(s);
		return s;
	}

	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
		PreparedStatement s = connection.prepareStatement(sql, autoGeneratedKeys);
		statements.add(s);
		return s;
	}

	public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
		PreparedStatement s = connection.prepareStatement(sql, columnIndexes);
		statements.add(s);
		return s;
	}

	public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
		PreparedStatement s = connection.prepareStatement(sql, columnNames);
		statements.add(s);
		return s;
	}

	public Clob createClob() throws SQLException {
		return connection.createClob();
	}

	public Blob createBlob() throws SQLException {
		return connection.createBlob();
	}

	public NClob createNClob() throws SQLException {
		return connection.createNClob();
	}

	@Override
	public void close() throws Exception{
		close(false);
	}
	
	public void close(boolean evict) {
		statements.forEach(s -> {
			try{s.close();}catch(Exception ex) {}
		});
		statements.clear();
		try{
			if(null != connection) {
				if(evict) {
					pool.evict(connection);
				}else {
					connection.close();
				}
			}
		}catch(Exception ex) {}
	}

	public Object execute(String query) throws SQLException {
		PreparedStatement stmt = prepareStatement(query);
		//Statement stmt = createStatement();
		if(stmt.execute()) {
			return stmt.getResultSet();
		}else {
			return stmt.getUpdateCount();
		}
	}
	
	public Map<String, Object> executeAsMap(String query) throws SQLException, IOException{
		String json = executeAsString(query);
		return ThreadContext.jsonToMap(json);
	}
	
	public ObjectNode executeAsObject(String query) throws SQLException, JsonProcessingException{
		
		ObjectNode node = pool.mapper().createObjectNode();
		
		ResultSet rset = null;
		
		Object ret = execute(query);
		
		if((ret instanceof HikariProxyResultSet) || (ret instanceof ResultSet)) {
			rset = (ResultSet) ret;
		}
		
		if(null != rset) {
			node.putPOJO("records", rset);
		}else {
			node.put("updated", (Integer)ret);
		}
		
		return node;
	}
	
	public String executeAsString(String query) throws SQLException, JsonProcessingException{
		ObjectNode node = executeAsObject(query);
		return pool.mapper().writeValueAsString(node);
	}
	
	public int executeInsert(String query) throws SQLException {
		Statement stmt = createStatement();
		return stmt.executeUpdate(query);
	}

}
