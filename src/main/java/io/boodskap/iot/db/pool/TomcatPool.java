package io.boodskap.iot.db.pool;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.tinylog.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import io.boodskap.iot.db.ConnectionPool;
import io.boodskap.iot.model.ITomcatJDBCArgs;
import io.boodskap.iot.sql.TomcatResultSetSerializer;

public class TomcatPool extends ConnectionPool {

	private static final ObjectMapper mapper = new ObjectMapper();
	
	static {
		SimpleModule module = new SimpleModule();
		module.addSerializer(new TomcatResultSetSerializer());
		mapper.registerModule(module);
	}
	
	private final String domainKey;
	private final String id;
	private final ITomcatJDBCArgs p;
	private DataSource ds;
	
	public TomcatPool(final String domainKey, final String id, final ITomcatJDBCArgs p) {
		this.domainKey = domainKey;
		this.id = id;
		this.p = p;
	}

	@Override
	public void create() throws Exception {
		
		Logger.info(String.format("Initiating Tomcat JDBC Connection pool %s.%s", domainKey, id));
		
		PoolProperties cfg = new PoolProperties();
		
		cfg.setUrl(p.getUrl());
		cfg.setUsername(p.getUsername());
		cfg.setPassword(p.getPassword());
		cfg.setDriverClassName(p.getDriverClassName());

		if(null != p.getAbandonWhenPercentageFull()) cfg.setAbandonWhenPercentageFull(p.getAbandonWhenPercentageFull());
		if(null != p.getAccessToUnderlyingConnectionAllowed()) cfg.setAccessToUnderlyingConnectionAllowed(p.getAccessToUnderlyingConnectionAllowed());
		if(null != p.getAlternateUsernameAllowed()) cfg.setAlternateUsernameAllowed(p.getAlternateUsernameAllowed());
		if(null != p.getCommitOnReturn()) cfg.setCommitOnReturn(p.getCommitOnReturn());
		if(null != p.getDefaultAutoCommit()) cfg.setDefaultAutoCommit(p.getDefaultAutoCommit());
		if(null != p.getDefaultCatalog()) cfg.setDefaultCatalog(p.getDefaultCatalog());
		if(null != p.getDefaultReadOnly()) cfg.setDefaultReadOnly(p.getDefaultReadOnly());
		if(null != p.getDefaultTransactionIsolation()) cfg.setDefaultTransactionIsolation(p.getDefaultTransactionIsolation());
		if(null != p.getFairQueue()) cfg.setFairQueue(p.getFairQueue());
		if(null != p.getIgnoreExceptionOnPreLoad()) cfg.setIgnoreExceptionOnPreLoad(p.getIgnoreExceptionOnPreLoad());
		if(null != p.getInitialSize()) cfg.setInitialSize(p.getInitialSize());
		if(null != p.getInitSQL()) cfg.setInitSQL(p.getInitSQL());
		if(null != p.getLogAbandoned()) cfg.setLogAbandoned(p.getLogAbandoned());
		if(null != p.getLogValidationErrors()) cfg.setLogValidationErrors(p.getLogValidationErrors());
		if(null != p.getMaxActive()) cfg.setMaxActive(p.getMaxActive());
		if(null != p.getMaxAge()) cfg.setMaxAge(p.getMaxAge());
		if(null != p.getMaxIdle()) cfg.setMaxIdle(p.getMaxIdle());
		if(null != p.getMaxWait()) cfg.setMaxWait(p.getMaxWait());
		if(null != p.getMinEvictableIdleTimeMillis()) cfg.setMinEvictableIdleTimeMillis(p.getMinEvictableIdleTimeMillis());
		if(null != p.getMinIdle()) cfg.setMinIdle(p.getMinIdle());
		if(null != p.getNumTestsPerEvictionRun()) cfg.setNumTestsPerEvictionRun(p.getNumTestsPerEvictionRun());
		if(null != p.getPropagateInterruptState()) cfg.setPropagateInterruptState(p.getPropagateInterruptState());
		if(null != p.getRemoveAbandoned()) cfg.setRemoveAbandoned(p.getRemoveAbandoned());
		if(null != p.getRemoveAbandonedTimeout()) cfg.setRemoveAbandonedTimeout(p.getRemoveAbandonedTimeout());
		if(null != p.getRollbackOnReturn()) cfg.setRollbackOnReturn(p.getRollbackOnReturn());
		if(null != p.getSuspectTimeout()) cfg.setSuspectTimeout(p.getSuspectTimeout());
		if(null != p.getTestOnBorrow()) cfg.setTestOnBorrow(p.getTestOnBorrow());
		if(null != p.getTestOnConnect()) cfg.setTestOnConnect(p.getTestOnConnect());
		if(null != p.getTestOnReturn()) cfg.setTestOnReturn(p.getTestOnReturn());
		if(null != p.getTestWhileIdle()) cfg.setTestWhileIdle(p.getTestWhileIdle());
		if(null != p.getTimeBetweenEvictionRunsMillis()) cfg.setTimeBetweenEvictionRunsMillis(p.getTimeBetweenEvictionRunsMillis());
		if(null != p.getUseDisposableConnectionFacade()) cfg.setUseDisposableConnectionFacade(p.getUseDisposableConnectionFacade());
		if(null != p.getUseEquals()) cfg.setUseEquals(p.getUseEquals());
		if(null != p.getUseLock()) cfg.setUseLock(p.getUseLock());
		if(null != p.getUseStatementFacade())cfg.setUseStatementFacade(p.getUseStatementFacade());
		if(null != p.getValidationInterval()) cfg.setValidationInterval(p.getValidationInterval());
		if(null != p.getValidationQuery()) cfg.setValidationQuery(p.getValidationQuery());
		if(null != p.getValidationQueryTimeout()) cfg.setValidationQueryTimeout(p.getValidationQueryTimeout());
		
		DataSource ds = new DataSource();
        ds.setPoolProperties(cfg);

		Connection conn = ds.getConnection();
		conn.close();
		this.ds = ds;
	}

	@Override
	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}

	@Override
	public void close() {
		if(null != ds) {
			Logger.warn(String.format("Closing Tomcat JDBC Connection pool %s.%s", domainKey, id));
			try {ds.close();ds = null;}catch(Exception ex) {Logger.warn(ex);}
		}
	}

	@Override
	public String getDomainKey() {
		return domainKey;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public ObjectMapper mapper() {
		return mapper;
	}
	
}
