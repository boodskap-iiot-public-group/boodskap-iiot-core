package io.boodskap.iot.db.pool;

import java.sql.Connection;
import java.sql.SQLException;

import org.tinylog.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import io.boodskap.iot.db.ConnectionPool;
import io.boodskap.iot.model.IHikariArgs;
import io.boodskap.iot.sql.HikariResultSetSerializer;

public class HikariPool extends ConnectionPool {
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	static {
		SimpleModule module = new SimpleModule();
		module.addSerializer(new HikariResultSetSerializer());
		mapper.registerModule(module);
	}
	
	private final String domainKey;
	private final String id;
	private final IHikariArgs p;
	private HikariDataSource ds;

	public HikariPool(final String domainKey, final String id, final IHikariArgs p) {
		this.domainKey = domainKey;
		this.id = id;
		this.p = p;
	}

	@Override
	public void create() throws Exception {
		
		Logger.info(String.format("Initiating Hikari Connection pool %s.%s", domainKey, id));
		
		HikariConfig cfg = new HikariConfig();
		
		cfg.setJdbcUrl(p.getJdbcUrl());
		cfg.setUsername(p.getUsername());
		cfg.setPassword(p.getPassword());
		if(null != p.getDataSourceClassName()) {
			cfg.setDataSourceClassName(p.getDataSourceClassName());
		}else {
			cfg.setDriverClassName(p.getDriverClassName());
		}
		
		if(null != p.getAllowPoolSuspension()) cfg.setAllowPoolSuspension(p.getAllowPoolSuspension());
		if(null != p.getAutoCommit()) cfg.setAutoCommit(p.getAutoCommit());
		if(null != p.getCatalog()) cfg.setCatalog(p.getCatalog());
		if(null != p.getConnectionInitSql()) cfg.setConnectionInitSql(p.getConnectionInitSql());
		if(null != p.getConnectionTestQuery()) cfg.setConnectionTestQuery(p.getConnectionTestQuery());
		if(null != p.getConnectionTimeout()) cfg.setConnectionTimeout(p.getConnectionTimeout());
		if(null != p.getIdleTimeout()) cfg.setIdleTimeout(p.getIdleTimeout());
		if(null != p.getInitializationFailTimeout()) cfg.setInitializationFailTimeout(p.getInitializationFailTimeout());
		if(null != p.getIsolateInternalQueries()) cfg.setIsolateInternalQueries(p.getIsolateInternalQueries());
		if(null != p.getLeakDetectionThreshold()) cfg.setLeakDetectionThreshold(p.getLeakDetectionThreshold());
		if(null != p.getMaximumPoolSize()) cfg.setMaximumPoolSize(p.getMaximumPoolSize());
		if(null != p.getMaxLifetime()) cfg.setMaxLifetime(p.getMaxLifetime());
		if(null != p.getMinimumIdle()) cfg.setMinimumIdle(p.getMinimumIdle());
		if(null != p.getReadOnly()) cfg.setReadOnly(p.getReadOnly());
		if(null != p.getSchema()) cfg.setSchema(p.getSchema());
		if(null != p.getTransactionIsolation()) cfg.setTransactionIsolation(p.getTransactionIsolation());
		if(null != p.getValidationTimeout()) cfg.setValidationTimeout(p.getValidationTimeout());
		
		HikariDataSource ds = new HikariDataSource(cfg);
		Connection conn = ds.getConnection();
		conn.close();
		this.ds = ds;
		
	}

	@Override
	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}

	@Override
	public void evict(Connection c) {
		ds.evictConnection(c);
	}

	@Override
	public void close() {
		if(null != ds) {
			Logger.warn(String.format("Closing Hikari Connection pool %s.%s", domainKey, id));
			try {ds.close(); ds=null;}catch(Exception ex) {Logger.warn(ex);}
		}
	}

	@Override
	public String getDomainKey() {
		return domainKey;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public ObjectMapper mapper() {
		return mapper;
	}
	
}
