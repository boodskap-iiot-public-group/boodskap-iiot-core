package io.boodskap.iot.db.pool;

import java.sql.Connection;
import java.sql.SQLException;

import org.tinylog.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import io.boodskap.iot.db.ConnectionPool;
import io.boodskap.iot.model.IC3P0Args;
import io.boodskap.iot.sql.C3P0ResultSetSerializer;

public class C3P0Pool extends ConnectionPool {
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	static {
		SimpleModule module = new SimpleModule();
		module.addSerializer(new C3P0ResultSetSerializer());
		mapper.registerModule(module);
	}
	
	private final String domainKey;
	private final String id;
	private final IC3P0Args p;
	private ComboPooledDataSource ds;

	public C3P0Pool(final String domainKey, final String id, final IC3P0Args p) {
		this.domainKey = domainKey;
		this.id = id;
		this.p = p;
	}

	@Override
	public void create() throws Exception {
		
		Logger.info(String.format("Initiating C3P0 Connection pool %s.%s", domainKey, id));
		
		ComboPooledDataSource ds = new ComboPooledDataSource();
		
		ds.setJdbcUrl(p.getJdbcUrl());
		ds.setUser(p.getUser());
		ds.setPassword(p.getPassword());
		ds.setDriverClass(p.getDriverClass());

		if(null != p.getAcquireIncrement()) ds.setAcquireIncrement(p.getAcquireIncrement());
		if(null != p.getAcquireRetryAttempts()) ds.setAcquireRetryAttempts(p.getAcquireRetryAttempts());
		if(null != p.getAcquireRetryDelay()) ds.setAcquireRetryDelay(p.getAcquireRetryDelay());
		if(null != p.getAutoCommitOnClose()) ds.setAutoCommitOnClose(p.getAutoCommitOnClose());
		if(null != p.getAutomaticTestTable()) ds.setAutomaticTestTable(p.getAutomaticTestTable());
		if(null != p.getBreakAfterAcquireFailure()) ds.setBreakAfterAcquireFailure(p.getBreakAfterAcquireFailure());
		if(null != p.getCheckoutTimeout()) ds.setCheckoutTimeout(p.getCheckoutTimeout());
		if(null != p.getDebugUnreturnedConnectionStackTraces()) ds.setDebugUnreturnedConnectionStackTraces(p.getDebugUnreturnedConnectionStackTraces());
		if(null != p.getForceIgnoreUnresolvedTransactions()) ds.setForceIgnoreUnresolvedTransactions(p.getForceIgnoreUnresolvedTransactions());
		if(null != p.getForceSynchronousCheckins()) ds.setForceSynchronousCheckins(p.getForceSynchronousCheckins());
		if(null != p.getForceUseNamedDriverClass()) ds.setForceUseNamedDriverClass(p.getForceUseNamedDriverClass());
		if(null != p.getIdentityToken()) ds.setIdentityToken(p.getIdentityToken());
		if(null != p.getIdleConnectionTestPeriod()) ds.setIdleConnectionTestPeriod(p.getIdleConnectionTestPeriod());
		if(null != p.getInitialPoolSize()) ds.setInitialPoolSize(p.getInitialPoolSize());
		if(null != p.getLoginTimeout()) ds.setLoginTimeout(p.getLoginTimeout());
		if(null != p.getMaxAdministrativeTaskTime()) ds.setMaxAdministrativeTaskTime(p.getMaxAdministrativeTaskTime());
		if(null != p.getMaxConnectionAge()) ds.setMaxConnectionAge(p.getMaxConnectionAge());
		if(null != p.getMaxIdleTime()) ds.setMaxIdleTime(p.getMaxIdleTime());
		if(null != p.getMaxIdleTimeExcessConnections()) ds.setMaxIdleTimeExcessConnections(p.getMaxIdleTimeExcessConnections());
		if(null != p.getMaxPoolSize()) ds.setMaxPoolSize(p.getMaxPoolSize());
		if(null != p.getMaxStatements()) ds.setMaxStatements(p.getMaxStatements());
		if(null != p.getMaxStatementsPerConnection()) ds.setMaxStatementsPerConnection(p.getMaxStatementsPerConnection());
		if(null != p.getMinPoolSize()) ds.setMinPoolSize(p.getMinPoolSize());
		if(null != p.getNumHelperThreads()) ds.setNumHelperThreads(p.getNumHelperThreads());
		if(null != p.getOverrideDefaultPassword()) ds.setOverrideDefaultPassword(p.getOverrideDefaultPassword());
		if(null != p.getOverrideDefaultUser()) ds.setOverrideDefaultUser(p.getOverrideDefaultUser());
		if(null != p.getPreferredTestQuery()) ds.setPreferredTestQuery(p.getPreferredTestQuery());
		if(null != p.getPrivilegeSpawnedThreads()) ds.setPrivilegeSpawnedThreads(p.getPrivilegeSpawnedThreads());
		if(null != p.getPropertyCycle()) ds.setPropertyCycle(p.getPropertyCycle());
		if(null != p.getStatementCacheNumDeferredCloseThreads()) ds.setStatementCacheNumDeferredCloseThreads(p.getStatementCacheNumDeferredCloseThreads());
		if(null != p.getTestConnectionOnCheckin()) ds.setTestConnectionOnCheckin(p.getTestConnectionOnCheckin());
		if(null != p.getTestConnectionOnCheckout())ds.setTestConnectionOnCheckout(p.getTestConnectionOnCheckout());
		if(null != p.getUnreturnedConnectionTimeout()) ds.setUnreturnedConnectionTimeout(p.getUnreturnedConnectionTimeout());
		if(null != p.getUserOverridesAsString()) ds.setUserOverridesAsString(p.getUserOverridesAsString());
		if(null != p.getUsesTraditionalReflectiveProxies()) ds.setUsesTraditionalReflectiveProxies(p.getUsesTraditionalReflectiveProxies());
				
		Connection conn = ds.getConnection();
		conn.close();
		this.ds = ds;
	}

	@Override
	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}

	@Override
	public void close() {
		if(null != ds) {
			Logger.warn(String.format("Closing C3P0 Connection pool %s.%s", domainKey, id));
			try {ds.close(); ds = null;}catch(Exception ex) {Logger.warn(ex);}
		}
	}

	@Override
	public String getDomainKey() {
		return domainKey;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public ObjectMapper mapper() {
		return mapper;
	}
	
}
