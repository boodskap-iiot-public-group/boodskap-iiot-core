package io.boodskap.iot.db.pool;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp2.BasicDataSource;
import org.tinylog.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import io.boodskap.iot.db.ConnectionPool;
import io.boodskap.iot.model.IDBCPArgs;
import io.boodskap.iot.sql.DBCPResultSetSerializer;

public class DBCPPool extends ConnectionPool {
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	static {
		SimpleModule module = new SimpleModule();
		module.addSerializer(new DBCPResultSetSerializer());
		mapper.registerModule(module);
	}
	
	private final String domainKey;
	private final String id;
	private final IDBCPArgs p;
	private BasicDataSource ds;

	public DBCPPool(final String domainKey, final String id, final IDBCPArgs p) {
		this.domainKey = domainKey;
		this.id = id;
		this.p = p;
	}
	
	@Override
	public void create() throws SQLException {
		
		Logger.info(String.format("Initiating DBCP Connection pool %s.%s", domainKey, id));
		
		BasicDataSource ds = new BasicDataSource();
		
		ds.setUrl(p.getUrl());
		ds.setUsername(p.getUsername());
		ds.setPassword(p.getPassword());
		ds.setDriverClassName(p.getDriverClassName());

		if(null != p.getAbandonedUsageTracking()) ds.setAbandonedUsageTracking(p.getAbandonedUsageTracking());
		if(null != p.getAccessToUnderlyingConnectionAllowed()) ds.setAccessToUnderlyingConnectionAllowed(p.getAccessToUnderlyingConnectionAllowed());
		if(null != p.getCacheState()) ds.setCacheState(p.getCacheState());
		if(null != p.getDefaultAutoCommit()) ds.setDefaultAutoCommit(p.getDefaultAutoCommit());
		if(null != p.getDefaultCatalog()) ds.setDefaultCatalog(p.getDefaultCatalog());
		if(null != p.getDefaultQueryTimeout()) ds.setDefaultQueryTimeout(p.getDefaultQueryTimeout());
		if(null != p.getDefaultReadOnly()) ds.setDefaultReadOnly(p.getDefaultReadOnly());
		if(null != p.getDefaultSchema()) ds.setDefaultSchema(p.getDefaultSchema());
		if(null != p.getDefaultTransactionIsolation()) ds.setDefaultTransactionIsolation(p.getDefaultTransactionIsolation());
		if(null != p.getEnableAutoCommitOnReturn()) ds.setEnableAutoCommitOnReturn(p.getEnableAutoCommitOnReturn());
		if(null != p.getFastFailValidation()) ds.setFastFailValidation(p.getFastFailValidation());
		if(null != p.getInitialSize()) ds.setInitialSize(p.getInitialSize());
		if(null != p.getLifo()) ds.setLifo(p.getLifo());
		if(null != p.getLogAbandoned()) ds.setLogAbandoned(p.getLogAbandoned());
		if(null != p.getLogExpiredConnections()) ds.setLogExpiredConnections(p.getLogExpiredConnections());
		if(null != p.getLoginTimeout()) ds.setLoginTimeout(p.getLoginTimeout());
		if(null != p.getMaxConnLifetimeMillis()) ds.setMaxConnLifetimeMillis(p.getMaxConnLifetimeMillis());
		if(null != p.getMaxIdle()) ds.setMaxIdle(p.getMaxIdle());
		if(null != p.getMaxOpenPreparedStatements()) ds.setMaxOpenPreparedStatements(p.getMaxOpenPreparedStatements());
		if(null != p.getMaxTotal()) ds.setMaxTotal(p.getMaxTotal());
		if(null != p.getMaxWaitMillis()) ds.setMaxWaitMillis(p.getMaxWaitMillis());
		if(null != p.getMinEvictableIdleTimeMillis()) ds.setMinEvictableIdleTimeMillis(p.getMinEvictableIdleTimeMillis());
		if(null != p.getMinIdle()) ds.setMinIdle(p.getMinIdle());
		if(null != p.getNumTestsPerEvictionRun()) ds.setNumTestsPerEvictionRun(p.getNumTestsPerEvictionRun());
		if(null != p.getPoolPreparedStatements()) ds.setPoolPreparedStatements(p.getPoolPreparedStatements());
		if(null != p.getRemoveAbandonedOnBorrow()) ds.setRemoveAbandonedOnBorrow(p.getRemoveAbandonedOnBorrow());
		if(null != p.getRemoveAbandonedOnMaintenance()) ds.setRemoveAbandonedOnMaintenance(p.getRemoveAbandonedOnMaintenance());
		if(null != p.getRollbackOnReturn()) ds.setRollbackOnReturn(p.getRollbackOnReturn());
		if(null != p.getSoftMinEvictableIdleTimeMillis()) ds.setSoftMinEvictableIdleTimeMillis(p.getSoftMinEvictableIdleTimeMillis());
		if(null != p.getTestOnBorrow()) ds.setTestOnBorrow(p.getTestOnBorrow());
		if(null != p.getTestOnCreate()) ds.setTestOnCreate(p.getTestOnCreate());
		if(null != p.getTestOnReturn()) ds.setTestOnReturn(p.getTestOnReturn());
		if(null != p.getTestWhileIdle()) ds.setTestWhileIdle(p.getTestWhileIdle());
		if(null != p.getTimeBetweenEvictionRunsMillis()) ds.setTimeBetweenEvictionRunsMillis(p.getTimeBetweenEvictionRunsMillis());
		if(null != p.getValidationQuery()) ds.setValidationQuery(p.getValidationQuery());
		if(null != p.getValidationQueryTimeout()) ds.setValidationQueryTimeout(p.getValidationQueryTimeout());
		
		Connection conn = ds.getConnection();
		conn.close();
		this.ds = ds;
	}

	@Override
	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}

	@Override
	public void close() {
		if(null != ds) {
			Logger.warn(String.format("Closing DBCP Connection pool %s.%s", domainKey, id));
			try {ds.close(); ds=null;}catch(Exception ex) {Logger.warn(ex);}
		}
	}

	@Override
	public String getDomainKey() {
		return domainKey;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public ObjectMapper mapper() {
		return mapper;
	}
	
}
