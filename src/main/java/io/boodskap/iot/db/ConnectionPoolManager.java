package io.boodskap.iot.db;

import java.util.HashMap;
import java.util.Map;

import org.tinylog.Logger;

import io.boodskap.iot.db.pool.C3P0Pool;
import io.boodskap.iot.db.pool.DBCPPool;
import io.boodskap.iot.db.pool.HikariPool;
import io.boodskap.iot.db.pool.TomcatPool;
import io.boodskap.iot.model.IConnectionPool;

public class ConnectionPoolManager {
	
	private static final Map<String, Map<String, ConnectionPool>> POOLS = new HashMap<>();

	private ConnectionPoolManager() {
	}
	
	public static final ConnectionPool get(String domainKey, String name) throws Exception {
		
		Map<String, ConnectionPool> pools = POOLS.get(domainKey);
		if(null == pools) {
			pools = new HashMap<>();
			Logger.info(String.format("Initializing pool for domain: %s", domainKey));
			POOLS.put(domainKey, pools);
		}
		
		ConnectionPool pool = pools.get(name);
		
		if(null == pool) {
			
			IConnectionPool cpool = IConnectionPool.get(domainKey, name);
			if(null != cpool) {
				
				switch(cpool.getType()) {
				case C3P0:
					pool = new C3P0Pool(domainKey, name, cpool.getC3p0Args());
					break;
				case DBCP:
					pool = new DBCPPool(domainKey, name, cpool.getDbcpArgs());
					break;
				case HIKARI:
					pool = new HikariPool(domainKey, name, cpool.getHikariArgs());
					break;
				case TOMCAT:
					pool = new TomcatPool(domainKey, name, cpool.getTomcatArgs());
					break;
				}
				
				Logger.info(String.format("Creating %s for domain:%s id:%s", pool.getClass().getSimpleName(), domainKey, name));
				
				boolean created = false;
				
				try {
					pool.create();
					Logger.info(String.format("Created %s for domain:%s id:%s", pool.getClass().getSimpleName(), domainKey, name));					
					created = true;
				}finally {
					if(created) {
						pools.put(name, pool);
					}else {
						pool.close();
					}
				}
			}

		}

		return pool;
	}
	
	public static final void close(String domainKey, String name) {
		
		Map<String, ConnectionPool> pools = POOLS.get(domainKey);
		if(null != pools) {
			ConnectionPool pool = pools.get(name);
			if(null != pool) {
				try {
					pool.close();
				} catch (Exception e) {
					Logger.warn(e, "Error closing pool {}.{}", domainKey, name);
				}
				pools.remove(name);
			}
		}
	}
	
	public static final void close() {
		
		POOLS.forEach( (k,v) -> {
			v.values().forEach(p -> {
				Logger.info(String.format("Closing %s for domain:%s id:%s", p.getClass().getSimpleName(), p.getDomainKey(), p.getId()));
				try {
					p.close();
				} catch (Exception e) {
					Logger.warn(e, "Error closing pool {}.{}", p.getDomainKey(), p.getId());
				}
			});
		});
		
		POOLS.clear();
	}
}
