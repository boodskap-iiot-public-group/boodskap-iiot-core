package io.boodskap.iot.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class BasicConnectionPool implements AutoCloseable {
	
	private final Map<String, PreparedStatement> prepared = new HashMap<>();
	
	public abstract Connection getConnection() throws Exception;
	
	public abstract void create() throws Exception;
	
	public abstract ObjectMapper mapper();

	public void evict(Connection c) throws Exception {
		throw new UnsupportedOperationException("Eviction not implemented for this DataSource");
	}
	
	public PreparedStatement prepared(String id) {
		return prepared.get(id);
	}
	
	public PreparedStatement remove(String id) {
		return prepared.remove(id);
	}
	
	public PreparedStatement prepare(String id, String sql) throws Exception {
		
		PreparedStatement pstmt = null;
		Connection c = getConnection();
		
		try {
			pstmt = c.prepareStatement(sql);
			prepared.put(id, pstmt);
		}finally {
			c.close();
		}
		
		return pstmt;
	}
	
}
