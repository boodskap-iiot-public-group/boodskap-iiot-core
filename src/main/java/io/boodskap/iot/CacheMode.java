package io.boodskap.iot;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Enumeration of all supported caching modes. Cache mode is specified in {@link org.apache.ignite.configuration.CacheConfiguration}
 * and cannot be changed after cache has started.
 */
@JsonSerialize(as=CacheMode.class)
public enum CacheMode {
    /**
     * Specifies local-only cache behaviour. In this mode caches residing on
     * different grid nodes will not know about each other.
     * <p>
     * Other than distribution, {@code local} caches still have all
     * the caching features, such as eviction, expiration, swapping,
     * querying, etc... This mode is very useful when caching read-only data
     * or data that automatically expires at a certain interval and
     * then automatically reloaded from persistence store.
     */
    LOCAL,

    /**
     * Specifies fully replicated cache behavior. In this mode all the keys are distributed
     * to all participating nodes. User still has affinity control
     * over subset of nodes for any given key via {@link AffinityFunction}
     * configuration.
     */
    REPLICATED,

    /**
     * Specifies partitioned cache behaviour. In this mode the overall
     * key set will be divided into partitions and all partitions will be split
     * equally between participating nodes. User has affinity
     * control over key assignment via {@link AffinityFunction}
     * configuration.
     * <p>
     * Note that partitioned cache is always fronted by local
     * {@code 'near'} cache which stores most recent data. You
     * can configure the size of near cache via {@link NearCacheConfiguration#getNearEvictionPolicyFactory()}
     * configuration property.
     */
    PARTITIONED;

}