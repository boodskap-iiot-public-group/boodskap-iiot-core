/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.dao;

import org.json.JSONObject;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;

public interface IDAO<T> {
					
	public void createOrUpdate(T e) throws StorageException;

	public Class<? extends T> clazz();
	
	public EntityIterator<T> load() throws StorageException;
	
	public long count() throws StorageException;
	
	public void delete() throws StorageException;
	
	public SearchResult<T> searchByQuery(String query, int page, int pageSize) throws StorageException;

	public SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException;

	public JSONObject deleteByQuery(String query) throws StorageException;

	public JSONObject updateByQuery(String what, String how) throws StorageException;
	
}
