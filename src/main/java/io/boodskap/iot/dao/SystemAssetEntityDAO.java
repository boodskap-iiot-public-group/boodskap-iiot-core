/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.dao;

import java.util.Collection;

import io.boodskap.iot.BoodskapSystem;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.model.ISystemAssetEntity;

public interface SystemAssetEntityDAO<T extends ISystemAssetEntity> extends IDAO<T> {
	
	public static <T extends ISystemAssetEntity> SystemAssetEntityDAO<T> get() {
		return BoodskapSystem.storage().getSystemAssetEntityDAO();
	}
	
	public EntityIterator<T> load(String assetId) throws StorageException;
	
	public T create(String assetId, String entityType, String entityId) throws StorageException;
	
	public T get(String assetId, String entityType, String entityId) throws StorageException;
	
	public long count(String assetId) throws StorageException;
	
	public void delete(String assetId) throws StorageException;
	
	public void delete(String assetId, String enityType) throws StorageException;
	
	public void delete(String assetId, String enityType, String entityId) throws StorageException;
	
	public Collection<T> list(String assetId, String entityType, int page, int pageSize) throws StorageException;
	
	public Collection<T> listNext(String assetId, String entityType, String entityId, int page, int pageSize) throws StorageException;
	
}
