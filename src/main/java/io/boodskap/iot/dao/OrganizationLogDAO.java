/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.dao;

import java.util.Collection;
import java.util.Date;

import io.boodskap.iot.BoodskapSystem;
import io.boodskap.iot.StorageException;
import io.boodskap.iot.model.IOrganizationLog;

public interface OrganizationLogDAO<T extends IOrganizationLog> extends IOrganizationDAO<T> {
	
	public static OrganizationLogDAO<IOrganizationLog> get() {
		return BoodskapSystem.storage().getOrganizationLogDAO();
	}

	public T create(String domainKey, String orgId, String id) throws StorageException;
	
	public T get(String domainKey, String orgId, String id) throws StorageException;
	
	public Collection<T> list(String domainKey, String orgId, int page, int pageSize) throws StorageException;
	
	public Collection<T> listNext(String domainKey, String orgId, String id, int page, int pageSize) throws StorageException;
	
	public long count(String domainKey, String orgId) throws StorageException;

	public void delete(String domainKey, String orgId) throws StorageException;

	public void delete(String domainKey, String orgId, String id) throws StorageException;

	public void deleteBefore(String domainKey, String orgId, Date date) throws StorageException;

	public void deleteAfter(String domainKey, String orgId, Date date) throws StorageException;

	public void deleteBetween(String domainKey, String orgId, Date from, Date to) throws StorageException;

	public void deleteBefore(String domainKey, Date date) throws StorageException;

	public void deleteAfter(String domainKey, Date date) throws StorageException;

	public void deleteBetween(String domainKey, Date from, Date to) throws StorageException;

	public void deleteBefore(Date date) throws StorageException;

	public void deleteAfter(Date date) throws StorageException;

	public void deleteBetween(Date from, Date to) throws StorageException;
}
