/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.dao;

import org.json.JSONObject;

import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;

public interface IOrganizationDAO<T> extends IDomainDAO<T> {
	
	public EntityIterator<T> load(String domainKey, String orgId) throws StorageException;
		
	public SearchResult<T> searchByQuery(String domainKey, String orgId, String query, int page, int pageSize) throws StorageException;

	public SearchResult<JSONObject> selectByQuery(String domainKey, String orgId, String what, String how) throws StorageException;
	
	public JSONObject deleteByQuery(String domainKey, String orgId, String query) throws StorageException;

	public JSONObject updateByQuery(String domainKey, String orgId, String what, String how) throws StorageException;
	
	public long count(String domainKey, String orgId) throws StorageException;
	
	public void delete(String domainKey, String orgId) throws StorageException;
	
	public default EntityIterator<T> load(String domainKey) throws StorageException{
		throw new StorageException("Not yet implemented");		
	}
	
	public default long count(String domainKey) throws StorageException{
		throw new StorageException("Not yet implemented");		
	}
	
	public default void delete(String domainKey) throws StorageException{
		throw new StorageException("Not yet implemented");		
	}
	
	public default SearchResult<T> searchByQuery(String domainKey, String query, int page, int pageSize) throws StorageException{
		throw new StorageException("Not yet implemented");		
	}

	public default SearchResult<JSONObject> selectByQuery(String domainKey, String what, String how) throws StorageException{
		throw new StorageException("Not yet implemented");		
	}
	
	public default JSONObject deleteByQuery(String domainKey, String query) throws StorageException{
		throw new StorageException("Not yet implemented");		
	}

	public default JSONObject updateByQuery(String domainKey, String what, String how) throws StorageException{
		throw new StorageException("Not yet implemented");		
	}

	public default SearchResult<T> searchByQuery(String query, int page, int pageSize) throws StorageException{
		throw new StorageException("Not yet implemented");		
	}

	public default SearchResult<JSONObject> selectByQuery(String what, String how) throws StorageException{
		throw new StorageException("Not yet implemented");		
	}

	public default JSONObject deleteByQuery(String query) throws StorageException{
		throw new StorageException("Not yet implemented");		
	}

	public default JSONObject updateByQuery(String what, String how) throws StorageException{
		throw new StorageException("Not yet implemented");		
	}
	
}
