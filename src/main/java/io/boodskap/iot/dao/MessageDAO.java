/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot.dao;

import java.util.Collection;

import org.json.JSONObject;

import io.boodskap.iot.BoodskapSystem;
import io.boodskap.iot.SearchResult;
import io.boodskap.iot.StorageException;

public interface MessageDAO{
	
	public static MessageDAO get() {
		return BoodskapSystem.dynamicStorage().getMessageDAO();
	}
	
	public EntityIterator<JSONObject> load(String domainKey, String specId) throws StorageException;
	
	public long count(String domainKey, String specId) throws StorageException;
	
	public void createOrUpdate(String domainKey, String specId, String messageId, JSONObject e) throws StorageException;
	
	public JSONObject get(String domainKey, String specId, String messageId) throws StorageException;
	
	public void delete(String domainKey, String specId, String messageId) throws StorageException;
	
	public void delete(String domainKey, String specId) throws StorageException;
	
	public Collection<JSONObject> list(String domainKey, String specId, int page, int pageSize) throws StorageException;

	public Collection<JSONObject> listNext(String domainKey, String specId, String messageId, int page, int pageSize) throws StorageException;

	public SearchResult<JSONObject> searchByQuery(String domainKey, String specId, String sql, int page, int pageSize) throws StorageException;

	public JSONObject selectByQuery(String domainKey, String specId, String what, String how) throws StorageException;

	public JSONObject updateByQuery(String domainKey, String specId, String what, String how) throws StorageException;

	public JSONObject deleteByQuery(String domainKey, String specId, String sql) throws StorageException;
}
