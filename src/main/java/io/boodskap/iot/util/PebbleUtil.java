package io.boodskap.iot.util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.loader.ClasspathLoader;
import com.mitchellbosecke.pebble.loader.Loader;
import com.mitchellbosecke.pebble.template.PebbleTemplate;

import io.boodskap.iot.ThreadContext;

public class PebbleUtil {
	
	private static final List<Loader<?>> LOADERS = new ArrayList<>();
	
	static {
		LOADERS.add(new ClasspathLoader());
	}
	
	public static String merge(String template, String jsonContent) throws JsonParseException, JsonMappingException, IOException {
		
		Map<String, Object> props = ThreadContext.jsonToMap(jsonContent);
		PebbleEngine engine = new PebbleEngine.Builder().loader(new ClasspathLoader()).build();
		PebbleTemplate compiledTemplate = engine.getTemplate(template);
		
		Writer writer = new StringWriter();
		compiledTemplate.evaluate(writer, props);

		String output = writer.toString();
		
		return output;
	}
	
	public static String merge(PebbleTemplate t, Map<String, Object> props) throws JsonParseException, JsonMappingException, IOException {

		Writer writer = new StringWriter();
		
		t.evaluate(writer, props);

		String output = writer.toString();
		
		return output;
	}
	
	public static String mergeClasspathTemplate(String template, String jsonContent) throws JsonParseException, JsonMappingException, IOException {
		return merge(template, jsonContent);
	}
	
	public static String mergeClasspathTemplate(String template, Map<String, Object> props) throws JsonParseException, JsonMappingException, IOException {

		PebbleEngine engine = new PebbleEngine.Builder().loader(new ClasspathLoader()).build();
		PebbleTemplate compiledTemplate = engine.getTemplate(template);
		
		Writer writer = new StringWriter();
		compiledTemplate.evaluate(writer, props);

		String output = writer.toString();
		
		return output;
	}
	
	public static String mergeNamedTemplate(String name, Map<String, Object> props) throws JsonParseException, JsonMappingException, IOException {
		return mergeClasspathTemplate(String.format("templates/%s.twig", name), props);
	}
	
	public static void main(String[] args) throws Exception {
		
		Map<String, Object> props = new HashMap<>();
		props.put("fname", "Jegan");
		props.put("lname", "Vincent");
		
		String merged = PebbleUtil.mergeNamedTemplate("test", props);
		
		System.out.println(merged);
	}
	
}
