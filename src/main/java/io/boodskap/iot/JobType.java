package io.boodskap.iot;

public enum JobType {

	/*
	 * DISTRIBUTED jobs are started and stopped on demand, new jobs are distributed evenly on running nodes 
	 */
	DISTRIBUTED,
	/**
	 *  NODE_SINGLETON jobs run only once instance on each node of the cluster
	 */
	NODE_SINGLETON,
	/**
	 * NODE_SINGLETON jobs run only once instance in the entire cluster
	 */
	CLUSTER_SINGLETON,
	/**
	 * ATOMIC jobs are very similar to DISTRIBUTED jobs, but they won't get started automatically and also, they gets started on the same node where the caller resides
	 */
	ATOMIC,
	/**
	 * SCALABLE jobs are the same and they run one instance per each node in the cluster
	 */
	SCALABLE,
}
