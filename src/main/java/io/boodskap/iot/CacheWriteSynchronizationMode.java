package io.boodskap.iot;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Mode indicating how Ignite should wait for write replies from other nodes. Default
 * value is {@link #PRIMARY_SYNC}}, which means that Ignite will wait for write or commit to complete on
 * {@code primary} node, but will not wait for backups to be updated.
 * <p>
 * Note that regardless of write synchronization mode, cache data will always remain fully
 * consistent across all participating nodes.
 * <p>
 * Write synchronization mode may be configured via {@link org.apache.ignite.configuration.CacheConfiguration#getWriteSynchronizationMode()}
 * configuration property.
 */
@JsonSerialize(as=CacheWriteSynchronizationMode.class)
public enum CacheWriteSynchronizationMode {
    /**
     * Flag indicating that Ignite should wait for write or commit replies from all nodes.
     * This behavior guarantees that whenever any of the atomic or transactional writes
     * complete, all other participating nodes which cache the written data have been updated.
     */
    FULL_SYNC,

    /**
     * Flag indicating that Ignite will not wait for write or commit responses from participating nodes,
     * which means that remote nodes may get their state updated a bit after any of the cache write methods
     * complete, or after {@link Transaction#commit()} method completes.
     */
    FULL_ASYNC,

    /**
     * This flag only makes sense for {@link CacheMode#PARTITIONED} and {@link CacheMode#REPLICATED} modes.
     * When enabled, Ignite will wait for write or commit to complete on {@code primary} node, but will not wait for
     * backups to be updated.
     */
    PRIMARY_SYNC;

 }