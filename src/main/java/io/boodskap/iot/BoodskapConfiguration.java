/*******************************************************************************
 * Copyright (C) 2019 Boodskap Inc
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package io.boodskap.iot;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BoodskapConfiguration implements IConfig{
	
	private static final long serialVersionUID = 7078124126063442833L;
	
	public static final String HTTP_X_API = "X-BSKP-API";
	public static final String HTTP_X_TOKEN = "X-BSKP-TOKEN";
	public static final String HTTP_X_DOMAIN_KEY = "X-BSKP-DKEY";
	public static final String HTTP_X_ORG_ID = "X-BSKP-ORGID";

	private String clusterId;
	private String nodeId;
	private String cacheFactory;
	private String gridFactory;
	private String storageFactory;
	private String rawStorageFactory;
	private String dynamicStorageFactory;
	private int logOfferTimeout;
	private boolean workerMode;
	private int defaultSearchPageSize;
	private boolean autoCreateDevices;
	private boolean disableLogin;
	private ThreadPoolStrategy eventThreadPoolStrategy = ThreadPoolStrategy.FIXED;
	private int eventThreadPool = 10;
	private boolean disableApiTrace;
	private String dataDir;
	private String sharedDir;
	private boolean isNetworkSahredDrive = true;
	private boolean persistRawData;
	private boolean persistEntityRawData;
	private int messageProcessors = Runtime.getRuntime().availableProcessors();
	private int messageOfferTimeout = 1;
	
	private SQLConfiguration igniteSql;
	
	private List<String> serviceFactories = new ArrayList<>();

	public BoodskapConfiguration() {
	}
	
	public static BoodskapConfiguration parse(InputStream in) throws IOException {
		return parse(IOUtils.toString(in, "UTF-8"));
	}
	
	public static BoodskapConfiguration parse(String cfg) {
		return ThreadContext.jsonToObject(cfg, BoodskapConfiguration.class);
	}
	
	@Override
	@JsonIgnore
	public int getStartPriority() {
		return 0;
	}

	@Override
	public void setDefaults() {
		
		setClusterId("boodskap-cluster");
		setLogOfferTimeout(2);
		setWorkerMode(false);
		setDefaultSearchPageSize(25);
		setAutoCreateDevices(true);
		setDisableLogin(false);
		
		setGridFactory("io.boodskap.iot.spi.ignite.grid.IgniteGridFactory");
		setCacheFactory("io.boodskap.iot.spi.ignite.cache.IgniteCacheFactory");
		setRawStorageFactory("io.boodskap.iot.spi.storage.ignite.IgniteRawStorageFactory");
		setStorageFactory("io.boodskap.iot.spi.storage.ignite.IgniteStorageFactory");
		setDynamicStorageFactory("io.boodskap.iot.spi.storage.ignite.IgniteDynamicStorageFactory");
		
		/**
		 * Add all the default services
		 */
		getServiceFactories().add("io.boodskap.iot.api.service.BoodskapApiServiceFactory");
	}

	public String getGridFactory() {
		return gridFactory;
	}

	public String getRawStorageFactory() {
		return rawStorageFactory;
	}

	public String getDynamicStorageFactory() {
		return dynamicStorageFactory;
	}

	public void setDynamicStorageFactory(String dynamicStorageFactory) {
		this.dynamicStorageFactory = dynamicStorageFactory;
	}

	public String getStorageFactory() {
		return storageFactory;
	}

	public String getCacheFactory() {
		return cacheFactory;
	}

	public int getLogOfferTimeout() {
		return logOfferTimeout;
	}

	public void setGridFactory(String gridFactory) {
		this.gridFactory = gridFactory;
	}

	public void setRawStorageFactory(String rawStorageFactory) {
		this.rawStorageFactory = rawStorageFactory;
	}

	public void setStorageFactory(String storageFactory) {
		this.storageFactory = storageFactory;
	}

	public void setCacheFactory(String cacheFactory) {
		this.cacheFactory = cacheFactory;
	}

	public void setLogOfferTimeout(int logOfferTimeout) {
		this.logOfferTimeout = logOfferTimeout;
	}

	public List<String> getServiceFactories() {
		return serviceFactories;
	}

	public void setServiceFactories(List<String> serviceFactories) {
		this.serviceFactories = serviceFactories;
	}

	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public boolean isWorkerMode() {
		return workerMode;
	}

	public void setWorkerMode(boolean workerMode) {
		this.workerMode = workerMode;
	}

	public int getDefaultSearchPageSize() {
		return defaultSearchPageSize;
	}

	public void setDefaultSearchPageSize(int defaultSearchPageSize) {
		this.defaultSearchPageSize = defaultSearchPageSize;
	}

	public boolean isAutoCreateDevices() {
		return autoCreateDevices;
	}

	public void setAutoCreateDevices(boolean autoCreateDevices) {
		this.autoCreateDevices = autoCreateDevices;
	}

	public boolean isDisableLogin() {
		return disableLogin;
	}

	public void setDisableLogin(boolean disableLogin) {
		this.disableLogin = disableLogin;
	}

	public SQLConfiguration getIgniteSql() {
		return igniteSql;
	}

	public void setIgniteSql(SQLConfiguration igniteSql) {
		this.igniteSql = igniteSql;
	}

	public String getDataDir() {
		return dataDir;
	}

	public void setDataDir(String dataDir) {
		this.dataDir = dataDir;
	}

	public ThreadPoolStrategy getEventThreadPoolStrategy() {
		return eventThreadPoolStrategy;
	}

	public void setEventThreadPoolStrategy(ThreadPoolStrategy eventThreadPoolStrategy) {
		this.eventThreadPoolStrategy = eventThreadPoolStrategy;
	}

	public int getEventThreadPool() {
		return eventThreadPool;
	}

	public void setEventThreadPool(int eventThreadPool) {
		this.eventThreadPool = eventThreadPool;
	}

	public boolean isDisableApiTrace() {
		return disableApiTrace;
	}

	public void setDisableApiTrace(boolean disableApiTrace) {
		this.disableApiTrace = disableApiTrace;
	}

	public String getSharedDir() {
		return sharedDir;
	}

	public void setSharedDir(String sharedDir) {
		this.sharedDir = sharedDir;
	}

	public boolean isNetworkSahredDrive() {
		return isNetworkSahredDrive;
	}

	public void setNetworkSahredDrive(boolean isNetworkSahredDrive) {
		this.isNetworkSahredDrive = isNetworkSahredDrive;
	}

	public boolean isPersistRawData() {
		return persistRawData;
	}

	public void setPersistRawData(boolean persistRawData) {
		this.persistRawData = persistRawData;
	}

	public boolean isPersistEntityRawData() {
		return persistEntityRawData;
	}

	public void setPersistEntityRawData(boolean persistEntityRawData) {
		this.persistEntityRawData = persistEntityRawData;
	}

	public int getMessageProcessors() {
		return messageProcessors;
	}

	public void setMessageProcessors(int messageProcessors) {
		this.messageProcessors = messageProcessors;
	}

	public int getMessageOfferTimeout() {
		return messageOfferTimeout;
	}

	public void setMessageOfferTimeout(int messageOfferTimeout) {
		this.messageOfferTimeout = messageOfferTimeout;
	}

}
