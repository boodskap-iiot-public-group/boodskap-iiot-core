package io.boodskap.iot;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=LogSeverity.class)
public enum LogSeverity {

	/**
	 * Trace log entries contain very fine-grained debug information, typically the flow through.
	 */
	TRACE,

	/**
	 * Debug log entries contain common debug information.
	 */
	DEBUG,

	/**
	 * Info log entries contain important relevant information.
	 */
	INFO,

	/**
	 * Warn log entries contain technical warnings. Typically warnings do not prevent the application from continuing.
	 */
	WARN,

	/**
	 * Error log entries contain severe technical errors. Typically errors prevent a function from continuing normally.
	 */
	ERROR,

	/**
	 * Off severity level disables any logging.
	 */
	OFF;

}
