#!/bin/bash
PLATFORM=cassandra
docker build -f Dockerfile --build-arg BSKP_PLATFORM=${PLATFORM} -t boodskapiotplatform/platform-${PLATFORM}:latest ./target/${PLATFORM}
